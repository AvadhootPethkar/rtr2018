// Headers
#include<Windows.h>
#include<stdio.h> // For file handeling
#include<gl/GL.h>
#include<gl/GLU.h>
#pragma comment(lib, "opengl32.lib") 
#pragma comment(lib, "glu32.lib") 
#define WIN_WIDTH 800 
#define WIN_HEIGHT 600 

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Variable Declarations
HWND gHwnd = NULL;
bool bFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC gHdc = NULL; // Change 5
HGLRC ghrc = NULL; // Change 6
bool gbActiveWindow = false; // Change 7
FILE *gpFile = NULL;
GLfloat gfTriangleX = 1.0f;
GLfloat gfTriangleY = -1.0f;
GLfloat gfCircleX = -1.0f;
GLfloat gfCircleY = -1.0f;
GLfloat gfAngle = 0.0f;
GLfloat gfLineY = 1.0f;
// Variables for Lights:
bool bLights = false;
GLfloat lightAmbiant[] = { 0.0f,0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
//GLfloat lightSpecular[] = { 1.0f,1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat light_model_ambiant[] = { 0.2f,0.2f, 0.2f, 1.0f };
GLfloat light_model_local_viewer[] = { 0.0f };
GLUquadric *quadric[24];
GLfloat angleOfXRotation = 0.0f;
GLfloat angleOfYRotation = 0.0f;
GLfloat angleOfZRotation = 0.0f;
GLint keyPressed = 0;

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function declaration
	int initialize(void);
	void display(void); // Change 1
	void update(void);

	// Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false; // For game loop
	int iRet = 0;

	// Code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log flile cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created \n");
	}
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Change 11
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, // Extended style
		szAppName,
		TEXT("FFPOGL Native Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, // Change 14
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetFixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "Window Destroyed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Succeeded \n");
	}
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Play game here....
			if (gbActiveWindow == true)
			{
				// Call 'update' here
				update();
			}
			display(); // Change 2
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	// Function declaration
	void ToggelFullScreen(void);
	void resize(int, int);
	void uninitialize(void);

	// code
	switch (iMsg) {
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND: // Change 5
		return(0); // IMP

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'f':
		case 'F':
			ToggelFullScreen();
			break;
		case 'l':
		case 'L':
			if (bLights == false)
			{
				bLights = true;
				glEnable(GL_LIGHTING);
			}
			else
			{
				bLights = false;
				glDisable(GL_LIGHTING);
			}
			break;

		case 'X':
		case 'x':
			keyPressed = 1;
			angleOfXRotation = 0.0f;
			break;

		case 'Y':
		case 'y':
			keyPressed = 2;
			angleOfYRotation = 0.0f;
			break;

		case 'Z':
		case 'z':
			keyPressed = 3;
			angleOfZRotation = 0.0f;
			break;

		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		default:
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize(); // Change 27
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggelFullScreen(void) {
	MONITORINFO mi;

	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(
					MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}

int initialize(void) {
	// function declaration
	void resize(int, int);
	void drawCube();

	// Variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	// initialize pf structure
	// To make members of structure 0.
	memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // Change 6
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	gHdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(gHdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(gHdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}
	ghrc = wglCreateContext(gHdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return(-4);
	}
	glShadeModel(GL_SMOOTH);
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbiant);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambiant);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);
	glEnable(GL_LIGHT0);

	for (GLint i = 0; i < 24; i++)
	{
		quadric[i] = gluNewQuadric();
	}
	//glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);
	//glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	//glMaterialfv(GL_FRONT, GL_POSITION, materialPosition);
	//glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	//glMaterialfv(GL_FRONT, GL_SHININESS, materialShinyness);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void resize(int width, int height) {
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width <= height)
	{
		glOrtho(0.0f,
			15.5f,
			0.0f,
			(15.5f * (GLfloat)height / (GLfloat)width),
			-10.0f,
			10.0f);
	}
	else
	{
		glOrtho(0.0f,
			(15.5f * (GLfloat)width / (GLfloat)height),
			0.0f,
			15.5f,
			-10.0f,
			10.0f);
	}
}

void display(void) {
	// Function declaration:
	void draw24Spheres();

	// Code:
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	if (keyPressed == 1)
	{
		glRotatef(angleOfXRotation, 1.0f, 0.0f, 0.0f);
		lightPosition[1] = angleOfXRotation;
	}
	else if (keyPressed == 2)
	{
		glRotatef(angleOfYRotation, 0.0f, 1.0f, 0.0f);
		lightPosition[2] = angleOfYRotation;
	}
	else if (keyPressed == 3)
	{
		glRotatef(angleOfZRotation, 0.0f, 0.0f, 1.0f);
		lightPosition[0] = angleOfZRotation;

	}
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	draw24Spheres();
	SwapBuffers(gHdc);
}

void draw24Spheres(void) {
	GLfloat materialAmbiant[4];
	GLfloat materialDiffuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShininess[1];
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// emerald
	materialAmbiant[0] = 0.0215f;
	materialAmbiant[1] = 0.1745f;
	materialAmbiant[2] = 0.0215f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.07568f;
	materialDiffuse[1] = 0.61424f;
	materialDiffuse[2] = 0.07568f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.633f;
	materialSpecular[1] = 0.727811f;
	materialSpecular[2] = 0.633f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 14.0f, 0.0f);
	gluSphere(quadric[0], 1.0f, 30, 30);

	// jade
	materialAmbiant[0] = 0.135f;
	materialAmbiant[1] = 0.2225f;
	materialAmbiant[2] = 0.1575f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.54f;
	materialDiffuse[1] = 0.89f;
	materialDiffuse[2] = 0.63f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.316228f;
	materialSpecular[1] = 0.316228f;
	materialSpecular[2] = 0.316228f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.1 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 11.5f, 0.0f);
	gluSphere(quadric[1], 1.0f, 30, 30);

	// obsidian
	materialAmbiant[0] = 0.05375f;
	materialAmbiant[1] = 0.05f;
	materialAmbiant[2] = 0.06625f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.18275f;
	materialDiffuse[1] = 0.17f;
	materialDiffuse[2] = 0.22525f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.332741f;
	materialSpecular[1] = 0.328634f;
	materialSpecular[2] = 0.346435f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.3 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 9.0f, 0.0f);
	gluSphere(quadric[2], 1.0f, 30, 30);

	// pearl
	materialAmbiant[0] = 0.25f;
	materialAmbiant[1] = 0.20725f;
	materialAmbiant[2] = 0.20725f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 1.0f;
	materialDiffuse[1] = 0.829f;
	materialDiffuse[2] = 0.829f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.296648f;
	materialSpecular[1] = 0.296648f;
	materialSpecular[2] = 0.296648f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.088 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 6.5f, 0.0f);
	gluSphere(quadric[3], 1.0f, 30, 30);

	// ruby
	materialAmbiant[0] = 0.1745f;
	materialAmbiant[1] = 0.01175f;
	materialAmbiant[2] = 0.01175f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.61424f;
	materialDiffuse[1] = 0.04136f;
	materialDiffuse[2] = 0.04136f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.727811f;
	materialSpecular[1] = 0.626959f;
	materialSpecular[2] = 0.626959f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 4.0f, 0.0f);
	gluSphere(quadric[4], 1.0f, 30, 30);

	// turquoise
	materialAmbiant[0] = 0.1f;
	materialAmbiant[1] = 0.18725f;
	materialAmbiant[2] = 0.1745f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.396f;
	materialDiffuse[1] = 0.74151f;
	materialDiffuse[2] = 0.69102f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.297254f;
	materialSpecular[1] = 0.30829f;
	materialSpecular[2] = 0.306678f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.1 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 1.5f, 0.0f);
	gluSphere(quadric[5], 1.0f, 30, 30);

//---------------------------------------------------------------

// brass
	materialAmbiant[0] = 0.329412f;
	materialAmbiant[1] = 0.223529f;
	materialAmbiant[2] = 0.027451f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.780392f;
	materialDiffuse[1] = 0.568627f;
	materialDiffuse[2] = 0.113725f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.992157f;
	materialSpecular[1] = 0.941176f;
	materialSpecular[2] = 0.807843f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.21794872 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.0f, 14.0f, 0.0f);
	gluSphere(quadric[6], 1.0f, 30, 30);

	// bronze
	materialAmbiant[0] = 0.2125f;
	materialAmbiant[1] = 0.1275f;
	materialAmbiant[2] = 0.054f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.714f;
	materialDiffuse[1] = 0.4284f;
	materialDiffuse[2] = 0.18144f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.393548f;
	materialSpecular[1] = 0.271906f;
	materialSpecular[2] = 0.166721f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.2 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.0f, 11.5f, 0.0f);
	gluSphere(quadric[7], 1.0f, 30, 30);

	// chrome
	materialAmbiant[0] = 0.25f;
	materialAmbiant[1] = 0.25f;
	materialAmbiant[2] = 0.25f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.4f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.774597f;
	materialSpecular[1] = 0.774597f;
	materialSpecular[2] = 0.774597f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.0f, 9.0f, 0.0f);
	gluSphere(quadric[8], 1.0f, 30, 30);

	// copper
	materialAmbiant[0] = 0.19125f;
	materialAmbiant[1] = 0.0735f;
	materialAmbiant[2] = 0.0225f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.7038f;
	materialDiffuse[1] = 0.27048f;
	materialDiffuse[2] = 0.0828f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.256777f;
	materialSpecular[1] = 0.137622f;
	materialSpecular[2] = 0.086014f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.1 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.0f, 6.5f, 0.0f);
	gluSphere(quadric[9], 1.0f, 30, 30);

	// gold
	materialAmbiant[0] = 0.24725f;
	materialAmbiant[1] = 0.1995f;
	materialAmbiant[2] = 0.0745f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.75164f;
	materialDiffuse[1] = 0.60648f;
	materialDiffuse[2] = 0.22648f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.628281f;
	materialSpecular[1] = 0.555802f;
	materialSpecular[2] = 0.366065f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.4 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.0f, 4.0f, 0.0f);
	gluSphere(quadric[10], 1.0f, 30, 30);

	// silver
	materialAmbiant[0] = 0.19225f;
	materialAmbiant[1] = 0.19225f;
	materialAmbiant[2] = 0.19225f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.50754f;
	materialDiffuse[1] = 0.50754f;
	materialDiffuse[2] = 0.50754f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.508273f;
	materialSpecular[1] = 0.508273f;
	materialSpecular[2] = 0.508273f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.4 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.0f, 1.5f, 0.0f);
	gluSphere(quadric[11], 1.0f, 30, 30);

	//---------------------------------------------------------------

	// black
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.0f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.01f;
	materialDiffuse[1] = 0.01f;
	materialDiffuse[2] = 0.01f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.50f;
	materialSpecular[1] = 0.50f;
	materialSpecular[2] = 0.50f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 14.0f, 0.0f);
	gluSphere(quadric[12], 1.0f, 30, 30);

	// cyan
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.1f;
	materialAmbiant[2] = 0.06f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.0f;
	materialDiffuse[1] = 0.50980392f;
	materialDiffuse[2] = 0.50980392f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.50196078f;
	materialSpecular[1] = 0.50196078f;
	materialSpecular[2] = 0.50196078f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 11.5f, 0.0f);
	gluSphere(quadric[13], 1.0f, 30, 30);

	// green
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.0f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.1f;
	materialDiffuse[1] = 0.35f;
	materialDiffuse[2] = 0.1f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.45f;
	materialSpecular[1] = 0.55f;
	materialSpecular[2] = 0.45f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 9.0f, 0.0f);
	gluSphere(quadric[14], 1.0f, 30, 30);

	// red
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.0f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.0f;
	materialDiffuse[2] = 0.0f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.6f;
	materialSpecular[2] = 0.6f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 6.5f, 0.0f);
	gluSphere(quadric[15], 1.0f, 30, 30);

	// white
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.0f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.55f;
	materialDiffuse[1] = 0.55f;
	materialDiffuse[2] = 0.55f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.70f;
	materialSpecular[1] = 0.70f;
	materialSpecular[2] = 0.70f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 4.0f, 0.0f);
	gluSphere(quadric[16], 1.0f, 30, 30);

	// yellow
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.0f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.0f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.60f;
	materialSpecular[1] = 0.60f;
	materialSpecular[2] = 0.50f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 1.5f, 0.0f);
	gluSphere(quadric[17], 1.0f, 30, 30);

	//---------------------------------------------------------------

	// black
	materialAmbiant[0] = 0.02f;
	materialAmbiant[1] = 0.02f;
	materialAmbiant[2] = 0.02f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.01f;
	materialDiffuse[1] = 0.01f;
	materialDiffuse[2] = 0.01f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.4f;
	materialSpecular[1] = 0.4f;
	materialSpecular[2] = 0.4f;
	materialSpecular[3] = 0.4f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.0f, 14.0f, 0.0f);
	gluSphere(quadric[18], 1.0f, 30, 30);

	// cyan
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.05f;
	materialAmbiant[2] = 0.05f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.5f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.4f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.7f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.0f, 11.5f, 0.0f);
	gluSphere(quadric[19], 1.0f, 30, 30);

	// green
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.05f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.04f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.0f, 9.0f, 0.0f);
	gluSphere(quadric[20], 1.0f, 30, 30);

	// red
	materialAmbiant[0] = 0.05f;
	materialAmbiant[1] = 0.0f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.4f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.04f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.0f, 6.5f, 0.0f);
	gluSphere(quadric[21], 1.0f, 30, 30);

	// white
	materialAmbiant[0] = 0.05f;
	materialAmbiant[1] = 0.05f;
	materialAmbiant[2] = 0.05f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.5f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.7f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.0f, 4.0f, 0.0f);
	gluSphere(quadric[22], 1.0f, 30, 30);

	// yellow
	materialAmbiant[0] = 0.05f;
	materialAmbiant[1] = 0.05f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.0f, 1.5f, 0.0f);
	gluSphere(quadric[23], 1.0f, 30, 30);

}

void update(void) {
	angleOfXRotation -= 0.5f;
	if (angleOfXRotation > 360.0f)
	{
		angleOfXRotation += 360.0f;
	}
	angleOfYRotation -= 0.5f;
	if (angleOfYRotation > 360.0f)
	{
		angleOfYRotation += 360.0f;
	}
	angleOfZRotation -= 0.5f;
	if (angleOfZRotation > 360.0f)
	{
		angleOfZRotation += 360.0f;
	}
}

void uninitialize(void) {
	if (bFullScreen == true)
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}
	// Break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
		gHdc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "File Closed Successfully \n");
		fclose(gpFile);
		gpFile = NULL;
	}
	for (GLint i = 0; i < 24; i++)
	{
		if (quadric[i])
		{
			gluDeleteQuadric(quadric[i]);
		}
	}
}

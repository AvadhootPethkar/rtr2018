// Headers
#include<Windows.h>
#include<stdio.h> // For file handeling
#include<gl/GL.h>
#include<gl/GLU.h>
#define _USE_MATH_DEFINES 1
#include<math.h>
#pragma comment(lib, "opengl32.lib") 
#pragma comment(lib, "glu32.lib") 
#define WIN_WIDTH 800 
#define WIN_HEIGHT 600 

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Variable Declarations
HWND gHwnd = NULL;
bool bFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC gHdc = NULL; // Change 5
HGLRC ghrc = NULL; // Change 6
bool gbActiveWindow = false; // Change 7
FILE *gpFile = NULL;

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function declaration
	int initialize(void);
	void display(void); // Change 1

	// Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false; // For game loop
	int iRet = 0;

	// Code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log flile cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created \n");
	}
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Change 11
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, // Extended style
		szAppName,
		TEXT("FFPOGL Native Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, // Change 14
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetFixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "Window Destroyed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Succeeded \n");
	}
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Play game here....
			if (gbActiveWindow == true)
			{
				// Call 'update' here
			}
			display(); // Change 2
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	// Function declaration
	void ToggelFullScreen(void);
	void resize(int, int);
	void uninitialize(void);

	// code
	switch (iMsg) {
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND: // Change 5
		return(0); // IMP

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'f':
		case 'F':
			ToggelFullScreen();
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		default:
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize(); // Change 27
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggelFullScreen(void) {
	MONITORINFO mi;

	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(
					MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}

int initialize(void) {
	// function declaration
	void resize(int, int);
	void drawTriangle();

	// Variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	// initialize pf structure
	// To make members of structure 0.
	memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // Change 6
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	gHdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(gHdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(gHdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}
	ghrc = wglCreateContext(gHdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return(-4);
	}
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

GLfloat getDistance(GLfloat x1, GLfloat x2, GLfloat y1, GLfloat y2) {
	GLfloat xSquare = powf((x2 - x1), 2.0f);
	GLfloat ySquare = powf((y2 - y1), 2.0f);
	return sqrtf((xSquare + ySquare));
}

GLfloat getOffset(GLfloat coA, GLfloat coB, GLfloat coC, GLfloat distAB, GLfloat distBC, GLfloat distAC, GLfloat perimeter) {
	GLfloat value = (coA * distBC) + (coB * distAC) + (coC * distAB);
	return(value / perimeter);
}

void drawCircle(GLfloat fRadius, GLfloat fOffsetX, GLfloat fOffsetY) {
	int iPoints = 10000;
	double dAngle = 0.0;
	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 0.0f, 0.0);
	for (int i = 0; i < iPoints; i++)
	{
		dAngle = 2 * M_PI * i / iPoints;
		glVertex2f(fRadius * cos(dAngle) + fOffsetX, fRadius * sin(dAngle) + fOffsetY);
	}
	glEnd();
}

void drawTriangle(GLfloat fLength) {
	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(0.0f, fLength);
	glVertex2f(-fLength, -fLength);
	glVertex2f(fLength, -fLength);
	glEnd();
}

void resize(int width, int height) {
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display(void) {
	//GLfloat coX1 = 0.0f;
	//GLfloat coY1 = 1.0f;
	//GLfloat coX2 = -1.0f;
	//GLfloat coY2 = -1.0f;
	//GLfloat coX3 = 1.0f;
	//GLfloat coY3 = -1.0f;
	GLfloat coX1 = 0.0f;
	GLfloat coY1 = 0.5f;
	GLfloat coX2 = -0.5f;
	GLfloat coY2 = -0.5f;
	GLfloat coX3 = 0.5f;
	GLfloat coY3 = -0.5f;
	GLfloat fdistAB = 0.0f;
	GLfloat fdistBC = 0.0f;
	GLfloat fdistAC = 0.0f;
	GLfloat fPerimeter = 0.0f;
	GLfloat fSemiPerimeter = 0.0f;
	GLfloat fAreaOfTriangle = 0.0f;
	GLfloat fRadiusOfInCircle = 0.0f;
	GLfloat fOffsetX = 0.0f;
	GLfloat fOffsetY = 0.0f;
	GLfloat fTriangleHeight = 0.0f;
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,
		0.0f,
		-3.0f);
	drawTriangle(0.5f);
	fdistAB = getDistance(coX1, coX2, coY1, coY2);
	fdistBC = getDistance(coX2, coX3, coY2, coY3);
	fdistAC = getDistance(coX1, coX3, coY1, coY3);
	//fAreaOfTriangle = 0.5f * 1.0f * 0.5f;

	fPerimeter = fdistAB + fdistBC + fdistAC;
	fSemiPerimeter = fPerimeter / 2;

	//fRadiusOfInCircle = (2 * fAreaOfTriangle) / fSemiPerimeter;
	fRadiusOfInCircle = sqrtf((fSemiPerimeter - fdistAB) * (fSemiPerimeter - fdistBC) * (fSemiPerimeter - fdistAC) / fSemiPerimeter);
	fOffsetX = getOffset(coX1, coX2, coX3, fdistAB, fdistBC, fdistAC, fPerimeter);
	fOffsetY = getOffset(coY1, coY2, coY3, fdistAB, fdistBC, fdistAC, fPerimeter);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,
		0.0f,
		-3.0f);
	drawCircle(fRadiusOfInCircle, fOffsetX, fOffsetY);
	SwapBuffers(gHdc);
}

void uninitialize(void) {
	if (bFullScreen == true)
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}
	// Break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
		gHdc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "File Closed Successfully \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

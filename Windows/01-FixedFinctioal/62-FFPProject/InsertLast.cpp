#include"InsertLast.h"

int InsertLast(struct node **head, struct node **tail, float x,float y, char tempName[50]) {
	struct node *newnode = NULL;

	int retVal = CreateNode(newnode, x,y, tempName);
	if (!retVal) return retVal;
	if (*head == NULL)
	{
		*head = *tail = newnode;
	}
	else				//If Linked List contains atleast one node
	{
		(*tail)->next = newnode;
		newnode->prev = *tail;
		*tail = newnode;
	}

	return 1;
}

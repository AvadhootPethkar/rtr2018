#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS
#endif // _win32

// Headers
#include<Windows.h>
#include <math.h>  // Header File For Windows Math Library     ( ADD )
#include<stdio.h> // For file handeling
#include<gl/GL.h>
#include<gl/GLU.h>
#include"StackUsingLinkedList.h"
#include"node.h"
#include"InsertLast.h"
#include"DeleteLast.h"
#pragma comment(lib, "opengl32.lib") 
#pragma comment(lib, "glu32.lib") 
#define WIN_WIDTH 800 
#define WIN_HEIGHT 600 

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Variable Declarations
//struct FStackFrameValues
//{
//	GLfloat x;
//	GLfloat y;
//	struct FStackFrameValues *next;
//};
HWND gHwnd = NULL;
bool bFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC gHdc = NULL; // Change 5
HGLRC ghrc = NULL; // Change 6
bool gbActiveWindow = false; // Change 7
FILE *gpFile = NULL;
GLfloat gfTriangleX = 1.0f;
GLfloat gfTriangleY = -1.0f;
GLfloat gfCircleX = -1.0f;
GLfloat gfCircleY = -1.0f;
GLfloat gfAngle = 0.0f;
GLfloat gfLineY = 1.0f;
GLfloat gActivationRecord_X = 1.5f;
GLfloat gActivationRecord_Y = 0.0f;
GLfloat gActivationRecord_Z = 0.0f;
struct node *head;
struct node *tail;

//GLint gFunctionCounter = 0;

// Font variables
GLuint  base;                           // Base Display List For The Font Set
GLfloat cnt1;                           // 1st Counter Used To Move Text & For Coloring
GLfloat cnt2;                           // 2nd Counter Used To Move Text & For Coloring

// Animation variables
GLfloat positionStart_X = 1.5f;
GLfloat positionStart_Y = 0.0f;
GLfloat positionStart_Z = 0;
GLfloat positionEnd_X = positionStart_X;
GLfloat positionEnd_Y = positionStart_Y;
GLfloat positionEnd_Z = 0;
GLint arrayValue = 0;
GLint arrayCounter = 0;
GLint stackFrameCounter = 0;
GLfloat gStackFrame_X = -1.5f;
GLfloat gStackFrame_Y = -2.0f;
int flagInserted = 0;
char tempName[50];



float lastFixYPos = -2.00f;
bool animateFlag = false;
bool gYFlag = true;

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function declaration
	int initialize(void);
	void display(void); // Change 1
	void update(void);

	// Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false; // For game loop
	int iRet = 0;

	// Code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log flile cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created \n");
	}
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Change 11
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, // Extended style
		szAppName,
		TEXT("FFPOGL Native Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, // Change 14
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetFixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "Window Destroyed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Succeeded \n");
	}
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Play game here....
			if (gbActiveWindow == true)
			{
				// Call 'update' here
				update();
			}
			display(); // Change 2
		}
	}
	return((int)msg.wParam);
}

void update(void) {
	gfAngle += 0.1f;
	if (gfAngle >= 360.0f)
	{
		gfAngle = 0.0f;
	}
	if (animateFlag == false && (arrayCounter <= gArrayCounter))
	{
		arrayValue = gFunctionArray[arrayCounter];
		strcpy(tempName, functionNameArray[arrayCounter++]);
		animateFlag = true;
		//positionEnd_X = positionStart_X;
		//positionEnd_Y = positionStart_Y;
		gYFlag = true;
	}
	if (animateFlag)
	{
		if (arrayValue == 1)
		{
			if(flagInserted==0)
				InsertLast(&head, &tail, positionStart_X, positionStart_Y, tempName);

			flagInserted=1;
			if ((tail->y <= 1.4f) && (gYFlag))
			{
				tail->y += 0.005f;
			}
			else
			{
				gYFlag = false;
				if (tail->x >= -1.5f)
				{
					tail->x -= 0.005f;
				}
				else
				{
					if (tail->y >= lastFixYPos)
					{
						tail->y -= 0.005f;
					}
					else
					{
						animateFlag = false;
						stackFrameCounter++;
						lastFixYPos += 0.35f;
						flagInserted = 0;
						//stackFrame_Y += 0.7f;
					}
				}
			}
		}
		else
		{
			if (tail != NULL || gArrayCounter < arrayCounter)
			{

				if ((tail->y <= 1.4f) && (gYFlag))//Check
				{
					tail->y += 0.005f;
				}
				else
				{
					if (tail->x >= -6.5f)
					{
						tail->x -= 0.005f;
					}
					else
					{
						animateFlag = false;
						stackFrameCounter--;
						DeleteLast(&head, &tail);
						lastFixYPos -= 0.35f;
						//gStackFrame_Y -= 0.3f;
					}
				}
			}
		}
	}
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	// Function declaration
	void ToggelFullScreen(void);
	void resize(int, int);
	void uninitialize(void);

	// code
	switch (iMsg) {
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND: // Change 5
		return(0); // IMP

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'f':
		case 'F':
			ToggelFullScreen();
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		default:
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize(); // Change 27
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggelFullScreen(void) {
	MONITORINFO mi;

	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(
					MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}

int initialize(void) {
	// function declaration
	void resize(int, int);
	GLvoid BuildFont(GLvoid);

	// Variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	// initialize pf structure
	// To make members of structure 0.
	memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // Change 6
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;
	gHdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(gHdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(gHdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}
	ghrc = wglCreateContext(gHdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return(-4);
	}
	StackImplementation();
	GLint i = 0;
	while (gFunctionArray[i] != 2)
	{
		fprintf(gpFile, "%d \n", gFunctionArray[i++]);
	}
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	BuildFont();
	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void resize(int width, int height) {
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display(void) {

	// Function Declaration
	void drawCube(void);
	void drawActivationRecord(void);
	GLvoid glPrint(const char *fmt, ...);
	void drawConstantStackFrames(void);

	// Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glLoadIdentity();
	glTranslatef(-1.5f,
		-1.0f,
		-6.0f);
	drawCube();

	if (tail != NULL)
	{
		if (animateFlag)
		{
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			glTranslatef(tail->x,
				tail->y,
				-6.0f);
			glScalef(1.0f, 0.5f, 1.0f);
			//glRotatef(gfAngle, 1.0f, 0.0f, 0.0f);
			drawActivationRecord();
			glRasterPos3f(-0.8f, 0.2f, 1.1f);
			glPrint("Activation Record:");
			glRasterPos3f(-0.8f, 0.0f, 1.1f);
			glPrint(tail->functionName);
		}
	}

	if (tail != NULL)
	{
		drawConstantStackFrames();
	}
	SwapBuffers(gHdc);
}

void drawConstantStackFrames(void)
{
	void drawActivationRecord(void);
	GLvoid glPrint(const char *fmt, ...);

//	GLint stackFrame_X = -1.5f;
//	GLint stackFrame_Y = -2.0f;

	struct node *temp=head;
	int tempC=0;
	while (head != NULL && tempC<stackFrameCounter) {
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		/*glTranslatef(-0.4f,
			0.0f,
			0.0f);*/
		glTranslatef(temp->x,
			temp->y,
			-6.0f);
		glScalef(1.0f, 0.5f, 1.0f);
		//glRotatef(gfAngle, 1.0f, 0.0f, 0.0f);
		drawActivationRecord();
		glRasterPos3f(-0.8f, 0.2f, 1.1f);
		glPrint("Activation Record:");
		glRasterPos3f(-0.8f, 0.0f, 1.1f);
		glPrint(temp->functionName);
		//stackFrame_Y += 0.1f;
		temp=temp->next;
		tempC++;
	}



	//for (GLint i = 0; i < stackFrameCounter; i++)
	//{
	//	glMatrixMode(GL_MODELVIEW);
	//	glLoadIdentity();
	//	glTranslatef(-0.5f,
	//		0.0f,
	//		0.0f);
	//	glTranslatef(stackFrame_X,
	//		stackFrame_Y,
	//		-6.0f);
	//	glScalef(1.0f, 0.5f, 1.0f);
	//	//glRotatef(gfAngle, 1.0f, 0.0f, 0.0f);
	//	drawActivationRecord();
	//	glRasterPos3f(-0.8f, 0.2f, 1.1f);
	//	glPrint("Activation Record 1:");
	//	glRasterPos3f(-0.8f, 0.0f, 1.1f);
	//	glPrint("main()");
	//	stackFrame_Y += 0.1f;
	//}
}

void drawCube() {
	glBegin(GL_QUADS);
	glColor4f(1.0f, 1.0f, 1.0f,0.1f);
	glVertex3f(1.0f, 2.0f, 1.0f);    // x, y
	glVertex3f(-1.0f, 2.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, 2.0f, -1.0f);    // x, y
	glVertex3f(1.0f, 2.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, 2.0f, -1.0f);    // x, y
	glVertex3f(1.0f, 2.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, 2.0f, 1.0f);    // x, y
	glVertex3f(-1.0f, 2.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);    // x, y
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);    // x, y
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//glColor3f(1.0f, 0.0f, 0.0f);
	//glColor3f(0.0f, 1.0f, 0.0f);
	//glColor3f(0.0f, 0.0f, 1.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glEnd();
}

void drawActivationRecord() {
	glBegin(GL_QUADS);
	glColor4f(1.0f, 1.0f, 1.0f, 0.3f);
	glVertex3f(1.0f, 0.5f, 1.0f);    // x, y
	glVertex3f(-1.0f, 0.5f, 1.0f);
	glVertex3f(-1.0f, -0.2f, 1.0f);
	glVertex3f(1.0f, -0.2f, 1.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, 0.5f, -1.0f);    // x, y
	glVertex3f(1.0f, 0.5f, 1.0f);
	glVertex3f(1.0f, -0.2f, 1.0f);
	glVertex3f(1.0f, -0.2f, -1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, 0.5f, -1.0f);    // x, y
	glVertex3f(1.0f, 0.5f, -1.0f);
	glVertex3f(1.0f, -0.2f, -1.0f);
	glVertex3f(-1.0f, -0.2f, -1.0f);

	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, 0.5f, 1.0f);    // x, y
	glVertex3f(-1.0f, 0.5f, -1.0f);
	glVertex3f(-1.0f, -0.2f, -1.0f);
	glVertex3f(-1.0f, -0.2f, 1.0f);

	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 0.5f, -1.0f);    // x, y
	glVertex3f(-1.0f, 0.5f, -1.0f);
	glVertex3f(-1.0f, 0.5f, 1.0f);
	glVertex3f(1.0f, 0.5f, 1.0f);

	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -0.2f, -1.0f);    // x, y
	glVertex3f(-1.0f, -0.2f, -1.0f);
	glVertex3f(-1.0f, -0.2f, 1.0f);
	glVertex3f(1.0f, -0.2f, 1.0f);

	//glColor3f(1.0f, 0.0f, 0.0f);
	//glColor3f(0.0f, 1.0f, 0.0f);
	//glColor3f(0.0f, 0.0f, 1.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glEnd();
}

GLvoid BuildFont(GLvoid)                    // Build Our Bitmap Font
{
	HFONT   font;                       // Windows Font ID
	HFONT   oldfont;                    // Used For Good House Keeping

	base = glGenLists(96);

	font = CreateFont(-24,
		0,
		0,
		0,
		FW_BOLD,
		FALSE,
		FALSE,
		FALSE,
		ANSI_CHARSET,
		OUT_TT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		ANTIALIASED_QUALITY,
		FF_DONTCARE | DEFAULT_PITCH,
		"Courier New");

	oldfont = (HFONT)SelectObject(gHdc, font);       // Selects The Font We Want
	wglUseFontBitmaps(gHdc, 32, 96, base);           // Builds 96 Characters Starting At Character 32
	SelectObject(gHdc, oldfont);             // Selects The Font We Want
	DeleteObject(font);                 // Delete The Font
}

GLvoid KillFont(GLvoid)                     // Delete The Font List
{
	glDeleteLists(base, 96);                // Delete All 96 Characters ( NEW )
}

GLvoid glPrint(const char *fmt, ...)                // Custom GL "Print" Routine
{
	char        text[256];              // Holds Our String
	va_list     ap;

	if (fmt == NULL)                    // If There's No Text
		return;

	va_start(ap, fmt);                  // Parses The String For Variables
	vsprintf(text, fmt, ap);                // And Converts Symbols To Actual Numbers
	va_end(ap);

	glPushAttrib(GL_LIST_BIT);              // Pushes The Display List Bits     ( NEW )
	glListBase(base - 32);

	glCallLists(strlen(text), GL_UNSIGNED_BYTE, text);  // Draws The Display List Text  ( NEW )
	glPopAttrib();                      // Pops The Display List Bits   ( NEW )
}

void uninitialize(void) {
	if (bFullScreen == true)
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}
	// Break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
		gHdc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "File Closed Successfully \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

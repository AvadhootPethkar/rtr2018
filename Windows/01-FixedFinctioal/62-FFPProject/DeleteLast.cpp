#include"DeleteLast.h"
#include"InsertLast.h"

int DeleteLast(struct node **head, struct node **tail)
{
	struct node *temp = *head;
	if (*head == NULL)
	{
		return 0;
	}
	else if ((*head)->next == NULL)
	{
		free(temp);
		*head = *tail = NULL;
	}
	else
	{
		while (temp->next != *tail)
		{
			temp = temp->next;
		}
		temp->next = NULL;
		free(*tail);
		*tail = temp;
	}
	return 1;
}

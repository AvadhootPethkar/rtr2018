#include<string.h>
#include"node.h"

int CreateNode(node * &newnode, float x,float y, char tempName[50])
{
	newnode = (struct node *)malloc(sizeof(struct node));
	if (newnode == NULL)
	{
		return 0;
	}
	newnode->next = NULL;
	//newnode->prev = NULL;
	newnode->x = x;
	newnode->y = y;
	strcpy_s(newnode->functionName, tempName);
	return 1;
}

//void DisplayList(struct node *head) {
//	if (head)
//	{
//		while (head)
//		{
//			printf("|%d|->", head->data);
//			head = head->next;
//		}
//	}
//	else
//	{
//		printf("Linked List is Empty\n"); 
//	}
//}

int Count(struct node *head) {
	int count = 0;

	while (head != NULL)
	{
		count++;
		head = head->next;
	}
	return count;
}

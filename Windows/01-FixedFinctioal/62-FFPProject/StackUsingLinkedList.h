#include <stdio.h>
#include <stdlib.h>

struct StackNode
{
	int data;
	struct StackNode *next;
};

static int gFunctionCallCounter = 0;
static int gArrayCounter = 0;
static int gFunctionArray[100];
struct StackNode *root = NULL;
char functionNameArray[50][50];

int StackImplementation() {
	// Function Declaration:
	void push(struct StackNode**, int);
	int pop(struct StackNode**);
	int peek(struct StackNode*);

	// Variable Declaration:

	// Code:
	gFunctionCallCounter++;
	gFunctionArray[gArrayCounter] = 1;
	strcpy(functionNameArray[gArrayCounter++], "Main()");
	push(&root, 10);
	push(&root, 20);
	push(&root, 30);
	pop(&root);
	peek(root);
	pop(&root);
	//printf("%d popped from stack\n", pop(&root));
	//printf("%d popped from stack\n", pop(&root));
	//printf("Top element is %d\n", peek(root));
	gFunctionArray[gArrayCounter++] = 0;
	gFunctionArray[gArrayCounter++] = 2;
	return(0);
}

void push(struct StackNode **root, int data) {
	// Function Declaration:
	struct StackNode* newNode(int);

	// Variable Declaration:
	struct StackNode *stackNode = NULL;

	// Code:
	gFunctionCallCounter++;
	gFunctionArray[gArrayCounter] = 1;
	strcpy(functionNameArray[gArrayCounter++], "push()");
	stackNode = newNode(data);
	stackNode->next = *root;
	*root = stackNode;
	printf("%d pushed to stack\n", data);
	gFunctionArray[gArrayCounter++] = 0;
	return;
}

struct StackNode* newNode(int data) {
	// Variable Declaration:
	struct StackNode *stackNode = NULL;

	// Code:
	gFunctionCallCounter++;
	gFunctionArray[gArrayCounter] = 1;
	strcpy(functionNameArray[gArrayCounter++], "newNode()");
	stackNode = (struct StackNode *)malloc(sizeof(struct StackNode));
	stackNode->data = data;
	stackNode->next = NULL;
	gFunctionArray[gArrayCounter++] = 0;
	return(stackNode);
}

int pop(struct StackNode **root) {
	// Function Declaration:
	int isEmpty(struct StackNode *);

	// Variable Declaration:
	struct StackNode *temp = NULL;
	int popped = NULL;

	// Code:
	gFunctionCallCounter++;
	gFunctionArray[gArrayCounter] = 1;
	strcpy(functionNameArray[gArrayCounter++], "pop()");
	if (isEmpty(*root))
	{
		return(INT_MIN);
	}
	temp = *root;
	*root = (*root)->next;
	popped = temp->data;
	free(temp);
	gFunctionArray[gArrayCounter++] = 0;
	return(popped);
}

int peek(struct StackNode *root) {
	// Function Declaration:
	int isEmpty(struct StackNode *);

	// Code:
	gFunctionCallCounter++;
	gFunctionArray[gArrayCounter] = 1;
	strcpy(functionNameArray[gArrayCounter++], "peek()");
	if (isEmpty(root))
	{
		return(INT_MIN);
	}
	gFunctionArray[gArrayCounter++] = 0;
	return(root->data);
}

int isEmpty(struct StackNode *root) {
	// Code:
	gFunctionCallCounter++;
	gFunctionArray[gArrayCounter] = 1;
	strcpy(functionNameArray[gArrayCounter++], "isEmpty()");
	gFunctionArray[gArrayCounter++] = 0;
	return(!root);
}


#pragma once
#include<stdlib.h>
#include<conio.h>
#include<stdio.h>

struct node {
	float x;
	float y;
	struct node *next;
	struct node *prev;
	char functionName[50];
};

int CreateNode(node * &newnode, float x,float y , char tempName[50]);
//void DisplayList(struct node *head);
int Count(struct node *head);

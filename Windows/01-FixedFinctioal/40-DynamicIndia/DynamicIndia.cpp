// Headers
#include<Windows.h>
#include<stdio.h> // For file handeling
#define _USE_MATH_DEFINES 1
#include<math.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include "DynamicIndia.h"
#pragma comment(lib, "opengl32.lib") 
#pragma comment(lib, "glu32.lib") 
#pragma comment(lib, "Winmm.lib") 
#define WIN_WIDTH 800 
#define WIN_HEIGHT 600 

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Variable Declarations
HWND gHwnd = NULL;
bool bFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC gHdc = NULL; // Change 5
HGLRC ghrc = NULL; // Change 6
bool gbActiveWindow = false; // Change 7
FILE *gpFile = NULL;
HINSTANCE g_hInstance = NULL;
GLfloat gfLeftI_X = -2.0f;
GLfloat gfA_X = 2.3f;
GLfloat gfN_Y = 2.7f;
GLfloat gfRightI_Y = -2.7f;
GLfloat gfDOrangeColor_RValue = 0.0f;
GLfloat gfDOrangeColor_GValue = 0.0f;
GLfloat gfDOrangeColor_BValue = 0.0f;
GLfloat gfDGreenColor_RValue = 0.0f;
GLfloat gfDGreenColor_GValue = 0.0f;
GLfloat gfDGreenColor_BValue = 0.0f;

// Flag Variables:
GLfloat gfUpperFlagOrangeX = -12.0f;
GLfloat gfUpperFlagOrangeY = 0.03f;
GLfloat gfUpperFlagWhiteX = -12.0f;
GLfloat gfUpperFlagWhiteY = 0.0f;
GLfloat gfUpperFlagGreenX = -12.0f;
GLfloat gfUpperFlagGreenY = -0.03f;

// Plane Variables:
GLfloat gfMiddleTranslateFunction_X = -6.0f;
GLfloat gfMiddleTranslateFunction_Y = 0.0f;

GLfloat gfUpperLeftAngle = M_PI;
GLfloat gfUpperLeftAngle_X = 0.0f;
GLfloat gfUpperLeftAngle_Y = 0.0f;

GLfloat gfLowerLeftAngle = M_PI;
GLfloat gfLowerLeftAngle_X = 0.0f;
GLfloat gfLowerLeftAngle_Y = 0.0f;

GLfloat gfUpperRightAngle = 3 * (M_PI / 2);
GLfloat gfUpperRightAngle_X = 0.0f;
GLfloat gfUpperRightAngle_Y = 0.0f;

GLfloat gfLowerRightAngle = (M_PI / 2);
GLfloat gfLowerRightAngle_X = 0.0f;
GLfloat gfLowerRightAngle_Y = 0.0f;

// Flag Color variable:
GLfloat gfFlagOrangeColor_R = 1.0;
GLfloat gfFlagOrangeColor_G = 0.6f;
GLfloat gfFlagOrangeColor_B = 0.2f;

GLfloat gfFlagWhiteColor_R = 1.0;
GLfloat gfFlagWhiteColor_G = 1.0f;
GLfloat gfFlagWhiteColor_B = 1.0f;

GLfloat gfFlagGreenColor_R = 0.07;
GLfloat gfFlagGreenColor_G = 0.53f;
GLfloat gfFlagGreenColor_B = 0.02f;

// Small Flag X,Y Values:
GLfloat gfSmallFlagOrangeY = 0.01f;
GLfloat gfSmallFlagWhiteY = 0.0f;
GLfloat gfSmallFlagGreenY = -0.01f;

GLfloat gfSmallFlagOrangeX1 = 1.25f;
GLfloat gfSmallFlagWhiteX1 = 1.25f;
GLfloat gfSmallFlagGreenX1 = 1.25f;
GLfloat gfSmallFlagOrangeX2 = 1.54f;
GLfloat gfSmallFlagWhiteX2 = 1.54f;
GLfloat gfSmallFlagGreenX2 = 1.54f;


GLfloat gfVaringValue = 0.006;

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function declaration
	int initialize(void);
	void display(void); // Change 1
	void update(void);

	// Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Dynamic India");
	bool bDone = false; // For game loop
	int iRet = 0;

	// Code
	g_hInstance = hInstance;
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log flile cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created \n");
	}
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Change 11
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, // Extended style
		szAppName,
		TEXT("FFPOGL Native Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, // Change 14
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetFixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "Window Destroyed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Succeeded \n");
	}
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Play game here....
			if (gbActiveWindow == true)
			{
				// Call 'update' here
				update();
			}
			display(); // Change 2
		}
	}
	return((int)msg.wParam);
}

void update(void) {
	void drawFlagSmall(void);
	if (gfLeftI_X <= -0.4f)
	{
		gfLeftI_X += gfVaringValue;
	}
	else
	{
		if (gfA_X >= 0.4f)
		{
			gfA_X -= gfVaringValue;
		}
		else
		{
			if (gfN_Y >= 0.0f)
			{
				gfN_Y -= gfVaringValue;
			}
			else
			{
				if (gfRightI_Y <= 0.0f)
				{
					gfRightI_Y += gfVaringValue;
				}
				else
				{
					if (gfDOrangeColor_RValue <= 1.0f)
					{
						gfDOrangeColor_RValue += gfVaringValue;
					}
					if (gfDOrangeColor_GValue <= 0.6f)
					{
						gfDOrangeColor_GValue += gfVaringValue;
					}
					if (gfDOrangeColor_BValue <= 0.2f)
					{
						gfDOrangeColor_BValue += gfVaringValue;
					}
					if (gfDGreenColor_RValue <= 0.07f)
					{
						gfDGreenColor_RValue += gfVaringValue;
					}
					if (gfDGreenColor_BValue <= 0.53f)
					{
						gfDGreenColor_BValue += gfVaringValue;
					}
					if (gfDGreenColor_GValue <= 0.02f)
					{
						gfDGreenColor_GValue += gfVaringValue;
					}
					else
					{
						if ((gfDOrangeColor_RValue >= 1.0f) && (gfDOrangeColor_GValue >= 0.6f) && (gfDOrangeColor_BValue >= 0.2f) && (gfDGreenColor_RValue >= 0.07f) && (gfDGreenColor_GValue >= 0.02f) && (gfDGreenColor_BValue >= 0.53f) )
						{
							if (gfMiddleTranslateFunction_X <= 6.5f)
							{
								gfMiddleTranslateFunction_X += 0.00219f;
								gfUpperFlagOrangeX -= gfVaringValue;
								gfUpperFlagWhiteX -= gfVaringValue;
								gfUpperFlagGreenX -= gfVaringValue;
							}
							if (gfMiddleTranslateFunction_X >= 6.5f)
							{
								gfUpperFlagOrangeX += 0.12f;
								gfUpperFlagWhiteX += 0.12f;
								gfUpperFlagGreenX += 0.12f;

								gfFlagOrangeColor_R -= 0.0025f;
								gfFlagOrangeColor_G -= 0.0025f;
								gfFlagOrangeColor_B -= 0.0025f;
								gfFlagWhiteColor_R -= 0.0025f;
								gfFlagWhiteColor_G -= 0.0025f;
								gfFlagWhiteColor_B -= 0.0025f;
								gfFlagGreenColor_R -= 0.0025f;
								gfFlagGreenColor_G -= 0.0025f;
								gfFlagGreenColor_B -= 0.0025f;

							}
							//drawLine(0.84f, 0.0f, 1.14f, 0.0f);
							if (gfUpperFlagOrangeX >= 0.84f &&
								gfUpperFlagWhiteX >= 0.84f &&
								gfUpperFlagGreenX >= 0.84f) {
								drawFlagSmall();
							}
						}
					}
				}
			}
		}
	}
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	// Function declaration
	void ToggelFullScreen(void);
	void resize(int, int);
	void uninitialize(void);

	// Variable Declaration
	static BOOL hBitmap;

	// code
	switch (iMsg) {
	case WM_CREATE:
		hBitmap = PlaySound(MAKEINTRESOURCE(SOUND_WAVE), NULL, SND_RESOURCE | SND_ASYNC);
		if (hBitmap == FALSE)
		{
			fprintf(gpFile, "Fail to load Sound Bitmap \n");
			DestroyWindow(hwnd);
		}
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND: // Change 5
		return(0); // IMP

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			//case 'f':
			//case 'F':
			//	ToggelFullScreen();
			//	break;

		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		default:
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize(); // Change 27
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggelFullScreen(void) {
	MONITORINFO mi;

	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(
					MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}

int initialize(void) {
	// function declaration
	void resize(int, int);
	void drawTriangle();
	void ToggelFullScreen(void);
	// Variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// Code
	// initialize pf structure
	// To make members of structure 0.
	memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // Change 6
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	gHdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(gHdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(gHdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}
	ghrc = wglCreateContext(gHdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return(-4);
	}
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	resize(WIN_WIDTH, WIN_HEIGHT);
	ToggelFullScreen();
	return(0);
}

void resize(int width, int height) {
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display(void) {
	// Function Declaration:
	void drawLine(float x1, float y1, float x2, float y2);
	void displayPlain(void);

	// Code:
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW); // L-I
	glLoadIdentity();
	glTranslatef(gfLeftI_X,
		0.0f,
		-4.0f);
	glLineWidth(6.0f);
	drawLine(-1.0f, 1.0f, -1.0f, -1.0f);
	glMatrixMode(GL_MODELVIEW);	// N
	glLoadIdentity();
	glTranslatef(0.06f,
		gfN_Y,
		-4.0f);
	//glLineWidth(3.0f);
	drawLine(-1.0f, 1.0f, -1.0f, -1.0f);
	drawLine(-1.0f, 1.0f, -0.5f, -1.0f);
	drawLine(-0.5f, 1.0f, -0.5f, -1.0f);

	glMatrixMode(GL_MODELVIEW);	// D
	glLoadIdentity();
	glTranslatef(0.88f,
		0.0f,
		-4.0f);
	//drawLine(-1.0f, 1.0f, -1.0f, -1.0f);	//R|
	glBegin(GL_LINES);
	glColor3f(gfDOrangeColor_RValue, gfDOrangeColor_GValue, gfDOrangeColor_BValue);
	glVertex2f(-1.0f, 1.0f);
	glColor3f(gfDGreenColor_RValue, gfDGreenColor_BValue, gfDGreenColor_GValue);
	glVertex2f(-1.0f, -1.0f);
	glEnd();

	//drawLine(-1.0f, 1.0f, -0.5f, 1.0f);	//U-
	glBegin(GL_LINES);
	glColor3f(gfDOrangeColor_RValue, gfDOrangeColor_GValue, gfDOrangeColor_BValue);
	glVertex2f(-1.03f, 1.0f);
	glVertex2f(-0.5f, 1.0f);
	glEnd();

	//drawLine(-0.5f, 1.0f, -0.5f, -1.0f);	//L|
	glBegin(GL_LINES);
	glColor3f(gfDOrangeColor_RValue, gfDOrangeColor_GValue, gfDOrangeColor_BValue);
	glVertex2f(-0.5f, 1.0f);
	glColor3f(gfDGreenColor_RValue, gfDGreenColor_BValue, gfDGreenColor_GValue);
	glVertex2f(-0.5f, -1.0f);
	glEnd();

	//drawLine(-0.5f, -1.0f, -1.0f, -1.0f);	//D-
	glBegin(GL_LINES);
	glColor3f(gfDGreenColor_RValue, gfDGreenColor_BValue, gfDGreenColor_GValue);
	glVertex2f(-0.5f, -1.0f);
	glVertex2f(-1.03f, -1.0f);
	glEnd();

	glMatrixMode(GL_MODELVIEW); // R-I
	glLoadIdentity();
	glTranslatef(1.76f,
		gfRightI_Y,
		-4.0f);
	glLineWidth(6.0f);
	drawLine(-1.0f, 1.0f, -1.0f, -1.0f);
	glMatrixMode(GL_MODELVIEW); // A
	glLoadIdentity();
	glTranslatef(gfA_X,
		0.0f,
		-4.0f);
	glLineWidth(6.0f);
	drawLine(1.0f, 1.0f, 0.7f, -1.0f);
	//drawLine(0.84f, 0.0f, 1.14f, 0.0f);
	drawLine(1.0f, 1.0f, 1.3f, -1.0f);
	if ((gfDOrangeColor_RValue >= 1.0f) && (gfDOrangeColor_GValue >= 0.6f) && (gfDOrangeColor_BValue >= 0.2f) && (gfDGreenColor_RValue >= 0.07f) && (gfDGreenColor_GValue >= 0.02f) && (gfDGreenColor_BValue >= 0.53f))
	{
		displayPlain();
	}
	SwapBuffers(gHdc);
}

void displayPlain(void) {
	// Function Declaration:
	void drawPlain();
	void drawLine(float x1, float y1, float x2, float y2);
	void drawSquare(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);
	void drawFlag();
	void drawFlagSmall(void);

	// Code:
	glMatrixMode(GL_MODELVIEW); // Upper Plane
	glLoadIdentity();
	glTranslatef(-2.5f,
		3.0f,
		-4.0f);
	if (gfUpperLeftAngle < (3.0f * (M_PI / 2)))
	{
		gfUpperLeftAngle_X = 3.0f * cos(gfUpperLeftAngle);
		gfUpperLeftAngle_Y = 3.0f * sin(gfUpperLeftAngle);
		glTranslatef(gfUpperLeftAngle_X,
			gfUpperLeftAngle_Y,
			-4.0f);
	}
	glColor3f(0.182f, 0.226f, 0.238f);
	drawPlain();
	drawFlag();
	gfUpperLeftAngle += 0.001f;

	glMatrixMode(GL_MODELVIEW); // Middle Plane
	glLoadIdentity();
	glTranslatef(gfMiddleTranslateFunction_X,
		gfMiddleTranslateFunction_Y,
		-8.0f);
	//glColor3f(1.0f, 0.6f, 0.2f);
	glColor3f(0.182f, 0.226f, 0.238f);
	drawPlain();
	drawFlag();

	if (gfMiddleTranslateFunction_X > 6.5f)
	{
		glMatrixMode(GL_MODELVIEW); // Small Flag
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -4.0f);
		glLineWidth(3.0f);

		gfSmallFlagOrangeY = 0.01f;
		gfSmallFlagWhiteY = 0.0f;
		gfSmallFlagGreenY = -0.01f;
		gfSmallFlagOrangeX1 = 1.25f;
		gfSmallFlagWhiteX1 = 1.25f;
		gfSmallFlagGreenX1 = 1.25f;
		gfSmallFlagOrangeX2 = 1.54f;
		gfSmallFlagWhiteX2 = 1.54f;
		gfSmallFlagGreenX2 = 1.54f;
		drawFlagSmall();
	}

	glMatrixMode(GL_MODELVIEW); // Lower Plane
	glLoadIdentity();
	glTranslatef(-2.5f,
		-3.0f,
		-4.0f);
	if (gfLowerLeftAngle > (M_PI / 2))
	{
		gfLowerLeftAngle_X = 3.0f * cos(gfLowerLeftAngle);
		gfLowerLeftAngle_Y = 3.0f * sin(gfLowerLeftAngle);
		glTranslatef(gfLowerLeftAngle_X,
			gfLowerLeftAngle_Y,
			-4.0f);
	}
	glColor3f(0.182f, 0.226f, 0.238f);
	drawPlain();
	drawFlag();
	gfLowerLeftAngle -= 0.001f;

	if (gfMiddleTranslateFunction_X >= 3.5f)
	{
		glMatrixMode(GL_MODELVIEW); // Upper Plane
		glLoadIdentity();
		glTranslatef(3.5f,
			3.0f,
			-4.0f);
		if (gfUpperRightAngle < (2.0f * M_PI))
		{
			gfUpperRightAngle_X = 3.0f * cos(gfUpperRightAngle);
			gfUpperRightAngle_Y = 3.0f * sin(gfUpperRightAngle);
			glTranslatef(gfUpperRightAngle_X,
				gfUpperRightAngle_Y,
				-4.0f);
		}
		glColor3f(0.182f, 0.226f, 0.238f);
		drawPlain();
		gfSmallFlagOrangeY = 0.01f;
		gfSmallFlagWhiteY = 0.0f;
		gfSmallFlagGreenY = -0.01f;
		gfSmallFlagOrangeX1 = -0.89f;
		gfSmallFlagWhiteX1 = -0.89f;
		gfSmallFlagGreenX1 = -0.89f;
		gfSmallFlagOrangeX2 = -0.60f;
		gfSmallFlagWhiteX2 = -0.60f;
		gfSmallFlagGreenX2 = -0.60f;
		drawFlagSmall();
		gfUpperRightAngle += 0.001f;


		glMatrixMode(GL_MODELVIEW); // Lower Plane
		glLoadIdentity();
		glTranslatef(3.5f,
			-3.0f,
			-4.0f);
		if (gfLowerRightAngle > 0)
		{
			gfLowerRightAngle_X = 3.0f * cos(gfLowerRightAngle);
			gfLowerRightAngle_Y = 3.0f * sin(gfLowerRightAngle);
			glTranslatef(gfLowerRightAngle_X,
				gfLowerRightAngle_Y,
				-4.0f);
		}
		glColor3f(0.182f, 0.226f, 0.238f);
		drawPlain();
		gfSmallFlagOrangeY = 0.01f;
		gfSmallFlagWhiteY = 0.0f;
		gfSmallFlagGreenY = -0.01f;
		gfSmallFlagOrangeX1 = -0.89f;
		gfSmallFlagWhiteX1 = -0.89f;
		gfSmallFlagGreenX1 = -0.89f;
		gfSmallFlagOrangeX2 = -0.60f;
		gfSmallFlagWhiteX2 = -0.60f;
		gfSmallFlagGreenX2 = -0.60f;
		drawFlagSmall();
		gfLowerRightAngle -= 0.001f;
	}
}

void drawPlain() {
	// Function Declaration:
	void drawLine(float x1, float y1, float x2, float y2);
	void drawFlagLine(float x1, float y1, float x2, float y2);
	void drawSquare(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);

	// Code:
	glColor3f(182.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);
	drawSquare(0.3f, 0.1f, -0.4f, 0.1f, -0.4f, -0.1f, 0.3f, -0.1f);

	glBegin(GL_TRIANGLES);	// Upper Wing
	//glColor3f(1.0f, 0.4f, 1.0f);
	glColor3f(182.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);
	glVertex2f(-0.4f, 0.5f);
	glVertex2f(-0.2f, 0.1f);
	glVertex2f(0.2f, 0.1f);
	glEnd();

	glBegin(GL_TRIANGLES);	// Lower Wing
	//glColor3f(1.0f, 0.4f, 1.0f);
	glColor3f(182.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);
	glVertex2f(-0.4f, -0.5f);
	glVertex2f(-0.2f, -0.1f);
	glVertex2f(0.2f, -0.1f);
	glEnd();

	glBegin(GL_TRIANGLES);	// Front Nosel
	//glColor3f(1.0f, 0.4f, 1.0f);
	glColor3f(182.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);
	glVertex2f(0.3f, 0.1f);
	glVertex2f(0.6f, 0.0f);
	glVertex2f(0.3f, -0.1f);
	glEnd();

	drawSquare(-0.4f, 0.1f, -0.6f, 0.3f, -0.6f, -0.3f, -0.4f, -0.1f);	// Back Tail

	glLineWidth(3.0f);	// I
	glColor3f(0.0f, 0.0f, 0.0f);
	drawFlagLine(-0.33f, 0.08f, -0.33f, -0.08f);

	glLineWidth(3.0f);	// A
	drawFlagLine(-0.16f, 0.08f, -0.25f, -0.08f);
	drawFlagLine(-0.16f, 0.08f, -0.1f, -0.08f);

	glLineWidth(3.0f);	// F
	drawFlagLine(0.02f, 0.08f, 0.02f, -0.08f);
	drawFlagLine(0.02f, 0.08f, 0.12f, 0.08f);
	drawFlagLine(0.02f, -0.005f, 0.12f, -0.005f);
}

void drawFlag() {
	void drawLine(float x1, float y1, float x2, float y2);
	void drawFlagLine(float x1, float y1, float x2, float y2);

	glColor3f(gfFlagOrangeColor_R, gfFlagOrangeColor_G, gfFlagOrangeColor_B);
	drawFlagLine(gfUpperFlagOrangeX, gfUpperFlagOrangeY, -0.60f, gfUpperFlagOrangeY);
	glColor3f(gfFlagWhiteColor_R, gfFlagWhiteColor_G, gfFlagWhiteColor_B);
	drawFlagLine(gfUpperFlagWhiteX, gfUpperFlagWhiteY, -0.60f, gfUpperFlagWhiteY);
	glColor3f(gfFlagGreenColor_R, gfFlagGreenColor_G, gfFlagGreenColor_B);
	drawFlagLine(gfUpperFlagGreenX, gfUpperFlagGreenY, -0.60f, gfUpperFlagGreenY);
}

void drawFlagSmall() {
	void drawFlagLine(float x1, float y1, float x2, float y2);

	glColor3f(1.0f, 0.6f, 0.2f);
	drawFlagLine(gfSmallFlagOrangeX1, gfSmallFlagOrangeY, gfSmallFlagOrangeX2, gfSmallFlagOrangeY);
	glColor3f(1.0f, 1.0f, 1.0f);
	drawFlagLine(gfSmallFlagWhiteX1, gfSmallFlagWhiteY, gfSmallFlagWhiteX2, gfSmallFlagWhiteY);
	glColor3f(0.07f, 0.53f, 0.02f);
	drawFlagLine(gfSmallFlagGreenX1, gfSmallFlagGreenY, gfSmallFlagGreenX2, gfSmallFlagGreenY);
}

void drawLine(float x1, float y1, float x2, float y2) {
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(x1, y1);
	glColor3f(0.07f, 0.53f, 0.02f);
	glVertex2f(x2, y2);
	glEnd();
}

void drawFlagLine(float x1, float y1, float x2, float y2) {
	glBegin(GL_LINES);
	glVertex2f(x1, y1);
	glVertex2f(x2, y2);
	glEnd();
}

void drawSquare(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
	glBegin(GL_QUADS);
	glVertex2f(x1, y1);
	glVertex2f(x2, y2);
	glVertex2f(x3, y3);
	glVertex2f(x4, y4);
	glEnd();
}

void uninitialize(void) {
	if (bFullScreen == true)
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}
	// Break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
		gHdc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "File Closed Successfully \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

// Headers
#include<Windows.h>
#include<stdio.h> // For file handeling
#include<gl/glew.h>
#include<gl/GL.h>
#include"vmath.h"
#include "Sphere.h"

#pragma comment(lib, "glew32.lib") 
#pragma comment(lib, "opengl32.lib") 
#pragma comment(lib, "Sphere.lib") 
#define WIN_WIDTH 800 
#define WIN_HEIGHT 600
const int MATERIAL_ROWS = 6;
const int MATERIAL_COLUMNS = 4;

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Variable Declarations
HWND gHwnd = NULL;
bool bFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC gHdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;
GLenum gResult;

// Shader variables
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_sphere;
GLuint vbo_position_sphere;
GLuint vbo_normal_sphere;
GLuint vbo_element_sphere;

GLuint texture_smily;

GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;
GLuint lKeyPressedUniform;
GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;
GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint lightPositionUniform;
GLuint materialShinynessUniform;

mat4 perspectiveProjectionMatrix;

// Animating variables
GLfloat angleOfXRotation = 0.0f;
GLfloat angleOfYRotation = 0.0f;
GLfloat angleOfZRotation = 0.0f;

// Light Variables
bool isLKeyPressed = false;
bool gbLight = false;
GLint keyPressed = 0;
GLfloat lightAmbiant[] = { 0.0f,0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

// Viewport Variables
GLuint gX;
GLuint gY;
GLuint gWidth;
GLuint gHeight;

struct SphereMaterial
{
	GLfloat materialAmbiant[4];
	GLfloat materialDiffuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShinyness;
};

// Sphere Variables
GLfloat sphere_vertices[1146];
GLfloat sphere_normal[1146];
GLfloat sphere_textures[764];
unsigned short sphere_elements[2280];
GLuint gNumVertices;
GLuint gNumElements;
SphereMaterial material[MATERIAL_ROWS][MATERIAL_COLUMNS];

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function declaration
	int initialize(void);
	void display(void); // Change 1
	void update(void);

	// Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false; // For game loop
	int iRet = 0;

	// Code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log flile cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created \n");
	}
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Change 11
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, // Extended style
		szAppName,
		TEXT("FFPOGL Native Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, // Change 14
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetFixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "Window Destroyed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Succeeded \n");
	}
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Play game here....
			if (gbActiveWindow == true)
			{
				// Call 'update' here
				update();
			}
			display(); // Change 2
		}
	}
	return((int)msg.wParam);
}

void update(void) {
	// variable Declaration
	GLfloat iOfset = 0.01f;
	// Code:
	angleOfXRotation -= iOfset;
	if (angleOfXRotation > 360.0f)
	{
		angleOfXRotation += 360.0f;
	}
	angleOfYRotation -= iOfset;
	if (angleOfYRotation > 360.0f)
	{
		angleOfYRotation += 360.0f;
	}
	angleOfZRotation -= iOfset;
	if (angleOfZRotation > 360.0f)
	{
		angleOfZRotation += 360.0f;
	}
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	// Function declaration
	void ToggelFullScreen(void);
	void resize(int, int);
	void uninitialize(void);

	// code
	switch (iMsg) {
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND: // Change 5
		return(0); // IMP

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'l':
		case 'L':
			if (isLKeyPressed == false)
			{
				gbLight = true;
				isLKeyPressed = true;
			}
			else
			{
				gbLight = false;
				isLKeyPressed = false;
			}
			break;
		case 'X':
		case 'x':
			keyPressed = 1;
			angleOfXRotation = 0.0f;
			break;

		case 'Y':
		case 'y':
			keyPressed = 2;
			angleOfYRotation = 0.0f;
			break;

		case 'Z':
		case 'z':
			keyPressed = 3;
			angleOfZRotation = 0.0f;
			break;

		default:
			break;
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'f':
		case 'F':
			ToggelFullScreen();
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		default:
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize(); // Change 27
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggelFullScreen(void) {
	// Variable Declaration:
	MONITORINFO mi;

	// Code:
	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(
					MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}

int initialize(void) {
	// function declaration
	GLvoid resize(GLint, GLint);
	GLvoid uninitialize(GLvoid);
	GLvoid InitializeMaterials(GLvoid);

	// Variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	GLint iPixelFormatIndex;

	// Shader Error Checking Variables
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	GLchar *szInfoLog = NULL;

	// Code
	// initialize pf structure
	// To make members of structure 0.
	memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // Change 6
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	gHdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(gHdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(gHdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(gHdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return(-4);
	}

	gResult = glewInit();
	if (gResult != GLEW_OK)
	{
		fprintf(gpFile, "glewInit failed \n");
		uninitialize();
		DestroyWindow(0);
	}

	// VERTEX SHADER
	// Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform int u_lKeyPressed;" \
		"out vec3 tNorm;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{" \
		"		vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"	\
		"		tNorm = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"		light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
		"		float tn_dot_ld = max(dot(tNorm, light_direction), 0.0);" \
		"		viewer_vector = vec3(-eye_coordinates.xyz);" \
		"	}" \
		"	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER
	// Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 tNorm;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_shininess;"	\
		"uniform int u_lKeyPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{" \
		"		vec3 ntNorm = normalize(tNorm);" \
		"		vec3 nlight_direction = normalize(light_direction);" \
		"		vec3 nviewer_vector  = normalize(viewer_vector);" \
		"		vec3 reflection_vector  = reflect(-nlight_direction, ntNorm);" \
		"		float tn_dot_ld = max(dot(ntNorm, nlight_direction), 0.0);" \
		"		vec3 ambient = u_la * u_ka;" \
		"		vec3 diffuse =u_ld * u_kd * tn_dot_ld;" \
		"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, nviewer_vector), 0.0), u_shininess);" \
		"		vec3 phong_ads_light = ambient + diffuse + specular;" \
		"		FragColor = vec4(phong_ads_light, 1.0);"	\
		"	}" \
		"	else" \
		"	{" \
		"		FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"	}" \
		"}";

	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
	modelUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");

	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");

	materialShinynessUniform = glGetUniformLocation(gShaderProgramObject, "u_shininess");

	getSphereVertexData(sphere_vertices, sphere_normal, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	InitializeMaterials();

	// Create vao_square
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(sphere_vertices),
		sphere_vertices,
		GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glBindVertexArray(0);

	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(sphere_normal),
		sphere_normal,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glGenBuffers(1, &vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		sizeof(sphere_elements),
		sphere_elements,
		GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glVertexAttrib3f(AMC_ATTRIBUTE_TEXCOORD0, 0.0f, 0.0f, 1.0f);
	glBindVertexArray(0);

	glClearDepth(1.0f);
	//glEnable(GL_CULL_FACE);
	//glDisable(GL_CULL_FACE);
	glEnable(GL_FRONT_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

	perspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void resize(int width, int height) {
	// Code:
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);


	gWidth = width / MATERIAL_ROWS;
	gHeight= height / MATERIAL_ROWS;
	gX = (width - (gWidth * MATERIAL_COLUMNS)) / 2;
	gY = (height - (gHeight * MATERIAL_ROWS)) / 2;
	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		1.0f,
		100.0f);
}

void display(void) {

	// Function Declaration

	// Code:
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	for (int i = 0; i < MATERIAL_COLUMNS; i++)
	{
		for (int j = 0; j < MATERIAL_ROWS; j++)
		{
			glUseProgram(gShaderProgramObject);
			glViewport(gX + (i * gWidth), gY + (j * gHeight), gWidth, gHeight);
			if (gbLight == true)
			{
				glUniform3fv(laUniform, 1, lightAmbiant);
				glUniform3fv(ldUniform, 1, lightDiffuse);
				glUniform3fv(lsUniform, 1, lightSpecular);
				glUniform4fv(lightPositionUniform, 1, lightPosition);

				glUniform3fv(kaUniform, 1, material[i][j].materialAmbiant);
				glUniform3fv(kdUniform, 1, material[i][j].materialDiffuse);
				glUniform3fv(ksUniform, 1, material[i][j].materialSpecular);
				glUniform1f(materialShinynessUniform, material[i][j].materialShinyness);

				if (keyPressed == 1)
				{
					lightPosition[0] = 0.0f;
					lightPosition[1] = sinf(angleOfXRotation) * 100.0f - 3.0f;
					lightPosition[2] = cosf(angleOfXRotation) * 100.0f - 3.0f;
				}

				if (keyPressed == 2)
				{
					lightPosition[0] = sinf(angleOfYRotation) * 100.0f - 3.0f;
					lightPosition[1] = 0.0f;
					lightPosition[2] = cosf(angleOfYRotation) * 100.0f - 3.0f;
				}

				if (keyPressed == 3)
				{
					lightPosition[0] = sin(angleOfZRotation) * 100.0f - 3.0f;
					lightPosition[1] = cosf(angleOfZRotation) * 100.0f - 3.0f;
					lightPosition[2] = 0.0f;
				}

				glUniform4fv(lightPositionUniform, 1, (GLfloat*)lightPosition);
				glUniform1i(lKeyPressedUniform, 1);
			}
			else
			{
				glUniform1i(lKeyPressedUniform, 0);
			}
			// SPHERE
			// Declaration of matrices
			mat4 modelMatrix;
			mat4 viewMatrix;
			mat4 modelViewProjectionMatrix;

			// Initialize above matrices to identity
			modelMatrix = mat4::identity();
			viewMatrix = mat4::identity();
			modelViewProjectionMatrix = mat4::identity();

			// Do necessary transformations
			modelMatrix = translate(0.0f,
				0.0f,
				-2.0f);

			// Do necessary Matrix Multiplication

			// This was internally donw by gluOrtho2d() in FFP

			// Send Necessary matrix to shader in respective uniform
			glUniformMatrix4fv(modelUniform,
				1,
				GL_FALSE,
				modelMatrix);

			glUniformMatrix4fv(viewUniform,
				1,
				GL_FALSE,
				viewMatrix);

			glUniformMatrix4fv(projectionUniform,
				1,
				GL_FALSE,
				perspectiveProjectionMatrix);

			// Bind with vao
			glBindVertexArray(vao_sphere);

			// Similarly bind With Textures If Any


			// Draw The Necessary Senn
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

			// Unbind vao
			glBindVertexArray(0);

			// Unuse Program
			glUseProgram(0);
		}
	}


	SwapBuffers(gHdc);
}

GLvoid InitializeMaterials(GLvoid) {
	// emerald
	material[0][0].materialAmbiant[0] = 0.0215f;
	material[0][0].materialAmbiant[1] = 0.1745f;
	material[0][0].materialAmbiant[2] = 0.0215f;
	material[0][0].materialAmbiant[3] = 1.0f;

	material[0][0].materialDiffuse[0] = 0.07568f;
	material[0][0].materialDiffuse[1] = 0.61424f;
	material[0][0].materialDiffuse[2] = 0.07568f;
	material[0][0].materialDiffuse[3] = 1.0f;

	material[0][0].materialSpecular[0] = 0.633f;
	material[0][0].materialSpecular[1] = 0.727811f;
	material[0][0].materialSpecular[2] = 0.633f;
	material[0][0].materialSpecular[3] = 1.0f;

	material[0][0].materialShinyness = 0.6 * 128;

	// jade
	material[0][1].materialAmbiant[0] = 0.135f;
	material[0][1].materialAmbiant[1] = 0.2225f;
	material[0][1].materialAmbiant[2] = 0.1575f;
	material[0][1].materialAmbiant[3] = 1.0f;

	material[0][1].materialDiffuse[0] = 0.54f;
	material[0][1].materialDiffuse[1] = 0.89f;
	material[0][1].materialDiffuse[2] = 0.63f;
	material[0][1].materialDiffuse[3] = 1.0f;

	material[0][1].materialSpecular[0] = 0.316228f;
	material[0][1].materialSpecular[1] = 0.316228f;
	material[0][1].materialSpecular[2] = 0.316228f;
	material[0][1].materialSpecular[3] = 1.0f;

	material[0][1].materialShinyness = 0.1 * 128;

	// obsidian
	material[0][2].materialAmbiant[0] = 0.05375f;
	material[0][2].materialAmbiant[1] = 0.05f;
	material[0][2].materialAmbiant[2] = 0.06625f;
	material[0][2].materialAmbiant[3] = 1.0f;

	material[0][2].materialDiffuse[0] = 0.18275f;
	material[0][2].materialDiffuse[1] = 0.17f;
	material[0][2].materialDiffuse[2] = 0.22525f;
	material[0][2].materialDiffuse[3] = 1.0f;

	material[0][2].materialSpecular[0] = 0.332741f;
	material[0][2].materialSpecular[1] = 0.328634f;
	material[0][2].materialSpecular[2] = 0.346435f;
	material[0][2].materialSpecular[3] = 1.0f;

	material[0][2].materialShinyness = 0.3 * 128;

	// pearl
	material[0][3].materialAmbiant[0] = 0.25f;
	material[0][3].materialAmbiant[1] = 0.20725f;
	material[0][3].materialAmbiant[2] = 0.20725f;
	material[0][3].materialAmbiant[3] = 1.0f;
	
	material[0][3].materialDiffuse[0] = 1.0f;
	material[0][3].materialDiffuse[1] = 0.829f;
	material[0][3].materialDiffuse[2] = 0.829f;
	material[0][3].materialDiffuse[3] = 1.0f;

	material[0][3].materialSpecular[0] = 0.296648f;
	material[0][3].materialSpecular[1] = 0.296648f;
	material[0][3].materialSpecular[2] = 0.296648f;
	material[0][3].materialSpecular[3] = 1.0f;

	material[0][3].materialShinyness = 0.088 * 128;
	
	// ruby
	material[1][0].materialAmbiant[0] = 0.1745f;
	material[1][0].materialAmbiant[1] = 0.01175f;
	material[1][0].materialAmbiant[2] = 0.01175f;
	material[1][0].materialAmbiant[3] = 1.0f;

	material[1][0].materialDiffuse[0] = 0.61424f;
	material[1][0].materialDiffuse[1] = 0.04136f;
	material[1][0].materialDiffuse[2] = 0.04136f;
	material[1][0].materialDiffuse[3] = 1.0f;

	material[1][0].materialSpecular[0] = 0.727811f;
	material[1][0].materialSpecular[1] = 0.626959f;
	material[1][0].materialSpecular[2] = 0.626959f;
	material[1][0].materialSpecular[3] = 1.0f;

	material[1][0].materialShinyness = 0.6 * 128;

	// turquoise
	material[1][1].materialAmbiant[0] = 0.1f;
	material[1][1].materialAmbiant[1] = 0.18725f;
	material[1][1].materialAmbiant[2] = 0.1745f;
	material[1][1].materialAmbiant[3] = 1.0f;

	material[1][1].materialDiffuse[0] = 0.396f;
	material[1][1].materialDiffuse[1] = 0.74151f;
	material[1][1].materialDiffuse[2] = 0.69102f;
	material[1][1].materialDiffuse[3] = 1.0f;

	material[1][1].materialSpecular[0] = 0.297254f;
	material[1][1].materialSpecular[1] = 0.30829f;
	material[1][1].materialSpecular[2] = 0.306678f;
	material[1][1].materialSpecular[3] = 1.0f;

	material[1][1].materialShinyness = 0.1 * 128;
//---------------------------------------------------------------

	// brass
	material[1][2].materialAmbiant[0] = 0.329412f;
	material[1][2].materialAmbiant[1] = 0.223529f;
	material[1][2].materialAmbiant[2] = 0.027451f;
	material[1][2].materialAmbiant[3] = 1.0f;

	material[1][2].materialDiffuse[0] = 0.780392f;
	material[1][2].materialDiffuse[1] = 0.568627f;
	material[1][2].materialDiffuse[2] = 0.113725f;
	material[1][2].materialDiffuse[3] = 1.0f;

	material[1][2].materialSpecular[0] = 0.992157f;
	material[1][2].materialSpecular[1] = 0.941176f;
	material[1][2].materialSpecular[2] = 0.807843f;
	material[1][2].materialSpecular[3] = 1.0f;

	material[1][2].materialShinyness = 0.21794872 * 128;

	// bronze
	material[1][3].materialAmbiant[0] = 0.2125f;
	material[1][3].materialAmbiant[1] = 0.1275f;
	material[1][3].materialAmbiant[2] = 0.054f;
	material[1][3].materialAmbiant[3] = 1.0f;

	material[1][3].materialDiffuse[0] = 0.714f;
	material[1][3].materialDiffuse[1] = 0.4284f;
	material[1][3].materialDiffuse[2] = 0.18144f;
	material[1][3].materialDiffuse[3] = 1.0f;

	material[1][3].materialSpecular[0] = 0.393548f;
	material[1][3].materialSpecular[1] = 0.271906f;
	material[1][3].materialSpecular[2] = 0.166721f;
	material[1][3].materialSpecular[3] = 1.0f;

	material[1][3].materialShinyness = 0.2 * 128;

	// chrome
	material[2][0].materialAmbiant[0] = 0.25f;
	material[2][0].materialAmbiant[1] = 0.25f;
	material[2][0].materialAmbiant[2] = 0.25f;
	material[2][0].materialAmbiant[3] = 1.0f;

	material[2][0].materialDiffuse[0] = 0.4f;
	material[2][0].materialDiffuse[1] = 0.4f;
	material[2][0].materialDiffuse[2] = 0.4f;
	material[2][0].materialDiffuse[3] = 1.0f;

	material[2][0].materialSpecular[0] = 0.774597f;
	material[2][0].materialSpecular[1] = 0.774597f;
	material[2][0].materialSpecular[2] = 0.774597f;
	material[2][0].materialSpecular[3] = 1.0f;

	material[2][0].materialShinyness = 0.6 * 128;

	// copper
	material[2][1].materialAmbiant[0] = 0.19125f;
	material[2][1].materialAmbiant[1] = 0.0735f;
	material[2][1].materialAmbiant[2] = 0.0225f;
	material[2][1].materialAmbiant[3] = 1.0f;

	material[2][1].materialDiffuse[0] = 0.7038f;
	material[2][1].materialDiffuse[1] = 0.27048f;
	material[2][1].materialDiffuse[2] = 0.0828f;
	material[2][1].materialDiffuse[3] = 1.0f;

	material[2][1].materialSpecular[0] = 0.256777f;
	material[2][1].materialSpecular[1] = 0.137622f;
	material[2][1].materialSpecular[2] = 0.086014f;
	material[2][1].materialSpecular[3] = 1.0f;

	material[2][1].materialShinyness = 0.1 * 128;

	// gold
	material[2][2].materialAmbiant[0] = 0.24725f;
	material[2][2].materialAmbiant[1] = 0.1995f;
	material[2][2].materialAmbiant[2] = 0.0745f;
	material[2][2].materialAmbiant[3] = 1.0f;

	material[2][2].materialDiffuse[0] = 0.75164f;
	material[2][2].materialDiffuse[1] = 0.60648f;
	material[2][2].materialDiffuse[2] = 0.22648f;
	material[2][2].materialDiffuse[3] = 1.0f;

	material[2][2].materialSpecular[0] = 0.628281f;
	material[2][2].materialSpecular[1] = 0.555802f;
	material[2][2].materialSpecular[2] = 0.366065f;
	material[2][2].materialSpecular[3] = 1.0f;

	material[2][2].materialShinyness = 0.4 * 128;

	// silver
	material[2][3].materialAmbiant[0] = 0.19225f;
	material[2][3].materialAmbiant[1] = 0.19225f;
	material[2][3].materialAmbiant[2] = 0.19225f;
	material[2][3].materialAmbiant[3] = 1.0f;

	material[2][3].materialDiffuse[0] = 0.50754f;
	material[2][3].materialDiffuse[1] = 0.50754f;
	material[2][3].materialDiffuse[2] = 0.50754f;
	material[2][3].materialDiffuse[3] = 1.0f;

	material[2][3].materialSpecular[0] = 0.508273f;
	material[2][3].materialSpecular[1] = 0.508273f;
	material[2][3].materialSpecular[2] = 0.508273f;
	material[2][3].materialSpecular[3] = 1.0f;

	material[2][3].materialShinyness = 0.4 * 128;

//---------------------------------------------------------------

	// black
	material[3][0].materialAmbiant[0] = 0.0f;
	material[3][0].materialAmbiant[1] = 0.0f;
	material[3][0].materialAmbiant[2] = 0.0f;
	material[3][0].materialAmbiant[3] = 1.0f;

	material[3][0].materialDiffuse[0] = 0.01f;
	material[3][0].materialDiffuse[1] = 0.01f;
	material[3][0].materialDiffuse[2] = 0.01f;
	material[3][0].materialDiffuse[3] = 1.0f;

	material[3][0].materialSpecular[0] = 0.50f;
	material[3][0].materialSpecular[1] = 0.50f;
	material[3][0].materialSpecular[2] = 0.50f;
	material[3][0].materialSpecular[3] = 1.0f;

	material[3][0].materialShinyness = 0.25 * 128;

	// cyan
	material[3][1].materialAmbiant[0] = 0.0f;
	material[3][1].materialAmbiant[1] = 0.1f;
	material[3][1].materialAmbiant[2] = 0.06f;
	material[3][1].materialAmbiant[3] = 1.0f;

	material[3][1].materialDiffuse[0] = 0.0f;
	material[3][1].materialDiffuse[1] = 0.50980392f;
	material[3][1].materialDiffuse[2] = 0.50980392f;
	material[3][1].materialDiffuse[3] = 1.0f;

	material[3][1].materialSpecular[0] = 0.50196078f;
	material[3][1].materialSpecular[1] = 0.50196078f;
	material[3][1].materialSpecular[2] = 0.50196078f;
	material[3][1].materialSpecular[3] = 1.0f;

	material[3][1].materialShinyness = 0.25 * 128;

	// green
	material[3][2].materialAmbiant[0] = 0.0f;
	material[3][2].materialAmbiant[1] = 0.0f;
	material[3][2].materialAmbiant[2] = 0.0f;
	material[3][2].materialAmbiant[3] = 1.0f;

	material[3][2].materialDiffuse[0] = 0.1f;
	material[3][2].materialDiffuse[1] = 0.35f;
	material[3][2].materialDiffuse[2] = 0.1f;
	material[3][2].materialDiffuse[3] = 1.0f;

	material[3][2].materialSpecular[0] = 0.45f;
	material[3][2].materialSpecular[1] = 0.55f;
	material[3][2].materialSpecular[2] = 0.45f;
	material[3][2].materialSpecular[3] = 1.0f;

	material[3][2].materialShinyness = 0.25 * 128;

	// red
	material[3][3].materialAmbiant[0] = 0.0f;
	material[3][3].materialAmbiant[1] = 0.0f;
	material[3][3].materialAmbiant[2] = 0.0f;
	material[3][3].materialAmbiant[3] = 1.0f;

	material[3][3].materialDiffuse[0] = 0.5f;
	material[3][3].materialDiffuse[1] = 0.0f;
	material[3][3].materialDiffuse[2] = 0.0f;
	material[3][3].materialDiffuse[3] = 1.0f;

	material[3][3].materialSpecular[0] = 0.7f;
	material[3][3].materialSpecular[1] = 0.6f;
	material[3][3].materialSpecular[2] = 0.6f;
	material[3][3].materialSpecular[3] = 1.0f;

	material[3][3].materialShinyness = 0.25 * 128;

	// white
	material[4][0].materialAmbiant[0] = 0.0f;
	material[4][0].materialAmbiant[1] = 0.0f;
	material[4][0].materialAmbiant[2] = 0.0f;
	material[4][0].materialAmbiant[3] = 1.0f;

	material[4][0].materialDiffuse[0] = 0.55f;
	material[4][0].materialDiffuse[1] = 0.55f;
	material[4][0].materialDiffuse[2] = 0.55f;
	material[4][0].materialDiffuse[3] = 1.0f;

	material[4][0].materialSpecular[0] = 0.70f;
	material[4][0].materialSpecular[1] = 0.70f;
	material[4][0].materialSpecular[2] = 0.70f;
	material[4][0].materialSpecular[3] = 1.0f;

	material[4][0].materialShinyness = 0.25 * 128;

	// yellow
	material[4][1].materialAmbiant[0] = 0.0f;
	material[4][1].materialAmbiant[1] = 0.0f;
	material[4][1].materialAmbiant[2] = 0.0f;
	material[4][1].materialAmbiant[3] = 1.0f;

	material[4][1].materialDiffuse[0] = 0.5f;
	material[4][1].materialDiffuse[1] = 0.5f;
	material[4][1].materialDiffuse[2] = 0.0f;
	material[4][1].materialDiffuse[3] = 1.0f;

	material[4][1].materialSpecular[0] = 0.60f;
	material[4][1].materialSpecular[1] = 0.60f;
	material[4][1].materialSpecular[2] = 0.50f;
	material[4][1].materialSpecular[3] = 1.0f;

	material[4][1].materialShinyness = 0.25 * 128;

//---------------------------------------------------------------

	// black
	material[4][2].materialAmbiant[0] = 0.02f;
	material[4][2].materialAmbiant[1] = 0.02f;
	material[4][2].materialAmbiant[2] = 0.02f;
	material[4][2].materialAmbiant[3] = 1.0f;

	material[4][2].materialDiffuse[0] = 0.01f;
	material[4][2].materialDiffuse[1] = 0.01f;
	material[4][2].materialDiffuse[2] = 0.01f;
	material[4][2].materialDiffuse[3] = 1.0f;

	material[4][2].materialSpecular[0] = 0.4f;
	material[4][2].materialSpecular[1] = 0.4f;
	material[4][2].materialSpecular[2] = 0.4f;
	material[4][2].materialSpecular[3] = 0.4f;

	material[4][2].materialShinyness = 0.078125 * 128;

	// cyan
	material[4][3].materialAmbiant[0] = 0.0f;
	material[4][3].materialAmbiant[1] = 0.05f;
	material[4][3].materialAmbiant[2] = 0.05f;
	material[4][3].materialAmbiant[3] = 1.0f;

	material[4][3].materialDiffuse[0] = 0.4f;
	material[4][3].materialDiffuse[1] = 0.5f;
	material[4][3].materialDiffuse[2] = 0.5f;
	material[4][3].materialDiffuse[3] = 1.0f;

	material[4][3].materialSpecular[0] = 0.4f;
	material[4][3].materialSpecular[1] = 0.7f;
	material[4][3].materialSpecular[2] = 0.7f;
	material[4][3].materialSpecular[3] = 1.0f;

	material[4][3].materialShinyness = 0.078125 * 128;

	// green
	material[5][0].materialAmbiant[0] = 0.0f;
	material[5][0].materialAmbiant[1] = 0.05f;
	material[5][0].materialAmbiant[2] = 0.0f;
	material[5][0].materialAmbiant[3] = 1.0f;

	material[5][0].materialDiffuse[0] = 0.4f;
	material[5][0].materialDiffuse[1] = 0.5f;
	material[5][0].materialDiffuse[2] = 0.4f;
	material[5][0].materialDiffuse[3] = 1.0f;

	material[5][0].materialSpecular[0] = 0.04f;
	material[5][0].materialSpecular[1] = 0.7f;
	material[5][0].materialSpecular[2] = 0.04f;
	material[5][0].materialSpecular[3] = 1.0f;

	material[5][0].materialShinyness = 0.078125 * 128;

	// red
	material[5][1].materialAmbiant[0] = 0.05f;
	material[5][1].materialAmbiant[1] = 0.0f;
	material[5][1].materialAmbiant[2] = 0.0f;
	material[5][1].materialAmbiant[3] = 1.0f;

	material[5][1].materialDiffuse[0] = 0.5f;
	material[5][1].materialDiffuse[1] = 0.4f;
	material[5][1].materialDiffuse[2] = 0.4f;
	material[5][1].materialDiffuse[3] = 1.0f;

	material[5][1].materialSpecular[0] = 0.7f;
	material[5][1].materialSpecular[1] = 0.04f;
	material[5][1].materialSpecular[2] = 0.04f;
	material[5][1].materialSpecular[3] = 1.0f;

	material[5][1].materialShinyness = 0.078125 * 128;

	// white
	material[5][2].materialAmbiant[0] = 0.05f;
	material[5][2].materialAmbiant[1] = 0.05f;
	material[5][2].materialAmbiant[2] = 0.05f;
	material[5][2].materialAmbiant[3] = 1.0f;

	material[5][2].materialDiffuse[0] = 0.5f;
	material[5][2].materialDiffuse[1] = 0.5f;
	material[5][2].materialDiffuse[2] = 0.5f;
	material[5][2].materialDiffuse[3] = 1.0f;

	material[5][2].materialSpecular[0] = 0.7f;
	material[5][2].materialSpecular[1] = 0.7f;
	material[5][2].materialSpecular[2] = 0.7f;
	material[5][2].materialSpecular[3] = 0.4f;

	material[5][2].materialShinyness = 0.078125 * 128;

	// yellow
	material[5][3].materialAmbiant[0] = 0.05f;
	material[5][3].materialAmbiant[1] = 0.05f;
	material[5][3].materialAmbiant[2] = 0.0f;
	material[5][3].materialAmbiant[3] = 1.0f;

	material[5][3].materialDiffuse[0] = 0.5f;
	material[5][3].materialDiffuse[1] = 0.5f;
	material[5][3].materialDiffuse[2] = 0.4f;
	material[5][3].materialDiffuse[3] = 1.0f;

	material[5][3].materialSpecular[0] = 0.7f;
	material[5][3].materialSpecular[1] = 0.7f;
	material[5][3].materialSpecular[2] = 0.04f;
	material[5][3].materialSpecular[3] = 1.0f;

	material[5][3].materialShinyness = 0.078125 * 128;
}

void uninitialize(void) {
	// Code:
	if (bFullScreen == true)
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}
	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}
	if (vbo_position_sphere)
	{
		glDeleteBuffers(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	if (vao_sphere)
	{
		glDeleteBuffers(1, &vao_sphere);
		vao_sphere = 0;
	}
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);
	// Break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
		gHdc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "File Closed Successfully \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

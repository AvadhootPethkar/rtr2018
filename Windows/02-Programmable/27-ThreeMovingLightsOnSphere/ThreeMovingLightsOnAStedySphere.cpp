// Headers
#include<Windows.h>
#include<stdio.h> // For file handeling
#include<gl/glew.h>
#include<gl/GL.h>
#include"vmath.h"
#include "Sphere.h"

#pragma comment(lib, "glew32.lib") 
#pragma comment(lib, "opengl32.lib") 
#pragma comment(lib, "Sphere.lib") 
#define WIN_WIDTH 800 
#define WIN_HEIGHT 600 

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Variable Declarations
HWND gHwnd = NULL;
bool bFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC gHdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;
GLenum gResult;

// Shader variables
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_sphere;
GLuint vbo_position_sphere;
GLuint vbo_normal_sphere;
GLuint vbo_element_sphere;

GLuint texture_smily;

GLuint modelUniform;
GLuint viewUniform;
GLuint projectionUniform;
GLuint lKeyPressedUniform;
GLuint laUniformRed;
GLuint ldUniformRed;
GLuint lsUniformRed;
GLuint lightPositionUniformRed;

GLuint laUniformGreen;
GLuint ldUniformGreen;
GLuint lsUniformGreen;
GLuint lightPositionUniformGreen;

GLuint laUniformBlue;
GLuint ldUniformBlue;
GLuint lsUniformBlue;
GLuint lightPositionUniformBlue;

GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint materialShinynessUniform;

mat4 perspectiveProjectionMatrix;

// Animating variables

// Light Variables
bool isLKeyPressed = false;
bool gbLight = false;
GLfloat lightAmbiantZero[] = { 0.0f,0.0f, 0.0f, 1.0f };
GLfloat lightDiffuseZero[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightSpecularZero[] = { 1.0f,0.0f, 0.0f, 1.0f };
GLfloat lightPositionZero[] = { -2.0f,0.0f,0.0f,1.0f };

GLfloat lightAmbiantOne[] = { 0.0f,0.0f, 0.0f, 1.0f };
GLfloat lightDiffuseOne[] = { 0.0f,1.0f,0.0f,1.0f };
GLfloat lightSpecularOne[] = { 0.0f,1.0f, 0.0f, 1.0f };
GLfloat lightPositionOne[] = { 0.0f,0.0f, 1.0f, 1.0f };

GLfloat lightAmbiantTwo[] = { 0.0f,0.0f, 0.0f, 1.0f };
GLfloat lightDiffuseTwo[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat lightSpecularTwo[] = { 0.0f,0.0f, 1.0f, 1.0f };
GLfloat lightPositionTwo[] = { 0.0f,0.0f, 1.0f, 1.0f };

GLfloat materialAmbiant[] = { 0.0f,0.0f, 0.0f, 1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f, 1.0f, 1.0f };
GLfloat materialShinyness = 128.0f;

// Animating variables
GLfloat lightAngleZero = 0.0f;
GLfloat lightAngleOne = 0.0f;
GLfloat lightAngleTwo = 0.0f;

// Sphere Variables
GLfloat sphere_vertices[1146];
GLfloat sphere_normal[1146];
GLfloat sphere_textures[764];
unsigned short sphere_elements[2280];
GLuint gNumVertices;
GLuint gNumElements;

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function declaration
	int initialize(void);
	void display(void); // Change 1
	void update(void);

	// Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false; // For game loop
	int iRet = 0;

	// Code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log flile cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created \n");
	}
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Change 11
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, // Extended style
		szAppName,
		TEXT("FFPOGL Native Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, // Change 14
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetFixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "Window Destroyed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Succeeded \n");
	}
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Play game here....
			if (gbActiveWindow == true)
			{
				// Call 'update' here
				update();
			}
			display(); // Change 2
		}
	}
	return((int)msg.wParam);
}

void update(void) {
	// Code:
	lightAngleZero -= 0.0005f;
	if (lightAngleZero > 360.0f)
	{
		lightAngleZero += 360.0f;
	}
	lightAngleOne -= 0.0005f;
	if (lightAngleOne > 360.0f)
	{
		lightAngleOne += 360.0f;
	}
	lightAngleTwo -= 0.0005f;
	if (lightAngleTwo > 360.0f)
	{
		lightAngleTwo += 360.0f;
	}
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	// Function declaration
	void ToggelFullScreen(void);
	void resize(int, int);
	void uninitialize(void);

	// code
	switch (iMsg) {
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND: // Change 5
		return(0); // IMP

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'l':
		case 'L':
			if (isLKeyPressed == false)
			{
				gbLight = true;
				isLKeyPressed = true;
			}
			else
			{
				gbLight = false;
				isLKeyPressed = false;
			}
			break;
			break;
		default:
			break;
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'f':
		case 'F':
			ToggelFullScreen();
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		default:
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize(); // Change 27
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggelFullScreen(void) {
	// Variable Declaration:
	MONITORINFO mi;

	// Code:
	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(
					MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}

int initialize(void) {
	// function declaration
	GLvoid resize(GLint, GLint);
	GLvoid uninitialize(GLvoid);

	// Variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	GLint iPixelFormatIndex;

	// Shader Error Checking Variables
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	GLchar *szInfoLog = NULL;

	// Code
	// initialize pf structure
	// To make members of structure 0.
	memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // Change 6
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	gHdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(gHdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(gHdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(gHdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return(-4);
	}

	gResult = glewInit();
	if (gResult != GLEW_OK)
	{
		fprintf(gpFile, "glewInit failed \n");
		uninitialize();
		DestroyWindow(0);
	}

	// VERTEX SHADER
	// Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_la_red;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_ls_red;" \
		"uniform vec4 u_light_position_red;" \
		"uniform vec3 u_la_green;" \
		"uniform vec3 u_ld_green;" \
		"uniform vec3 u_ls_green;" \
		"uniform vec4 u_light_position_green;" \
		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_ls_blue;" \
		"uniform vec4 u_light_position_blue;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_shininess;"	\
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{" \
		"		vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"	\
		"		vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal );" \
		"		vec3 viewer_vector = normalize(-eye_coordinates.xyz);" \

		"		vec3 light_direction_red = normalize(vec3(u_light_position_red) - eye_coordinates.xyz );" \
		"		vec3 reflection_vector_red = reflect(-light_direction_red, transformed_normals);" \
		"		float tn_dot_ld_red = max(dot(transformed_normals, light_direction_red), 0.0);" \
		"		vec3 ambient_red = u_la_red * u_ka;" \
		"		vec3 diffuse_red =u_ld_red * u_kd * tn_dot_ld_red;" \
		"		vec3 specular_red = u_ls_red * u_ks * pow(max(dot(reflection_vector_red, viewer_vector), 0.0), u_shininess);" \

		"		vec3 light_direction_green = normalize(vec3(u_light_position_green) - eye_coordinates.xyz );" \
		"		vec3 reflection_vector_green = reflect(-light_direction_green, transformed_normals);" \
		"		float tn_dot_ld_green = max(dot(transformed_normals, light_direction_green), 0.0);" \
		"		vec3 ambient_green = u_la_green * u_ka;" \
		"		vec3 diffuse_green =u_ld_green * u_kd * tn_dot_ld_green;" \
		"		vec3 specular_green = u_ls_green * u_ks * pow(max(dot(reflection_vector_green, viewer_vector), 0.0), u_shininess);" \

		"		vec3 light_direction_blue = normalize(vec3(u_light_position_blue) - eye_coordinates.xyz );" \
		"		vec3 reflection_vector_blue = reflect(-light_direction_blue, transformed_normals);" \
		"		float tn_dot_ld_blue = max(dot(transformed_normals, light_direction_blue), 0.0);" \
		"		vec3 ambient_blue = u_la_blue * u_ka;" \
		"		vec3 diffuse_blue =u_ld_blue * u_kd * tn_dot_ld_blue;" \
		"		vec3 specular_blue = u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue, viewer_vector), 0.0), u_shininess);" \

		"		phong_ads_light = ambient_red + diffuse_red + specular_red;" \
		"		phong_ads_light = phong_ads_light + ambient_green + diffuse_green + specular_green;" \
		"		phong_ads_light = phong_ads_light + ambient_blue + diffuse_blue + specular_blue;" \
		"	}" \
		"	else" \
		"	{" \
		"		phong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"	}" \
		"	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER
	// Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 phong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"	FragColor = vec4(phong_ads_light, 1.0);"	\
		"}";

	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
	modelUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");

	laUniformRed = glGetUniformLocation(gShaderProgramObject, "u_la_red");
	ldUniformRed = glGetUniformLocation(gShaderProgramObject, "u_ld_red");
	lsUniformRed = glGetUniformLocation(gShaderProgramObject, "u_ls_red");
	lightPositionUniformRed = glGetUniformLocation(gShaderProgramObject, "u_light_position_red");

	laUniformGreen = glGetUniformLocation(gShaderProgramObject, "u_la_green");
	ldUniformGreen = glGetUniformLocation(gShaderProgramObject, "u_ld_green");
	lsUniformGreen = glGetUniformLocation(gShaderProgramObject, "u_ls_green");
	lightPositionUniformGreen = glGetUniformLocation(gShaderProgramObject, "u_light_position_green");

	laUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_la_blue");
	ldUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_ld_blue");
	lsUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_ls_blue");
	lightPositionUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_light_position_blue");

	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");

	materialShinynessUniform = glGetUniformLocation(gShaderProgramObject, "u_shininess");

	getSphereVertexData(sphere_vertices, sphere_normal, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// Create vao_square
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);

	glBufferData(GL_ARRAY_BUFFER,
		sizeof(sphere_vertices),
		sphere_vertices,
		GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glBindVertexArray(0);

	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(sphere_normal),
		sphere_normal,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glGenBuffers(1, &vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		sizeof(sphere_elements),
		sphere_elements,
		GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glVertexAttrib3f(AMC_ATTRIBUTE_TEXCOORD0, 0.0f, 0.0f, 1.0f);
	glBindVertexArray(0);

	glClearDepth(1.0f);
	//glEnable(GL_CULL_FACE);
	//glDisable(GL_CULL_FACE);
	glEnable(GL_FRONT_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	perspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void resize(int width, int height) {
	// Code:
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display(void) {

	// Function Declaration

	// Code:
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);

	// SQUARE
	// Declaration of matrices
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 modelViewProjectionMatrix;

	// Initialize above matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelMatrix = translate(0.0f,
		0.0f,
		-2.0f);

	// Do necessary Matrix Multiplication

	// This was internally donw by gluOrtho2d() in FFP

	// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(modelUniform,
		1,
		GL_FALSE,
		modelMatrix);

	glUniformMatrix4fv(viewUniform,
		1,
		GL_FALSE,
		viewMatrix);

	glUniformMatrix4fv(projectionUniform,
		1,
		GL_FALSE,
		perspectiveProjectionMatrix);

	glUniform3fv(laUniformRed, 1, lightAmbiantZero);
	glUniform3fv(ldUniformRed, 1, lightDiffuseZero);
	glUniform3fv(lsUniformRed, 1, lightSpecularZero);
	float lightPositionZero[] = {
		0.0f, 100.0f*cos(lightAngleZero), 100.0f*sin(lightAngleZero), 1.0f
	};

	glUniform4fv(lightPositionUniformRed, 1, (GLfloat*)lightPositionZero);

	glUniform3fv(laUniformGreen, 1, lightAmbiantOne);
	glUniform3fv(ldUniformGreen, 1, lightDiffuseOne);
	glUniform3fv(lsUniformGreen, 1, lightSpecularOne);
	float lightPositionOne[] = {
		100.0f*cos(lightAngleOne), 0.0f, 100.0f*sin(lightAngleOne), 1.0f
	};

	glUniform4fv(lightPositionUniformGreen, 1, (GLfloat*)lightPositionOne);

	glUniform3fv(laUniformBlue, 1, lightAmbiantTwo);
	glUniform3fv(ldUniformBlue, 1, lightDiffuseTwo);
	glUniform3fv(lsUniformBlue, 1, lightSpecularTwo);
	float lightPositionTwo[] = {
		100.0f*cos(lightAngleTwo), 100.0f*sin(lightAngleTwo), 0.0f, 1.0f
	};

	glUniform4fv(lightPositionUniformBlue, 1, (GLfloat*)lightPositionTwo);

	glUniform3fv(kaUniform, 1, materialAmbiant);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShinynessUniform, materialShinyness);

	if (gbLight == true)
	{
		glUniform1i(lKeyPressedUniform, 1);
	}
	else
	{
		glUniform1i(lKeyPressedUniform, 0);
	}

	// Bind with vao
	glBindVertexArray(vao_sphere);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// Unbind vao
	glBindVertexArray(0);

	// Unuse Program
	glUseProgram(0);
	SwapBuffers(gHdc);
}

void uninitialize(void) {
	// Code:
	if (bFullScreen == true)
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}
	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}
	if (vbo_position_sphere)
	{
		glDeleteBuffers(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	if (vao_sphere)
	{
		glDeleteBuffers(1, &vao_sphere);
		vao_sphere = 0;
	}
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);
	// Break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
		gHdc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "File Closed Successfully \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

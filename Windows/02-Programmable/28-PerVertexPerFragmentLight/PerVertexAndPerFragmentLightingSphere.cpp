// Headers
#include<Windows.h>
#include<stdio.h> // For file handeling
#include<gl/glew.h>
#include<gl/GL.h>
#include"vmath.h"
#include "Sphere.h"

#pragma comment(lib, "glew32.lib") 
#pragma comment(lib, "opengl32.lib") 
#pragma comment(lib, "Sphere.lib") 
#define WIN_WIDTH 800 
#define WIN_HEIGHT 600 

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Variable Declarations
HWND gHwnd = NULL;
bool bFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC gHdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;
GLenum gResult;

GLuint vao_sphere;
GLuint vbo_position_sphere;
GLuint vbo_normal_sphere;
GLuint vbo_element_sphere;

// PerVertex Variables
GLuint modelUniformPerVertex;
GLuint viewUniformPerVertex;
GLuint projectionUniformPerVertex;

GLuint laUniformPerVertex;
GLuint ldUniformPerVertex;
GLuint lsUniformPerVertex;
GLuint lightPositionUniformPerVertex;

GLuint kaUniformPerVertex;
GLuint kdUniformPerVertex;
GLuint ksUniformPerVertex;
GLuint materialShinynessUniformPerVertex;
GLuint lKeyPressedUniformPerVertex;

// PerVertex Shader variables
GLuint gVertexShaderObjectPerVertex;
GLuint gFragmentShaderObjectPerVertex;
GLuint gShaderProgramObjectPerVertex;

// PerFragment Variables
GLuint modelUniformPerFragment;
GLuint viewUniformPerFragment;
GLuint projectionUniformPerFragment;

GLuint laUniformPerFragment;
GLuint ldUniformPerFragment;
GLuint lsUniformPerFragment;
GLuint lightPositionUniformPerFragment;

GLuint kaUniformPerFragment;
GLuint kdUniformPerFragment;
GLuint ksUniformPerFragment;
GLuint materialShinynessUniformPerFragment;
GLuint lKeyPressedUniformPerFragment;

// PerFragment Shader variables
GLuint gVertexShaderObjectPerFragment;
GLuint gFragmentShaderObjectPerFragment;
GLuint gShaderProgramObjectPerFragment;
bool gbFragmentShaderToggel = false;

mat4 perspectiveProjectionMatrix;

// Animating variables

// Light Variables
bool isLKeyPressed = false;
bool gbLight = false;
GLfloat lightAmbiant[] = { 0.0f,0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat materialAmbiant[] = { 0.0f,0.0f, 0.0f, 1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f, 1.0f, 1.0f };
GLfloat materialShinyness = 128.0f;

// Sphere Variables
GLfloat sphere_vertices[1146];
GLfloat sphere_normal[1146];
GLfloat sphere_textures[764];
unsigned short sphere_elements[2280];
GLuint gNumVertices;
GLuint gNumElements;

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function declaration
	int initialize(void);
	void display(void); // Change 1
	void update(void);

	// Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false; // For game loop
	int iRet = 0;

	// Code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log flile cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created \n");
	}
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Change 11
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, // Extended style
		szAppName,
		TEXT("FFPOGL Native Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, // Change 14
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetFixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "Window Destroyed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Succeeded \n");
	}
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Play game here....
			if (gbActiveWindow == true)
			{
				// Call 'update' here
				update();
			}
			display(); // Change 2
		}
	}
	return((int)msg.wParam);
}

void update(void) {
	// Code:
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	// Function declaration
	void ToggelFullScreen(void);
	void resize(int, int);
	void uninitialize(void);

	// code
	switch (iMsg) {
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND: // Change 5
		return(0); // IMP

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'l':
		case 'L':
			if (isLKeyPressed == false)
			{
				gbLight = true;
				isLKeyPressed = true;
			}
			else
			{
				gbLight = false;
				isLKeyPressed = false;
			}
			break;

		case 'e':
		case 'E':
			DestroyWindow(hwnd);
			break;

		case 'f':
		case 'F':
			gbFragmentShaderToggel = true;
			break;

		case 'v':
		case 'V':
			gbFragmentShaderToggel = false;
			break;

		default:
			break;
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			ToggelFullScreen();
			break;
		default:
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize(); // Change 27
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggelFullScreen(void) {
	// Variable Declaration:
	MONITORINFO mi;

	// Code:
	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(
					MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}

int initialize(void) {
	// function declaration
	GLvoid resize(GLint, GLint);
	GLvoid uninitialize(GLvoid);

	// Variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	GLint iPixelFormatIndex;

	// Shader Error Checking Variables
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	GLchar *szInfoLog = NULL;

	// Code
	// initialize pf structure
	// To make members of structure 0.
	memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // Change 6
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	gHdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(gHdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(gHdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(gHdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return(-4);
	}

	gResult = glewInit();
	if (gResult != GLEW_OK)
	{
		fprintf(gpFile, "glewInit failed \n");
		uninitialize();
		DestroyWindow(0);
	}

	// VERTEX SHADER PerVertex
	// Define Vertex Shader Object
	gVertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCodePerVertex =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_shininess;"	\
		"uniform vec4 u_light_position;" \
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{" \
		"		vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"	\
		"		vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal );" \
		"		vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz );" \
		"		float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" \
		"		vec3 ambient = u_la * u_ka;" \
		"		vec3 diffuse =u_ld * u_kd * tn_dot_ld;" \
		"		vec3 reflection_vector = reflect(-light_direction, transformed_normals);" \
		"		vec3 viewer_vector = normalize(-eye_coordinates.xyz);" \
		"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_shininess);" \
		"		phong_ads_light = ambient + diffuse + specular;" \
		"	}" \
		"	else" \
		"	{" \
		"		phong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"	}" \
		"	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObjectPerVertex, 1, (const GLchar **)&vertexShaderSourceCodePerVertex, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObjectPerVertex);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerVertex, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER PerVertex
	// Define Fragment Shader Object
	gFragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCodePerVertex =
		"#version 450 core" \
		"\n" \
		"in vec3 phong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"	FragColor = vec4(phong_ads_light, 1.0);"	\
		"}";

	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObjectPerVertex, 1, (const GLchar **)&fragmentShaderSourceCodePerVertex, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObjectPerVertex);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerVertex, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObjectPerVertex = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObjectPerVertex, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObjectPerVertex, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObjectPerVertex);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObjectPerVertex, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
	modelUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_model_matrix");
	viewUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_view_matrix");
	projectionUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_projection_matrix");

	lKeyPressedUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_lKeyPressed");
	lightPositionUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position");

	laUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_la");
	ldUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ld");
	lsUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ls");

	kaUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ka");
	kdUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_kd");
	ksUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ks");

	materialShinynessUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_shininess");

	// VERTEX SHADER PerFragment
	// Define Vertex Shader Object
	gVertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCodePerFragment =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform int u_lKeyPressed;" \
		"out vec3 tNorm;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{" \
		"		vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"	\
		"		tNorm = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"		light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
		"		float tn_dot_ld = max(dot(tNorm, light_direction), 0.0);" \
		"		viewer_vector = vec3(-eye_coordinates.xyz);" \
		"	}" \
		"	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObjectPerFragment, 1, (const GLchar **)&vertexShaderSourceCodePerFragment, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObjectPerFragment);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerFragment, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER PerFragment
	// Define Fragment Shader Object
	gFragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCodePerFragment =
		"#version 450 core" \
		"\n" \
		"in vec3 tNorm;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_shininess;"	\
		"uniform int u_lKeyPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{" \
		"		vec3 ntNorm = normalize(tNorm);" \
		"		vec3 nlight_direction = normalize(light_direction);" \
		"		vec3 nviewer_vector  = normalize(viewer_vector);" \
		"		vec3 reflection_vector  = reflect(-nlight_direction, ntNorm);" \
		"		float tn_dot_ld = max(dot(ntNorm, nlight_direction), 0.0);" \
		"		vec3 ambient = u_la * u_ka;" \
		"		vec3 diffuse =u_ld * u_kd * tn_dot_ld;" \
		"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, nviewer_vector), 0.0), u_shininess);" \
		"		vec3 phong_ads_light = ambient + diffuse + specular;" \
		"		FragColor = vec4(phong_ads_light, 1.0);"	\
		"	}" \
		"	else" \
		"	{" \
		"		FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"	}" \
		"}";

	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObjectPerFragment, 1, (const GLchar **)&fragmentShaderSourceCodePerFragment, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObjectPerFragment);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerFragment, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObjectPerFragment = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObjectPerFragment, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObjectPerFragment, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObjectPerFragment);

	// Error checking
	iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObjectPerFragment, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerFragment, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
	modelUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_model_matrix");
	viewUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_view_matrix");
	projectionUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_projection_matrix");

	lKeyPressedUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lKeyPressed");
	lightPositionUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position");

	laUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_la");
	ldUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ld");
	lsUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ls");

	kaUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ka");
	kdUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_kd");
	ksUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ks");

	materialShinynessUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_shininess");

	getSphereVertexData(sphere_vertices, sphere_normal, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// Create vao_square
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);

	glBufferData(GL_ARRAY_BUFFER,
		sizeof(sphere_vertices),
		sphere_vertices,
		GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glBindVertexArray(0);

	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(sphere_normal),
		sphere_normal,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glGenBuffers(1, &vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		sizeof(sphere_elements),
		sphere_elements,
		GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glVertexAttrib3f(AMC_ATTRIBUTE_TEXCOORD0, 0.0f, 0.0f, 1.0f);
	glBindVertexArray(0);

	glClearDepth(1.0f);
	//glEnable(GL_CULL_FACE);
	//glDisable(GL_CULL_FACE);
	glEnable(GL_FRONT_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	perspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void resize(int width, int height) {
	// Code:
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display(void) {

	// Function Declaration

	// Code:
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	if (gbFragmentShaderToggel)
	{
		glUseProgram(gShaderProgramObjectPerFragment);
	}
	else
	{
		glUseProgram(gShaderProgramObjectPerVertex);
	}

	// SQUARE
	// Declaration of matrices
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 modelViewProjectionMatrix;

	// Initialize above matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelMatrix = translate(0.0f,
		0.0f,
		-2.0f);

	// Do necessary Matrix Multiplication

	// This was internally donw by gluOrtho2d() in FFP

	// Send Necessary matrix to shader in respective uniform
	if (gbFragmentShaderToggel)
	{
		glUniformMatrix4fv(modelUniformPerFragment,
			1,
			GL_FALSE,
			modelMatrix);

		glUniformMatrix4fv(viewUniformPerFragment,
			1,
			GL_FALSE,
			viewMatrix);

		glUniformMatrix4fv(projectionUniformPerFragment,
			1,
			GL_FALSE,
			perspectiveProjectionMatrix);

		glUniform3fv(laUniformPerFragment, 1, lightAmbiant);
		glUniform3fv(ldUniformPerFragment, 1, lightDiffuse);
		glUniform3fv(lsUniformPerFragment, 1, lightSpecular);
		glUniform4fv(lightPositionUniformPerFragment, 1, lightPosition);

		glUniform3fv(kaUniformPerFragment, 1, materialAmbiant);
		glUniform3fv(kdUniformPerFragment, 1, materialDiffuse);
		glUniform3fv(ksUniformPerFragment, 1, materialSpecular);
		glUniform1f(materialShinynessUniformPerFragment, materialShinyness);

		float lightPosition[] = {
			0.0f, 0.0f, 2.0f, 1.0f
		};

		glUniform4fv(lightPositionUniformPerFragment, 1, (GLfloat*)lightPosition);

		if (gbLight == true)
		{
			glUniform1i(lKeyPressedUniformPerFragment, 1);
		}
		else
		{
			glUniform1i(lKeyPressedUniformPerFragment, 0);
		}
	}
	else
	{
		glUniformMatrix4fv(modelUniformPerVertex,
			1,
			GL_FALSE,
			modelMatrix);

		glUniformMatrix4fv(viewUniformPerVertex,
			1,
			GL_FALSE,
			viewMatrix);

		glUniformMatrix4fv(projectionUniformPerVertex,
			1,
			GL_FALSE,
			perspectiveProjectionMatrix);

		glUniform3fv(laUniformPerVertex, 1, lightAmbiant);
		glUniform3fv(ldUniformPerVertex, 1, lightDiffuse);
		glUniform3fv(lsUniformPerVertex, 1, lightSpecular);
		glUniform4fv(lightPositionUniformPerVertex, 1, lightPosition);

		glUniform3fv(kaUniformPerVertex, 1, materialAmbiant);
		glUniform3fv(kdUniformPerVertex, 1, materialDiffuse);
		glUniform3fv(ksUniformPerVertex, 1, materialSpecular);
		glUniform1f(materialShinynessUniformPerVertex, materialShinyness);

		float lightPosition[] = {
			0.0f, 0.0f, 2.0f, 1.0f
		};

		glUniform4fv(lightPositionUniformPerVertex, 1, (GLfloat*)lightPosition);

		if (gbLight == true)
		{
			glUniform1i(lKeyPressedUniformPerVertex, 1);
		}
		else
		{
			glUniform1i(lKeyPressedUniformPerVertex, 0);
		}
	}

	// Bind with vao
	glBindVertexArray(vao_sphere);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// Unbind vao
	glBindVertexArray(0);

	// Unuse Program
	glUseProgram(0);
	SwapBuffers(gHdc);
}

void uninitialize(void) {
	// Code:
	if (bFullScreen == true)
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}
	if (vbo_normal_sphere)
	{
		glDeleteBuffers(1, &vbo_normal_sphere);
		vbo_normal_sphere = 0;
	}
	if (vbo_position_sphere)
	{
		glDeleteBuffers(1, &vbo_position_sphere);
		vbo_position_sphere = 0;
	}
	if (vao_sphere)
	{
		glDeleteBuffers(1, &vao_sphere);
		vao_sphere = 0;
	}
	glUseProgram(gShaderProgramObjectPerVertex);
	glDetachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);
	gFragmentShaderObjectPerVertex = 0;
	glDetachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
	gVertexShaderObjectPerVertex = 0;
	glDeleteProgram(gShaderProgramObjectPerVertex);
	gShaderProgramObjectPerVertex = 0;
	glUseProgram(0);
	// Break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
		gHdc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "File Closed Successfully \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

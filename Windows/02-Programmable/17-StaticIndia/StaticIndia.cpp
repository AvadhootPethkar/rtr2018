// Headers
#include<Windows.h>
#include<stdio.h> // For file handeling
#include<gl/glew.h>
#include<gl/GL.h>
#include"vmath.h"

#pragma comment(lib, "glew32.lib") 
#pragma comment(lib, "opengl32.lib") 
#define WIN_WIDTH 800 
#define WIN_HEIGHT 600 

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Variable Declarations
HWND gHwnd = NULL;
bool bFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC gHdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;
GLenum gResult;

// Shader variables
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vaoI;
GLuint vboI;
GLuint vaoNCross;
GLuint vboNCross;
GLuint vaoD;
GLuint vboD;
GLuint vaoDUp;
GLuint vboDUp;
GLuint vaoDDown;
GLuint vboDDown;
GLuint vaoALeft;
GLuint vboALeft;
GLuint vaoACenter;
GLuint vboACenter;
GLuint vboColor;
GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;

//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function declaration
	int initialize(void);
	void display(void); // Change 1
	void update(void);

	// Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false; // For game loop
	int iRet = 0;

	// Code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log flile cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created \n");
	}
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Change 11
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, // Extended style
		szAppName,
		TEXT("FFPOGL Native Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, // Change 14
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetFixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "Window Destroyed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Succeeded \n");
	}
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Play game here....
			if (gbActiveWindow == true)
			{
				// Call 'update' here
				update();
			}
			display(); // Change 2
		}
	}
	return((int)msg.wParam);
}

void update(void) {
	// Code:
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	// Function declaration
	void ToggelFullScreen(void);
	void resize(int, int);
	void uninitialize(void);

	// code
	switch (iMsg) {
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND: // Change 5
		return(0); // IMP

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'f':
		case 'F':
			ToggelFullScreen();
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		default:
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize(); // Change 27
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggelFullScreen(void) {
	// Variable Declaration:
	MONITORINFO mi;

	// Code:
	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(
					MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}

int initialize(void) {
	// function declaration
	GLvoid resize(GLint, GLint);
	GLvoid uninitialize(GLvoid);

	// Variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	GLint iPixelFormatIndex;

	// Shader Error Checking Variables
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	GLchar *szInfoLog = NULL;

	// Code
	// initialize pf structure
	// To make members of structure 0.
	memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // Change 6
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	gHdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(gHdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(gHdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(gHdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return(-4);
	}

	gResult = glewInit();
	if (gResult != GLEW_OK)
	{
		fprintf(gpFile, "glewInit failed \n");
		uninitialize();
		DestroyWindow(0);
	}

	// VERTEX SHADER
	// Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}";
	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER
	// Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = out_color;" \
		"}";
	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	const GLfloat fIVertices[] =
	{
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
	};

	const GLfloat fNCrossVertices[] =
	{
		-1.0f, 1.0f, 0.0f,
		-0.5f, -1.0f,0.0f,
	};

	const GLfloat fACenterVertices[] =
	{
		-1.0f, 1.0f, 0.0f,
		-0.5f, 1.0f,0.0f,
	};
	const GLfloat fALeftVertices[] =
	{
		1.0f, 1.0f, 0.0f,
		0.7f, -1.0f, 0.0f,
	};
	const GLfloat triColor[] =
	{
		1.0f, 0.6f, 0.2f,
		0.07f, 0.53f, 0.02f
	};
	const GLfloat fOrangeColor[] =
	{
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f
	};
	const GLfloat fGreenColor[] =
	{
		0.07f, 0.53f, 0.02f,
		0.07f, 0.53f, 0.02f
	};
	const GLfloat fWhiteColor[] =
	{
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f
	};

	// Create vaoI
	glGenVertexArrays(1, &vaoI);
	glBindVertexArray(vaoI);
	glGenBuffers(1, &vboI);

	//IVerticesVBO
	glBindBuffer(GL_ARRAY_BUFFER, vboI);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fIVertices),
		fIVertices,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//IColorVBO
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(triColor),
		triColor,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Create vaoNCross
	glGenVertexArrays(1, &vaoNCross);
	//NCrossVertices
	glBindVertexArray(vaoNCross);
	glGenBuffers(1, &vboNCross);
	glBindBuffer(GL_ARRAY_BUFFER, vboNCross);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fNCrossVertices),
		fNCrossVertices,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//IColorVBO
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(triColor),
		triColor,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Create vaoALeft
	glGenVertexArrays(1, &vaoALeft);
	//ALeftVertices
	glBindVertexArray(vaoALeft);
	glGenBuffers(1, &vboALeft);
	glBindBuffer(GL_ARRAY_BUFFER, vboALeft);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fALeftVertices),
		fALeftVertices,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//IColorVBO
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(triColor),
		triColor,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Create vaoDUp
	glGenVertexArrays(1, &vaoDUp);
	// ACenterVertices
	glBindVertexArray(vaoDUp);
	//vboDUpVertices
	glGenBuffers(1, &vboDUp);
	glBindBuffer(GL_ARRAY_BUFFER, vboDUp);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fACenterVertices),
		fACenterVertices,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//vboColor
	glGenBuffers(1, &vboDUp);
	glBindBuffer(GL_ARRAY_BUFFER, vboDUp);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fOrangeColor),
		fOrangeColor,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//vaoDDown
	glGenVertexArrays(1, &vaoDDown);
	// ACenterVertices
	glBindVertexArray(vaoDDown);
	//vboDUpVertices
	glGenBuffers(1, &vboDDown);
	glBindBuffer(GL_ARRAY_BUFFER, vboDDown);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fACenterVertices),
		fACenterVertices,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//vboColor
	glGenBuffers(1, &vboDDown);
	glBindBuffer(GL_ARRAY_BUFFER, vboDDown);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fGreenColor),
		fGreenColor,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Create vaoACenter
	glGenVertexArrays(1, &vaoACenter);
	// ACenterVertices
	glBindVertexArray(vaoACenter);
	glGenBuffers(1, &vboACenter);
	glBindBuffer(GL_ARRAY_BUFFER, vboACenter);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fACenterVertices),
		fACenterVertices,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//ACenterColorVBO
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fWhiteColor),
		fWhiteColor,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	perspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void resize(int width, int height) {
	// Code:
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display(void) {

	// Function Declaration

	// Code:
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
	// Declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	// I
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(-0.4f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoI);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// NLeft
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(-0.06f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoI);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// NCross
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(-0.06f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoNCross);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// NRight
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(0.44f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoI);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// DLeft
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(0.88f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoI);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// DUp
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(0.88f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoDUp);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// DDown
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(0.88f,
		-2.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoDDown);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// DRight
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(1.38f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoI);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// ISecond
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(1.76f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoI);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// ALeft
	// Initialize above matrices to identity
	//modelViewMatrix = mat4::identity();
	//modelViewProjectionMatrix = mat4::identity();

	//// Do necessary transformations
	//modelViewMatrix = translate(0.4f,
	//	0.0f,
	//	-3.0f);

	//// Do necessary Matrix Multiplication
	//modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	//// This was internally donw by gluOrtho2d() in FFP

	//// Send Necessary matrix to shader in respective uniform
	//glUniformMatrix4fv(mvpUniform,
	//	1,
	//	GL_FALSE,
	//	modelViewProjectionMatrix);

	//// Bind with vao
	//glBindVertexArray(vaoALeft);

	//// Similarly bind With Textures If Any


	//// Draw The Necessary Senn
	//glDrawArrays(GL_LINES,
	//	0,
	//	3);

	//// Unbind vao
	//glBindVertexArray(0);

	// ACenterOrange
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(2.63f,
		-0.988f,
		-3.9f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoDUp);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// ACenterWhite
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(2.63f,
		-1.0f,
		-3.9f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoACenter);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// ACenterGreen
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(2.63f,
		-1.01f,
		-3.9f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoDDown);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// ARight
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(2.4f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoNCross);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// Unuse Program
	glUseProgram(0);
	SwapBuffers(gHdc);
}

void uninitialize(void) {
	// Code:
	if (bFullScreen == true)
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}
	if (vboI)
	{
		glDeleteBuffers(1, &vboI);
		vboI = 0;
	}
	if (vaoI)
	{
		glDeleteBuffers(1, &vaoI);
		vaoI = 0;
	}
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);
	// Break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
		gHdc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "File Closed Successfully \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

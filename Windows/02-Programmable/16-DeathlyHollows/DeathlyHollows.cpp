// Headers
#include<Windows.h>
#include<stdio.h> // For file handeling
#include<gl/glew.h>
#include<gl/GL.h>
#include"vmath.h"

#pragma comment(lib, "glew32.lib") 
#pragma comment(lib, "opengl32.lib") 
#define WIN_WIDTH 800 
#define WIN_HEIGHT 600 

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Variable Declarations
HWND gHwnd = NULL;
bool bFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HDC gHdc = NULL;
HGLRC ghrc = NULL;
bool gbActiveWindow = false;
FILE *gpFile = NULL;
GLenum gResult;

// Shader variables
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;

// Circle Variables
GLuint vaoCircle;
GLuint vboCirclePosition;
GLuint vboCircleColor;
GLfloat fRadiusOfInCircle = 0.0f;
GLfloat fOffsetX = 0.0f;
GLfloat fOffsetY = 0.0f;
static int iCirclePoints = 0;
const GLint iPoints = 10000;

// Triangle Variables
GLuint vaoTriangle;
GLuint vboTrianglePosition;
GLuint vboTriangleColor;
const GLint iTrianglePoints = 3;

// Line Variables
GLuint vaoLine;
GLuint vboLinePosition;
GLuint vboLineColor;
const GLint iLinePoints = 2;

// Animating Variables
GLfloat gfTriangleX = 1.0f;
GLfloat gfTriangleY = -1.0f;
GLfloat gfCircleX = -1.0f;
GLfloat gfCircleY = -1.0f;
GLfloat gfAngle = 0.0f;
GLfloat gfLineY = 1.0f;


//WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow) {
	// Function declaration
	int initialize(void);
	void display(void); // Change 1
	void update(void);

	// Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false; // For game loop
	int iRet = 0;

	// Code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log flile cannot be created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Successfully Created \n");
	}
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC; // Change 11
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW, // Extended style
		szAppName,
		TEXT("FFPOGL Native Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, // Change 14
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	gHwnd = hwnd;
	iRet = initialize();
	if (iRet == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf(gpFile, "SetFixelFormat() Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf(gpFile, "wglCreateContext Failed \n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf(gpFile, "Window Destroyed \n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf(gpFile, "Initialization Succeeded \n");
	}
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// Play game here....
			if (gbActiveWindow == true)
			{
				// Call 'update' here
				update();
			}
			display(); // Change 2
		}
	}
	return((int)msg.wParam);
}

void update(void) {
	// Code:
	if (gfTriangleX >= 0.0f)
	{
		gfTriangleX -= 0.001f;
		if (gfAngle <= 360.0f)
		{
			gfAngle += 0.1f;
		}
		else {
			gfAngle = 0.0f;
		}
	}
	else {
		gfAngle = 0.0f;
	}
	if (gfTriangleY <= 0.0f)
	{
		gfTriangleY += 0.001f;
		if (gfAngle <= 360.0f)
		{
			gfAngle += 0.1f;
		}
		else {
			gfAngle = 0.0f;
		}
	}
	if (gfCircleX <= 0.0f)
	{
		gfCircleX += 0.001f;
	}
	if (gfCircleY <= 0.0f)
	{
		gfCircleY += 0.001f;
	}
	if (gfLineY >= 0.0f)
	{
		gfLineY -= 0.001f;
	}
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {
	// Function declaration
	void ToggelFullScreen(void);
	void resize(int, int);
	void uninitialize(void);

	// code
	switch (iMsg) {
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND: // Change 5
		return(0); // IMP

	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case 'f':
		case 'F':
			ToggelFullScreen();
			break;
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		default:
			break;
		}
		break;

	case WM_DESTROY:
		uninitialize(); // Change 27
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggelFullScreen(void) {
	// Variable Declaration:
	MONITORINFO mi;

	// Code:
	if (bFullScreen == false)
	{
		dwStyle = GetWindowLong(gHwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(gHwnd, &wpPrev) &&
				GetMonitorInfo(
					MonitorFromWindow(gHwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(gHwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(gHwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		bFullScreen = true;
	}
	else
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
		bFullScreen = false;
	}
}

int initialize(void) {
	// function declaration
	GLvoid resize(GLint, GLint);
	GLvoid uninitialize(GLvoid);
	GLfloat getDistance(GLfloat x1, GLfloat x2, GLfloat y1, GLfloat y2);
	GLfloat getOffset(GLfloat coA, GLfloat coB, GLfloat coC, GLfloat distAB, GLfloat distBC, GLfloat distAC, GLfloat perimeter);

	// Variable declaration
	PIXELFORMATDESCRIPTOR pfd;
	GLint iPixelFormatIndex;

	// Dethly Hollows Variables
	GLfloat coX1 = 0.0f;
	GLfloat coY1 = 0.5f;
	GLfloat coX2 = -0.5f;
	GLfloat coY2 = -0.5f;
	GLfloat coX3 = 0.5f;
	GLfloat coY3 = -0.5f;
	GLfloat fdistAB = 0.0f;
	GLfloat fdistBC = 0.0f;
	GLfloat fdistAC = 0.0f;
	GLfloat fPerimeter = 0.0f;
	GLfloat fSemiPerimeter = 0.0f;
	GLfloat fAreaOfTriangle = 0.0f;
	GLfloat fTriangleHeight = 0.0f;

	// Shader Error Checking Variables
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	GLchar *szInfoLog = NULL;

	// Code
	// initialize pf structure
	// To make members of structure 0.
	memset((void *)&pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // Change 6
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	gHdc = GetDC(gHwnd);
	iPixelFormatIndex = ChoosePixelFormat(gHdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}
	if (SetPixelFormat(gHdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		return(-2);
	}

	ghrc = wglCreateContext(gHdc);
	if (ghrc == NULL)
	{
		return(-3);
	}
	if (wglMakeCurrent(gHdc, ghrc) == FALSE)
	{
		return(-4);
	}

	gResult = glewInit();
	if (gResult != GLEW_OK)
	{
		fprintf(gpFile, "glewInit failed \n");
		uninitialize();
		DestroyWindow(0);
	}

	// VERTEX SHADER
	// Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}";
	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER
	// Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = out_color;" \
		"}";
	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				DestroyWindow(0);
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	// Distance Calculatin between triangle lines
	fdistAB = getDistance(coX1, coX2, coY1, coY2);
	fdistBC = getDistance(coX2, coX3, coY2, coY3);
	fdistAC = getDistance(coX1, coX3, coY1, coY3);

	fPerimeter = fdistAB + fdistBC + fdistAC;
	fSemiPerimeter = fPerimeter / 2;

	fRadiusOfInCircle = sqrtf((fSemiPerimeter - fdistAB) * (fSemiPerimeter - fdistBC) * (fSemiPerimeter - fdistAC) / fSemiPerimeter);
	fOffsetX = getOffset(coX1, coX2, coX3, fdistAB, fdistBC, fdistAC, fPerimeter);
	fOffsetY = getOffset(coY1, coY2, coY3, fdistAB, fdistBC, fdistAC, fPerimeter);

	// Circle
	// Create vao
	glGenVertexArrays(1, &vaoCircle);
	glBindVertexArray(vaoCircle);
	glGenBuffers(1, &vboCirclePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboCirclePosition);
	glBufferData(GL_ARRAY_BUFFER,
		6 * sizeof(GLfloat),
		NULL,
		GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Color
	glGenBuffers(1, &vboCircleColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboCircleColor);
	glBufferData(GL_ARRAY_BUFFER,
		6 * sizeof(GLfloat),
		NULL,
		GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	// Triangle
	// Create vao
	glGenVertexArrays(1, &vaoTriangle);
	glBindVertexArray(vaoTriangle);
	glGenBuffers(1, &vboTrianglePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboTrianglePosition);
	glBufferData(GL_ARRAY_BUFFER,
		4 * 2 * sizeof(GLfloat),
		NULL,
		GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Color
	glGenBuffers(1, &vboTriangleColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboTriangleColor);
	glBufferData(GL_ARRAY_BUFFER,
		4 * 2 * sizeof(GLfloat),
		NULL,
		GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	// Line
	// Create vao
	glGenVertexArrays(1, &vaoLine);
	glBindVertexArray(vaoLine);
	glGenBuffers(1, &vboLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboLinePosition);
	glBufferData(GL_ARRAY_BUFFER,
		6 * sizeof(GLfloat),
		NULL,
		GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Color
	glGenBuffers(1, &vboLineColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboLineColor);
	glBufferData(GL_ARRAY_BUFFER,
		6 * sizeof(GLfloat),
		NULL,
		GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

	perspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

GLfloat getDistance(GLfloat x1, GLfloat x2, GLfloat y1, GLfloat y2) {
	GLfloat xSquare = powf((x2 - x1), 2.0f);
	GLfloat ySquare = powf((y2 - y1), 2.0f);
	return sqrtf((xSquare + ySquare));
}

GLfloat getOffset(GLfloat coA, GLfloat coB, GLfloat coC, GLfloat distAB, GLfloat distBC, GLfloat distAC, GLfloat perimeter) {
	GLfloat value = (coA * distBC) + (coB * distAC) + (coC * distAB);
	return(value / perimeter);
}

GLvoid drawLine(GLfloat fCoordinate) {
	// function declaration

	// Variable declaration
	GLfloat fLineVertices[3 * iLinePoints];
	GLfloat fLineColor[3 * iLinePoints];

	//Code
	// Line Vertices
	fLineVertices[0] = 0.0f;
	fLineVertices[1] = fCoordinate;
	fLineVertices[2] = 0.0f;

	fLineVertices[3] = 0.0f;
	fLineVertices[4] = -fCoordinate;
	fLineVertices[5] = 0.0f;

	// Line Color
	fLineColor[0] = 1.0f;
	fLineColor[1] = 0.0f;
	fLineColor[2] = 0.0f;

	fLineColor[3] = 1.0f;
	fLineColor[4] = 0.0f;
	fLineColor[5] = 0.0f;

	// Line Vertices
	glBindBuffer(GL_ARRAY_BUFFER, vboLinePosition);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fLineVertices),
		fLineVertices,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Circle Color
	glBindBuffer(GL_ARRAY_BUFFER, vboLineColor);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fLineColor),
		fLineColor,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

GLvoid drawTriangle(GLfloat fCoordinate) {
	// function declaration

	// Variable declaration
	GLfloat fTriangleVertices[3 * iTrianglePoints];
	GLfloat fTriangleColor[3 * iTrianglePoints];

	//Code
	//Triangle Vertices
	fTriangleVertices[0] = 0.0f;
	fTriangleVertices[1] = fCoordinate;
	fTriangleVertices[2] = 0.0f;

	fTriangleVertices[3] = -fCoordinate;
	fTriangleVertices[4] = -fCoordinate;
	fTriangleVertices[5] = 0.0f;

	fTriangleVertices[6] = fCoordinate;
	fTriangleVertices[7] = -fCoordinate;
	fTriangleVertices[8] = 0.0f;

	//Triangle Color
	fTriangleColor[0] = 0.0f;
	fTriangleColor[1] = fCoordinate;
	fTriangleColor[2] = 0.0f;

	fTriangleColor[3] = 0.0f;
	fTriangleColor[4] = fCoordinate;
	fTriangleColor[5] = 0.0f;

	fTriangleColor[6] = 0.0f;
	fTriangleColor[7] = fCoordinate;
	fTriangleColor[8] = 0.0f;

	// Circle Vertices
	glBindBuffer(GL_ARRAY_BUFFER, vboTrianglePosition);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fTriangleVertices),
		fTriangleVertices,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Circle Color
	glBindBuffer(GL_ARRAY_BUFFER, vboTriangleColor);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fTriangleColor),
		fTriangleColor,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void drawCircle(GLfloat fRadius, GLfloat fOffsetX, GLfloat fOffsetY) {
	// function declaration

	// Variable declaration
	GLdouble dAngle = 0.0;
	GLfloat fCircleVertices[3 * iPoints];
	GLfloat fCircleColor[3 * iPoints];

	// Code:
	for (GLint i = 0; i < iPoints; i +=3)
	{
		dAngle = 2 * M_PI * i / iPoints;
		fCircleVertices[i++] = fRadius * cos(dAngle) + fOffsetX;
		fCircleVertices[i++] = fRadius * sin(dAngle) + fOffsetY;
		fCircleVertices[i++] = 0.0f;

		// Circle Vertices
		glBindBuffer(GL_ARRAY_BUFFER, vboCirclePosition);
		glBufferData(GL_ARRAY_BUFFER,
			sizeof(fCircleVertices),
			fCircleVertices,
			GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	for (GLint i = 0; i < iPoints; i += 3)
	{
		fCircleColor[i++] = 0.0f;
		fCircleColor[i++] = 1.0f;
		fCircleColor[i++] = 0.0f;

		// Circle Color
		glBindBuffer(GL_ARRAY_BUFFER, vboCircleColor);
		glBufferData(GL_ARRAY_BUFFER,
			sizeof(fCircleColor),
			fCircleColor,
			GL_DYNAMIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}

GLvoid drawDethlyHollows()
{
	// Function Declaration

	// Variable Declaration

	// Code
	//// Draw Triangle
	//glBindVertexArray(vaoTriangle);
	//glLineWidth(3.0f);
	//drawTriangle(0.5f);
	//glDrawArrays(GL_LINE_LOOP,
	//	0,
	//	iTrianglePoints);
	//glBindVertexArray(0);

	//// Draw Circle
	//glBindVertexArray(vaoCircle);
	//glPointSize(3.0f);
	//drawCircle(fRadiusOfInCircle, fOffsetX, fOffsetY);
	//glDrawArrays(GL_POINTS,
	//	0,
	//	iPoints);
	//glBindVertexArray(0);

	// Draw Line
	//glBindVertexArray(vaoLine);
	//glLineWidth(3.0f);
	//drawLine(0.5f);
	//glDrawArrays(GL_LINES,
	//	0,
	//	iLinePoints);
	//glBindVertexArray(0);
}

void resize(int width, int height) {
	// Code:
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display(void) {

	// Function Declaration

	// Code:
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(gShaderProgramObject);
	// Declaration of matrices
	mat4 translationMatrix;
	mat4 rotationMatrix;
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	// Triangle
	// Initialize above matrices to identity
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	translationMatrix = translate(gfTriangleX,
		gfTriangleY,
		-3.0f);

	rotationMatrix = rotate(gfAngle, 0.0f, 1.0f, 0.0f);

	// Do necessary Matrix Multiplication
	modelViewMatrix = translationMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoTriangle);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	// Draw Triangle
	glLineWidth(3.0f);
	drawTriangle(0.5f);
	glDrawArrays(GL_LINE_LOOP,
		0,
		iTrianglePoints);
	//drawDethlyHollows();

	// Unbind vao
	glBindVertexArray(0);

	// Circle
	// Initialize above matrices to identity
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	translationMatrix = translate(gfCircleX,
		gfCircleY,
		-3.0f);

	rotationMatrix = rotate(gfAngle, 0.0f, 1.0f, 0.0f);

	// Do necessary Matrix Multiplication
	modelViewMatrix = translationMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoCircle);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	// Draw Circle
	glPointSize(3.0f);
	drawCircle(fRadiusOfInCircle, fOffsetX, fOffsetY);
	glDrawArrays(GL_POINTS,
		0,
		iPoints);

	// Unbind vao
	glBindVertexArray(0);

	// Line
	// Initialize above matrices to identity
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	translationMatrix = translate(0.0f,
		gfLineY,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoLine);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	// Draw Line
	glLineWidth(3.0f);
	drawLine(0.5f);
	glDrawArrays(GL_LINES,
		0,
		iLinePoints);

	// Unbind vao
	glBindVertexArray(0);

	// Unuse Program
	glUseProgram(0);
	SwapBuffers(gHdc);
}

void uninitialize(void) {
	// Code:
	if (bFullScreen == true)
	{
		SetWindowLong(gHwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(gHwnd, &wpPrev);
		SetWindowPos(gHwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |
			SWP_NOSIZE | SWP_NOOWNERZORDER);
		ShowCursor(TRUE);
	}
	if (vboCirclePosition)
	{
		glDeleteBuffers(1, &vboCirclePosition);
		vboCirclePosition = 0;
	}
	if (vaoCircle)
	{
		glDeleteBuffers(1, &vaoCircle);
		vaoCircle = 0;
	}
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);
	// Break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}
	if (gHdc)
	{
		ReleaseDC(gHwnd, gHdc);
		gHdc = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "File Closed Successfully \n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

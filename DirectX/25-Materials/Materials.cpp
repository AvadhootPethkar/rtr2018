#include <Windows.h>
#include <stdio.h>  // for file I/O

#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning(disable:4838)
#include "XNAMath\xnamath.h"
#include "Sphere.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "Sphere.lib")

#define WIN_WIDTH  800
#define WIN_HEIGHT 600

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
FILE *gpFile = NULL;
char gszLogFileName[] = "Log.txt";

HWND ghwnd = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

float gClearColor[4];
IDXGISwapChain *gpIDXGISwapChain = NULL;
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Position = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Normal = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;
D3D11_VIEWPORT d3dViewPort;

// For culling
ID3D11RasterizerState *gpID3D11RasterizerState = NULL;

ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;

ID3D11Buffer *gpID3D11Buffer_IndexBuffer = NULL;

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
unsigned int gNumElements;
unsigned int gNumVertices;

bool gbLight = false;

struct CBUFFER
{
	XMMATRIX WorldMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;
	XMVECTOR La;
	XMVECTOR Ld;
	XMVECTOR Ls;
	XMVECTOR Light_Position;

	XMVECTOR Ka;
	XMVECTOR Kd;
	XMVECTOR Ks;
	float Material_Shininess;

	unsigned int KeyPressed;
};

// Viewport Variables
unsigned int gX;
unsigned int gY;
unsigned int gWidth;
unsigned int gHeight;

const int MATERIAL_ROWS = 6;
const int MATERIAL_COLUMNS = 4;

// Animating variables
float angleOfXRotation = 0.0f;
float angleOfYRotation = 0.0f;
float angleOfZRotation = 0.0f;

int keyPressed = 0;

float lightAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
float lightDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float lightPosition[4] = { 100.0f, 100.0f, -100.0f, 1.0f };
float materialAmbient[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
float materialDiffuse[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float materialSpecular[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
float materialShininess = 128.0f;

struct SphereMaterial
{
	float materialAmbiant[4];
	float materialDiffuse[4];
	float materialSpecular[4];
	float materialShininess;
};

SphereMaterial material[MATERIAL_ROWS][MATERIAL_COLUMNS];

XMMATRIX gPerspectiveProjectionMatrix;

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	// variables 
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Direct3D11");
	bool bDone = false;

	// code
	// create file for logging
	if (fopen_s(&gpFile, gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Cannot Create log file!"), TEXT("Error"), MB_OK | MB_ICONSTOP | MB_TOPMOST);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log.txt file created...\n");
		fclose(gpFile);
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szClassName,
		TEXT("Direct3D11 Window"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// initialize D3D
	HRESULT hr;
	hr = initialize();
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "initialize() Failed. Exiting Now...\n");
		fclose(gpFile);
		DestroyWindow(hwnd);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "initialize() succeeded.\n");
		fclose(gpFile);
	}

	// Game Loop 
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// render
			display();
			update();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
			}
		}
	}

	// clean-up
	uninitialize();

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	HRESULT resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize();

	// variable declarations
	HRESULT hr;

	// code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() Failed.\n");
				fclose(gpFile);
				return(hr);
			}
			else
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "resize() Succeeded.\n");
				fclose(gpFile);
			}
		}
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;

		case 0x46:
			if (gbFullscreen == false)
			{
				ToggleFullScreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullscreen = false;
			}
			break;

		default:
			break;
		}
		break;

	case WM_CHAR:
		switch (wParam)
		{
		case 'L':
		case 'l':
			gbLight = !gbLight;
			break;
		case 'X':
		case 'x':
			keyPressed = 1;
			angleOfXRotation = 0.0f;
			break;

		case 'Y':
		case 'y':
			keyPressed = 2;
			angleOfYRotation = 0.0f;
			break;

		case 'Z':
		case 'z':
			keyPressed = 3;
			angleOfZRotation = 0.0f;
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_CLOSE:
		uninitialize();
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	MONITORINFO MI;

	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			MI = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev)
				&& GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &MI))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd,
					HWND_TOP,
					MI.rcMonitor.left,
					MI.rcMonitor.top,
					MI.rcMonitor.right - MI.rcMonitor.left,
					MI.rcMonitor.bottom - MI.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
	}
}

HRESULT initialize(void)
{
	// function declarations
	void uninitialize(void);
	HRESULT resize(int, int);
	void InitializeMaterials(void);

	// variable declarations
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = { D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE };
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;

	// code
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);

	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,
			d3dDriverType,
			NULL,
			createDeviceFlags,
			&d3dFeatureLevel_required,
			numFeatureLevels,
			D3D11_SDK_VERSION,
			&dxgiSwapChainDesc,
			&gpIDXGISwapChain,
			&gpID3D11Device,
			&d3dFeatureLevel_acquired,
			&gpID3D11DeviceContext
		);

		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() failed..\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() succeeded..\n");
		fprintf_s(gpFile, "The chosen driver is of: ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type. \n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "Warp Type. \n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference Type. \n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type. \n");
		}

		fprintf_s(gpFile, "The supported Highest Feature Level is: ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile, "11.0\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(gpFile, "10.1\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile, "10.0\n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown.\n");
		}
		fclose(gpFile);
	}

	// VERTEX SHADER
	const char *vertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"	float4x4 worldMatrix;" \
		"	float4x4 viewMatrix;" \
		"	float4x4 projectionMatrix;" \
		"	float4   la;" \
		"	float4   ld;" \
		"	float4   ls;" \
		"	float4   light_position;" \
		"	float4   ka;" \
		"	float4   kd;" \
		"	float4   ks;" \
		"	float    material_shininess;" \
		"	uint	 keyPressed;" \
		"}" \
		"struct vertex_output" \
		"{" \
		"	float4 position : SV_POSITION;" \
		"	float3 tnorm		   : NORMAL0;" \
		"	float3 light_direction : NORMAL1;" \
		"	float3 viewer_vector   : NORMAL2;" \
		"};" \
		"vertex_output main(float4 pos: POSITION, float4 normal: NORMAL)" \
		"{" \
		"	vertex_output output;" \
		"	if (keyPressed == 1)" \
		"	{ " \
		"		float4 eyeCoordinate = mul(worldMatrix, pos);" \
		"		eyeCoordinate = mul(viewMatrix, eyeCoordinate);" \
		"		float3 tnorm = mul((float3x3) worldMatrix, (float3)normal);" \
		"		float3 light_direction = (float3)(light_position - eyeCoordinate);" \
		"		float3 viewer_vector = normalize(-eyeCoordinate.xyz);" \
		"		output.tnorm = tnorm;" \
		"		output.light_direction = light_direction;" \
		"		output.viewer_vector = viewer_vector;" \
		"	} " \
		"	float4 position = mul(worldMatrix, pos);" \
		"	position = mul(viewMatrix, position);" \
		"	position = mul(projectionMatrix, position);" \
		"	output.position = position;" \
		"	return(output);" \
		"}";

	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pID3DBlob_VertexShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for Vertex Shader: %s.\n",
				(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile() succeeded for Vertex Shader.\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(), NULL, &gpID3D11VertexShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateVertexShader() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateVertexShader() succeeded.\n");
		fclose(gpFile);
	}

	// set vertex shader in pipeline
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, NULL, NULL);

	// PIXEL SHADER 
	const char *pixelShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"	float4x4 worldMatrix;" \
		"	float4x4 viewMatrix;" \
		"	float4x4 projectionMatrix;" \
		"	float4   la;" \
		"	float4   ld;" \
		"	float4   ls;" \
		"	float4   light_position;" \
		"	float4   ka;" \
		"	float4   kd;" \
		"	float4   ks;" \
		"	float    material_shininess;" \
		"	uint	 keyPressed;" \
		"}" \
		"struct vertex_output" \
		"{" \
		"	float4 position		   : SV_POSITION;" \
		"	float3 tnorm		   : NORMAL0;" \
		"	float3 light_direction : NORMAL1;" \
		"	float3 viewer_vector   : NORMAL2;" \
		"};" \
		"float4 main(float4 pos: SV_POSITION, vertex_output input) : SV_TARGET" \
		"{" \
		"	float4 phong_ads_color = float4(1.0,1.0,1.0,1.0);" \
		"	if (keyPressed == 1)" \
		"	{ " \
		"		float3 ntnorm = normalize(input.tnorm);" \
		"		float3 nlight_direction = normalize(input.light_direction);" \
		"		float3 nviewer_vector = normalize(input.viewer_vector);" \
		"		float3 reflection_vector = reflect(-nlight_direction, ntnorm);" \
		"		float  tn_dot_ld = max(dot(ntnorm, nlight_direction), 0.0);" \
		"		float4 ambient = la * ka;" \
		"		float4 diffuse = ld * kd * tn_dot_ld;" \
		"		float4 specular = ls * ks * pow(max(dot(reflection_vector, nviewer_vector), 0.0), material_shininess);" \
		"		phong_ads_color = ambient + diffuse + specular;" \
		"	} " \
		"	float4 color = phong_ads_color;" \
		"	return(color);" \
		"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;

	hr = D3DCompile(pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode,
		&pID3DBlob_Error);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() failed for Pixel Shader: %s.\n",
				(char *)pID3DBlob_Error->GetBufferPointer());
			fclose(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile() succeeded for Pixel Shader.\n");
		fclose(gpFile);
	}

	hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pID3DBlob_PixelShaderCode->GetBufferSize(), NULL, &gpID3D11PixelShader);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreatePixelShader() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreatePixelShader() succeeded.\n");
		fclose(gpFile);
	}

	// set pixel shader in pipeline
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, NULL, NULL);

	// create and set input layout
	D3D11_INPUT_ELEMENT_DESC inputElementsDesc[2];

	// position
	inputElementsDesc[0].SemanticName = "POSITION";
	inputElementsDesc[0].SemanticIndex = 0;
	inputElementsDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementsDesc[0].InputSlot = 0;
	inputElementsDesc[0].AlignedByteOffset = 0;
	inputElementsDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementsDesc[0].InstanceDataStepRate = 0;

	// color
	inputElementsDesc[1].SemanticName = "NORMAL";
	inputElementsDesc[1].SemanticIndex = 0;
	inputElementsDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementsDesc[1].InputSlot = 1;
	inputElementsDesc[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	inputElementsDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementsDesc[1].InstanceDataStepRate = 0;

	hr = gpID3D11Device->CreateInputLayout(inputElementsDesc, _ARRAYSIZE(inputElementsDesc),
		pID3DBlob_VertexShaderCode->GetBufferPointer(), pID3DBlob_VertexShaderCode->GetBufferSize(),
		&gpID3D11InputLayout);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() succeeded.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);

	//gpID3D11VertexShader->Release();
	//gpID3D11VertexShader = NULL;

	//gpID3D11PixelShader->Release();
	//gpID3D11PixelShader = NULL;

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	InitializeMaterials();

	// create vertex buffer for cube	
	D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Cube_Position;
	ZeroMemory(&bufferDesc_VertexBuffer_Cube_Position, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_VertexBuffer_Cube_Position.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_VertexBuffer_Cube_Position.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_vertices);
	bufferDesc_VertexBuffer_Cube_Position.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_VertexBuffer_Cube_Position.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_VertexBuffer_Cube_Position, NULL, &gpID3D11Buffer_VertexBuffer_Position);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() failed for Vertex Buffer.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() succeeded for Vertex Buffer.\n");
		fclose(gpFile);
	}

	// copy vertices into above buffer
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Position, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_vertices, sizeof(sphere_vertices));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Position, NULL);

	// create vertex buffer for cube-normal
	D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Cube_Normal;
	ZeroMemory(&bufferDesc_VertexBuffer_Cube_Normal, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_VertexBuffer_Cube_Normal.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_VertexBuffer_Cube_Normal.ByteWidth = sizeof(float) * ARRAYSIZE(sphere_normals);
	bufferDesc_VertexBuffer_Cube_Normal.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc_VertexBuffer_Cube_Normal.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc_VertexBuffer_Cube_Normal, NULL, &gpID3D11Buffer_VertexBuffer_Normal);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() failed for Vertex Buffer Normal.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() succeeded for Vertex Buffer Normal.\n");
		fclose(gpFile);
	}

	// copy vertices into above buffer	
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Normal, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_normals, sizeof(sphere_normals));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Normal, NULL);

	// create index buffer 	
	D3D11_BUFFER_DESC bufferDesc_IndexBuffer;
	ZeroMemory(&bufferDesc_IndexBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_IndexBuffer.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc_IndexBuffer.ByteWidth = gNumElements * sizeof(short);
	bufferDesc_IndexBuffer.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDesc_IndexBuffer.CPUAccessFlags = D3D10_CPU_ACCESS_WRITE;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc_IndexBuffer, NULL, &gpID3D11Buffer_IndexBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() failed for Index Buffer.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() succeeded for Index Buffer.\n");
		fclose(gpFile);
	}
	// copy vertices into above buffer	
	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_IndexBuffer, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_elements, gNumElements * sizeof(short));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_IndexBuffer, NULL);

	// define and set constant buffer
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, NULL, &gpID3D11Buffer_ConstantBuffer);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() failed for Constant Buffer.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() succeeded for Constant Buffer.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
	gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

	// Cullung Dissable
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory((void *)&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.CullMode = D3D11_CULL_NONE; // culling disable
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	hr = gpID3D11Device->CreateRasterizerState(&rasterizerDesc, &gpID3D11RasterizerState);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRasterizerState() failed for Constant Buffer.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRasterizerState() succeeded for Constant Buffer.\n");
		fclose(gpFile);
	}
	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	// black
	gClearColor[0] = 0.5f;
	gClearColor[1] = 0.5f;
	gClearColor[2] = 0.5f;
	gClearColor[3] = 1.0f;

	// set projection matrix
	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	// call resize for first time
	hr = resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "resize() failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "resize() succeeded.\n");
		fclose(gpFile);
	}

	return(S_OK);
}

HRESULT resize(int width, int height)
{
	// code
	HRESULT hr = S_OK;

	// free any size-dependent resource
	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// resize swap chain buffers accordingly
	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	// get back buffers from swap chain
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);

	// get render target view from d3d11 device using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() failed. \n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() succeeded. \n");
		fclose(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// create depth stencil buffer ( or zbuffer )	
	D3D11_TEXTURE2D_DESC textureDesc;
	ZeroMemory(&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));
	textureDesc.Width = (UINT)width;
	textureDesc.Height = (UINT)height;
	textureDesc.ArraySize = 1;
	textureDesc.MipLevels = 1;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;
	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer;
	gpID3D11Device->CreateTexture2D(&textureDesc, NULL, &pID3D11Texture2D_DepthBuffer);

	// create depth stencil view from above depth stencil buffer	
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &depthStencilViewDesc, &gpID3D11DepthStencilView);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() failed. \n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() succeeded. \n");
		fclose(gpFile);
	}
	pID3D11Texture2D_DepthBuffer->Release();
	pID3D11Texture2D_DepthBuffer = NULL;

	// set render target view as render target
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);

	// set viewport
	d3dViewPort.TopLeftX = 0.0f;
	d3dViewPort.TopLeftY = 0.0f;
	d3dViewPort.Width = (FLOAT)width;
	d3dViewPort.Height = (FLOAT)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f;
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	gWidth = width / MATERIAL_ROWS;
	gHeight = height / MATERIAL_ROWS;
	gX = (width - (gWidth * MATERIAL_COLUMNS)) / 2;
	gY = (height - (gHeight * MATERIAL_ROWS)) / 2;

	// set perspective matrix
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(
		XMConvertToRadians(45.0f),
		(float)width / (float)height,
		0.1f,
		100.0f
	);

	return(hr);
}

void display(void)
{
	// function declaration
	void drawSphere(int rows, int columns);

	// code
	// clear render targer view to a chosen color
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	d3dViewPort.Width = gWidth;
	d3dViewPort.Height = gHeight;
	for (int i = 0; i < MATERIAL_COLUMNS; i++)
	{
		for (int j = 0; j < MATERIAL_ROWS; j++)
		{
			d3dViewPort.TopLeftX = gX + (i * gWidth);
			d3dViewPort.TopLeftY = gY + (j * gHeight);
			gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);
			drawSphere(i, j);
		}
	}

	// switch between front and back buffers
	gpIDXGISwapChain->Present(0, 0);
}

void drawSphere(int rows, int columns)
{
	// select which vertex buffer to display
	UINT stride = sizeof(float) * 3;
	UINT offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer_Position,
		&stride, &offset);

	// normal buffer	
	stride = sizeof(float) * 3;
	offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_VertexBuffer_Normal,
		&stride, &offset);

	// index buffer	
	gpID3D11DeviceContext->IASetIndexBuffer(gpID3D11Buffer_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	// select geometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// translation is concerned with world matrix transformation
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX translationMatrix = XMMatrixIdentity();

	// translations
	translationMatrix = XMMatrixTranslation(0.0f, 0.0f, 2.0f);

	worldMatrix = translationMatrix; // order imp

	// load the data into the constant buffer
	CBUFFER constantBuffer;
	ZeroMemory(&constantBuffer, sizeof(CBUFFER));
	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;
	if (gbLight == true)
	{
		constantBuffer.KeyPressed = 1;
		constantBuffer.La = XMVectorSet(lightAmbient[0], lightAmbient[1], lightAmbient[2], lightAmbient[3]);
		constantBuffer.Ld = XMVectorSet(lightDiffuse[0], lightDiffuse[1], lightDiffuse[2], lightDiffuse[3]);
		constantBuffer.Ls = XMVectorSet(lightSpecular[0], lightSpecular[1], lightSpecular[2], lightSpecular[3]);
		constantBuffer.Ka = XMVectorSet(material[rows][columns].materialAmbiant[0], material[rows][columns].materialAmbiant[1], material[rows][columns].materialAmbiant[2], material[rows][columns].materialAmbiant[3]);
		constantBuffer.Kd = XMVectorSet(material[rows][columns].materialDiffuse[0], material[rows][columns].materialDiffuse[1], material[rows][columns].materialDiffuse[2], material[rows][columns].materialDiffuse[3]);
		constantBuffer.Ks = XMVectorSet(material[rows][columns].materialSpecular[0], material[rows][columns].materialSpecular[1], material[rows][columns].materialSpecular[2], material[rows][columns].materialSpecular[3]);
		constantBuffer.Material_Shininess = material[rows][columns].materialShininess;

		if (keyPressed == 1)
		{
			lightPosition[0] = 0.0f;
			lightPosition[1] = sinf(angleOfXRotation) * 100.0f - 3.0f;
			lightPosition[2] = cosf(angleOfXRotation) * 100.0f - 3.0f;
		}

		if (keyPressed == 2)
		{
			lightPosition[0] = sinf(angleOfYRotation) * 100.0f - 3.0f;
			lightPosition[1] = 0.0f;
			lightPosition[2] = cosf(angleOfYRotation) * 100.0f - 3.0f;
		}

		if (keyPressed == 3)
		{
			lightPosition[0] = sin(angleOfZRotation) * 100.0f - 3.0f;
			lightPosition[1] = cosf(angleOfZRotation) * 100.0f - 3.0f;
			lightPosition[2] = 0.0f;
		}
		constantBuffer.Light_Position = XMVectorSet(lightPosition[0], lightPosition[1], lightPosition[2], lightPosition[3]);
	}
	else
	{
		constantBuffer.KeyPressed = 0;
	}
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0,
		NULL, &constantBuffer, 0, 0);

	// draw vertex buffer to render target
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);
}

void update(void)
{
	// variable Declaration
	float iOfset = 0.001f;
	// Code:
	angleOfXRotation -= iOfset;
	if (angleOfXRotation > 360.0f)
	{
		angleOfXRotation += 360.0f;
	}
	angleOfYRotation -= iOfset;
	if (angleOfYRotation > 360.0f)
	{
		angleOfYRotation += 360.0f;
	}
	angleOfZRotation -= iOfset;
	if (angleOfZRotation > 360.0f)
	{
		angleOfZRotation += 360.0f;
	}
}

void uninitialize(void)
{
	// code
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "uninitalize() succeeded. \n");
		fprintf_s(gpFile, "Log file closed..\n");
		fclose(gpFile);
	}
}

void InitializeMaterials(void) {
	// emerald
	material[0][0].materialAmbiant[0] = 0.0215f;
	material[0][0].materialAmbiant[1] = 0.1745f;
	material[0][0].materialAmbiant[2] = 0.0215f;
	material[0][0].materialAmbiant[3] = 1.0f;

	material[0][0].materialDiffuse[0] = 0.07568f;
	material[0][0].materialDiffuse[1] = 0.61424f;
	material[0][0].materialDiffuse[2] = 0.07568f;
	material[0][0].materialDiffuse[3] = 1.0f;

	material[0][0].materialSpecular[0] = 0.633f;
	material[0][0].materialSpecular[1] = 0.727811f;
	material[0][0].materialSpecular[2] = 0.633f;
	material[0][0].materialSpecular[3] = 1.0f;

	material[0][0].materialShininess = 0.6 * 128;

	// jade
	material[0][1].materialAmbiant[0] = 0.135f;
	material[0][1].materialAmbiant[1] = 0.2225f;
	material[0][1].materialAmbiant[2] = 0.1575f;
	material[0][1].materialAmbiant[3] = 1.0f;

	material[0][1].materialDiffuse[0] = 0.54f;
	material[0][1].materialDiffuse[1] = 0.89f;
	material[0][1].materialDiffuse[2] = 0.63f;
	material[0][1].materialDiffuse[3] = 1.0f;

	material[0][1].materialSpecular[0] = 0.316228f;
	material[0][1].materialSpecular[1] = 0.316228f;
	material[0][1].materialSpecular[2] = 0.316228f;
	material[0][1].materialSpecular[3] = 1.0f;

	material[0][1].materialShininess = 0.1 * 128;

	// obsidian
	material[0][2].materialAmbiant[0] = 0.05375f;
	material[0][2].materialAmbiant[1] = 0.05f;
	material[0][2].materialAmbiant[2] = 0.06625f;
	material[0][2].materialAmbiant[3] = 1.0f;

	material[0][2].materialDiffuse[0] = 0.18275f;
	material[0][2].materialDiffuse[1] = 0.17f;
	material[0][2].materialDiffuse[2] = 0.22525f;
	material[0][2].materialDiffuse[3] = 1.0f;

	material[0][2].materialSpecular[0] = 0.332741f;
	material[0][2].materialSpecular[1] = 0.328634f;
	material[0][2].materialSpecular[2] = 0.346435f;
	material[0][2].materialSpecular[3] = 1.0f;

	material[0][2].materialShininess = 0.3 * 128;

	// pearl
	material[0][3].materialAmbiant[0] = 0.25f;
	material[0][3].materialAmbiant[1] = 0.20725f;
	material[0][3].materialAmbiant[2] = 0.20725f;
	material[0][3].materialAmbiant[3] = 1.0f;

	material[0][3].materialDiffuse[0] = 1.0f;
	material[0][3].materialDiffuse[1] = 0.829f;
	material[0][3].materialDiffuse[2] = 0.829f;
	material[0][3].materialDiffuse[3] = 1.0f;

	material[0][3].materialSpecular[0] = 0.296648f;
	material[0][3].materialSpecular[1] = 0.296648f;
	material[0][3].materialSpecular[2] = 0.296648f;
	material[0][3].materialSpecular[3] = 1.0f;

	material[0][3].materialShininess = 0.088 * 128;

	// ruby
	material[1][0].materialAmbiant[0] = 0.1745f;
	material[1][0].materialAmbiant[1] = 0.01175f;
	material[1][0].materialAmbiant[2] = 0.01175f;
	material[1][0].materialAmbiant[3] = 1.0f;

	material[1][0].materialDiffuse[0] = 0.61424f;
	material[1][0].materialDiffuse[1] = 0.04136f;
	material[1][0].materialDiffuse[2] = 0.04136f;
	material[1][0].materialDiffuse[3] = 1.0f;

	material[1][0].materialSpecular[0] = 0.727811f;
	material[1][0].materialSpecular[1] = 0.626959f;
	material[1][0].materialSpecular[2] = 0.626959f;
	material[1][0].materialSpecular[3] = 1.0f;

	material[1][0].materialShininess = 0.6 * 128;

	// turquoise
	material[1][1].materialAmbiant[0] = 0.1f;
	material[1][1].materialAmbiant[1] = 0.18725f;
	material[1][1].materialAmbiant[2] = 0.1745f;
	material[1][1].materialAmbiant[3] = 1.0f;

	material[1][1].materialDiffuse[0] = 0.396f;
	material[1][1].materialDiffuse[1] = 0.74151f;
	material[1][1].materialDiffuse[2] = 0.69102f;
	material[1][1].materialDiffuse[3] = 1.0f;

	material[1][1].materialSpecular[0] = 0.297254f;
	material[1][1].materialSpecular[1] = 0.30829f;
	material[1][1].materialSpecular[2] = 0.306678f;
	material[1][1].materialSpecular[3] = 1.0f;

	material[1][1].materialShininess = 0.1 * 128;
	//---------------------------------------------------------------

		// brass
	material[1][2].materialAmbiant[0] = 0.329412f;
	material[1][2].materialAmbiant[1] = 0.223529f;
	material[1][2].materialAmbiant[2] = 0.027451f;
	material[1][2].materialAmbiant[3] = 1.0f;

	material[1][2].materialDiffuse[0] = 0.780392f;
	material[1][2].materialDiffuse[1] = 0.568627f;
	material[1][2].materialDiffuse[2] = 0.113725f;
	material[1][2].materialDiffuse[3] = 1.0f;

	material[1][2].materialSpecular[0] = 0.992157f;
	material[1][2].materialSpecular[1] = 0.941176f;
	material[1][2].materialSpecular[2] = 0.807843f;
	material[1][2].materialSpecular[3] = 1.0f;

	material[1][2].materialShininess = 0.21794872 * 128;

	// bronze
	material[1][3].materialAmbiant[0] = 0.2125f;
	material[1][3].materialAmbiant[1] = 0.1275f;
	material[1][3].materialAmbiant[2] = 0.054f;
	material[1][3].materialAmbiant[3] = 1.0f;

	material[1][3].materialDiffuse[0] = 0.714f;
	material[1][3].materialDiffuse[1] = 0.4284f;
	material[1][3].materialDiffuse[2] = 0.18144f;
	material[1][3].materialDiffuse[3] = 1.0f;

	material[1][3].materialSpecular[0] = 0.393548f;
	material[1][3].materialSpecular[1] = 0.271906f;
	material[1][3].materialSpecular[2] = 0.166721f;
	material[1][3].materialSpecular[3] = 1.0f;

	material[1][3].materialShininess = 0.2 * 128;

	// chrome
	material[2][0].materialAmbiant[0] = 0.25f;
	material[2][0].materialAmbiant[1] = 0.25f;
	material[2][0].materialAmbiant[2] = 0.25f;
	material[2][0].materialAmbiant[3] = 1.0f;

	material[2][0].materialDiffuse[0] = 0.4f;
	material[2][0].materialDiffuse[1] = 0.4f;
	material[2][0].materialDiffuse[2] = 0.4f;
	material[2][0].materialDiffuse[3] = 1.0f;

	material[2][0].materialSpecular[0] = 0.774597f;
	material[2][0].materialSpecular[1] = 0.774597f;
	material[2][0].materialSpecular[2] = 0.774597f;
	material[2][0].materialSpecular[3] = 1.0f;

	material[2][0].materialShininess = 0.6 * 128;

	// copper
	material[2][1].materialAmbiant[0] = 0.19125f;
	material[2][1].materialAmbiant[1] = 0.0735f;
	material[2][1].materialAmbiant[2] = 0.0225f;
	material[2][1].materialAmbiant[3] = 1.0f;

	material[2][1].materialDiffuse[0] = 0.7038f;
	material[2][1].materialDiffuse[1] = 0.27048f;
	material[2][1].materialDiffuse[2] = 0.0828f;
	material[2][1].materialDiffuse[3] = 1.0f;

	material[2][1].materialSpecular[0] = 0.256777f;
	material[2][1].materialSpecular[1] = 0.137622f;
	material[2][1].materialSpecular[2] = 0.086014f;
	material[2][1].materialSpecular[3] = 1.0f;

	material[2][1].materialShininess = 0.1 * 128;

	// gold
	material[2][2].materialAmbiant[0] = 0.24725f;
	material[2][2].materialAmbiant[1] = 0.1995f;
	material[2][2].materialAmbiant[2] = 0.0745f;
	material[2][2].materialAmbiant[3] = 1.0f;

	material[2][2].materialDiffuse[0] = 0.75164f;
	material[2][2].materialDiffuse[1] = 0.60648f;
	material[2][2].materialDiffuse[2] = 0.22648f;
	material[2][2].materialDiffuse[3] = 1.0f;

	material[2][2].materialSpecular[0] = 0.628281f;
	material[2][2].materialSpecular[1] = 0.555802f;
	material[2][2].materialSpecular[2] = 0.366065f;
	material[2][2].materialSpecular[3] = 1.0f;

	material[2][2].materialShininess = 0.4 * 128;

	// silver
	material[2][3].materialAmbiant[0] = 0.19225f;
	material[2][3].materialAmbiant[1] = 0.19225f;
	material[2][3].materialAmbiant[2] = 0.19225f;
	material[2][3].materialAmbiant[3] = 1.0f;

	material[2][3].materialDiffuse[0] = 0.50754f;
	material[2][3].materialDiffuse[1] = 0.50754f;
	material[2][3].materialDiffuse[2] = 0.50754f;
	material[2][3].materialDiffuse[3] = 1.0f;

	material[2][3].materialSpecular[0] = 0.508273f;
	material[2][3].materialSpecular[1] = 0.508273f;
	material[2][3].materialSpecular[2] = 0.508273f;
	material[2][3].materialSpecular[3] = 1.0f;

	material[2][3].materialShininess = 0.4 * 128;

	//---------------------------------------------------------------

		// black
	material[3][0].materialAmbiant[0] = 0.0f;
	material[3][0].materialAmbiant[1] = 0.0f;
	material[3][0].materialAmbiant[2] = 0.0f;
	material[3][0].materialAmbiant[3] = 1.0f;

	material[3][0].materialDiffuse[0] = 0.01f;
	material[3][0].materialDiffuse[1] = 0.01f;
	material[3][0].materialDiffuse[2] = 0.01f;
	material[3][0].materialDiffuse[3] = 1.0f;

	material[3][0].materialSpecular[0] = 0.50f;
	material[3][0].materialSpecular[1] = 0.50f;
	material[3][0].materialSpecular[2] = 0.50f;
	material[3][0].materialSpecular[3] = 1.0f;

	material[3][0].materialShininess = 0.25 * 128;

	// cyan
	material[3][1].materialAmbiant[0] = 0.0f;
	material[3][1].materialAmbiant[1] = 0.1f;
	material[3][1].materialAmbiant[2] = 0.06f;
	material[3][1].materialAmbiant[3] = 1.0f;

	material[3][1].materialDiffuse[0] = 0.0f;
	material[3][1].materialDiffuse[1] = 0.50980392f;
	material[3][1].materialDiffuse[2] = 0.50980392f;
	material[3][1].materialDiffuse[3] = 1.0f;

	material[3][1].materialSpecular[0] = 0.50196078f;
	material[3][1].materialSpecular[1] = 0.50196078f;
	material[3][1].materialSpecular[2] = 0.50196078f;
	material[3][1].materialSpecular[3] = 1.0f;

	material[3][1].materialShininess = 0.25 * 128;

	// green
	material[3][2].materialAmbiant[0] = 0.0f;
	material[3][2].materialAmbiant[1] = 0.0f;
	material[3][2].materialAmbiant[2] = 0.0f;
	material[3][2].materialAmbiant[3] = 1.0f;

	material[3][2].materialDiffuse[0] = 0.1f;
	material[3][2].materialDiffuse[1] = 0.35f;
	material[3][2].materialDiffuse[2] = 0.1f;
	material[3][2].materialDiffuse[3] = 1.0f;

	material[3][2].materialSpecular[0] = 0.45f;
	material[3][2].materialSpecular[1] = 0.55f;
	material[3][2].materialSpecular[2] = 0.45f;
	material[3][2].materialSpecular[3] = 1.0f;

	material[3][2].materialShininess = 0.25 * 128;

	// red
	material[3][3].materialAmbiant[0] = 0.0f;
	material[3][3].materialAmbiant[1] = 0.0f;
	material[3][3].materialAmbiant[2] = 0.0f;
	material[3][3].materialAmbiant[3] = 1.0f;

	material[3][3].materialDiffuse[0] = 0.5f;
	material[3][3].materialDiffuse[1] = 0.0f;
	material[3][3].materialDiffuse[2] = 0.0f;
	material[3][3].materialDiffuse[3] = 1.0f;

	material[3][3].materialSpecular[0] = 0.7f;
	material[3][3].materialSpecular[1] = 0.6f;
	material[3][3].materialSpecular[2] = 0.6f;
	material[3][3].materialSpecular[3] = 1.0f;

	material[3][3].materialShininess = 0.25 * 128;

	// white
	material[4][0].materialAmbiant[0] = 0.0f;
	material[4][0].materialAmbiant[1] = 0.0f;
	material[4][0].materialAmbiant[2] = 0.0f;
	material[4][0].materialAmbiant[3] = 1.0f;

	material[4][0].materialDiffuse[0] = 0.55f;
	material[4][0].materialDiffuse[1] = 0.55f;
	material[4][0].materialDiffuse[2] = 0.55f;
	material[4][0].materialDiffuse[3] = 1.0f;

	material[4][0].materialSpecular[0] = 0.70f;
	material[4][0].materialSpecular[1] = 0.70f;
	material[4][0].materialSpecular[2] = 0.70f;
	material[4][0].materialSpecular[3] = 1.0f;

	material[4][0].materialShininess = 0.25 * 128;

	// yellow
	material[4][1].materialAmbiant[0] = 0.0f;
	material[4][1].materialAmbiant[1] = 0.0f;
	material[4][1].materialAmbiant[2] = 0.0f;
	material[4][1].materialAmbiant[3] = 1.0f;

	material[4][1].materialDiffuse[0] = 0.5f;
	material[4][1].materialDiffuse[1] = 0.5f;
	material[4][1].materialDiffuse[2] = 0.0f;
	material[4][1].materialDiffuse[3] = 1.0f;

	material[4][1].materialSpecular[0] = 0.60f;
	material[4][1].materialSpecular[1] = 0.60f;
	material[4][1].materialSpecular[2] = 0.50f;
	material[4][1].materialSpecular[3] = 1.0f;

	material[4][1].materialShininess = 0.25 * 128;

	//---------------------------------------------------------------

		// black
	material[4][2].materialAmbiant[0] = 0.02f;
	material[4][2].materialAmbiant[1] = 0.02f;
	material[4][2].materialAmbiant[2] = 0.02f;
	material[4][2].materialAmbiant[3] = 1.0f;

	material[4][2].materialDiffuse[0] = 0.01f;
	material[4][2].materialDiffuse[1] = 0.01f;
	material[4][2].materialDiffuse[2] = 0.01f;
	material[4][2].materialDiffuse[3] = 1.0f;

	material[4][2].materialSpecular[0] = 0.4f;
	material[4][2].materialSpecular[1] = 0.4f;
	material[4][2].materialSpecular[2] = 0.4f;
	material[4][2].materialSpecular[3] = 0.4f;

	material[4][2].materialShininess = 0.078125 * 128;

	// cyan
	material[4][3].materialAmbiant[0] = 0.0f;
	material[4][3].materialAmbiant[1] = 0.05f;
	material[4][3].materialAmbiant[2] = 0.05f;
	material[4][3].materialAmbiant[3] = 1.0f;

	material[4][3].materialDiffuse[0] = 0.4f;
	material[4][3].materialDiffuse[1] = 0.5f;
	material[4][3].materialDiffuse[2] = 0.5f;
	material[4][3].materialDiffuse[3] = 1.0f;

	material[4][3].materialSpecular[0] = 0.4f;
	material[4][3].materialSpecular[1] = 0.7f;
	material[4][3].materialSpecular[2] = 0.7f;
	material[4][3].materialSpecular[3] = 1.0f;

	material[4][3].materialShininess = 0.078125 * 128;

	// green
	material[5][0].materialAmbiant[0] = 0.0f;
	material[5][0].materialAmbiant[1] = 0.05f;
	material[5][0].materialAmbiant[2] = 0.0f;
	material[5][0].materialAmbiant[3] = 1.0f;

	material[5][0].materialDiffuse[0] = 0.4f;
	material[5][0].materialDiffuse[1] = 0.5f;
	material[5][0].materialDiffuse[2] = 0.4f;
	material[5][0].materialDiffuse[3] = 1.0f;

	material[5][0].materialSpecular[0] = 0.04f;
	material[5][0].materialSpecular[1] = 0.7f;
	material[5][0].materialSpecular[2] = 0.04f;
	material[5][0].materialSpecular[3] = 1.0f;

	material[5][0].materialShininess = 0.078125 * 128;

	// red
	material[5][1].materialAmbiant[0] = 0.05f;
	material[5][1].materialAmbiant[1] = 0.0f;
	material[5][1].materialAmbiant[2] = 0.0f;
	material[5][1].materialAmbiant[3] = 1.0f;

	material[5][1].materialDiffuse[0] = 0.5f;
	material[5][1].materialDiffuse[1] = 0.4f;
	material[5][1].materialDiffuse[2] = 0.4f;
	material[5][1].materialDiffuse[3] = 1.0f;

	material[5][1].materialSpecular[0] = 0.7f;
	material[5][1].materialSpecular[1] = 0.04f;
	material[5][1].materialSpecular[2] = 0.04f;
	material[5][1].materialSpecular[3] = 1.0f;

	material[5][1].materialShininess = 0.078125 * 128;

	// white
	material[5][2].materialAmbiant[0] = 0.05f;
	material[5][2].materialAmbiant[1] = 0.05f;
	material[5][2].materialAmbiant[2] = 0.05f;
	material[5][2].materialAmbiant[3] = 1.0f;

	material[5][2].materialDiffuse[0] = 0.5f;
	material[5][2].materialDiffuse[1] = 0.5f;
	material[5][2].materialDiffuse[2] = 0.5f;
	material[5][2].materialDiffuse[3] = 1.0f;

	material[5][2].materialSpecular[0] = 0.7f;
	material[5][2].materialSpecular[1] = 0.7f;
	material[5][2].materialSpecular[2] = 0.7f;
	material[5][2].materialSpecular[3] = 0.4f;

	material[5][2].materialShininess = 0.078125 * 128;

	// yellow
	material[5][3].materialAmbiant[0] = 0.05f;
	material[5][3].materialAmbiant[1] = 0.05f;
	material[5][3].materialAmbiant[2] = 0.0f;
	material[5][3].materialAmbiant[3] = 1.0f;

	material[5][3].materialDiffuse[0] = 0.5f;
	material[5][3].materialDiffuse[1] = 0.5f;
	material[5][3].materialDiffuse[2] = 0.4f;
	material[5][3].materialDiffuse[3] = 1.0f;

	material[5][3].materialSpecular[0] = 0.7f;
	material[5][3].materialSpecular[1] = 0.7f;
	material[5][3].materialSpecular[2] = 0.04f;
	material[5][3].materialSpecular[3] = 1.0f;

	material[5][3].materialShininess = 0.078125 * 128;
}

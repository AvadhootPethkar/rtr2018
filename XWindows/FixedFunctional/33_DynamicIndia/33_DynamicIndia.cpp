#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#define _USE_MATH_DEFINES 1
#include<math.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

// OpenGL header files
#include<GL/gl.h>	// Change 1
#include<GL/glu.h>	// Change 1
#include<GL/glx.h>	// Change 1

// namespace
using namespace std;

// global variable declarations
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800; 
int giWindowHeight = 600; 
static GLXContext gGLXContext;

// Animating variables
GLfloat gfLeftI_X = -2.0f;
GLfloat gfA_X = 2.3f;
GLfloat gfN_Y = 2.7f;
GLfloat gfRightI_Y = -2.7f;
GLfloat gfDOrangeColor_RValue = 0.0f;
GLfloat gfDOrangeColor_GValue = 0.0f;
GLfloat gfDOrangeColor_BValue = 0.0f;
GLfloat gfDGreenColor_RValue = 0.0f;
GLfloat gfDGreenColor_GValue = 0.0f;
GLfloat gfDGreenColor_BValue = 0.0f;

// Flag Variables:
GLfloat gfUpperFlagOrangeX = -12.0f;
GLfloat gfUpperFlagOrangeY = 0.03f;
GLfloat gfUpperFlagWhiteX = -12.0f;
GLfloat gfUpperFlagWhiteY = 0.0f;
GLfloat gfUpperFlagGreenX = -12.0f;
GLfloat gfUpperFlagGreenY = -0.03f;

// Plane Variables:
GLfloat gfMiddleTranslateFunction_X = -6.0f;
GLfloat gfMiddleTranslateFunction_Y = 0.0f;

GLfloat gfUpperLeftAngle = M_PI;
GLfloat gfUpperLeftAngle_X = 0.0f;
GLfloat gfUpperLeftAngle_Y = 0.0f;

GLfloat gfLowerLeftAngle = M_PI;
GLfloat gfLowerLeftAngle_X = 0.0f;
GLfloat gfLowerLeftAngle_Y = 0.0f;

GLfloat gfUpperRightAngle = 3 * (M_PI / 2);
GLfloat gfUpperRightAngle_X = 0.0f;
GLfloat gfUpperRightAngle_Y = 0.0f;

GLfloat gfLowerRightAngle = (M_PI / 2);
GLfloat gfLowerRightAngle_X = 0.0f;
GLfloat gfLowerRightAngle_Y = 0.0f;

// Flag Color variable:
GLfloat gfFlagOrangeColor_R = 1.0;
GLfloat gfFlagOrangeColor_G = 0.6f;
GLfloat gfFlagOrangeColor_B = 0.2f;

GLfloat gfFlagWhiteColor_R = 1.0;
GLfloat gfFlagWhiteColor_G = 1.0f;
GLfloat gfFlagWhiteColor_B = 1.0f;

GLfloat gfFlagGreenColor_R = 0.07;
GLfloat gfFlagGreenColor_G = 0.53f;
GLfloat gfFlagGreenColor_B = 0.02f;

// Small Flag X,Y Values:
GLfloat gfSmallFlagOrangeY = 0.01f;
GLfloat gfSmallFlagWhiteY = 0.0f;
GLfloat gfSmallFlagGreenY = -0.01f;

GLfloat gfSmallFlagOrangeX1 = 1.25f;
GLfloat gfSmallFlagWhiteX1 = 1.25f;
GLfloat gfSmallFlagGreenX1 = 1.25f;
GLfloat gfSmallFlagOrangeX2 = 1.54f;
GLfloat gfSmallFlagWhiteX2 = 1.54f;
GLfloat gfSmallFlagGreenX2 = 1.54f;


GLfloat gfVaringValue = 0.006;

// entry-point function
int main(void)
{
	// function prototypes
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize(void);
	int initialize(void);
	void resize(int width, int height);
	void display(void);
	void update(void);
	
	// variable declarations
	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;
	char keys[26];
	bool bDone = false;

	// code
	CreateWindow();
	initialize();

	// Message loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay) )
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					break;

				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							break;

						default:
							break;
					}
				
				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
				switch(keys[0])
				{
					case 'F':
					case 'f':
						if(bFullscreen == false)
						{
							ToggleFullscreen();
							bFullscreen = true;
						}
						else
						{
							ToggleFullscreen();
							bFullscreen = false;
						}
						break;

					default:
						break;
				}
					break;

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						
						case 2:
							break;

						case 3:
							break;
						
						case 4:
							break;
						
						case 5:
							break;

						default:
							break;
					}
					break;

				case MotionNotify:
					break;

				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;

				case Expose:
					break;

				case DestroyNotify:
					break;

				case 33:
					bDone = true;
					break;

				default:
					break;
			}
		}
		update();
		display();
	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[] = { GLX_RGBA, 
						GLX_DOUBLEBUFFER,
						GLX_WINDOW_BIT,
						GLX_RED_SIZE, 8, 
						GLX_GREEN_SIZE, 8, 
						GLX_BLUE_SIZE, 8, 
						GLX_ALPHA_SIZE, 8, 
						None};

	// code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \n Exitting Now... \n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, 
					defaultScreen, 
					frameBufferAttributes);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable To Alocate Memory For Visual Info. \n Exitting Now...\n");
		uninitialize();
		exit(1);
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),
				gpXVisualInfo->visual,
				AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	winAttribs.event_mask = VisibilityChangeMask | KeyPressMask | ButtonPressMask 
				| PointerMotionMask | StructureNotifyMask | ExposureMask;
	
	styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;

	gWindow = XCreateWindow(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			0, 
			0, 
			giWindowWidth, 
			giWindowHeight, 
			0, 
			gpXVisualInfo->depth, 
			InputOutput, 
			gpXVisualInfo->visual, 
			styleMask, 
			&winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting now...\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "First X Window");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

int initialize(void)
{
	// function prototypes
	void uninitialize(void);
	void resize(int width, int height);

	// variable declarations
	
	// code
	gGLXContext = glXCreateContext(gpDisplay, 
					gpXVisualInfo,
					NULL, 
					GL_TRUE );
	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	resize(giWindowWidth, giWindowHeight);
	return(0);
}

void resize(int width, int height)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	
	// code
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display(void)
{
	// function prototypes
	void drawLine(float x1, float y1, float x2, float y2);
	void displayPlain(void);

	// variable declarations
	
	// code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW); // L-I
	glLoadIdentity();
	glTranslatef(gfLeftI_X,
		0.0f,
		-4.0f);
	glLineWidth(6.0f);
	drawLine(-1.0f, 1.0f, -1.0f, -1.0f);
	glMatrixMode(GL_MODELVIEW);	// N
	glLoadIdentity();
	glTranslatef(0.06f,
		gfN_Y,
		-4.0f);
	//glLineWidth(3.0f);
	drawLine(-1.0f, 1.0f, -1.0f, -1.0f);
	drawLine(-1.0f, 1.0f, -0.5f, -1.0f);
	drawLine(-0.5f, 1.0f, -0.5f, -1.0f);

	glMatrixMode(GL_MODELVIEW);	// D
	glLoadIdentity();
	glTranslatef(0.88f,
		0.0f,
		-4.0f);
	//drawLine(-1.0f, 1.0f, -1.0f, -1.0f);	//R|
	glBegin(GL_LINES);
	glColor3f(gfDOrangeColor_RValue, gfDOrangeColor_GValue, gfDOrangeColor_BValue);
	glVertex2f(-1.0f, 1.0f);
	glColor3f(gfDGreenColor_RValue, gfDGreenColor_BValue, gfDGreenColor_GValue);
	glVertex2f(-1.0f, -1.0f);
	glEnd();

	//drawLine(-1.0f, 1.0f, -0.5f, 1.0f);	//U-
	glBegin(GL_LINES);
	glColor3f(gfDOrangeColor_RValue, gfDOrangeColor_GValue, gfDOrangeColor_BValue);
	glVertex2f(-1.03f, 1.0f);
	glVertex2f(-0.5f, 1.0f);
	glEnd();

	//drawLine(-0.5f, 1.0f, -0.5f, -1.0f);	//L|
	glBegin(GL_LINES);
	glColor3f(gfDOrangeColor_RValue, gfDOrangeColor_GValue, gfDOrangeColor_BValue);
	glVertex2f(-0.5f, 1.0f);
	glColor3f(gfDGreenColor_RValue, gfDGreenColor_BValue, gfDGreenColor_GValue);
	glVertex2f(-0.5f, -1.0f);
	glEnd();

	//drawLine(-0.5f, -1.0f, -1.0f, -1.0f);	//D-
	glBegin(GL_LINES);
	glColor3f(gfDGreenColor_RValue, gfDGreenColor_BValue, gfDGreenColor_GValue);
	glVertex2f(-0.5f, -1.0f);
	glVertex2f(-1.03f, -1.0f);
	glEnd();

	glMatrixMode(GL_MODELVIEW); // R-I
	glLoadIdentity();
	glTranslatef(1.76f,
		gfRightI_Y,
		-4.0f);
	glLineWidth(6.0f);
	drawLine(-1.0f, 1.0f, -1.0f, -1.0f);
	glMatrixMode(GL_MODELVIEW); // A
	glLoadIdentity();
	glTranslatef(gfA_X,
		0.0f,
		-4.0f);
	glLineWidth(6.0f);
	drawLine(1.0f, 1.0f, 0.7f, -1.0f);
	//drawLine(0.84f, 0.0f, 1.14f, 0.0f);
	drawLine(1.0f, 1.0f, 1.3f, -1.0f);
	if ((gfDOrangeColor_RValue >= 1.0f) && (gfDOrangeColor_GValue >= 0.6f) && (gfDOrangeColor_BValue >= 0.2f) && (gfDGreenColor_RValue >= 0.07f) && (gfDGreenColor_GValue >= 0.02f) && (gfDGreenColor_BValue >= 0.53f))
	{
		displayPlain();
	}

	glXSwapBuffers(gpDisplay, gWindow);
}

void displayPlain(void) {
	// Function Declaration:
	void drawPlain();
	void drawLine(float x1, float y1, float x2, float y2);
	void drawSquare(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);
	void drawFlag();
	void drawFlagSmall(void);

	// Code:
	glMatrixMode(GL_MODELVIEW); // Upper Plane
	glLoadIdentity();
	glTranslatef(-2.5f,
		3.0f,
		-4.0f);
	if (gfUpperLeftAngle < (3.0f * (M_PI / 2)))
	{
		gfUpperLeftAngle_X = 3.0f * cos(gfUpperLeftAngle);
		gfUpperLeftAngle_Y = 3.0f * sin(gfUpperLeftAngle);
		glTranslatef(gfUpperLeftAngle_X,
			gfUpperLeftAngle_Y,
			-4.0f);
	}
	glColor3f(0.182f, 0.226f, 0.238f);
	drawPlain();
	drawFlag();
	gfUpperLeftAngle += 0.001f;

	glMatrixMode(GL_MODELVIEW); // Middle Plane
	glLoadIdentity();
	glTranslatef(gfMiddleTranslateFunction_X,
		gfMiddleTranslateFunction_Y,
		-8.0f);
	//glColor3f(1.0f, 0.6f, 0.2f);
	glColor3f(0.182f, 0.226f, 0.238f);
	drawPlain();
	drawFlag();

	if (gfMiddleTranslateFunction_X > 6.5f)
	{
		glMatrixMode(GL_MODELVIEW); // Small Flag
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -4.0f);
		glLineWidth(3.0f);

		gfSmallFlagOrangeY = 0.01f;
		gfSmallFlagWhiteY = 0.0f;
		gfSmallFlagGreenY = -0.01f;
		gfSmallFlagOrangeX1 = 1.25f;
		gfSmallFlagWhiteX1 = 1.25f;
		gfSmallFlagGreenX1 = 1.25f;
		gfSmallFlagOrangeX2 = 1.54f;
		gfSmallFlagWhiteX2 = 1.54f;
		gfSmallFlagGreenX2 = 1.54f;
		drawFlagSmall();
	}

	glMatrixMode(GL_MODELVIEW); // Lower Plane
	glLoadIdentity();
	glTranslatef(-2.5f,
		-3.0f,
		-4.0f);
	if (gfLowerLeftAngle > (M_PI / 2))
	{
		gfLowerLeftAngle_X = 3.0f * cos(gfLowerLeftAngle);
		gfLowerLeftAngle_Y = 3.0f * sin(gfLowerLeftAngle);
		glTranslatef(gfLowerLeftAngle_X,
			gfLowerLeftAngle_Y,
			-4.0f);
	}
	glColor3f(0.182f, 0.226f, 0.238f);
	drawPlain();
	drawFlag();
	gfLowerLeftAngle -= 0.001f;

	if (gfMiddleTranslateFunction_X >= 3.5f)
	{
		glMatrixMode(GL_MODELVIEW); // Upper Plane
		glLoadIdentity();
		glTranslatef(3.5f,
			3.0f,
			-4.0f);
		if (gfUpperRightAngle < (2.0f * M_PI))
		{
			gfUpperRightAngle_X = 3.0f * cos(gfUpperRightAngle);
			gfUpperRightAngle_Y = 3.0f * sin(gfUpperRightAngle);
			glTranslatef(gfUpperRightAngle_X,
				gfUpperRightAngle_Y,
				-4.0f);
		}
		glColor3f(0.182f, 0.226f, 0.238f);
		drawPlain();
		gfSmallFlagOrangeY = 0.01f;
		gfSmallFlagWhiteY = 0.0f;
		gfSmallFlagGreenY = -0.01f;
		gfSmallFlagOrangeX1 = -0.89f;
		gfSmallFlagWhiteX1 = -0.89f;
		gfSmallFlagGreenX1 = -0.89f;
		gfSmallFlagOrangeX2 = -0.60f;
		gfSmallFlagWhiteX2 = -0.60f;
		gfSmallFlagGreenX2 = -0.60f;
		drawFlagSmall();
		gfUpperRightAngle += 0.001f;


		glMatrixMode(GL_MODELVIEW); // Lower Plane
		glLoadIdentity();
		glTranslatef(3.5f,
			-3.0f,
			-4.0f);
		if (gfLowerRightAngle > 0)
		{
			gfLowerRightAngle_X = 3.0f * cos(gfLowerRightAngle);
			gfLowerRightAngle_Y = 3.0f * sin(gfLowerRightAngle);
			glTranslatef(gfLowerRightAngle_X,
				gfLowerRightAngle_Y,
				-4.0f);
		}
		glColor3f(0.182f, 0.226f, 0.238f);
		drawPlain();
		gfSmallFlagOrangeY = 0.01f;
		gfSmallFlagWhiteY = 0.0f;
		gfSmallFlagGreenY = -0.01f;
		gfSmallFlagOrangeX1 = -0.89f;
		gfSmallFlagWhiteX1 = -0.89f;
		gfSmallFlagGreenX1 = -0.89f;
		gfSmallFlagOrangeX2 = -0.60f;
		gfSmallFlagWhiteX2 = -0.60f;
		gfSmallFlagGreenX2 = -0.60f;
		drawFlagSmall();
		gfLowerRightAngle -= 0.001f;
	}
}

void drawPlain() {
	// Function Declaration:
	void drawLine(float x1, float y1, float x2, float y2);
	void drawFlagLine(float x1, float y1, float x2, float y2);
	void drawSquare(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);

	// Code:
	glColor3f(182.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);
	drawSquare(0.3f, 0.1f, -0.4f, 0.1f, -0.4f, -0.1f, 0.3f, -0.1f);

	glBegin(GL_TRIANGLES);	// Upper Wing
	//glColor3f(1.0f, 0.4f, 1.0f);
	glColor3f(182.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);
	glVertex2f(-0.4f, 0.5f);
	glVertex2f(-0.2f, 0.1f);
	glVertex2f(0.2f, 0.1f);
	glEnd();

	glBegin(GL_TRIANGLES);	// Lower Wing
	//glColor3f(1.0f, 0.4f, 1.0f);
	glColor3f(182.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);
	glVertex2f(-0.4f, -0.5f);
	glVertex2f(-0.2f, -0.1f);
	glVertex2f(0.2f, -0.1f);
	glEnd();

	glBegin(GL_TRIANGLES);	// Front Nosel
	//glColor3f(1.0f, 0.4f, 1.0f);
	glColor3f(182.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);
	glVertex2f(0.3f, 0.1f);
	glVertex2f(0.6f, 0.0f);
	glVertex2f(0.3f, -0.1f);
	glEnd();

	drawSquare(-0.4f, 0.1f, -0.6f, 0.3f, -0.6f, -0.3f, -0.4f, -0.1f);	// Back Tail

	glLineWidth(3.0f);	// I
	glColor3f(0.0f, 0.0f, 0.0f);
	drawFlagLine(-0.33f, 0.08f, -0.33f, -0.08f);

	glLineWidth(3.0f);	// A
	drawFlagLine(-0.16f, 0.08f, -0.25f, -0.08f);
	drawFlagLine(-0.16f, 0.08f, -0.1f, -0.08f);

	glLineWidth(3.0f);	// F
	drawFlagLine(0.02f, 0.08f, 0.02f, -0.08f);
	drawFlagLine(0.02f, 0.08f, 0.12f, 0.08f);
	drawFlagLine(0.02f, -0.005f, 0.12f, -0.005f);
}

void drawFlag() {
	void drawLine(float x1, float y1, float x2, float y2);
	void drawFlagLine(float x1, float y1, float x2, float y2);

	glColor3f(gfFlagOrangeColor_R, gfFlagOrangeColor_G, gfFlagOrangeColor_B);
	drawFlagLine(gfUpperFlagOrangeX, gfUpperFlagOrangeY, -0.60f, gfUpperFlagOrangeY);
	glColor3f(gfFlagWhiteColor_R, gfFlagWhiteColor_G, gfFlagWhiteColor_B);
	drawFlagLine(gfUpperFlagWhiteX, gfUpperFlagWhiteY, -0.60f, gfUpperFlagWhiteY);
	glColor3f(gfFlagGreenColor_R, gfFlagGreenColor_G, gfFlagGreenColor_B);
	drawFlagLine(gfUpperFlagGreenX, gfUpperFlagGreenY, -0.60f, gfUpperFlagGreenY);
}

void drawFlagSmall() {
	void drawFlagLine(float x1, float y1, float x2, float y2);

	glColor3f(1.0f, 0.6f, 0.2f);
	drawFlagLine(gfSmallFlagOrangeX1, gfSmallFlagOrangeY, gfSmallFlagOrangeX2, gfSmallFlagOrangeY);
	glColor3f(1.0f, 1.0f, 1.0f);
	drawFlagLine(gfSmallFlagWhiteX1, gfSmallFlagWhiteY, gfSmallFlagWhiteX2, gfSmallFlagWhiteY);
	glColor3f(0.07f, 0.53f, 0.02f);
	drawFlagLine(gfSmallFlagGreenX1, gfSmallFlagGreenY, gfSmallFlagGreenX2, gfSmallFlagGreenY);
}

void drawLine(float x1, float y1, float x2, float y2) {
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.6f, 0.2f);
	glVertex2f(x1, y1);
	glColor3f(0.07f, 0.53f, 0.02f);
	glVertex2f(x2, y2);
	glEnd();
}

void drawFlagLine(float x1, float y1, float x2, float y2) {
	glBegin(GL_LINES);
	glVertex2f(x1, y1);
	glVertex2f(x2, y2);
	glEnd();
}

void drawSquare(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
	glBegin(GL_QUADS);
	glVertex2f(x1, y1);
	glVertex2f(x2, y2);
	glVertex2f(x3, y3);
	glVertex2f(x4, y4);
	glEnd();
}

void update(void)
{
	// function prototypes
	void drawFlagSmall(void);

	// variable declarations
	
	// code
	if (gfLeftI_X <= -0.4f)
	{
		gfLeftI_X += gfVaringValue;
	}
	else
	{
		if (gfA_X >= 0.4f)
		{
			gfA_X -= gfVaringValue;
		}
		else
		{
			if (gfN_Y >= 0.0f)
			{
				gfN_Y -= gfVaringValue;
			}
			else
			{
				if (gfRightI_Y <= 0.0f)
				{
					gfRightI_Y += gfVaringValue;
				}
				else
				{
					if (gfDOrangeColor_RValue <= 1.0f)
					{
						gfDOrangeColor_RValue += gfVaringValue;
					}
					if (gfDOrangeColor_GValue <= 0.6f)
					{
						gfDOrangeColor_GValue += gfVaringValue;
					}
					if (gfDOrangeColor_BValue <= 0.2f)
					{
						gfDOrangeColor_BValue += gfVaringValue;
					}
					if (gfDGreenColor_RValue <= 0.07f)
					{
						gfDGreenColor_RValue += gfVaringValue;
					}
					if (gfDGreenColor_BValue <= 0.53f)
					{
						gfDGreenColor_BValue += gfVaringValue;
					}
					if (gfDGreenColor_GValue <= 0.02f)
					{
						gfDGreenColor_GValue += gfVaringValue;
					}
					else
					{
						if ((gfDOrangeColor_RValue >= 1.0f) && (gfDOrangeColor_GValue >= 0.6f) && (gfDOrangeColor_BValue >= 0.2f) && (gfDGreenColor_RValue >= 0.07f) && (gfDGreenColor_GValue >= 0.02f) && (gfDGreenColor_BValue >= 0.53f) )
						{
							if (gfMiddleTranslateFunction_X <= 6.5f)
							{
								gfMiddleTranslateFunction_X += 0.00219f;
								gfUpperFlagOrangeX -= gfVaringValue;
								gfUpperFlagWhiteX -= gfVaringValue;
								gfUpperFlagGreenX -= gfVaringValue;
							}
							if (gfMiddleTranslateFunction_X >= 6.5f)
							{
								gfUpperFlagOrangeX += 0.12f;
								gfUpperFlagWhiteX += 0.12f;
								gfUpperFlagGreenX += 0.12f;

								gfFlagOrangeColor_R -= 0.0025f;
								gfFlagOrangeColor_G -= 0.0025f;
								gfFlagOrangeColor_B -= 0.0025f;
								gfFlagWhiteColor_R -= 0.0025f;
								gfFlagWhiteColor_G -= 0.0025f;
								gfFlagWhiteColor_B -= 0.0025f;
								gfFlagGreenColor_R -= 0.0025f;
								gfFlagGreenColor_G -= 0.0025f;
								gfFlagGreenColor_B -= 0.0025f;

							}
							//drawLine(0.84f, 0.0f, 1.14f, 0.0f);
							if (gfUpperFlagOrangeX >= 0.84f &&
								gfUpperFlagWhiteX >= 0.84f &&
								gfUpperFlagGreenX >= 0.84f) {
								drawFlagSmall();
							}
						}
					}
				}
			}
		}
	}
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	// code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False, 
		StructureNotifyMask, 
		&xev);
}

void uninitialize(void)
{
	GLXContext currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}


#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#define _USE_MATH_DEFINES 1
#include<math.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

// OpenGL header files
#include<GL/gl.h>	// Change 1
#include<GL/glu.h>	// Change 1
#include<GL/glx.h>	// Change 1

// namespace
using namespace std;

// global variable declarations
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800; 
int giWindowHeight = 600; 
static GLXContext gGLXContext;

// Animating variables
GLfloat gfTriangleX = 1.0f;
GLfloat gfTriangleY = -1.0f;
GLfloat gfCircleX = -1.0f;
GLfloat gfCircleY = -1.0f;
GLfloat gfAngle = 0.0f;
GLfloat gfLineY = 1.0f;

// entry-point function
int main(void)
{
	// function prototypes
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize(void);
	int initialize(void);
	void resize(int width, int height);
	void display(void);
	void update(void);
	
	// variable declarations
	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;
	char keys[26];
	bool bDone = false;

	// code
	CreateWindow();
	initialize();

	// Message loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay) )
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					break;

				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							break;

						default:
							break;
					}
				
				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
				switch(keys[0])
				{
					case 'F':
					case 'f':
						if(bFullscreen == false)
						{
							ToggleFullscreen();
							bFullscreen = true;
						}
						else
						{
							ToggleFullscreen();
							bFullscreen = false;
						}
						break;

					default:
						break;
				}
					break;

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						
						case 2:
							break;

						case 3:
							break;
						
						case 4:
							break;
						
						case 5:
							break;

						default:
							break;
					}
					break;

				case MotionNotify:
					break;

				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;

				case Expose:
					break;

				case DestroyNotify:
					break;

				case 33:
					bDone = true;
					break;

				default:
					break;
			}
		}
		update();
		display();
	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[] = { GLX_RGBA, 
						GLX_DOUBLEBUFFER,
						GLX_WINDOW_BIT,
						GLX_RED_SIZE, 8, 
						GLX_GREEN_SIZE, 8, 
						GLX_BLUE_SIZE, 8, 
						GLX_ALPHA_SIZE, 8, 
						None};

	// code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \n Exitting Now... \n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, 
					defaultScreen, 
					frameBufferAttributes);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable To Alocate Memory For Visual Info. \n Exitting Now...\n");
		uninitialize();
		exit(1);
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),
				gpXVisualInfo->visual,
				AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	winAttribs.event_mask = VisibilityChangeMask | KeyPressMask | ButtonPressMask 
				| PointerMotionMask | StructureNotifyMask | ExposureMask;
	
	styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;

	gWindow = XCreateWindow(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			0, 
			0, 
			giWindowWidth, 
			giWindowHeight, 
			0, 
			gpXVisualInfo->depth, 
			InputOutput, 
			gpXVisualInfo->visual, 
			styleMask, 
			&winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting now...\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "First X Window");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

int initialize(void)
{
	// function prototypes
	void uninitialize(void);
	void resize(int width, int height);

	// variable declarations
	
	// code
	gGLXContext = glXCreateContext(gpDisplay, 
					gpXVisualInfo,
					NULL, 
					GL_TRUE );
	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	resize(giWindowWidth, giWindowHeight);
	return(0);
}

void resize(int width, int height)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	
	// code
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display(void)
{
	// function prototypes
	GLfloat getDistance(GLfloat x1, GLfloat x2, GLfloat y1, GLfloat y2);
	GLfloat getOffset(GLfloat coA, GLfloat coB, GLfloat coC, GLfloat distAB, GLfloat distBC, GLfloat distAC, GLfloat perimeter);
	void drawCircle(GLfloat fRadius, GLfloat fOffsetX, GLfloat fOffsetY);
	void drawTriangle(GLfloat fLength);
	void drawLine(void);

	// variable declarations
	GLfloat coX1 = 0.0f;
	GLfloat coY1 = 0.5f;
	GLfloat coX2 = -0.5f;
	GLfloat coY2 = -0.5f;
	GLfloat coX3 = 0.5f;
	GLfloat coY3 = -0.5f;
	GLfloat fdistAB = 0.0f;
	GLfloat fdistBC = 0.0f;
	GLfloat fdistAC = 0.0f;
	GLfloat fPerimeter = 0.0f;
	GLfloat fSemiPerimeter = 0.0f;
	GLfloat fAreaOfTriangle = 0.0f;
	GLfloat fRadiusOfInCircle = 0.0f;
	GLfloat fOffsetX = 0.0f;
	GLfloat fOffsetY = 0.0f;
	GLfloat fTriangleHeight = 0.0f;
	
	// code
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(gfTriangleX,
		gfTriangleY,
		-3.0f);
	glRotatef(gfAngle, 0.0f, 1.0f, 0.0f);
	drawTriangle(0.5f);
	fdistAB = getDistance(coX1, coX2, coY1, coY2);
	fdistBC = getDistance(coX2, coX3, coY2, coY3);
	fdistAC = getDistance(coX1, coX3, coY1, coY3);
	//fAreaOfTriangle = 0.5f * 1.0f * 0.5f;

	fPerimeter = fdistAB + fdistBC + fdistAC;
	fSemiPerimeter = fPerimeter / 2;

	//fRadiusOfInCircle = (2 * fAreaOfTriangle) / fSemiPerimeter;
	fRadiusOfInCircle = sqrtf((fSemiPerimeter - fdistAB) * (fSemiPerimeter - fdistBC) * (fSemiPerimeter - fdistAC) / fSemiPerimeter);
	fOffsetX = getOffset(coX1, coX2, coX3, fdistAB, fdistBC, fdistAC, fPerimeter);
	fOffsetY = getOffset(coY1, coY2, coY3, fdistAB, fdistBC, fdistAC, fPerimeter);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(gfCircleX,
		gfCircleY,
		-3.0f);
	glRotatef(gfAngle, 0.0f, 1.0f, 0.0f);
	drawCircle(fRadiusOfInCircle, fOffsetX, fOffsetY);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f,
		gfLineY,
		-3.0f);
	drawLine();

	glXSwapBuffers(gpDisplay, gWindow);
}

void drawCircle(GLfloat fRadius, GLfloat fOffsetX, GLfloat fOffsetY) {
	int iPoints = 10000;
	double dAngle = 0.0;
	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 0.0f, 0.0);
	for (int i = 0; i < iPoints; i++)
	{
		dAngle = 2 * M_PI * i / iPoints;
		glVertex2f(fRadius * cos(dAngle) + fOffsetX, fRadius * sin(dAngle) + fOffsetY);
	}
	glEnd();
}

void drawTriangle(GLfloat fLength) {
	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(0.0f, fLength);
	glVertex2f(-fLength, -fLength);
	glVertex2f(fLength, -fLength);
	glEnd();
}

GLfloat getDistance(GLfloat x1, GLfloat x2, GLfloat y1, GLfloat y2) {
	GLfloat xSquare = powf((x2 - x1), 2.0f);
	GLfloat ySquare = powf((y2 - y1), 2.0f);
	return sqrtf((xSquare + ySquare));
}

void drawLine(void) {
	glBegin(GL_LINES);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, 0.5f);
	glVertex2f(0.0f, -0.5f);
	glEnd();
}

GLfloat getOffset(GLfloat coA, GLfloat coB, GLfloat coC, GLfloat distAB, GLfloat distBC, GLfloat distAC, GLfloat perimeter) {
	GLfloat value = (coA * distBC) + (coB * distAC) + (coC * distAB);
	return(value / perimeter);
}

void update(void)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	
	// code
	gfAngle += 0.1f;
	if (gfTriangleX >= 0.0f)
	{
		gfTriangleX -= 0.001f;
	}
	if (gfTriangleY <= 0.0f)
	{
		gfTriangleY += 0.001f;
	}
	if (gfCircleX <= 0.0f)
	{
		gfCircleX += 0.001f;
	}
	if (gfCircleY <= 0.0f)
	{
		gfCircleY += 0.001f;
	}
	if (gfLineY >= 0.0f)
	{
		gfLineY -= 0.001f;
	}
	if (gfAngle >= 360.0f)
	{
		gfAngle = 0.0f;
	}
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	// code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False, 
		StructureNotifyMask, 
		&xev);
}

void uninitialize(void)
{
	GLXContext currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}


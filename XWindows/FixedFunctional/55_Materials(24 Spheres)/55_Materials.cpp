#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

// OpenGL header files
#include<GL/gl.h>	// Change 1
#include<GL/glu.h>	// Change 1
#include<GL/glx.h>	// Change 1

#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64

// namespace
using namespace std;

// global variable declarations
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800; 
int giWindowHeight = 600; 
static GLXContext gGLXContext;

// Earth and Sun variables
GLUquadric *quadric_Sun = NULL;

// Animating variables
GLfloat gfAngle = 0.0f;

// Variables for Lights:
bool bLights = false;
GLfloat lightAmbiant[] = { 0.0f,0.0f, 0.0f, 1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
//GLfloat lightSpecular[] = { 1.0f,1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat light_model_ambiant[] = { 0.2f,0.2f, 0.2f, 1.0f };
GLfloat light_model_local_viewer[] = { 0.0f };
GLUquadric *quadric[24];
GLfloat angleOfXRotation = 0.0f;
GLfloat angleOfYRotation = 0.0f;
GLfloat angleOfZRotation = 0.0f;
GLint keyPressed = 0;

// entry-point function
int main(void)
{
	// function prototypes
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize(void);
	int initialize(void);
	void resize(int width, int height);
	void display(void);
	void update(void);
	
	// variable declarations
	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;
	char keys[26];
	bool bDone = false;

	// code
	CreateWindow();
	initialize();

	// Message loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay) )
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					break;

				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							break;

						default:
							break;
					}
				
				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
				switch(keys[0])
				{
					case 'F':
					case 'f':
						if(bFullscreen == false)
						{
							ToggleFullscreen();
							bFullscreen = true;
						}
						else
						{
							ToggleFullscreen();
							bFullscreen = false;
						}
						break;

					case 'l':
					case 'L':
						if (bLights == false)
						{
							bLights = true;
							glEnable(GL_LIGHTING);
						}
						else
						{
							bLights = false;
							glDisable(GL_LIGHTING);
						}
						break;

					case 'X':
					case 'x':
						keyPressed = 1;
						angleOfXRotation = 0.0f;
						break;

					case 'Y':
					case 'y':
						keyPressed = 2;
						angleOfYRotation = 0.0f;
						break;

					case 'Z':
					case 'z':
						keyPressed = 3;
						angleOfZRotation = 0.0f;
						break;

					default:
						break;
				}
					break;

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						
						case 2:
							break;

						case 3:
							break;
						
						case 4:
							break;
						
						case 5:
							break;

						default:
							break;
					}
					break;

				case MotionNotify:
					break;

				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;

				case Expose:
					break;

				case DestroyNotify:
					break;

				case 33:
					bDone = true;
					break;

				default:
					break;
			}
		}
		update();
		display();
	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[] = { GLX_RGBA, 
						GLX_DOUBLEBUFFER, True,
						GLX_RED_SIZE, 8, 
						GLX_GREEN_SIZE, 8, 
						GLX_BLUE_SIZE, 8, 
						GLX_ALPHA_SIZE, 8, 
						GLX_DEPTH_SIZE, 24, 
						None};

	// code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \n Exitting Now... \n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, 
					defaultScreen, 
					frameBufferAttributes);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable To Alocate Memory For Visual Info. \n Exitting Now...\n");
		uninitialize();
		exit(1);
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),
				gpXVisualInfo->visual,
				AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	winAttribs.event_mask = VisibilityChangeMask | KeyPressMask | ButtonPressMask 
				| PointerMotionMask | StructureNotifyMask | ExposureMask;
	
	styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;

	gWindow = XCreateWindow(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			0, 
			0, 
			giWindowWidth, 
			giWindowHeight, 
			0, 
			gpXVisualInfo->depth, 
			InputOutput, 
			gpXVisualInfo->visual, 
			styleMask, 
			&winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting now...\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "OpenGL 3D Texure");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

int initialize(void)
{
	// function prototypes
	void resize(int width, int height);

	// variable declarations
	
	// code
	gGLXContext = glXCreateContext(gpDisplay, 
					gpXVisualInfo,
					NULL, 
					GL_TRUE );
	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbiant);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambiant);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);
	glEnable(GL_LIGHT0);

	for (GLint i = 0; i < 24; i++)
	{
		quadric[i] = gluNewQuadric();
	}
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	resize(giWindowWidth, giWindowHeight);
	return(0);
}

void resize(int width, int height)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	
	// code
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width <= height)
	{
		glOrtho(0.0f,
			15.5f,
			0.0f,
			(15.5f * (GLfloat)height / (GLfloat)width),
			-10.0f,
			10.0f);
	}
	else
	{
		glOrtho(0.0f,
			(15.5f * (GLfloat)width / (GLfloat)height),
			0.0f,
			15.5f,
			-10.0f,
			10.0f);
	}
}

void display(void)
{
	// function prototypes
	GLvoid draw24Spheres(GLvoid);

	// variable declarations
	
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	if (keyPressed == 1)
	{
		glRotatef(angleOfXRotation, 1.0f, 0.0f, 0.0f);
		lightPosition[1] = angleOfXRotation;
	}
	else if (keyPressed == 2)
	{
		glRotatef(angleOfYRotation, 0.0f, 1.0f, 0.0f);
		lightPosition[2] = angleOfYRotation;
	}
	else if (keyPressed == 3)
	{
		glRotatef(angleOfZRotation, 0.0f, 0.0f, 1.0f);
		lightPosition[0] = angleOfZRotation;

	}
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	draw24Spheres();

	glXSwapBuffers(gpDisplay, gWindow);
}

void draw24Spheres(void) {
	GLfloat materialAmbiant[4];
	GLfloat materialDiffuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShininess[1];
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// emerald
	materialAmbiant[0] = 0.0215f;
	materialAmbiant[1] = 0.1745f;
	materialAmbiant[2] = 0.0215f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.07568f;
	materialDiffuse[1] = 0.61424f;
	materialDiffuse[2] = 0.07568f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.633f;
	materialSpecular[1] = 0.727811f;
	materialSpecular[2] = 0.633f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 14.0f, 0.0f);
	gluSphere(quadric[0], 1.0f, 30, 30);

	// jade
	materialAmbiant[0] = 0.135f;
	materialAmbiant[1] = 0.2225f;
	materialAmbiant[2] = 0.1575f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.54f;
	materialDiffuse[1] = 0.89f;
	materialDiffuse[2] = 0.63f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.316228f;
	materialSpecular[1] = 0.316228f;
	materialSpecular[2] = 0.316228f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.1 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 11.5f, 0.0f);
	gluSphere(quadric[1], 1.0f, 30, 30);

	// obsidian
	materialAmbiant[0] = 0.05375f;
	materialAmbiant[1] = 0.05f;
	materialAmbiant[2] = 0.06625f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.18275f;
	materialDiffuse[1] = 0.17f;
	materialDiffuse[2] = 0.22525f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.332741f;
	materialSpecular[1] = 0.328634f;
	materialSpecular[2] = 0.346435f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.3 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 9.0f, 0.0f);
	gluSphere(quadric[2], 1.0f, 30, 30);

	// pearl
	materialAmbiant[0] = 0.25f;
	materialAmbiant[1] = 0.20725f;
	materialAmbiant[2] = 0.20725f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 1.0f;
	materialDiffuse[1] = 0.829f;
	materialDiffuse[2] = 0.829f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.296648f;
	materialSpecular[1] = 0.296648f;
	materialSpecular[2] = 0.296648f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.088 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 6.5f, 0.0f);
	gluSphere(quadric[3], 1.0f, 30, 30);

	// ruby
	materialAmbiant[0] = 0.1745f;
	materialAmbiant[1] = 0.01175f;
	materialAmbiant[2] = 0.01175f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.61424f;
	materialDiffuse[1] = 0.04136f;
	materialDiffuse[2] = 0.04136f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.727811f;
	materialSpecular[1] = 0.626959f;
	materialSpecular[2] = 0.626959f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 4.0f, 0.0f);
	gluSphere(quadric[4], 1.0f, 30, 30);

	// turquoise
	materialAmbiant[0] = 0.1f;
	materialAmbiant[1] = 0.18725f;
	materialAmbiant[2] = 0.1745f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.396f;
	materialDiffuse[1] = 0.74151f;
	materialDiffuse[2] = 0.69102f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.297254f;
	materialSpecular[1] = 0.30829f;
	materialSpecular[2] = 0.306678f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.1 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 1.5f, 0.0f);
	gluSphere(quadric[5], 1.0f, 30, 30);

//---------------------------------------------------------------

// brass
	materialAmbiant[0] = 0.329412f;
	materialAmbiant[1] = 0.223529f;
	materialAmbiant[2] = 0.027451f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.780392f;
	materialDiffuse[1] = 0.568627f;
	materialDiffuse[2] = 0.113725f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.992157f;
	materialSpecular[1] = 0.941176f;
	materialSpecular[2] = 0.807843f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.21794872 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.0f, 14.0f, 0.0f);
	gluSphere(quadric[6], 1.0f, 30, 30);

	// bronze
	materialAmbiant[0] = 0.2125f;
	materialAmbiant[1] = 0.1275f;
	materialAmbiant[2] = 0.054f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.714f;
	materialDiffuse[1] = 0.4284f;
	materialDiffuse[2] = 0.18144f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.393548f;
	materialSpecular[1] = 0.271906f;
	materialSpecular[2] = 0.166721f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.2 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.0f, 11.5f, 0.0f);
	gluSphere(quadric[7], 1.0f, 30, 30);

	// chrome
	materialAmbiant[0] = 0.25f;
	materialAmbiant[1] = 0.25f;
	materialAmbiant[2] = 0.25f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.4f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.774597f;
	materialSpecular[1] = 0.774597f;
	materialSpecular[2] = 0.774597f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.0f, 9.0f, 0.0f);
	gluSphere(quadric[8], 1.0f, 30, 30);

	// copper
	materialAmbiant[0] = 0.19125f;
	materialAmbiant[1] = 0.0735f;
	materialAmbiant[2] = 0.0225f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.7038f;
	materialDiffuse[1] = 0.27048f;
	materialDiffuse[2] = 0.0828f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.256777f;
	materialSpecular[1] = 0.137622f;
	materialSpecular[2] = 0.086014f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.1 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.0f, 6.5f, 0.0f);
	gluSphere(quadric[9], 1.0f, 30, 30);

	// gold
	materialAmbiant[0] = 0.24725f;
	materialAmbiant[1] = 0.1995f;
	materialAmbiant[2] = 0.0745f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.75164f;
	materialDiffuse[1] = 0.60648f;
	materialDiffuse[2] = 0.22648f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.628281f;
	materialSpecular[1] = 0.555802f;
	materialSpecular[2] = 0.366065f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.4 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.0f, 4.0f, 0.0f);
	gluSphere(quadric[10], 1.0f, 30, 30);

	// silver
	materialAmbiant[0] = 0.19225f;
	materialAmbiant[1] = 0.19225f;
	materialAmbiant[2] = 0.19225f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.50754f;
	materialDiffuse[1] = 0.50754f;
	materialDiffuse[2] = 0.50754f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.508273f;
	materialSpecular[1] = 0.508273f;
	materialSpecular[2] = 0.508273f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.4 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.0f, 1.5f, 0.0f);
	gluSphere(quadric[11], 1.0f, 30, 30);

	//---------------------------------------------------------------

	// black
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.0f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.01f;
	materialDiffuse[1] = 0.01f;
	materialDiffuse[2] = 0.01f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.50f;
	materialSpecular[1] = 0.50f;
	materialSpecular[2] = 0.50f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 14.0f, 0.0f);
	gluSphere(quadric[12], 1.0f, 30, 30);

	// cyan
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.1f;
	materialAmbiant[2] = 0.06f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.0f;
	materialDiffuse[1] = 0.50980392f;
	materialDiffuse[2] = 0.50980392f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.50196078f;
	materialSpecular[1] = 0.50196078f;
	materialSpecular[2] = 0.50196078f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 11.5f, 0.0f);
	gluSphere(quadric[13], 1.0f, 30, 30);

	// green
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.0f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.1f;
	materialDiffuse[1] = 0.35f;
	materialDiffuse[2] = 0.1f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.45f;
	materialSpecular[1] = 0.55f;
	materialSpecular[2] = 0.45f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 9.0f, 0.0f);
	gluSphere(quadric[14], 1.0f, 30, 30);

	// red
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.0f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.0f;
	materialDiffuse[2] = 0.0f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.6f;
	materialSpecular[2] = 0.6f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 6.5f, 0.0f);
	gluSphere(quadric[15], 1.0f, 30, 30);

	// white
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.0f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.55f;
	materialDiffuse[1] = 0.55f;
	materialDiffuse[2] = 0.55f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.70f;
	materialSpecular[1] = 0.70f;
	materialSpecular[2] = 0.70f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 4.0f, 0.0f);
	gluSphere(quadric[16], 1.0f, 30, 30);

	// yellow
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.0f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.0f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.60f;
	materialSpecular[1] = 0.60f;
	materialSpecular[2] = 0.50f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(12.5f, 1.5f, 0.0f);
	gluSphere(quadric[17], 1.0f, 30, 30);

	//---------------------------------------------------------------

	// black
	materialAmbiant[0] = 0.02f;
	materialAmbiant[1] = 0.02f;
	materialAmbiant[2] = 0.02f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.01f;
	materialDiffuse[1] = 0.01f;
	materialDiffuse[2] = 0.01f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.4f;
	materialSpecular[1] = 0.4f;
	materialSpecular[2] = 0.4f;
	materialSpecular[3] = 0.4f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.0f, 14.0f, 0.0f);
	gluSphere(quadric[18], 1.0f, 30, 30);

	// cyan
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.05f;
	materialAmbiant[2] = 0.05f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.5f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.4f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.7f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.0f, 11.5f, 0.0f);
	gluSphere(quadric[19], 1.0f, 30, 30);

	// green
	materialAmbiant[0] = 0.0f;
	materialAmbiant[1] = 0.05f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.4f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.04f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.0f, 9.0f, 0.0f);
	gluSphere(quadric[20], 1.0f, 30, 30);

	// red
	materialAmbiant[0] = 0.05f;
	materialAmbiant[1] = 0.0f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.4f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.04f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.0f, 6.5f, 0.0f);
	gluSphere(quadric[21], 1.0f, 30, 30);

	// white
	materialAmbiant[0] = 0.05f;
	materialAmbiant[1] = 0.05f;
	materialAmbiant[2] = 0.05f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.5f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.7f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.0f, 4.0f, 0.0f);
	gluSphere(quadric[22], 1.0f, 30, 30);

	// yellow
	materialAmbiant[0] = 0.05f;
	materialAmbiant[1] = 0.05f;
	materialAmbiant[2] = 0.0f;
	materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbiant);

	materialDiffuse[0] = 0.5f;
	materialDiffuse[1] = 0.5f;
	materialDiffuse[2] = 0.4f;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.7f;
	materialSpecular[1] = 0.7f;
	materialSpecular[2] = 0.04f;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	materialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(18.0f, 1.5f, 0.0f);
	gluSphere(quadric[23], 1.0f, 30, 30);

}

void update(void)
{
	// function prototypes

	// variable declarations
	
	// code
	angleOfXRotation -= 0.5f;
	if (angleOfXRotation > 360.0f)
	{
		angleOfXRotation += 360.0f;
	}
	angleOfYRotation -= 0.5f;
	if (angleOfYRotation > 360.0f)
	{
		angleOfYRotation += 360.0f;
	}
	angleOfZRotation -= 0.5f;
	if (angleOfZRotation > 360.0f)
	{
		angleOfZRotation += 360.0f;
	}
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	// code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False, 
		StructureNotifyMask, 
		&xev);
}

void uninitialize(void)
{
	GLXContext currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
	for (GLint i = 0; i < 24; i++)
	{
		if (quadric[i])
		{
			gluDeleteQuadric(quadric[i]);
		}
	}
}


#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include "Sphere.h"

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

// OpenGL header files
#include<GL/glew.h>	// Change 1
#include<GL/gl.h>	// Change 1
#include<GL/glx.h>	// Change 1

// Texure header
#include<SOIL/SOIL.h>

// VMath
#include"vmath.h"

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// namespace
using namespace std;
using namespace vmath;

// global variable declarations
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
// New addOn variables
typedef GLXContext (*GLXCreateContextAttribsARBProc)(Display *, GLXFBConfig, GLXContext, Bool, const int *) ;
GLXCreateContextAttribsARBProc GLXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;

int giWindowWidth = 800; 
int giWindowHeight = 600; 
static GLXContext gGLXContext;
GLint gResult;

// Sphere variables:
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gNumVertices;
GLuint gNumElements;

GLuint vao_Sphare;
GLuint vbo_position_sphere;
GLuint vbo_normal_sphere;
GLuint vbo_element_sphere;

// PerVertex Variables
GLuint modelUniformPerVertex;
GLuint viewUniformPerVertex;
GLuint projectionUniformPerVertex;

GLuint laUniformRedPerVertex;
GLuint ldUniformRedPerVertex;
GLuint lsUniformRedPerVertex;
GLuint lightPositionUniformRedPerVertex;

GLuint laUniformGreenPerVertex;
GLuint ldUniformGreenPerVertex;
GLuint lsUniformGreenPerVertex;
GLuint lightPositionUniformGreenPerVertex;

GLuint laUniformBluePerVertex;
GLuint ldUniformBluePerVertex;
GLuint lsUniformBluePerVertex;
GLuint lightPositionUniformBluePerVertex;

GLuint kaUniformPerVertex;
GLuint kdUniformPerVertex;
GLuint ksUniformPerVertex;
GLuint materialShinynessUniformPerVertex;
GLuint lKeyPressedUniformPerVertex;

// PerVertex Shader variables
GLuint gVertexShaderObjectPerVertex;
GLuint gFragmentShaderObjectPerVertex;
GLuint gShaderProgramObjectPerVertex;

// PerFragment Variables
GLuint modelUniformPerFragment;
GLuint viewUniformPerFragment;
GLuint projectionUniformPerFragment;

GLuint laUniformRedPerFragment;
GLuint ldUniformRedPerFragment;
GLuint lsUniformRedPerFragment;
GLuint lightPositionUniformRedPerFragment;

GLuint laUniformGreenPerFragment;
GLuint ldUniformGreenPerFragment;
GLuint lsUniformGreenPerFragment;
GLuint lightPositionUniformGreenPerFragment;

GLuint laUniformBluePerFragment;
GLuint ldUniformBluePerFragment;
GLuint lsUniformBluePerFragment;
GLuint lightPositionUniformBluePerFragment;

GLuint kaUniformPerFragment;
GLuint kdUniformPerFragment;
GLuint ksUniformPerFragment;
GLuint materialShinynessUniformPerFragment;
GLuint lKeyPressedUniformPerFragment;

// PerFragment Shader variables
GLuint gVertexShaderObjectPerFragment;
GLuint gFragmentShaderObjectPerFragment;
GLuint gShaderProgramObjectPerFragment;
bool gbFragmentShaderToggel = false;

mat4 perspectiveProjectionMatrix;

// Animating variables

// Light Variables
bool isLKeyPressed = false;
bool gbLight = false;
GLfloat lightAmbiantZero[] = { 0.0f,0.0f, 0.0f, 1.0f };
GLfloat lightDiffuseZero[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightSpecularZero[] = { 1.0f,0.0f, 0.0f, 1.0f };
GLfloat lightPositionZero[] = { -2.0f,0.0f,0.0f,1.0f };

GLfloat lightAmbiantOne[] = { 0.0f,0.0f, 0.0f, 1.0f };
GLfloat lightDiffuseOne[] = { 0.0f,1.0f,0.0f,1.0f };
GLfloat lightSpecularOne[] = { 0.0f,1.0f, 0.0f, 1.0f };
GLfloat lightPositionOne[] = { 0.0f,0.0f, 1.0f, 1.0f };

GLfloat lightAmbiantTwo[] = { 0.0f,0.0f, 0.0f, 1.0f };
GLfloat lightDiffuseTwo[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat lightSpecularTwo[] = { 0.0f,0.0f, 1.0f, 1.0f };
GLfloat lightPositionTwo[] = { 0.0f,0.0f, 1.0f, 1.0f };

GLfloat materialAmbiant[] = { 0.0f,0.0f, 0.0f, 1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f, 1.0f, 1.0f };
GLfloat materialShinyness = 128.0f;

// Animating variables
GLfloat lightAngleZero = 0.0f;
GLfloat lightAngleOne = 0.0f;
GLfloat lightAngleTwo = 0.0f;

// entry-point function
int main(void)
{
	// function prototypes
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize(void);
	int initialize(void);
	void resize(int width, int height);
	void display(void);
	void update(void);
	
	// variable declarations
	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;
	char keys[26];
	bool bDone = false;

	// code
	CreateWindow();
	initialize();

	// Message loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay) )
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					break;

				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							if(bFullscreen == false)
							{
								ToggleFullscreen();
								bFullscreen = true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen = false;
							}
							break;

						default:
							break;
					}
				
				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
				switch(keys[0])
				{
					case 'l':
					case 'L':
						if (isLKeyPressed == false)
						{
							gbLight = true;
							isLKeyPressed = true;
						}
						else
						{
							gbLight = false;
							isLKeyPressed = false;
						}
						break;

					case 'e':
					case 'E':
						bDone = true;
						break;

					case 'f':
					case 'F':
						gbFragmentShaderToggel = true;
						break;

					case 'v':
					case 'V':
						gbFragmentShaderToggel = false;
						break;
					default:
						break;
				}
					break;

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						
						case 2:
							break;

						case 3:
							break;
						
						case 4:
							break;
						
						case 5:
							break;

						default:
							break;
					}
					break;

				case MotionNotify:
					break;

				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;

				case Expose:
					break;

				case DestroyNotify:
					break;

				case 33:
					bDone = true;
					break;

				default:
					break;
			}
		}
		update();
		display();
	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[] = {  
						GLX_X_RENDERABLE, True,
						GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
						GLX_RENDER_TYPE, GLX_RGBA_BIT,
						GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
						
						GLX_DOUBLEBUFFER, True,
						GLX_RED_SIZE, 8, 
						GLX_GREEN_SIZE, 8, 
						GLX_BLUE_SIZE, 8, 
						GLX_ALPHA_SIZE, 8, 
						GLX_DEPTH_SIZE, 24, 
						
						GLX_STENCIL_SIZE, 8, 
						None};
	// New 4 local variables
	GLXFBConfig *pGLXFBConfig = NULL; // Array of FBConfig (Internally L.L.)
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNoOfFBConfigs = 0;

	// code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \n Exitting Now... \n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	
	// New addOn
	pGLXFBConfig = glXChooseFBConfig(gpDisplay, 
					defaultScreen, 
					frameBufferAttributes, 
					&iNoOfFBConfigs);
	printf("There are %d No Of FBConfig",iNoOfFBConfigs);
	
	int bestFrameBufferConfig = -1;
	int bestNoOfSamples = -1;
	int worstFrameBufferConfig = -1;
	int worstNoOfSamples = 999;
	
	for(int i = 0; i < iNoOfFBConfigs; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, 
							pGLXFBConfig[i]);
		if(pTempXVisualInfo)
		{
			int sampleBuffers;
			int samples;
			glXGetFBConfigAttrib(gpDisplay,
						pGLXFBConfig[i], 
						GLX_SAMPLE_BUFFERS, 
						&sampleBuffers);
			glXGetFBConfigAttrib(gpDisplay, 
						pGLXFBConfig[i], 
						GLX_SAMPLES,
						&samples);
			
			// To get Best From All
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNoOfSamples)
			{
				bestFrameBufferConfig = i;
				bestNoOfSamples = samples;
			}
			// To Get Worst From All
			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNoOfSamples) // To Check
			{
				worstFrameBufferConfig = i;
				worstNoOfSamples = samples;
			}
		} // for(pTempXVisualInfo) end
		XFree(pTempXVisualInfo);
	} // for end
	
	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
	
	gGLXFBConfig = bestGLXFBConfig;
	
	XFree(pGLXFBConfig);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, 
					bestGLXFBConfig);
	// ------- End Of Step A ---------
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable To Alocate Memory For Visual Info. \n Exitting Now...\n");
		uninitialize();
		exit(1);
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),
				gpXVisualInfo->visual,
				AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	winAttribs.event_mask = VisibilityChangeMask | KeyPressMask | ButtonPressMask 
				| PointerMotionMask | StructureNotifyMask | ExposureMask;
	
	styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;

	gWindow = XCreateWindow(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			0, 
			0, 
			giWindowWidth, 
			giWindowHeight, 
			0, 
			gpXVisualInfo->depth, 
			InputOutput, 
			gpXVisualInfo->visual, 
			styleMask, 
			&winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting now...\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "OpenGL 3D Texure");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

int initialize(void)
{
	// function prototypes
	void resize(int width, int height);
	GLvoid uninitialize(GLvoid);

	// variable declarations
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	GLchar *szInfoLog = NULL;
	
	// code
	// New AddOn
	GLXCreateContextAttribsARB = (GLXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
	if(GLXCreateContextAttribsARB == NULL)
	{
		printf("glXCreateContextAttribsARB Failed\n");
	}
	
	const int attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4, 
		GLX_CONTEXT_MINOR_VERSION_ARB, 5, 
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB, 
		None };
	// Now Get The Context
	gGLXContext = GLXCreateContextAttribsARB(gpDisplay,
						gGLXFBConfig, 
						0, 
						true, 
						attribs);
	if(!gGLXContext)
	{
		const int attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1, 
			GLX_CONTEXT_MINOR_VERSION_ARB, 0, 
			None };
		gGLXContext = GLXCreateContextAttribsARB(gpDisplay,
							gGLXFBConfig, 
							0, 
							true, 
							attribs);
	}
	if(!glXIsDirect(gpDisplay, 
			gGLXContext) )
	{
		printf("Obtained Context Not H/W randering Context");
	}
	else
	{
		printf("Obtained Context H/W randering Context");
	}
	
/*	gGLXContext = glXCreateContext(gpDisplay, 
					gpXVisualInfo,
					NULL, 
					GL_TRUE );
*/	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	gResult = glewInit();
	if(gResult != GLEW_OK)
	{
		printf("glewINit() failed\n");
		uninitialize();
		exit(0);
	}

	// VERTEX SHADER PerVertex
	// Define Vertex Shader Object
	gVertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCodePerVertex =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_la_red;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_ls_red;" \
		"uniform vec4 u_light_position_red;" \
		"uniform vec3 u_la_green;" \
		"uniform vec3 u_ld_green;" \
		"uniform vec3 u_ls_green;" \
		"uniform vec4 u_light_position_green;" \
		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_ls_blue;" \
		"uniform vec4 u_light_position_blue;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_shininess;"	\
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{" \
		"		vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"	\
		"		vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal );" \
		"		vec3 viewer_vector = normalize(-eye_coordinates.xyz);" \

		"		vec3 light_direction_red = normalize(vec3(u_light_position_red) - eye_coordinates.xyz );" \
		"		vec3 reflection_vector_red = reflect(-light_direction_red, transformed_normals);" \
		"		float tn_dot_ld_red = max(dot(transformed_normals, light_direction_red), 0.0);" \
		"		vec3 ambient_red = u_la_red * u_ka;" \
		"		vec3 diffuse_red =u_ld_red * u_kd * tn_dot_ld_red;" \
		"		vec3 specular_red = u_ls_red * u_ks * pow(max(dot(reflection_vector_red, viewer_vector), 0.0), u_shininess);" \

		"		vec3 light_direction_green = normalize(vec3(u_light_position_green) - eye_coordinates.xyz );" \
		"		vec3 reflection_vector_green = reflect(-light_direction_green, transformed_normals);" \
		"		float tn_dot_ld_green = max(dot(transformed_normals, light_direction_green), 0.0);" \
		"		vec3 ambient_green = u_la_green * u_ka;" \
		"		vec3 diffuse_green =u_ld_green * u_kd * tn_dot_ld_green;" \
		"		vec3 specular_green = u_ls_green * u_ks * pow(max(dot(reflection_vector_green, viewer_vector), 0.0), u_shininess);" \

		"		vec3 light_direction_blue = normalize(vec3(u_light_position_blue) - eye_coordinates.xyz );" \
		"		vec3 reflection_vector_blue = reflect(-light_direction_blue, transformed_normals);" \
		"		float tn_dot_ld_blue = max(dot(transformed_normals, light_direction_blue), 0.0);" \
		"		vec3 ambient_blue = u_la_blue * u_ka;" \
		"		vec3 diffuse_blue =u_ld_blue * u_kd * tn_dot_ld_blue;" \
		"		vec3 specular_blue = u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue, viewer_vector), 0.0), u_shininess);" \

		"		phong_ads_light = ambient_red + diffuse_red + specular_red;" \
		"		phong_ads_light = phong_ads_light + ambient_green + diffuse_green + specular_green;" \
		"		phong_ads_light = phong_ads_light + ambient_blue + diffuse_blue + specular_blue;" \
		"	}" \
		"	else" \
		"	{" \
		"		phong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"	}" \
		"	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObjectPerVertex, 1, (const GLchar **)&vertexShaderSourceCodePerVertex, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObjectPerVertex);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerVertex, iInfoLength, &written, szInfoLog);
				printf("Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER PerVertex
	// Define Fragment Shader Object
	gFragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCodePerVertex =
		"#version 450 core" \
		"\n" \
		"in vec3 phong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"	FragColor = vec4(phong_ads_light, 1.0);"	\
		"}";

	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObjectPerVertex, 1, (const GLchar **)&fragmentShaderSourceCodePerVertex, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObjectPerVertex);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerVertex, iInfoLength, &written, szInfoLog);
				printf("Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObjectPerVertex = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObjectPerVertex, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObjectPerVertex, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObjectPerVertex);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObjectPerVertex, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLength, &written, szInfoLog);
				printf("Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
	modelUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_model_matrix");
	viewUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_view_matrix");
	projectionUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_projection_matrix");

	lKeyPressedUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_lKeyPressed");

	laUniformRedPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_la_red");
	ldUniformRedPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ld_red");
	lsUniformRedPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ls_red");
	lightPositionUniformRedPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position_red");

	laUniformGreenPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_la_green");
	ldUniformGreenPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ld_green");
	lsUniformGreenPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ls_green");
	lightPositionUniformGreenPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position_green");

	laUniformBluePerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_la_blue");
	ldUniformBluePerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ld_blue");
	lsUniformBluePerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ls_blue");
	lightPositionUniformBluePerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position_blue");

	kaUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ka");
	kdUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_kd");
	ksUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ks");

	materialShinynessUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_shininess");

	// VERTEX SHADER PerFragment
	// Define Vertex Shader Object
	gVertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCodePerFragment =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \

		"uniform vec4 u_light_position_red;" \
		"uniform vec4 u_light_position_green;" \
		"uniform vec4 u_light_position_blue;" \
		"uniform int u_lKeyPressed;" \
		"out vec3 tNorm;" \

		"out vec3 light_direction_red;" \
		"out vec3 light_direction_green;" \
		"out vec3 light_direction_blue;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{" \
		"		vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"	\
		"		tNorm = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"		light_direction_red = vec3(u_light_position_red) - eye_coordinates.xyz;" \
		"		light_direction_green = vec3(u_light_position_green) - eye_coordinates.xyz;" \
		"		light_direction_blue = vec3(u_light_position_blue) - eye_coordinates.xyz;" \
		"		viewer_vector = vec3(-eye_coordinates.xyz);" \
		"	}" \
		"	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObjectPerFragment, 1, (const GLchar **)&vertexShaderSourceCodePerFragment, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObjectPerFragment);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObjectPerFragment, iInfoLength, &written, szInfoLog);
				printf("Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER PerFragment
	// Define Fragment Shader Object
	gFragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCodePerFragment =
		"#version 450 core" \
		"\n" \
		"in vec3 tNorm;" \
		"in vec3 light_direction_red;" \
		"in vec3 light_direction_green;" \
		"in vec3 light_direction_blue;" \

		"in vec3 viewer_vector;" \
		"uniform vec3 u_la_red;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_ls_red;" \

		"uniform vec3 u_la_green;" \
		"uniform vec3 u_ld_green;" \
		"uniform vec3 u_ls_green;" \

		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_ls_blue;" \

		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_shininess;"	\
		"uniform int u_lKeyPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{" \
		"		vec3 ntNorm = normalize(tNorm);" \
		"		vec3 nviewer_vector  = normalize(viewer_vector);" \

		"		vec3 nlight_direction_red = normalize(light_direction_red);" \
		"		vec3 reflection_vector_red  = reflect(-nlight_direction_red, ntNorm);" \
		"		float tn_dot_ld_red = max(dot(ntNorm, nlight_direction_red), 0.0);" \
		"		vec3 ambient_red = u_la_red * u_ka;" \
		"		vec3 diffuse_red =u_ld_red * u_kd * tn_dot_ld_red;" \
		"		vec3 specular_red = u_ls_red * u_ks * pow(max(dot(reflection_vector_red, nviewer_vector), 0.0), u_shininess);" \

		"		vec3 nlight_direction_green = normalize(light_direction_green);" \
		"		vec3 reflection_vector_green  = reflect(-nlight_direction_green, ntNorm);" \
		"		float tn_dot_ld_green = max(dot(ntNorm, nlight_direction_green), 0.0);" \
		"		vec3 ambient_green = u_la_green * u_ka;" \
		"		vec3 diffuse_green =u_ld_green * u_kd * tn_dot_ld_green;" \
		"		vec3 specular_green = u_ls_green * u_ks * pow(max(dot(reflection_vector_green, nviewer_vector), 0.0), u_shininess);" \

		"		vec3 nlight_direction_blue = normalize(light_direction_blue);" \
		"		vec3 reflection_vector_blue  = reflect(-nlight_direction_blue, ntNorm);" \
		"		float tn_dot_ld_blue = max(dot(ntNorm, nlight_direction_blue), 0.0);" \
		"		vec3 ambient_blue = u_la_blue * u_ka;" \
		"		vec3 diffuse_blue =u_ld_blue * u_kd * tn_dot_ld_blue;" \
		"		vec3 specular_blue = u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue, nviewer_vector), 0.0), u_shininess);" \

		"		vec3 phong_ads_light = ambient_red + diffuse_red + specular_red;" \
		"		phong_ads_light = phong_ads_light + ambient_green + diffuse_green + specular_green;" \
		"		phong_ads_light = phong_ads_light + ambient_blue + diffuse_blue + specular_blue;" \
		"		FragColor = vec4(phong_ads_light, 1.0);"	\
		"	}" \
		"	else" \
		"	{" \
		"		FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"	}" \
		"}";

	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObjectPerFragment, 1, (const GLchar **)&fragmentShaderSourceCodePerFragment, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObjectPerFragment);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObjectPerFragment, iInfoLength, &written, szInfoLog);
				printf("Fragment Shader Log Per Fragment : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObjectPerFragment = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObjectPerFragment, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObjectPerFragment, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObjectPerFragment);

	// Error checking
	iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObjectPerFragment, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObjectPerFragment, iInfoLength, &written, szInfoLog);
				printf("Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
	modelUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_model_matrix");
	viewUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_view_matrix");
	projectionUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_projection_matrix");

	lKeyPressedUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lKeyPressed");

	laUniformRedPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_la_red");
	ldUniformRedPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ld_red");
	lsUniformRedPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ls_red");
	lightPositionUniformRedPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position_red");

	laUniformGreenPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_la_green");
	ldUniformGreenPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ld_green");
	lsUniformGreenPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ls_green");
	lightPositionUniformGreenPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position_green");

	laUniformBluePerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_la_blue");
	ldUniformBluePerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ld_blue");
	lsUniformBluePerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ls_blue");
	lightPositionUniformBluePerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position_blue");

	kaUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ka");
	kdUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_kd");
	ksUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ks");

	materialShinynessUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_shininess");

	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// Create vao_square
	glGenVertexArrays(1, &vao_Sphare);
	glBindVertexArray(vao_Sphare);

	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);

	glBufferData(GL_ARRAY_BUFFER,
		sizeof(sphere_vertices),
		sphere_vertices,
		GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glBindVertexArray(0);

	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(sphere_normals),
		sphere_normals,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glGenBuffers(1, &vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		sizeof(sphere_elements),
		sphere_elements,
		GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glVertexAttrib3f(AMC_ATTRIBUTE_TEXCOORD0, 0.0f, 0.0f, 1.0f);
	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);	//to remove aliasing

	perspectiveProjectionMatrix = mat4::identity();

	resize(giWindowWidth, giWindowHeight);
	return(0);
}

void resize(int width, int height)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	
	// code
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display(void)
{
	// function prototypes

	// variable declarations
	
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (gbFragmentShaderToggel)
	{
		glUseProgram(gShaderProgramObjectPerFragment);
	}
	else
	{
		glUseProgram(gShaderProgramObjectPerVertex);
	}

	// SQUARE
	// Declaration of matrices
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 modelViewProjectionMatrix;

	// Initialize above matrices to identity
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelMatrix = translate(0.0f,
		0.0f,
		-2.0f);

	// Do necessary Matrix Multiplication

	// This was internally donw by gluOrtho2d() in FFP

	// Send Necessary matrix to shader in respective uniform
	if (gbFragmentShaderToggel)
	{
		glUniformMatrix4fv(modelUniformPerFragment,
			1,
			GL_FALSE,
			modelMatrix);

		glUniformMatrix4fv(viewUniformPerFragment,
			1,
			GL_FALSE,
			viewMatrix);

		glUniformMatrix4fv(projectionUniformPerFragment,
			1,
			GL_FALSE,
			perspectiveProjectionMatrix);

		glUniform3fv(laUniformRedPerFragment, 1, lightAmbiantZero);
		glUniform3fv(ldUniformRedPerFragment, 1, lightDiffuseZero);
		glUniform3fv(lsUniformRedPerFragment, 1, lightSpecularZero);
		float lightPosition[] = {
			0.0f, 100.0f*cos(lightAngleZero), 100.0f*sin(lightAngleZero), 1.0f
		};

		glUniform4fv(lightPositionUniformRedPerFragment, 1, (GLfloat*)lightPosition);

		glUniform3fv(laUniformGreenPerFragment, 1, lightAmbiantOne);
		glUniform3fv(ldUniformGreenPerFragment, 1, lightDiffuseOne);
		glUniform3fv(lsUniformGreenPerFragment, 1, lightSpecularOne);
		float lightPositionOne[] = {
			100.0f*cos(lightAngleOne), 0.0f, 100.0f*sin(lightAngleOne), 1.0f
		};

		glUniform4fv(lightPositionUniformGreenPerFragment, 1, (GLfloat*)lightPositionOne);

		glUniform3fv(laUniformBluePerFragment, 1, lightAmbiantTwo);
		glUniform3fv(ldUniformBluePerFragment, 1, lightDiffuseTwo);
		glUniform3fv(lsUniformBluePerFragment, 1, lightSpecularTwo);
		float lightPositionTwo[] = {
			100.0f*cos(lightAngleTwo), 100.0f*sin(lightAngleTwo), 0.0f, 1.0f
		};

		glUniform4fv(lightPositionUniformBluePerFragment, 1, (GLfloat*)lightPositionTwo);

		glUniform3fv(kaUniformPerFragment, 1, materialAmbiant);
		glUniform3fv(kdUniformPerFragment, 1, materialDiffuse);
		glUniform3fv(ksUniformPerFragment, 1, materialSpecular);
		glUniform1f(materialShinynessUniformPerFragment, materialShinyness);


		if (gbLight == true)
		{
			glUniform1i(lKeyPressedUniformPerFragment, 1);
		}
		else
		{
			glUniform1i(lKeyPressedUniformPerFragment, 0);
		}
	}
	else
	{
		glUniformMatrix4fv(modelUniformPerVertex,
			1,
			GL_FALSE,
			modelMatrix);

		glUniformMatrix4fv(viewUniformPerVertex,
			1,
			GL_FALSE,
			viewMatrix);

		glUniformMatrix4fv(projectionUniformPerVertex,
			1,
			GL_FALSE,
			perspectiveProjectionMatrix);

		glUniform3fv(laUniformRedPerVertex, 1, lightAmbiantZero);
		glUniform3fv(ldUniformRedPerVertex, 1, lightDiffuseZero);
		glUniform3fv(lsUniformRedPerVertex, 1, lightSpecularZero);
		float lightPositionZero[] = {
			0.0f, 100.0f*cos(lightAngleZero), 100.0f*sin(lightAngleZero), 1.0f
		};

		glUniform4fv(lightPositionUniformRedPerVertex, 1, (GLfloat*)lightPositionZero);

		glUniform3fv(laUniformGreenPerVertex, 1, lightAmbiantZero);
		glUniform3fv(ldUniformGreenPerVertex, 1, lightDiffuseZero);
		glUniform3fv(lsUniformGreenPerVertex, 1, lightSpecularZero);
		float lightPositionOne[] = {
			100.0f*cos(lightAngleOne), 0.0f, 100.0f*sin(lightAngleOne), 1.0f
		};

		glUniform4fv(lightPositionUniformGreenPerVertex, 1, (GLfloat*)lightPositionOne);

		glUniform3fv(laUniformGreenPerVertex, 1, lightAmbiantTwo);
		glUniform3fv(ldUniformGreenPerVertex, 1, lightDiffuseTwo);
		glUniform3fv(lsUniformGreenPerVertex, 1, lightSpecularTwo);
		float lightPositionTwo[] = {
			100.0f*cos(lightAngleTwo), 100.0f*sin(lightAngleTwo), 0.0f, 1.0f
		};

		glUniform4fv(lightPositionUniformBluePerVertex, 1, (GLfloat*)lightPositionTwo);

		glUniform3fv(kaUniformPerVertex, 1, materialAmbiant);
		glUniform3fv(kdUniformPerVertex, 1, materialDiffuse);
		glUniform3fv(ksUniformPerVertex, 1, materialSpecular);
		glUniform1f(materialShinynessUniformPerVertex, materialShinyness);


		if (gbLight == true)
		{
			glUniform1i(lKeyPressedUniformPerVertex, 1);
		}
		else
		{
			glUniform1i(lKeyPressedUniformPerVertex, 0);
		}
	}

	// Bind with vao
	glBindVertexArray(vao_Sphare);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// Unbind vao
	glBindVertexArray(0);

	// Unuse Program
	glUseProgram(0);

	glXSwapBuffers(gpDisplay, gWindow);
}

void update(void)
{
	// function prototypes

	// variable declarations
	
	// code
	lightAngleZero -= 0.0005f;
	if (lightAngleZero > 360.0f)
	{
		lightAngleZero += 360.0f;
	}
	lightAngleOne -= 0.0005f;
	if (lightAngleOne > 360.0f)
	{
		lightAngleOne += 360.0f;
	}
	lightAngleTwo -= 0.0005f;
	if (lightAngleTwo > 360.0f)
	{
		lightAngleTwo += 360.0f;
	}
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	// code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False, 
		StructureNotifyMask, 
		&xev);
}

void uninitialize(void)
{
	GLXContext currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}


#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#include "Sphere.h"

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

// OpenGL header files
#include<GL/glew.h>	// Change 1
#include<GL/gl.h>	// Change 1
#include<GL/glx.h>	// Change 1

// Texure header
#include<SOIL/SOIL.h>

// VMath
#include"vmath.h"

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// namespace
using namespace std;
using namespace vmath;

// global variable declarations
struct SphereMaterial
{
	GLfloat materialAmbient[4];
	GLfloat materialDiffuse[4];
	GLfloat materialSpecular[4];
	GLfloat materialShininess;
};

bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
// New addOn variables
typedef GLXContext (*GLXCreateContextAttribsARBProc)(Display *, GLXFBConfig, GLXContext, Bool, const int *) ;
GLXCreateContextAttribsARBProc GLXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;

int giWindowWidth = 800; 
int giWindowHeight = 600; 
static GLXContext gGLXContext;
GLint gResult;

// Shader variables
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

// Sphere variables:
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

GLuint gNumVertices;
GLuint gNumElements;

GLuint vao_sphere;
GLuint vbo_position_sphere;
GLuint vbo_normal_sphere;
GLuint vbo_element_sphere;

// Uniforms
GLuint mUniform;
GLuint vUniform;
GLuint pUniform;

GLuint laUniform;
GLuint ldUniform;
GLuint lsUniform;
GLuint lightPositionUniform;

GLuint kaUniform;
GLuint kdUniform;
GLuint ksUniform;
GLuint materialShininessUniform;

GLuint lKeyPressedUniform;

mat4 perspectiveProjectionMatrix;

// Animating variables
GLfloat angleOfXRotation = 0.0f;
GLfloat angleOfYRotation = 0.0f;
GLfloat angleOfZRotation = 0.0f;
GLint keyPressed = 0;

GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat materialShininess = 128.0f;
const int MATERIAL_ROWS = 6;
const int MATERIAL_COLUMNS = 4;

// Light Variables
bool isLKeyPressed = false;
bool gbLight = false;

// Viewport Variables
GLuint gX;
GLuint gY;
GLuint gWidth;
GLuint gHeight;

SphereMaterial material[MATERIAL_ROWS][MATERIAL_COLUMNS];

// entry-point function
int main(void)
{
	// function prototypes
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize(void);
	int initialize(void);
	void resize(int width, int height);
	void display(void);
	void update(void);
	
	// variable declarations
	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;
	char keys[26];
	bool bDone = false;

	// code
	CreateWindow();
	initialize();

	// Message loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay) )
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					break;

				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							break;

						default:
							break;
					}
				
				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
				switch(keys[0])
				{
					case 'F':
					case 'f':
						if(bFullscreen == false)
						{
							ToggleFullscreen();
							bFullscreen = true;
						}
						else
						{
							ToggleFullscreen();
							bFullscreen = false;
						}
						break;
					case 'l':
					case 'L':
						if (isLKeyPressed == false)
						{
							gbLight = true;
							isLKeyPressed = true;
						}
						else
						{
							gbLight = false;
							isLKeyPressed = false;
						}
						break;
					case 'X':
					case 'x':
						keyPressed = 1;
						angleOfXRotation = 0.0f;
						break;

					case 'Y':
					case 'y':
						keyPressed = 2;
						angleOfYRotation = 0.0f;
						break;

					case 'Z':
					case 'z':
						keyPressed = 3;
						angleOfZRotation = 0.0f;
						break;
					default:
						break;
				}
					break;

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						
						case 2:
							break;

						case 3:
							break;
						
						case 4:
							break;
						
						case 5:
							break;

						default:
							break;
					}
					break;

				case MotionNotify:
					break;

				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;

				case Expose:
					break;

				case DestroyNotify:
					break;

				case 33:
					bDone = true;
					break;

				default:
					break;
			}
		}
		update();
		display();
	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[] = {  
						GLX_X_RENDERABLE, True,
						GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
						GLX_RENDER_TYPE, GLX_RGBA_BIT,
						GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
						
						GLX_DOUBLEBUFFER, True,
						GLX_RED_SIZE, 8, 
						GLX_GREEN_SIZE, 8, 
						GLX_BLUE_SIZE, 8, 
						GLX_ALPHA_SIZE, 8, 
						GLX_DEPTH_SIZE, 24, 
						
						GLX_STENCIL_SIZE, 8, 
						None};
	// New 4 local variables
	GLXFBConfig *pGLXFBConfig = NULL; // Array of FBConfig (Internally L.L.)
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNoOfFBConfigs = 0;

	// code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \n Exitting Now... \n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	
	// New addOn
	pGLXFBConfig = glXChooseFBConfig(gpDisplay, 
					defaultScreen, 
					frameBufferAttributes, 
					&iNoOfFBConfigs);
	printf("There are %d No Of FBConfig",iNoOfFBConfigs);
	
	int bestFrameBufferConfig = -1;
	int bestNoOfSamples = -1;
	int worstFrameBufferConfig = -1;
	int worstNoOfSamples = 999;
	
	for(int i = 0; i < iNoOfFBConfigs; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, 
							pGLXFBConfig[i]);
		if(pTempXVisualInfo)
		{
			int sampleBuffers;
			int samples;
			glXGetFBConfigAttrib(gpDisplay,
						pGLXFBConfig[i], 
						GLX_SAMPLE_BUFFERS, 
						&sampleBuffers);
			glXGetFBConfigAttrib(gpDisplay, 
						pGLXFBConfig[i], 
						GLX_SAMPLES,
						&samples);
			
			// To get Best From All
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNoOfSamples)
			{
				bestFrameBufferConfig = i;
				bestNoOfSamples = samples;
			}
			// To Get Worst From All
			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNoOfSamples) // To Check
			{
				worstFrameBufferConfig = i;
				worstNoOfSamples = samples;
			}
		} // for(pTempXVisualInfo) end
		XFree(pTempXVisualInfo);
	} // for end
	
	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
	
	gGLXFBConfig = bestGLXFBConfig;
	
	XFree(pGLXFBConfig);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, 
					bestGLXFBConfig);
	// ------- End Of Step A ---------
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable To Alocate Memory For Visual Info. \n Exitting Now...\n");
		uninitialize();
		exit(1);
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),
				gpXVisualInfo->visual,
				AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	winAttribs.event_mask = VisibilityChangeMask | KeyPressMask | ButtonPressMask 
				| PointerMotionMask | StructureNotifyMask | ExposureMask;
	
	styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;

	gWindow = XCreateWindow(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			0, 
			0, 
			giWindowWidth, 
			giWindowHeight, 
			0, 
			gpXVisualInfo->depth, 
			InputOutput, 
			gpXVisualInfo->visual, 
			styleMask, 
			&winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting now...\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "OpenGL 3D Texure");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

int initialize(void)
{
	// function prototypes
	void resize(int width, int height);
	GLvoid uninitialize(GLvoid);
	GLvoid InitializeMaterials(GLvoid);

	// variable declarations
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	GLchar *szInfoLog = NULL;
	
	// code
	// New AddOn
	GLXCreateContextAttribsARB = (GLXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
	if(GLXCreateContextAttribsARB == NULL)
	{
		printf("glXCreateContextAttribsARB Failed\n");
	}
	
	const int attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4, 
		GLX_CONTEXT_MINOR_VERSION_ARB, 5, 
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB, 
		None };
	// Now Get The Context
	gGLXContext = GLXCreateContextAttribsARB(gpDisplay,
						gGLXFBConfig, 
						0, 
						true, 
						attribs);
	if(!gGLXContext)
	{
		const int attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1, 
			GLX_CONTEXT_MINOR_VERSION_ARB, 0, 
			None };
		gGLXContext = GLXCreateContextAttribsARB(gpDisplay,
							gGLXFBConfig, 
							0, 
							true, 
							attribs);
	}
	if(!glXIsDirect(gpDisplay, 
			gGLXContext) )
	{
		printf("Obtained Context Not H/W randering Context");
	}
	else
	{
		printf("Obtained Context H/W randering Context");
	}
	
/*	gGLXContext = glXCreateContext(gpDisplay, 
					gpXVisualInfo,
					NULL, 
					GL_TRUE );
*/	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	gResult = glewInit();
	if(gResult != GLEW_OK)
	{
		printf("glewINit() failed\n");
		uninitialize();
		exit(0);
	}

	// VERTEX SHADER
	// Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform int u_lKeyPressed;" \
		"out vec3 tNorm;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{" \
		"		vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"	\
		"		tNorm = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"		light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
		"		float tn_dot_ld = max(dot(tNorm, light_direction), 0.0);" \
		"		viewer_vector = vec3(-eye_coordinates.xyz);" \
		"	}" \
		"	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				printf("Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER
	// Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 tNorm;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_shininess;"	\
		"uniform int u_lKeyPressed;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{" \
		"		vec3 ntNorm = normalize(tNorm);" \
		"		vec3 nlight_direction = normalize(light_direction);" \
		"		vec3 nviewer_vector  = normalize(viewer_vector);" \
		"		vec3 reflection_vector  = reflect(-nlight_direction, ntNorm);" \
		"		float tn_dot_ld = max(dot(ntNorm, nlight_direction), 0.0);" \
		"		vec3 ambient = u_la * u_ka;" \
		"		vec3 diffuse =u_ld * u_kd * tn_dot_ld;" \
		"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, nviewer_vector), 0.0), u_shininess);" \
		"		vec3 phong_ads_light = ambient + diffuse + specular;" \
		"		FragColor = vec4(phong_ads_light, 1.0);"	\
		"	}" \
		"	else" \
		"	{" \
		"		FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
		"	}" \
		"}";

	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				printf("Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				printf("Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
	mUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	vUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
	lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_shininess");

	// Vertices, Colors, Shader attributes, vbo, vao initializations:
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	InitializeMaterials();

	// BLOCK FOR SPHERE:
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	// A. BUFFER BLOCK FOR VERTICES:
	glGenBuffers(1, &vbo_position_sphere);					// Buffer to store vertex position
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);		// Find that named object in memory
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);	// Takes data from CPU to GPU

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	// Release the buffer for vertices:
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// B. BUFFER BLOCK FOR NORMALS:
	glGenBuffers(1, &vbo_normal_sphere);					// Buffer to store vertex normals
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);		// Find that named object in memory
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);	// Takes data from CPU to GPU

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	// C. BUFFER BLOCK FOR ELEMENTS:
	glGenBuffers(1, &vbo_element_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// Release the buffer for colors:
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//glVertexAttrib3f(AMC_ATTRIBUTE_TEXCOORD0, 0.0f, 0.0f, 1.0f);
	glBindVertexArray(0);

	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);	//to remove aliasing

	perspectiveProjectionMatrix = mat4::identity();

	resize(giWindowWidth, giWindowHeight);
	return(0);
}

void resize(int width, int height)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	
	// code
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	gWidth = width / MATERIAL_ROWS;
	gHeight= height / MATERIAL_ROWS;
	gX = (width - (gWidth * MATERIAL_COLUMNS)) / 2;
	gY = (height - (gHeight * MATERIAL_ROWS)) / 2;
	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display(void)
{
	// function prototypes

	// variable declarations
	
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (int i = 0; i < MATERIAL_COLUMNS; i++)
	{
		for (int j = 0; j < MATERIAL_ROWS; j++)
		{
			glUseProgram(gShaderProgramObject);
			glViewport(gX + (i * gWidth), gY + (j * gHeight), gWidth, gHeight);
			if (gbLight == true)
			{
				glUniform3fv(laUniform, 1, lightAmbient);
				glUniform3fv(ldUniform, 1, lightDiffuse);
				glUniform3fv(lsUniform, 1, lightSpecular);
				glUniform4fv(lightPositionUniform, 1, lightPosition);

				glUniform3fv(kaUniform, 1, material[i][j].materialAmbient);
				glUniform3fv(kdUniform, 1, material[i][j].materialDiffuse);
				glUniform3fv(ksUniform, 1, material[i][j].materialSpecular);
				glUniform1f(materialShininessUniform, material[i][j].materialShininess);

				if (keyPressed == 1)
				{
					lightPosition[0] = 0.0f;
					lightPosition[1] = sinf(angleOfXRotation) * 100.0f - 3.0f;
					lightPosition[2] = cosf(angleOfXRotation) * 100.0f - 3.0f;
				}

				if (keyPressed == 2)
				{
					lightPosition[0] = sinf(angleOfYRotation) * 100.0f - 3.0f;
					lightPosition[1] = 0.0f;
					lightPosition[2] = cosf(angleOfYRotation) * 100.0f - 3.0f;
				}

				if (keyPressed == 3)
				{
					lightPosition[0] = sin(angleOfZRotation) * 100.0f - 3.0f;
					lightPosition[1] = cosf(angleOfZRotation) * 100.0f - 3.0f;
					lightPosition[2] = 0.0f;
				}

				glUniform4fv(lightPositionUniform, 1, (GLfloat*)lightPosition);
				glUniform1i(lKeyPressedUniform, 1);
			}
			else
			{
				glUniform1i(lKeyPressedUniform, 0);
			}
			// SPHERE
			// Declaration of matrices
			mat4 modelMatrix;
			mat4 viewMatrix;
			mat4 modelViewProjectionMatrix;

			// Initialize above matrices to identity
			modelMatrix = mat4::identity();
			viewMatrix = mat4::identity();
			modelViewProjectionMatrix = mat4::identity();

			// Do necessary transformations
			modelMatrix = translate(0.0f,
				0.0f,
				-2.0f);

			// Do necessary Matrix Multiplication

			// This was internally donw by gluOrtho2d() in FFP

			// Send Necessary matrix to shader in respective uniform
			glUniformMatrix4fv(mUniform,
				1,
				GL_FALSE,
				modelMatrix);

			glUniformMatrix4fv(vUniform,
				1,
				GL_FALSE,
				viewMatrix);

			glUniformMatrix4fv(pUniform,
				1,
				GL_FALSE,
				perspectiveProjectionMatrix);

			// Bind with vao
			glBindVertexArray(vao_sphere);

			// Similarly bind With Textures If Any


			// Draw The Necessary Senn
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
			glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

			// Unbind vao
			glBindVertexArray(0);

			// Unuse Program
			glUseProgram(0);
		}
	}

	glXSwapBuffers(gpDisplay, gWindow);
}

GLvoid InitializeMaterials(GLvoid) {
	// emerald
	material[0][0].materialAmbient[0] = 0.0215f;
	material[0][0].materialAmbient[1] = 0.1745f;
	material[0][0].materialAmbient[2] = 0.0215f;
	material[0][0].materialAmbient[3] = 1.0f;

	material[0][0].materialDiffuse[0] = 0.07568f;
	material[0][0].materialDiffuse[1] = 0.61424f;
	material[0][0].materialDiffuse[2] = 0.07568f;
	material[0][0].materialDiffuse[3] = 1.0f;

	material[0][0].materialSpecular[0] = 0.633f;
	material[0][0].materialSpecular[1] = 0.727811f;
	material[0][0].materialSpecular[2] = 0.633f;
	material[0][0].materialSpecular[3] = 1.0f;

	material[0][0].materialShininess = 0.6 * 128;

	// jade
	material[0][1].materialAmbient[0] = 0.135f;
	material[0][1].materialAmbient[1] = 0.2225f;
	material[0][1].materialAmbient[2] = 0.1575f;
	material[0][1].materialAmbient[3] = 1.0f;

	material[0][1].materialDiffuse[0] = 0.54f;
	material[0][1].materialDiffuse[1] = 0.89f;
	material[0][1].materialDiffuse[2] = 0.63f;
	material[0][1].materialDiffuse[3] = 1.0f;

	material[0][1].materialSpecular[0] = 0.316228f;
	material[0][1].materialSpecular[1] = 0.316228f;
	material[0][1].materialSpecular[2] = 0.316228f;
	material[0][1].materialSpecular[3] = 1.0f;

	material[0][1].materialShininess = 0.1 * 128;

	// obsidian
	material[0][2].materialAmbient[0] = 0.05375f;
	material[0][2].materialAmbient[1] = 0.05f;
	material[0][2].materialAmbient[2] = 0.06625f;
	material[0][2].materialAmbient[3] = 1.0f;

	material[0][2].materialDiffuse[0] = 0.18275f;
	material[0][2].materialDiffuse[1] = 0.17f;
	material[0][2].materialDiffuse[2] = 0.22525f;
	material[0][2].materialDiffuse[3] = 1.0f;

	material[0][2].materialSpecular[0] = 0.332741f;
	material[0][2].materialSpecular[1] = 0.328634f;
	material[0][2].materialSpecular[2] = 0.346435f;
	material[0][2].materialSpecular[3] = 1.0f;

	material[0][2].materialShininess = 0.3 * 128;

	// pearl
	material[0][3].materialAmbient[0] = 0.25f;
	material[0][3].materialAmbient[1] = 0.20725f;
	material[0][3].materialAmbient[2] = 0.20725f;
	material[0][3].materialAmbient[3] = 1.0f;
	
	material[0][3].materialDiffuse[0] = 1.0f;
	material[0][3].materialDiffuse[1] = 0.829f;
	material[0][3].materialDiffuse[2] = 0.829f;
	material[0][3].materialDiffuse[3] = 1.0f;

	material[0][3].materialSpecular[0] = 0.296648f;
	material[0][3].materialSpecular[1] = 0.296648f;
	material[0][3].materialSpecular[2] = 0.296648f;
	material[0][3].materialSpecular[3] = 1.0f;

	material[0][3].materialShininess = 0.088 * 128;
	
	// ruby
	material[1][0].materialAmbient[0] = 0.1745f;
	material[1][0].materialAmbient[1] = 0.01175f;
	material[1][0].materialAmbient[2] = 0.01175f;
	material[1][0].materialAmbient[3] = 1.0f;

	material[1][0].materialDiffuse[0] = 0.61424f;
	material[1][0].materialDiffuse[1] = 0.04136f;
	material[1][0].materialDiffuse[2] = 0.04136f;
	material[1][0].materialDiffuse[3] = 1.0f;

	material[1][0].materialSpecular[0] = 0.727811f;
	material[1][0].materialSpecular[1] = 0.626959f;
	material[1][0].materialSpecular[2] = 0.626959f;
	material[1][0].materialSpecular[3] = 1.0f;

	material[1][0].materialShininess = 0.6 * 128;

	// turquoise
	material[1][1].materialAmbient[0] = 0.1f;
	material[1][1].materialAmbient[1] = 0.18725f;
	material[1][1].materialAmbient[2] = 0.1745f;
	material[1][1].materialAmbient[3] = 1.0f;

	material[1][1].materialDiffuse[0] = 0.396f;
	material[1][1].materialDiffuse[1] = 0.74151f;
	material[1][1].materialDiffuse[2] = 0.69102f;
	material[1][1].materialDiffuse[3] = 1.0f;

	material[1][1].materialSpecular[0] = 0.297254f;
	material[1][1].materialSpecular[1] = 0.30829f;
	material[1][1].materialSpecular[2] = 0.306678f;
	material[1][1].materialSpecular[3] = 1.0f;

	material[1][1].materialShininess = 0.1 * 128;
//---------------------------------------------------------------

	// brass
	material[1][2].materialAmbient[0] = 0.329412f;
	material[1][2].materialAmbient[1] = 0.223529f;
	material[1][2].materialAmbient[2] = 0.027451f;
	material[1][2].materialAmbient[3] = 1.0f;

	material[1][2].materialDiffuse[0] = 0.780392f;
	material[1][2].materialDiffuse[1] = 0.568627f;
	material[1][2].materialDiffuse[2] = 0.113725f;
	material[1][2].materialDiffuse[3] = 1.0f;

	material[1][2].materialSpecular[0] = 0.992157f;
	material[1][2].materialSpecular[1] = 0.941176f;
	material[1][2].materialSpecular[2] = 0.807843f;
	material[1][2].materialSpecular[3] = 1.0f;

	material[1][2].materialShininess = 0.21794872 * 128;

	// bronze
	material[1][3].materialAmbient[0] = 0.2125f;
	material[1][3].materialAmbient[1] = 0.1275f;
	material[1][3].materialAmbient[2] = 0.054f;
	material[1][3].materialAmbient[3] = 1.0f;

	material[1][3].materialDiffuse[0] = 0.714f;
	material[1][3].materialDiffuse[1] = 0.4284f;
	material[1][3].materialDiffuse[2] = 0.18144f;
	material[1][3].materialDiffuse[3] = 1.0f;

	material[1][3].materialSpecular[0] = 0.393548f;
	material[1][3].materialSpecular[1] = 0.271906f;
	material[1][3].materialSpecular[2] = 0.166721f;
	material[1][3].materialSpecular[3] = 1.0f;

	material[1][3].materialShininess = 0.2 * 128;

	// chrome
	material[2][0].materialAmbient[0] = 0.25f;
	material[2][0].materialAmbient[1] = 0.25f;
	material[2][0].materialAmbient[2] = 0.25f;
	material[2][0].materialAmbient[3] = 1.0f;

	material[2][0].materialDiffuse[0] = 0.4f;
	material[2][0].materialDiffuse[1] = 0.4f;
	material[2][0].materialDiffuse[2] = 0.4f;
	material[2][0].materialDiffuse[3] = 1.0f;

	material[2][0].materialSpecular[0] = 0.774597f;
	material[2][0].materialSpecular[1] = 0.774597f;
	material[2][0].materialSpecular[2] = 0.774597f;
	material[2][0].materialSpecular[3] = 1.0f;

	material[2][0].materialShininess = 0.6 * 128;

	// copper
	material[2][1].materialAmbient[0] = 0.19125f;
	material[2][1].materialAmbient[1] = 0.0735f;
	material[2][1].materialAmbient[2] = 0.0225f;
	material[2][1].materialAmbient[3] = 1.0f;

	material[2][1].materialDiffuse[0] = 0.7038f;
	material[2][1].materialDiffuse[1] = 0.27048f;
	material[2][1].materialDiffuse[2] = 0.0828f;
	material[2][1].materialDiffuse[3] = 1.0f;

	material[2][1].materialSpecular[0] = 0.256777f;
	material[2][1].materialSpecular[1] = 0.137622f;
	material[2][1].materialSpecular[2] = 0.086014f;
	material[2][1].materialSpecular[3] = 1.0f;

	material[2][1].materialShininess = 0.1 * 128;

	// gold
	material[2][2].materialAmbient[0] = 0.24725f;
	material[2][2].materialAmbient[1] = 0.1995f;
	material[2][2].materialAmbient[2] = 0.0745f;
	material[2][2].materialAmbient[3] = 1.0f;

	material[2][2].materialDiffuse[0] = 0.75164f;
	material[2][2].materialDiffuse[1] = 0.60648f;
	material[2][2].materialDiffuse[2] = 0.22648f;
	material[2][2].materialDiffuse[3] = 1.0f;

	material[2][2].materialSpecular[0] = 0.628281f;
	material[2][2].materialSpecular[1] = 0.555802f;
	material[2][2].materialSpecular[2] = 0.366065f;
	material[2][2].materialSpecular[3] = 1.0f;

	material[2][2].materialShininess = 0.4 * 128;

	// silver
	material[2][3].materialAmbient[0] = 0.19225f;
	material[2][3].materialAmbient[1] = 0.19225f;
	material[2][3].materialAmbient[2] = 0.19225f;
	material[2][3].materialAmbient[3] = 1.0f;

	material[2][3].materialDiffuse[0] = 0.50754f;
	material[2][3].materialDiffuse[1] = 0.50754f;
	material[2][3].materialDiffuse[2] = 0.50754f;
	material[2][3].materialDiffuse[3] = 1.0f;

	material[2][3].materialSpecular[0] = 0.508273f;
	material[2][3].materialSpecular[1] = 0.508273f;
	material[2][3].materialSpecular[2] = 0.508273f;
	material[2][3].materialSpecular[3] = 1.0f;

	material[2][3].materialShininess = 0.4 * 128;

//---------------------------------------------------------------

	// black
	material[3][0].materialAmbient[0] = 0.0f;
	material[3][0].materialAmbient[1] = 0.0f;
	material[3][0].materialAmbient[2] = 0.0f;
	material[3][0].materialAmbient[3] = 1.0f;

	material[3][0].materialDiffuse[0] = 0.01f;
	material[3][0].materialDiffuse[1] = 0.01f;
	material[3][0].materialDiffuse[2] = 0.01f;
	material[3][0].materialDiffuse[3] = 1.0f;

	material[3][0].materialSpecular[0] = 0.50f;
	material[3][0].materialSpecular[1] = 0.50f;
	material[3][0].materialSpecular[2] = 0.50f;
	material[3][0].materialSpecular[3] = 1.0f;

	material[3][0].materialShininess = 0.25 * 128;

	// cyan
	material[3][1].materialAmbient[0] = 0.0f;
	material[3][1].materialAmbient[1] = 0.1f;
	material[3][1].materialAmbient[2] = 0.06f;
	material[3][1].materialAmbient[3] = 1.0f;

	material[3][1].materialDiffuse[0] = 0.0f;
	material[3][1].materialDiffuse[1] = 0.50980392f;
	material[3][1].materialDiffuse[2] = 0.50980392f;
	material[3][1].materialDiffuse[3] = 1.0f;

	material[3][1].materialSpecular[0] = 0.50196078f;
	material[3][1].materialSpecular[1] = 0.50196078f;
	material[3][1].materialSpecular[2] = 0.50196078f;
	material[3][1].materialSpecular[3] = 1.0f;

	material[3][1].materialShininess = 0.25 * 128;

	// green
	material[3][2].materialAmbient[0] = 0.0f;
	material[3][2].materialAmbient[1] = 0.0f;
	material[3][2].materialAmbient[2] = 0.0f;
	material[3][2].materialAmbient[3] = 1.0f;

	material[3][2].materialDiffuse[0] = 0.1f;
	material[3][2].materialDiffuse[1] = 0.35f;
	material[3][2].materialDiffuse[2] = 0.1f;
	material[3][2].materialDiffuse[3] = 1.0f;

	material[3][2].materialSpecular[0] = 0.45f;
	material[3][2].materialSpecular[1] = 0.55f;
	material[3][2].materialSpecular[2] = 0.45f;
	material[3][2].materialSpecular[3] = 1.0f;

	material[3][2].materialShininess = 0.25 * 128;

	// red
	material[3][3].materialAmbient[0] = 0.0f;
	material[3][3].materialAmbient[1] = 0.0f;
	material[3][3].materialAmbient[2] = 0.0f;
	material[3][3].materialAmbient[3] = 1.0f;

	material[3][3].materialDiffuse[0] = 0.5f;
	material[3][3].materialDiffuse[1] = 0.0f;
	material[3][3].materialDiffuse[2] = 0.0f;
	material[3][3].materialDiffuse[3] = 1.0f;

	material[3][3].materialSpecular[0] = 0.7f;
	material[3][3].materialSpecular[1] = 0.6f;
	material[3][3].materialSpecular[2] = 0.6f;
	material[3][3].materialSpecular[3] = 1.0f;

	material[3][3].materialShininess = 0.25 * 128;

	// white
	material[4][0].materialAmbient[0] = 0.0f;
	material[4][0].materialAmbient[1] = 0.0f;
	material[4][0].materialAmbient[2] = 0.0f;
	material[4][0].materialAmbient[3] = 1.0f;

	material[4][0].materialDiffuse[0] = 0.55f;
	material[4][0].materialDiffuse[1] = 0.55f;
	material[4][0].materialDiffuse[2] = 0.55f;
	material[4][0].materialDiffuse[3] = 1.0f;

	material[4][0].materialSpecular[0] = 0.70f;
	material[4][0].materialSpecular[1] = 0.70f;
	material[4][0].materialSpecular[2] = 0.70f;
	material[4][0].materialSpecular[3] = 1.0f;

	material[4][0].materialShininess = 0.25 * 128;

	// yellow
	material[4][1].materialAmbient[0] = 0.0f;
	material[4][1].materialAmbient[1] = 0.0f;
	material[4][1].materialAmbient[2] = 0.0f;
	material[4][1].materialAmbient[3] = 1.0f;

	material[4][1].materialDiffuse[0] = 0.5f;
	material[4][1].materialDiffuse[1] = 0.5f;
	material[4][1].materialDiffuse[2] = 0.0f;
	material[4][1].materialDiffuse[3] = 1.0f;

	material[4][1].materialSpecular[0] = 0.60f;
	material[4][1].materialSpecular[1] = 0.60f;
	material[4][1].materialSpecular[2] = 0.50f;
	material[4][1].materialSpecular[3] = 1.0f;

	material[4][1].materialShininess = 0.25 * 128;

//---------------------------------------------------------------

	// black
	material[4][2].materialAmbient[0] = 0.02f;
	material[4][2].materialAmbient[1] = 0.02f;
	material[4][2].materialAmbient[2] = 0.02f;
	material[4][2].materialAmbient[3] = 1.0f;

	material[4][2].materialDiffuse[0] = 0.01f;
	material[4][2].materialDiffuse[1] = 0.01f;
	material[4][2].materialDiffuse[2] = 0.01f;
	material[4][2].materialDiffuse[3] = 1.0f;

	material[4][2].materialSpecular[0] = 0.4f;
	material[4][2].materialSpecular[1] = 0.4f;
	material[4][2].materialSpecular[2] = 0.4f;
	material[4][2].materialSpecular[3] = 0.4f;

	material[4][2].materialShininess = 0.078125 * 128;

	// cyan
	material[4][3].materialAmbient[0] = 0.0f;
	material[4][3].materialAmbient[1] = 0.05f;
	material[4][3].materialAmbient[2] = 0.05f;
	material[4][3].materialAmbient[3] = 1.0f;

	material[4][3].materialDiffuse[0] = 0.4f;
	material[4][3].materialDiffuse[1] = 0.5f;
	material[4][3].materialDiffuse[2] = 0.5f;
	material[4][3].materialDiffuse[3] = 1.0f;

	material[4][3].materialSpecular[0] = 0.4f;
	material[4][3].materialSpecular[1] = 0.7f;
	material[4][3].materialSpecular[2] = 0.7f;
	material[4][3].materialSpecular[3] = 1.0f;

	material[4][3].materialShininess = 0.078125 * 128;

	// green
	material[5][0].materialAmbient[0] = 0.0f;
	material[5][0].materialAmbient[1] = 0.05f;
	material[5][0].materialAmbient[2] = 0.0f;
	material[5][0].materialAmbient[3] = 1.0f;

	material[5][0].materialDiffuse[0] = 0.4f;
	material[5][0].materialDiffuse[1] = 0.5f;
	material[5][0].materialDiffuse[2] = 0.4f;
	material[5][0].materialDiffuse[3] = 1.0f;

	material[5][0].materialSpecular[0] = 0.04f;
	material[5][0].materialSpecular[1] = 0.7f;
	material[5][0].materialSpecular[2] = 0.04f;
	material[5][0].materialSpecular[3] = 1.0f;

	material[5][0].materialShininess = 0.078125 * 128;

	// red
	material[5][1].materialAmbient[0] = 0.05f;
	material[5][1].materialAmbient[1] = 0.0f;
	material[5][1].materialAmbient[2] = 0.0f;
	material[5][1].materialAmbient[3] = 1.0f;

	material[5][1].materialDiffuse[0] = 0.5f;
	material[5][1].materialDiffuse[1] = 0.4f;
	material[5][1].materialDiffuse[2] = 0.4f;
	material[5][1].materialDiffuse[3] = 1.0f;

	material[5][1].materialSpecular[0] = 0.7f;
	material[5][1].materialSpecular[1] = 0.04f;
	material[5][1].materialSpecular[2] = 0.04f;
	material[5][1].materialSpecular[3] = 1.0f;

	material[5][1].materialShininess = 0.078125 * 128;

	// white
	material[5][2].materialAmbient[0] = 0.05f;
	material[5][2].materialAmbient[1] = 0.05f;
	material[5][2].materialAmbient[2] = 0.05f;
	material[5][2].materialAmbient[3] = 1.0f;

	material[5][2].materialDiffuse[0] = 0.5f;
	material[5][2].materialDiffuse[1] = 0.5f;
	material[5][2].materialDiffuse[2] = 0.5f;
	material[5][2].materialDiffuse[3] = 1.0f;

	material[5][2].materialSpecular[0] = 0.7f;
	material[5][2].materialSpecular[1] = 0.7f;
	material[5][2].materialSpecular[2] = 0.7f;
	material[5][2].materialSpecular[3] = 0.4f;

	material[5][2].materialShininess = 0.078125 * 128;

	// yellow
	material[5][3].materialAmbient[0] = 0.05f;
	material[5][3].materialAmbient[1] = 0.05f;
	material[5][3].materialAmbient[2] = 0.0f;
	material[5][3].materialAmbient[3] = 1.0f;

	material[5][3].materialDiffuse[0] = 0.5f;
	material[5][3].materialDiffuse[1] = 0.5f;
	material[5][3].materialDiffuse[2] = 0.4f;
	material[5][3].materialDiffuse[3] = 1.0f;

	material[5][3].materialSpecular[0] = 0.7f;
	material[5][3].materialSpecular[1] = 0.7f;
	material[5][3].materialSpecular[2] = 0.04f;
	material[5][3].materialSpecular[3] = 1.0f;

	material[5][3].materialShininess = 0.078125 * 128;
}

void update(void)
{
	// function prototypes

	// variable declarations
	GLfloat iOfset = 0.01f;
	
	// code
	angleOfXRotation -= iOfset;
	if (angleOfXRotation > 360.0f)
	{
		angleOfXRotation += 360.0f;
	}
	angleOfYRotation -= iOfset;
	if (angleOfYRotation > 360.0f)
	{
		angleOfYRotation += 360.0f;
	}
	angleOfZRotation -= iOfset;
	if (angleOfZRotation > 360.0f)
	{
		angleOfZRotation += 360.0f;
	}
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	// code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False, 
		StructureNotifyMask, 
		&xev);
}

void uninitialize(void)
{
	GLXContext currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}


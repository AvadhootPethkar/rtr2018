#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

// OpenGL header files
#include<GL/glew.h>	// Change 1
#include<GL/gl.h>	// Change 1
#include<GL/glx.h>	// Change 1

// Texure header
#include<SOIL/SOIL.h>

// VMath
#include"vmath.h"

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// namespace
using namespace std;
using namespace vmath;

// global variable declarations
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
// New addOn variables
typedef GLXContext (*GLXCreateContextAttribsARBProc)(Display *, GLXFBConfig, GLXContext, Bool, const int *) ;
GLXCreateContextAttribsARBProc GLXCreateContextAttribsARB = NULL;
GLXFBConfig gGLXFBConfig;

int giWindowWidth = 800; 
int giWindowHeight = 600; 
static GLXContext gGLXContext;
GLint gResult;

// Animating variables
GLfloat gfAngle = 0.0f;
GLuint texure_stone = 0;

// Shader variables
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

// Ortho variables
GLuint vaoI;
GLuint vboI;
GLuint vaoNCross;
GLuint vboNCross;
GLuint vaoD;
GLuint vboD;
GLuint vaoDUp;
GLuint vboDUp;
GLuint vaoDDown;
GLuint vboDDown;
GLuint vaoALeft;
GLuint vboALeft;
GLuint vaoACenter;
GLuint vboACenter;
GLuint vboColor;
GLuint mvpUniform;
mat4 perspectiveProjectionMatrix;

// entry-point function
int main(void)
{
	// function prototypes
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize(void);
	int initialize(void);
	void resize(int width, int height);
	void display(void);
	void update(void);
	
	// variable declarations
	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;
	char keys[26];
	bool bDone = false;

	// code
	CreateWindow();
	initialize();

	// Message loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay) )
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					break;

				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							break;

						default:
							break;
					}
				
				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
				switch(keys[0])
				{
					case 'F':
					case 'f':
						if(bFullscreen == false)
						{
							ToggleFullscreen();
							bFullscreen = true;
						}
						else
						{
							ToggleFullscreen();
							bFullscreen = false;
						}
						break;

					default:
						break;
				}
					break;

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						
						case 2:
							break;

						case 3:
							break;
						
						case 4:
							break;
						
						case 5:
							break;

						default:
							break;
					}
					break;

				case MotionNotify:
					break;

				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;

				case Expose:
					break;

				case DestroyNotify:
					break;

				case 33:
					bDone = true;
					break;

				default:
					break;
			}
		}
		update();
		display();
	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[] = { GLX_RGBA, 
						GLX_X_RENDERABLE, True,
						GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
						GLX_RENDER_TYPE, GLX_RGBA_BIT,
						GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
						
						GLX_DOUBLEBUFFER, True,
						GLX_RED_SIZE, 8, 
						GLX_GREEN_SIZE, 8, 
						GLX_BLUE_SIZE, 8, 
						GLX_ALPHA_SIZE, 8, 
						GLX_DEPTH_SIZE, 24, 
						
						GLX_STENCIL_SIZE, 8, 
						None};
	// New 4 local variables
	GLXFBConfig *pGLXFBConfig = NULL; // Array of FBConfig (Internally L.L.)
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *pTempXVisualInfo = NULL;
	int iNoOfFBConfigs = 0;

	// code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \n Exitting Now... \n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);
	
	// New addOn
	pGLXFBConfig = glXChooseFBConfig(gpDisplay, 
					defaultScreen, 
					frameBufferAttributes, 
					&iNoOfFBConfigs);
	printf("There are %d No Of FBConfig",iNoOfFBConfigs);
	
	int bestFrameBufferConfig = -1;
	int bestNoOfSamples = -1;
	int worstFrameBufferConfig = -1;
	int worstNoOfSamples = 999;
	
	for(int i = 0; i < iNoOfFBConfigs; i++)
	{
		pTempXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, 
							pGLXFBConfig[i]);
		if(pTempXVisualInfo)
		{
			int sampleBuffers;
			int samples;
			glXGetFBConfigAttrib(gpDisplay,
						pGLXFBConfig[i], 
						GLX_SAMPLE_BUFFERS, 
						&sampleBuffers);
			glXGetFBConfigAttrib(gpDisplay, 
						pGLXFBConfig[i], 
						GLX_SAMPLES,
						&samples);
			
			// To get Best From All
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNoOfSamples)
			{
				bestFrameBufferConfig = i;
				bestNoOfSamples = samples;
			}
			// To Get Worst From All
			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNoOfSamples) // To Check
			{
				worstFrameBufferConfig = i;
				worstNoOfSamples = samples;
			}
		} // for(pTempXVisualInfo) end
		XFree(pTempXVisualInfo);
	} // for end
	
	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
	
	gGLXFBConfig = bestGLXFBConfig;
	
	XFree(pGLXFBConfig);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXGetVisualFromFBConfig(gpDisplay, 
					bestGLXFBConfig);
	// ------- End Of Step A ---------
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable To Alocate Memory For Visual Info. \n Exitting Now...\n");
		uninitialize();
		exit(1);
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),
				gpXVisualInfo->visual,
				AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	winAttribs.event_mask = VisibilityChangeMask | KeyPressMask | ButtonPressMask 
				| PointerMotionMask | StructureNotifyMask | ExposureMask;
	
	styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;

	gWindow = XCreateWindow(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			0, 
			0, 
			giWindowWidth, 
			giWindowHeight, 
			0, 
			gpXVisualInfo->depth, 
			InputOutput, 
			gpXVisualInfo->visual, 
			styleMask, 
			&winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting now...\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "OpenGL 3D Texure");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

int initialize(void)
{
	// function prototypes
	void resize(int width, int height);
	GLvoid uninitialize(GLvoid);

	// variable declarations
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	GLchar *szInfoLog = NULL;
	
	// code
	// New AddOn
	GLXCreateContextAttribsARB = (GLXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");
	if(GLXCreateContextAttribsARB == NULL)
	{
		printf("glXCreateContextAttribsARB Failed\n");
	}
	
	const int attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4, 
		GLX_CONTEXT_MINOR_VERSION_ARB, 5, 
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB, 
		None };
	// Now Get The Context
	gGLXContext = GLXCreateContextAttribsARB(gpDisplay,
						gGLXFBConfig, 
						0, 
						true, 
						attribs);
	if(!gGLXContext)
	{
		const int attribs[] = {
			GLX_CONTEXT_MAJOR_VERSION_ARB, 1, 
			GLX_CONTEXT_MINOR_VERSION_ARB, 0, 
			None };
		gGLXContext = GLXCreateContextAttribsARB(gpDisplay,
							gGLXFBConfig, 
							0, 
							true, 
							attribs);
	}
	if(!glXIsDirect(gpDisplay, 
			gGLXContext) )
	{
		printf("Obtained Context Not H/W randering Context");
	}
	else
	{
		printf("Obtained Context H/W randering Context");
	}
	
/*	gGLXContext = glXCreateContext(gpDisplay, 
					gpXVisualInfo,
					NULL, 
					GL_TRUE );
*/	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	gResult = glewInit();
	if(gResult != GLEW_OK)
	{
		printf("glewINit() failed\n");
		uninitialize();
		exit(0);
	}

	// VERTEX SHADER
	// Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}";

	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				printf("Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER
	// Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = out_color;" \
		"}";

	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				printf("Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				printf("Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	const GLfloat fIVertices[] =
	{
		-1.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
	};

	const GLfloat fNCrossVertices[] =
	{
		-1.0f, 1.0f, 0.0f,
		-0.5f, -1.0f,0.0f,
	};

	const GLfloat fACenterVertices[] =
	{
		-1.0f, 1.0f, 0.0f,
		-0.5f, 1.0f,0.0f,
	};
	const GLfloat fALeftVertices[] =
	{
		1.0f, 1.0f, 0.0f,
		0.7f, -1.0f, 0.0f,
	};
	const GLfloat triColor[] =
	{
		1.0f, 0.6f, 0.2f,
		0.07f, 0.53f, 0.02f
	};
	const GLfloat fOrangeColor[] =
	{
		1.0f, 0.6f, 0.2f,
		1.0f, 0.6f, 0.2f
	};
	const GLfloat fGreenColor[] =
	{
		0.07f, 0.53f, 0.02f,
		0.07f, 0.53f, 0.02f
	};
	const GLfloat fWhiteColor[] =
	{
		1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f
	};

	// Create vaoI
	glGenVertexArrays(1, &vaoI);
	glBindVertexArray(vaoI);
	glGenBuffers(1, &vboI);

	//IVerticesVBO
	glBindBuffer(GL_ARRAY_BUFFER, vboI);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fIVertices),
		fIVertices,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//IColorVBO
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(triColor),
		triColor,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Create vaoNCross
	glGenVertexArrays(1, &vaoNCross);
	//NCrossVertices
	glBindVertexArray(vaoNCross);
	glGenBuffers(1, &vboNCross);
	glBindBuffer(GL_ARRAY_BUFFER, vboNCross);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fNCrossVertices),
		fNCrossVertices,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//IColorVBO
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(triColor),
		triColor,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Create vaoALeft
	glGenVertexArrays(1, &vaoALeft);
	//ALeftVertices
	glBindVertexArray(vaoALeft);
	glGenBuffers(1, &vboALeft);
	glBindBuffer(GL_ARRAY_BUFFER, vboALeft);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fALeftVertices),
		fALeftVertices,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//IColorVBO
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(triColor),
		triColor,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	// Create vaoDUp
	glGenVertexArrays(1, &vaoDUp);
	// ACenterVertices
	glBindVertexArray(vaoDUp);
	//vboDUpVertices
	glGenBuffers(1, &vboDUp);
	glBindBuffer(GL_ARRAY_BUFFER, vboDUp);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fACenterVertices),
		fACenterVertices,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//vboColor
	glGenBuffers(1, &vboDUp);
	glBindBuffer(GL_ARRAY_BUFFER, vboDUp);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fOrangeColor),
		fOrangeColor,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//vaoDDown
	glGenVertexArrays(1, &vaoDDown);
	// ACenterVertices
	glBindVertexArray(vaoDDown);
	//vboDUpVertices
	glGenBuffers(1, &vboDDown);
	glBindBuffer(GL_ARRAY_BUFFER, vboDDown);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fACenterVertices),
		fACenterVertices,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//vboColor
	glGenBuffers(1, &vboDDown);
	glBindBuffer(GL_ARRAY_BUFFER, vboDDown);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fGreenColor),
		fGreenColor,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Create vaoACenter
	glGenVertexArrays(1, &vaoACenter);
	// ACenterVertices
	glBindVertexArray(vaoACenter);
	glGenBuffers(1, &vboACenter);
	glBindBuffer(GL_ARRAY_BUFFER, vboACenter);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fACenterVertices),
		fACenterVertices,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//ACenterColorVBO
	glGenBuffers(1, &vboColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboColor);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(fWhiteColor),
		fWhiteColor,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);	//to remove aliasing

	perspectiveProjectionMatrix = mat4::identity();

	resize(giWindowWidth, giWindowHeight);
	return(0);
}

void resize(int width, int height)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	
	// code
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	perspectiveProjectionMatrix = perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);
}

void display(void)
{
	// function prototypes

	// variable declarations
	
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
	// Declaration of matrices
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	// I
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(-0.4f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoI);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// NLeft
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(-0.06f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoI);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// NCross
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(-0.06f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoNCross);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// NRight
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(0.44f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoI);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// DLeft
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(0.88f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoI);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// DUp
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(0.88f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoDUp);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// DDown
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(0.88f,
		-2.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoDDown);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// DRight
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(1.38f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoI);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// ISecond
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(1.76f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoI);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// ALeft
	// Initialize above matrices to identity
	//modelViewMatrix = mat4::identity();
	//modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(0.4f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

	// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoALeft);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// ACenterOrange
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(2.63f,
		-0.988f,
		-3.9f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoDUp);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// ACenterWhite
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(2.63f,
		-1.0f,
		-3.9f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoACenter);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// ACenterGreen
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(2.63f,
		-1.01f,
		-3.9f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoDDown);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// ARight
	// Initialize above matrices to identity
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	modelViewMatrix = translate(2.4f,
		0.0f,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoNCross);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_LINES,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// Unuse Program
	glUseProgram(0);

	glXSwapBuffers(gpDisplay, gWindow);
}

void update(void)
{
	// function prototypes

	// variable declarations
	
	// code
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	// code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False, 
		StructureNotifyMask, 
		&xev);
}

void uninitialize(void)
{
	GLXContext currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}


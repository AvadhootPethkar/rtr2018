#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

// OpenGL header files
#include<GL/glew.h>	// Change 1
#include<GL/gl.h>	// Change 1
#include<GL/glx.h>	// Change 1

// Texure header
#include<SOIL/SOIL.h>

// namespace
using namespace std;

// global variable declarations
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800; 
int giWindowHeight = 600; 
static GLXContext gGLXContext;
GLint gResult;

// Animating variables
GLfloat gfAngle = 0.0f;
GLuint texure_stone = 0;

// entry-point function
int main(void)
{
	// function prototypes
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize(void);
	int initialize(void);
	void resize(int width, int height);
	void display(void);
	void update(void);
	
	// variable declarations
	static int winWidth = giWindowWidth;
	static int winHeight = giWindowHeight;
	char keys[26];
	bool bDone = false;

	// code
	CreateWindow();
	initialize();

	// Message loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay) )
		{
			XNextEvent(gpDisplay, &event);
			switch(event.type)
			{
				case MapNotify:
					break;

				case KeyPress:
					keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
					switch(keysym)
					{
						case XK_Escape:
							bDone = true;
							break;

						default:
							break;
					}
				
				XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
				switch(keys[0])
				{
					case 'F':
					case 'f':
						if(bFullscreen == false)
						{
							ToggleFullscreen();
							bFullscreen = true;
						}
						else
						{
							ToggleFullscreen();
							bFullscreen = false;
						}
						break;

					default:
						break;
				}
					break;

				case ButtonPress:
					switch(event.xbutton.button)
					{
						case 1:
							break;
						
						case 2:
							break;

						case 3:
							break;
						
						case 4:
							break;
						
						case 5:
							break;

						default:
							break;
					}
					break;

				case MotionNotify:
					break;

				case ConfigureNotify:
					winWidth = event.xconfigure.width;
					winHeight = event.xconfigure.height;
					resize(winWidth, winHeight);
					break;

				case Expose:
					break;

				case DestroyNotify:
					break;

				case 33:
					bDone = true;
					break;

				default:
					break;
			}
		}
		update();
		display();
	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[] = { GLX_RGBA, 
						GLX_DOUBLEBUFFER, True,
						GLX_RED_SIZE, 8, 
						GLX_GREEN_SIZE, 8, 
						GLX_BLUE_SIZE, 8, 
						GLX_ALPHA_SIZE, 8, 
						GLX_DEPTH_SIZE, 24, 
						None};

	// code
	gpDisplay = XOpenDisplay(NULL);
	if(gpDisplay == NULL)
	{
		printf("ERROR : Unable To Open X Display. \n Exitting Now... \n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, 
					defaultScreen, 
					frameBufferAttributes);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable To Alocate Memory For Visual Info. \n Exitting Now...\n");
		uninitialize();
		exit(1);
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
				RootWindow(gpDisplay, gpXVisualInfo->screen),
				gpXVisualInfo->visual,
				AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);

	winAttribs.event_mask = VisibilityChangeMask | KeyPressMask | ButtonPressMask 
				| PointerMotionMask | StructureNotifyMask | ExposureMask;
	
	styleMask = CWBorderPixel | CWBackPixel | CWColormap | CWEventMask;

	gWindow = XCreateWindow(gpDisplay, 
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			0, 
			0, 
			giWindowWidth, 
			giWindowHeight, 
			0, 
			gpXVisualInfo->depth, 
			InputOutput, 
			gpXVisualInfo->visual, 
			styleMask, 
			&winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting now...\n");
		uninitialize();
		exit(1);
	}

	XStoreName(gpDisplay, gWindow, "OpenGL 3D Texure");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);
	XMapWindow(gpDisplay, gWindow);
}

int initialize(void)
{
	// function prototypes
	void resize(int width, int height);
	GLvoid uninitialize(GLvoid);

	// variable declarations
	
	// code
	gGLXContext = glXCreateContext(gpDisplay, 
					gpXVisualInfo,
					NULL, 
					GL_TRUE );
	
	glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
	gResult = glewInit();
	if(gResult != GLEW_OK)
	{
		printf("glewINit() failed\n");
		uninitialize();
		exit(0);
	}
	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_SMOOTH);							//to remove aliasing
	resize(giWindowWidth, giWindowHeight);
	return(0);
}

void resize(int width, int height)
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	
	// code
	if (height == 0) // Change 2a
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(void)
{
	// function prototypes

	// variable declarations
	
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glXSwapBuffers(gpDisplay, gWindow);
}

void update(void)
{
	// function prototypes

	// variable declarations
	
	// code
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = {0};

	// code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;
	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay, gpXVisualInfo->screen),
		False, 
		StructureNotifyMask, 
		&xev);
}

void uninitialize(void)
{
	GLXContext currentGLXContext = glXGetCurrentContext();
	if(currentGLXContext != NULL && currentGLXContext == gGLXContext)
	{
		glXMakeCurrent(gpDisplay, 0, 0);
	}
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay, gGLXContext);
	}
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}
	if(gColormap)
	{
		XFreeColormap(gpDisplay, gWindow);
	}
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}
	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}


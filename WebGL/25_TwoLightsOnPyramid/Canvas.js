// global variables
var canvas = null;
var gl = null;      // webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
    AMC_ATTRIBUTE_VERTEX : 0,
    AMC_ATTRIBUTE_COLOR : 1,
    AMC_ATTRIBUTE_NORMAL : 2,
    AMC_ATTRIBUTE_TEXTURE0 : 3,
};

var lightAmbiantZero = [ 0.0,0.0, 0.0 ];
var lightDiffuseZero = [ 1.0,0.0,0.0 ];
var lightSpecularZero = [ 1.0,0.0, 0.0 ];
var lightPositionZero = [ -2.0,0.0,0.0,1.0 ];

var lightAmbiantTwo = [ 0.0,0.0, 0.0 ];
var lightDiffuseTwo = [ 0.0,0.0,1.0];
var lightSpecularTwo = [ 0.0,0.0, 1.0 ];
var lightPositionTwo = [ 2.0,0.0, 0.0, 1.0 ];

var materialAmbient = [ 0.0,0.0, 0.0 ];
var materialDiffuse = [ 1.0,1.0,1.0];
var materialSpecular = [ 1.0,1.0, 1.0 ];
var materialShininess = 128.0;

var isLKeyPressed = false;

// Animating variables
var anglePyramid = 0.0;
var isAKeyPressed = true;

var vao_pyramid;
var vbo_position_pyramid;
var vbo_normal_pyramid;

// PerVertex Variables
var modelUniform;
var viewUniform;
var projectionUniform;
var lKeyPressedUniform;

var laUniformRed;
var ldUniformRed;
var lsUniformRed;
var lightPositionUniformRed;

var laUniformBlue;
var ldUniformBlue;
var lsUniformBlue;
var lightPositionUniformBlue;

var kaUniformPerVertex;
var kdUniformPerVertex;
var ksUniformPerVertex;
var materialShinyninessUniformPerVertex;


// PerVertex Shader variables
var vertexShaderObjectPerVertex;
var fragmentShaderObjectPerVertex;
var shaderProgramObjectPerVertex;

var perspectiveProjectionMatrix;

// Light Variables
var angleTriangle = 0.0;

// To start animation:
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame ||
null;

// onload function
function main() {
    // get <Canvas> element
    canvas = document.getElementById("AMC");
    if (!canvas) {
        console.log("Obtaining Canvas failed\n");
    } 
    else
        console.log("Obtaining Canvas successed\n");
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    window.addEventListener("keydown",keyDown, false);
    window.addEventListener("click",mouseDown, false);
    window.addEventListener("resize",resize, false);

    // initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen() {
    // Code
    var fullscreen_element =
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    // if not fullscreen
    if (fullscreen_element == null) {
        if (canvas.requestFullscreen) {
            canvas.requestFullscreen();
        }
        else if (canvas.mozRequestFullScreen) {
            canvas.mozRequestFullScreen();
        }
        else if (canvas.webkitRequestFullscreen) {
            canvas.webkitRequestFullscreen();
        }
        else if (canvas.msRequestFullscreen) {
            canvas.msRequestFullscreen();
        }
        bFullscreen = true;
    }
    else // if already fullscreen
    {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        }
        else if (document.webkitExitFullscreen) {
            document.webkitExitullscreen();
        }
        else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        bFullscreen = false;
    }
}

function init() {
    // code
    // get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if (gl == null) {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader PerVertex
    var vertexShaderSourceCodePerVertex =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec3 vNormal;" +
        "uniform mat4 u_model_matrix;" +
        "uniform mat4 u_view_matrix;" +
        "uniform mat4 u_projection_matrix;" +
        "uniform int u_lKeyPressed;" +
        "uniform vec3 u_la_red;" +
        "uniform vec3 u_ld_red;" +
        "uniform vec3 u_ls_red;" +
        "uniform vec4 u_light_position_red;" +
        "uniform vec3 u_la_green;" +
        "uniform vec3 u_ld_green;" +
        "uniform vec3 u_ls_green;" +
        "uniform vec4 u_light_position_green;" +
        "uniform vec3 u_la_blue;" +
        "uniform vec3 u_ld_blue;" +
        "uniform vec3 u_ls_blue;" +
        "uniform vec4 u_light_position_blue;" +
        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_shininess;"    +
        "out vec3 phong_ads_light;" +
        "void main(void)" +
        "{" +
        "   if(u_lKeyPressed == 1)" +
        "   {" +
        "       vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
        "       vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal );" +
        "       vec3 viewer_vector = normalize(-eye_coordinates.xyz);" +

        "       vec3 light_direction_red = normalize(vec3(u_light_position_red) - eye_coordinates.xyz );" +
        "       vec3 reflection_vector_red = reflect(-light_direction_red, transformed_normals);" +
        "       float tn_dot_ld_red = max(dot(transformed_normals, light_direction_red), 0.0);" +
        "       vec3 ambient_red = u_la_red * u_ka;" +
        "       vec3 diffuse_red =u_ld_red * u_kd * tn_dot_ld_red;" +
        "       vec3 specular_red = u_ls_red * u_ks * pow(max(dot(reflection_vector_red, viewer_vector), 0.0), u_shininess);" +

        "       vec3 light_direction_green = normalize(vec3(u_light_position_green) - eye_coordinates.xyz );" +
        "       vec3 reflection_vector_green = reflect(-light_direction_green, transformed_normals);" +
        "       float tn_dot_ld_green = max(dot(transformed_normals, light_direction_green), 0.0);" +
        "       vec3 ambient_green = u_la_green * u_ka;" +
        "       vec3 diffuse_green =u_ld_green * u_kd * tn_dot_ld_green;" +
        "       vec3 specular_green = u_ls_green * u_ks * pow(max(dot(reflection_vector_green, viewer_vector), 0.0), u_shininess);" +

        "       vec3 light_direction_blue = normalize(vec3(u_light_position_blue) - eye_coordinates.xyz );" +
        "       vec3 reflection_vector_blue = reflect(-light_direction_blue, transformed_normals);" +
        "       float tn_dot_ld_blue = max(dot(transformed_normals, light_direction_blue), 0.0);" +
        "       vec3 ambient_blue = u_la_blue * u_ka;" +
        "       vec3 diffuse_blue =u_ld_blue * u_kd * tn_dot_ld_blue;" +
        "       vec3 specular_blue = u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue, viewer_vector), 0.0), u_shininess);" +

        "       phong_ads_light = ambient_red + diffuse_red + specular_red;" +
        "       phong_ads_light = phong_ads_light + ambient_green + diffuse_green + specular_green;" +
        "       phong_ads_light = phong_ads_light + ambient_blue + diffuse_blue + specular_blue;" +
        "   }" +
        "   else" +
        "   {" +
        "       phong_ads_light = vec3(1.0, 1.0, 1.0);" +
        "   }" +
        "   gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
        "}";

    vertexShaderObjectPerVertex = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObjectPerVertex, vertexShaderSourceCodePerVertex);
    gl.compileShader(vertexShaderObjectPerVertex);

    if (!gl.getShaderParameter(vertexShaderObjectPerVertex, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(vertexShaderObjectPerVertex);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader
    var fragmentShaderSourceCodePerVertex =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec3 phong_ads_light;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "   FragColor = vec4(phong_ads_light, 1.0);"    +
        "}";

    fragmentShaderObjectPerVertex = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObjectPerVertex, fragmentShaderSourceCodePerVertex);
    gl.compileShader(fragmentShaderObjectPerVertex);

    if (!gl.getShaderParameter(fragmentShaderObjectPerVertex, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(fragmentShaderObjectPerVertex);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // shader program 
    shaderProgramObjectPerVertex = gl.createProgram();
    gl.attachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
    gl.attachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);

    // pre-linking binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObjectPerVertex, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObjectPerVertex, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

    // linking
    gl.linkProgram(shaderProgramObjectPerVertex);
    if (!gl.getProgramParameter(shaderProgramObjectPerVertex, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObjectPerVertex);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // get mvp uniform location
    modelUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_model_matrix");
    viewUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_view_matrix");
    projectionUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_projection_matrix");
    lKeyPressedUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_lKeyPressed");

    laUniformRed = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_la_red");
    ldUniformRed = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ld_red");
    lsUniformRed = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ls_red");
    lightPositionUniformRed = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position_red");

    laUniformBlue = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_la_blue");
    ldUniformBlue = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ld_blue");
    lsUniformBlue = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ls_blue");
    lightPositionUniformBlue = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position_blue");

    kaUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ka");
    kdUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_kd");
    ksUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ks");
    materialShininessUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_shininess");

    // vertex, colors, shader attribs, vbo, vao initalizations
    // sphere
    // triangle vertices
    var pyramidVertices = new Float32Array([
       // Front face
        0.0, 1.0, 0.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,

        //Back Face
        0.0, 1.0, 0.0,
        1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0,

        //Right face
        0.0, 1.0, 0.0,
        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,

        //Left face
        0.0, 1.0, 0.0,
        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        ]);

    var pyramideNormals = new Float32Array([
        // Front face
        0.0, 0.447214, 0.894427,
        0.0, 0.447214, 0.894427,
        0.0, 0.447214, 0.894427,

        //Back Face
        0.0, 0.4472141, -0.894427,
        0.0, 0.4472141, -0.894427,
        0.0, 0.4472141, -0.894427,

        //Right face
        0.894427, 0.4472141, 0.0,
        0.894427, 0.4472141, 0.0,
        0.894427, 0.4472141, 0.0,
        //Left face

        -0.894427, 0.4472141, 0.0,
        -0.894427, 0.4472141, 0.0,
        -0.894427, 0.4472141, 0.0,
        ]);

    vao_pyramid = gl.createVertexArray();
    gl.bindVertexArray(vao_pyramid);

    vbo_position_pyramid = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_pyramid);
    gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
        3,
        gl.FLOAT,
        false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_normal_pyramid = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_normal_pyramid);
    gl.bufferData(gl.ARRAY_BUFFER, pyramideNormals, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL,
        3,
        gl.FLOAT,
        false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // enable depth
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    // initialize projection matrix
    perspectiveProjectionMatrix = mat4.create();
}

function resize() {
    // Code
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    // set viewport
    gl.viewport(0, 0, canvas.width, canvas.height);

    // Perspective Projection
        mat4.perspective(perspectiveProjectionMatrix,45.0,
        parseFloat(canvas.width) / parseFloat(canvas.height),
        0.1,
        100.0);
}

function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObjectPerVertex);

    if (isLKeyPressed) {
        gl.uniform3fv(laUniformRed, lightAmbiantZero);
        gl.uniform3fv(ldUniformRed, lightDiffuseZero);
        gl.uniform3fv(lsUniformRed, lightSpecularZero);
        gl.uniform4fv(lightPositionUniformRed, lightPositionZero);

        gl.uniform3fv(laUniformBlue, lightAmbiantTwo);
        gl.uniform3fv(ldUniformBlue, lightDiffuseTwo);
        gl.uniform3fv(lsUniformBlue, lightSpecularTwo);
        gl.uniform4fv(lightPositionUniformBlue, lightPositionTwo);

        gl.uniform3fv(kaUniformPerVertex, materialAmbient);
        gl.uniform3fv(kdUniformPerVertex, materialDiffuse);
        gl.uniform3fv(ksUniformPerVertex, materialSpecular);
        gl.uniform1f(materialShininessUniform, materialShininess);

        gl.uniform1i(lKeyPressedUniform, 1);
    }
    else {
        gl.uniform1i(lKeyPressedUniform, 0);
    }
	var modelMatrix = mat4.create();
	var viewMatrix = mat4.create();
	
	var angleInRadian = degToRad(angleTriangle);
	mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-4.0]);
	mat4.rotateY(modelMatrix, modelMatrix,angleInRadian);
	gl.uniformMatrix4fv(modelUniform,false,modelMatrix);
	gl.uniformMatrix4fv(viewUniform,false,viewMatrix);
	gl.uniformMatrix4fv(projectionUniform,false,perspectiveProjectionMatrix);
	
	//draw pyramid
	gl.bindVertexArray(vao_pyramid);
	gl.drawArrays(gl.TRIANGLES, 0,12);
	gl.bindVertexArray(null);
    gl.bindVertexArray(null);

    gl.useProgram(null);

    // animation loop
    update();
    requestAnimationFrame(draw, canvas);
}

function update() {
    /* code */
    angleTriangle += 0.1;
    if (angleTriangle >= 360.0)
    {
        angleTriangle = 0.0;
    }
}

function uninitialize() {
    // Code
    if (shaderProgramObjectPerVertex) {
        if (fragmentShaderObjectPerVertex) {
            gl.detachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);
            gl.deleteShader(fragmentShaderObjectPerVertex);
            fragmentShaderObjectPerVertex = null;
        }
        
        if (vertexShaderObjectPerVertex) {
            gl.detachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
            gl.deleteShader(vertexShaderObjectPerVertex);
            vertexShaderObjectPerVertex = null;
        }

        gl.deleteProgram(shaderProgramObjectPerVertex);
        shaderProgramObjectPerVertex = null;
    }
}

function keyDown(event) {
    // Code
    switch(event.keyCode) {
        case 27:
            uninitialize();
            window.close();
            break;
        case 70: // For 'F' or 'f'
            toggleFullScreen();
            break;
        case 76: //For 'L' or 'l'
            if (isLKeyPressed == true) {
                isLKeyPressed = false;
            }
            else {
                isLKeyPressed = true;
            }
            break;
    }
}


function mouseDown() {
    // Code
}

function degToRad(degrees) {
    return(degrees*Math.PI/180.0);
}

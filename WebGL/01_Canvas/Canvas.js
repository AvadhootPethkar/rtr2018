// onload function
function main()
{
	// get <Canvas> element
	var canvas = document.getElementById("AMC");
	if (!canvas) {
		console.log("Obtaining Canvas failed\n");
	} 
	else
		console.log("Obtaining Canvas successed\n");

	// print canvas width and height on console
	console.log("Canvas width : " + canvas.width + "Canvas height : " + canvas.height);

	// get 2D context
	var context = canvas.getContext("2d");
	if (!context) {
		console.log("Obtaining 2D Context failed\n");
	}
	else
		console.log("Obtaining 2D Context successed\n");

	// fill canvas with black color
	context.fillStyle = "black";
	context.fillRect(0, 0, canvas.width, canvas.height);

	// center the text
	context.textAlign = "center"; // center horizontally
	context.textBaseLine = "middle"; // center vertically

	// text
	var str = "Hello World !!!";

	// text font
	context.font = "48px sans-serif";

	// text color
	context.fillStyle = "white";

	// display the text in center
	context.fillText(str,canvas.width/2, canvas.height/2);
}
// global variables
var canvas = null;
var context = null;

// onload function
function main()
{
	// get <Canvas> element
	canvas = document.getElementById("AMC");
	if (!canvas) {
		console.log("Obtaining Canvas failed\n");
	} 
	else
		console.log("Obtaining Canvas successed\n");

	// print canvas width and height on console
	console.log("Canvas width : " + canvas.width + "Canvas height : " + canvas.height);

	// get 2D context
	context = canvas.getContext("2d");
	if (!context) {
		console.log("Obtaining 2D Context failed\n");
	}
	else
		console.log("Obtaining 2D Context successed\n");

	// fill canvas with black color
	context.fillStyle = "black";
	context.fillRect(0, 0, canvas.width, canvas.height);

	// text
	drawText("Hello World !!!");

	window.addEventListener("keydown",keyDown, false);
	window.addEventListener("click",mouseDown, false);
}

function drawText(text) {
	// Code
	// center the text
	context.textAlign = "center"; // center horizontally
	context.textBaseLine = "middle"; // center vertically

	// text font
	context.font = "48px sans-serif";

	// text color
	context.fillStyle = "white";

	// display the text in center
	context.fillText(text,canvas.width/2, canvas.height/2);

}

function toggleFullScreen() {
	// Code
	var fullscreen_element =
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if (fullscreen_element == null) {
		if (canvas.requestFullscreen) {
			canvas.requestFullscreen();
		}
		else if (canvas.mozRequestFullScreen) {
			canvas.mozRequestFullScreen();
		}
		else if (canvas.webkitRequestFullscreen) {
			canvas.webkitRequestFullscreen();
		}
		else if (canvas.msRequestFullscreen) {
			canvas.msRequestFullscreen();
		}
	}
	else // if already fullscreen
	{
		if (document.exitFullscreen) {
			document.exitFullscreen();
		}
		else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}
		else if (document.webkitExitFullscreen) {
			document.webkitExitullscreen();
		}
		else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}
	}
}

function keyDown(event) {
	// Code
	switch(event.keyCode) {
		case 70: // For 'F' or 'f'
			toggleFullScreen();

			// repaint
			drawText("Hello World !!!");
			break;
	}
}


function mouseDown() {
	// Code
}

// global variables
var canvas = null;
var gl = null;      // webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
    AMC_ATTRIBUTE_VERTEX : 0,
    AMC_ATTRIBUTE_COLOR : 1,
    AMC_ATTRIBUTE_NORMAL : 2,
    AMC_ATTRIBUTE_TEXTURE0 : 3,
};

var lightAmbientZero = [ 0.0, 0.0, 0.0 ];
var lightDiffuseZero = [ 1.0, 0.0, 0.0 ];
var lightSpecularZero = [ 1.0, 0.0, 0.0 ];
var lightPositionZero = [ -2.0, 0.0, 0.0, 1.0 ];

var lightAmbientOne = [ 0.0, 0.0, 0.0 ];
var lightDiffuseOne = [ 0.0, 1.0, 0.0 ];
var lightSpecularOne = [ 0.0, 1.0, 0.0 ];
var lightPositionOne = [ 0.0, 0.0, 1.0, 1.0 ];

var lightAmbientTwo = [ 0.0, 0.0, 0.0 ];
var lightDiffuseTwo = [ 0.0, 0.0, 1.0 ];
var lightSpecularTwo = [ 0.0, 0.0, 1.0 ];
var lightPositionTwo = [ 0.0, 0.0, 1.0, 1.0 ];

var materialAmbient = [ 0.0, 0.0, 0.0 ];
var materialDiffuse = [ 1.0, 1.0, 1.0 ];
var materialSpecular = [ 1.0, 1.0, 1.0 ];
var materialShininess = 128.0;

// Animating variables
var lightAngleZero = 0.0;
var lightAngleOne = 0.0;
var lightAngleTwo = 0.0;

var sphere;

// PerVertex Variables
var mUniformPerVertex;
var vUniformPerVertex;
var pUniformPerVertex;

var laUniformRedPerVertex;
var ldUniformRedPerVertex;
var lsUniformRedPerVertex;
var lightPositionUniformRedPerVertex;

var laUniformGreenPerVertex;
var ldUniformGreenPerVertex;
var lsUniformGreenPerVertex;
var lightPositionUniformGreenPerVertex;

var laUniformBluePerVertex;
var ldUniformBluePerVertex;
var lsUniformBluePerVertex;
var lightPositionUniformBluePerVertex;

var kaUniformPerVertex;
var kdUniformPerVertex;
var ksUniformPerVertex;
var materialShinyninessUniformPerVertex;
var lKeyPressedUniformPerVertex;

// PerVertex Shader variables
var vertexShaderObjectPerVertex;
var fragmentShaderObjectPerVertex;
var shaderProgramObjectPerVertex;

// PerFragment Variables
var mUniformPerFragment;
var vUniformPerFragment;
var pUniformPerFragment;

var laUniformRedPerFragment;
var ldUniformRedPerFragment;
var lsUniformRedPerFragment;
var lightPositionUniformRedPerFragment;

var laUniformBluePerFragment;
var ldUniformBluePerFragment;
var lsUniformBluePerFragment;
var lightPositionUniformBluePerFragment;

var laUniformGreenPerFragment;
var ldUniformGreenPerFragment;
var lsUniformGreenPerFragment;
var lightPositionUniformGreenPerFragment;

var kaUniformPerFragment;
var kdUniformPerFragment;
var ksUniformPerFragment;
var materialShinyninessUniformPerFragment;
var lKeyPressedUniformPerFragment;

var vertexShaderObjectPerFragment;
var fragmentShaderObjectPerFragment;
var shaderProgramObjectPerFragment;

var bFragmentShaderToggel = false;

var perspectiveProjectionMatrix;

// Light Variables
var isLKeyPressed = false;

// To start animation:
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame ||
null;

// onload function
function main() {
    // get <Canvas> element
    canvas = document.getElementById("AMC");
    if (!canvas) {
        console.log("Obtaining Canvas failed\n");
    } 
    else
        console.log("Obtaining Canvas successed\n");
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    window.addEventListener("keydown",keyDown, false);
    window.addEventListener("click",mouseDown, false);
    window.addEventListener("resize",resize, false);

    // initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen() {
    // Code
    var fullscreen_element =
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    // if not fullscreen
    if (fullscreen_element == null) {
        if (canvas.requestFullscreen) {
            canvas.requestFullscreen();
        }
        else if (canvas.mozRequestFullScreen) {
            canvas.mozRequestFullScreen();
        }
        else if (canvas.webkitRequestFullscreen) {
            canvas.webkitRequestFullscreen();
        }
        else if (canvas.msRequestFullscreen) {
            canvas.msRequestFullscreen();
        }
        bFullscreen = true;
    }
    else // if already fullscreen
    {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        }
        else if (document.webkitExitFullscreen) {
            document.webkitExitullscreen();
        }
        else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        bFullscreen = false;
    }
}

function init() {
    // code
    // get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if (gl == null) {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader PerVertex
    var vertexShaderSourceCodePerVertex =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "precision mediump int;" +
        "in vec4 vPosition;" +
        "in vec3 vNormal;" +
        "uniform mat4 u_model_matrix;" +
        "uniform mat4 u_view_matrix;" +
        "uniform mat4 u_projection_matrix;" +
        "uniform mediump int u_lKeyPressed;" +
        "uniform vec3 u_la_red;" +
        "uniform vec3 u_ld_red;" +
        "uniform vec3 u_ls_red;" +
        "uniform vec4 u_light_position_red;" +

        "uniform vec3 u_la_green;" +
        "uniform vec3 u_ld_green;" +
        "uniform vec3 u_ls_green;" +
        "uniform vec4 u_light_position_green;" +

        "uniform vec3 u_la_blue;" +
        "uniform vec3 u_ld_blue;" +
        "uniform vec3 u_ls_blue;" +
        "uniform vec4 u_light_position_blue;" +

        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_shininess;"	+
        "out vec3 phong_ads_light;" +
        "void main(void)" +
        "{" +
        "	if(u_lKeyPressed == 1)" +
        "	{" +
        "		vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"	+
        "		vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal );" +
        "		vec3 viewer_vector = normalize(-eye_coordinates.xyz);" +

        "		vec3 light_direction_red = normalize(vec3(u_light_position_red) - eye_coordinates.xyz );" +
        "		vec3 reflection_vector_red = reflect(-light_direction_red, transformed_normals);" +
        "		float tn_dot_ld_red = max(dot(transformed_normals, light_direction_red), 0.0);" +
        "		vec3 ambient_red = u_la_red * u_ka;" +
        "		vec3 diffuse_red =u_ld_red * u_kd * tn_dot_ld_red;" +
        "		vec3 specular_red = u_ls_red * u_ks * pow(max(dot(reflection_vector_red, viewer_vector), 0.0), u_shininess);" +

        "		vec3 light_direction_green = normalize(vec3(u_light_position_green) - eye_coordinates.xyz );" +
        "		vec3 reflection_vector_green = reflect(-light_direction_green, transformed_normals);" +
        "		float tn_dot_ld_green = max(dot(transformed_normals, light_direction_green), 0.0);" +
        "		vec3 ambient_green = u_la_green * u_ka;" +
        "		vec3 diffuse_green =u_ld_green * u_kd * tn_dot_ld_green;" +
        "		vec3 specular_green = u_ls_green * u_ks * pow(max(dot(reflection_vector_green, viewer_vector), 0.0), u_shininess);" +

        "		vec3 light_direction_blue = normalize(vec3(u_light_position_blue) - eye_coordinates.xyz );" +
        "		vec3 reflection_vector_blue = reflect(-light_direction_blue, transformed_normals);" +
        "		float tn_dot_ld_blue = max(dot(transformed_normals, light_direction_blue), 0.0);" +
        "		vec3 ambient_blue = u_la_blue * u_ka;" +
        "		vec3 diffuse_blue =u_ld_blue * u_kd * tn_dot_ld_blue;" +
        "		vec3 specular_blue = u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue, viewer_vector), 0.0), u_shininess);" +

        "		phong_ads_light = ambient_red + diffuse_red + specular_red;" +
        "		phong_ads_light = phong_ads_light + ambient_green + diffuse_green + specular_green;" +
        "		phong_ads_light = phong_ads_light + ambient_blue + diffuse_blue + specular_blue;" +
        "	}" +
        "	else" +
        "	{" +
        "		phong_ads_light = vec3(1.0, 1.0, 1.0);" +
        "	}" +
        "	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
        "}";

    vertexShaderObjectPerVertex = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObjectPerVertex, vertexShaderSourceCodePerVertex);
    gl.compileShader(vertexShaderObjectPerVertex);

    if (!gl.getShaderParameter(vertexShaderObjectPerVertex, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(vertexShaderObjectPerVertex);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader
    var fragmentShaderSourceCodePerVertex =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "precision mediump int;" +
        "in vec3 phong_ads_light;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "	FragColor = vec4(phong_ads_light, 1.0);"	+
        "}";

    fragmentShaderObjectPerVertex = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObjectPerVertex, fragmentShaderSourceCodePerVertex);
    gl.compileShader(fragmentShaderObjectPerVertex);

    if (!gl.getShaderParameter(fragmentShaderObjectPerVertex, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(fragmentShaderObjectPerVertex);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // shader program 
    shaderProgramObjectPerVertex = gl.createProgram();
    gl.attachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
    gl.attachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);

    // pre-linking binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObjectPerVertex, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObjectPerVertex, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

    // linking
    gl.linkProgram(shaderProgramObjectPerVertex);
    if (!gl.getProgramParameter(shaderProgramObjectPerVertex, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObjectPerVertex);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // get mvp uniform location
    mUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_model_matrix");
    vUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_view_matrix");
    pUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_projection_matrix");
    lKeyPressedUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_lKeyPressed");

    laUniformRedPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_la_red");
    ldUniformRedPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ld_red");
    lsUniformRedPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ls_red");
    lightPositionUniformRedPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position_red");

    laUniformGreenPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_la_green");
    ldUniformGreenPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ld_green");
    lsUniformGreenPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ls_green");
    lightPositionUniformGreenPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position_green");

    laUniformBluePerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_la_blue");
    ldUniformBluePerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ld_blue");
    lsUniformBluePerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ls_blue");
    lightPositionUniformBluePerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position_blue");

    kaUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ka");
    kdUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_kd");
    ksUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ks");
    materialShinyninessUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_shininess");

// -----------------------------------------------------------------------------------------------------------------------

    // vertex shader PerFragment
    var vertexShaderSourceCodePerFragment =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "precision mediump int;" +
        "in vec4 vPosition;" +
        "in vec3 vNormal;" +
        "uniform mat4 u_model_matrix;" +
        "uniform mat4 u_view_matrix;" +
        "uniform mat4 u_projection_matrix;" +

        "uniform vec4 u_light_position_red;" +
        "uniform vec4 u_light_position_green;" +
        "uniform vec4 u_light_position_blue;" +
        "uniform int u_lKeyPressed;" +
        "out vec3 tNorm;" +

        "out vec3 light_direction_red;" +
        "out vec3 light_direction_green;" +
        "out vec3 light_direction_blue;" +
        "out vec3 viewer_vector;" +
        "void main(void)" +
        "{" +
        "	if(u_lKeyPressed == 1)" +
        "	{" +
        "		vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"	+
        "		tNorm = mat3(u_view_matrix * u_model_matrix) * vNormal;" +
        "		light_direction_red = vec3(u_light_position_red) - eye_coordinates.xyz;" +
        "		light_direction_green = vec3(u_light_position_green) - eye_coordinates.xyz;" +
        "		light_direction_blue = vec3(u_light_position_blue) - eye_coordinates.xyz;" +
        "		viewer_vector = vec3(-eye_coordinates.xyz);" +
        "	}" +
        "	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
        "}";

    vertexShaderObjectPerFragment = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObjectPerFragment, vertexShaderSourceCodePerFragment);
    gl.compileShader(vertexShaderObjectPerFragment);

    if (!gl.getShaderParameter(vertexShaderObjectPerFragment, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(vertexShaderObjectPerFragment);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader
    var fragmentShaderSourceCodePerFragment =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "precision mediump int;" +
        "in vec3 tNorm;" +
        "in vec3 light_direction_red;" +
        "in vec3 light_direction_green;" +
        "in vec3 light_direction_blue;" +

        "in vec3 viewer_vector;" +
        "uniform vec3 u_la_red;" +
        "uniform vec3 u_ld_red;" +
        "uniform vec3 u_ls_red;" +

        "uniform vec3 u_la_green;" +
        "uniform vec3 u_ld_green;" +
        "uniform vec3 u_ls_green;" +

        "uniform vec3 u_la_blue;" +
        "uniform vec3 u_ld_blue;" +
        "uniform vec3 u_ls_blue;" +

        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_shininess;"	+
        "uniform int u_lKeyPressed;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "	if(u_lKeyPressed == 1)" +
        "	{" +
        "		vec3 ntNorm = normalize(tNorm);" +
        "		vec3 nviewer_vector  = normalize(viewer_vector);" +

        "		vec3 nlight_direction_red = normalize(light_direction_red);" +
        "		vec3 reflection_vector_red  = reflect(-nlight_direction_red, ntNorm);" +
        "		float tn_dot_ld_red = max(dot(ntNorm, nlight_direction_red), 0.0);" +
        "		vec3 ambient_red = u_la_red * u_ka;" +
        "		vec3 diffuse_red =u_ld_red * u_kd * tn_dot_ld_red;" +
        "		vec3 specular_red = u_ls_red * u_ks * pow(max(dot(reflection_vector_red, nviewer_vector), 0.0), u_shininess);" +

        "		vec3 nlight_direction_green = normalize(light_direction_green);" +
        "		vec3 reflection_vector_green  = reflect(-nlight_direction_green, ntNorm);" +
        "		float tn_dot_ld_green = max(dot(ntNorm, nlight_direction_green), 0.0);" +
        "		vec3 ambient_green = u_la_green * u_ka;" +
        "		vec3 diffuse_green =u_ld_green * u_kd * tn_dot_ld_green;" +
        "		vec3 specular_green = u_ls_green * u_ks * pow(max(dot(reflection_vector_green, nviewer_vector), 0.0), u_shininess);" +

        "		vec3 nlight_direction_blue = normalize(light_direction_blue);" +
        "		vec3 reflection_vector_blue  = reflect(-nlight_direction_blue, ntNorm);" +
        "		float tn_dot_ld_blue = max(dot(ntNorm, nlight_direction_blue), 0.0);" +
        "		vec3 ambient_blue = u_la_blue * u_ka;" +
        "		vec3 diffuse_blue =u_ld_blue * u_kd * tn_dot_ld_blue;" +
        "		vec3 specular_blue = u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue, nviewer_vector), 0.0), u_shininess);" +

        "		vec3 phong_ads_light = ambient_red + diffuse_red + specular_red;" +
        "		phong_ads_light = phong_ads_light + ambient_green + diffuse_green + specular_green;" +
        "		phong_ads_light = phong_ads_light + ambient_blue + diffuse_blue + specular_blue;" +
        "		FragColor = vec4(phong_ads_light, 1.0);"	+
        "	}" +
        "	else" +
        "	{" +
        "		FragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
        "	}" +
        "}";

    fragmentShaderObjectPerFragment = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObjectPerFragment, fragmentShaderSourceCodePerFragment);
    gl.compileShader(fragmentShaderObjectPerFragment);

    if (!gl.getShaderParameter(fragmentShaderObjectPerFragment, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(fragmentShaderObjectPerFragment);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // shader program 
    shaderProgramObjectPerFragment = gl.createProgram();
    gl.attachShader(shaderProgramObjectPerFragment, vertexShaderObjectPerFragment);
    gl.attachShader(shaderProgramObjectPerFragment, fragmentShaderObjectPerFragment);

    // pre-linking binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObjectPerFragment, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObjectPerFragment, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

    // linking
    gl.linkProgram(shaderProgramObjectPerFragment);
    if (!gl.getProgramParameter(shaderProgramObjectPerFragment, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObjectPerFragment);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // get mvp uniform location
    mUniformPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_model_matrix");
    vUniformPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_view_matrix");
    pUniformPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_projection_matrix");
    lKeyPressedUniformPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_lKeyPressed");

    laUniformRedPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_la_red");
    ldUniformRedPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_ld_red");
    lsUniformRedPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_ls_red");
    lightPositionUniformRedPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_light_position_red");

    laUniformGreenPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_la_green");
    ldUniformGreenPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_ld_green");
    lsUniformGreenPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_ls_green");
    lightPositionUniformGreenPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_light_position_green");

    laUniformBluePerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_la_blue");
    ldUniformBluePerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_ld_blue");
    lsUniformBluePerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_ls_blue");
    lightPositionUniformBluePerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_light_position_blue");

    kaUniformPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_ka");
    kdUniformPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_kd");
    ksUniformPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_ks");
    materialShinynessUniformPerFragment = gl.getUniformLocation(shaderProgramObjectPerFragment, "u_shininess");


    // vertex, colors, shader attribs, vbo, vao initalizations
    // sphere
    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

    // set clear color
    gl.clearColor(0.0, 0.0, 1.0, 1.0);

    // enable depth
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    // initialize projection matrix
    perspectiveProjectionMatrix = mat4.create();
}

function resize() {
    // Code
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    // set viewport
    gl.viewport(0, 0, canvas.width, canvas.height);

    // Perspective Projection
        mat4.perspective(perspectiveProjectionMatrix,45.0,
        parseFloat(canvas.width) / parseFloat(canvas.height),
        0.1,
        100.0);
}

function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    if (bFragmentShaderToggel) {
        // Per Fragment
        gl.useProgram(shaderProgramObjectPerFragment);
        if (isLKeyPressed) {
            gl.uniform3fv(laUniformRedPerFragment, lightAmbientZero);
            gl.uniform3fv(ldUniformRedPerFragment, lightDiffuseZero);
            gl.uniform3fv(lsUniformRedPerFragment, lightSpecularZero);
            var lightPositionRed = [0.0, 100.0 * Math.cos(lightAngleZero), 100.0 * Math.sin(lightAngleZero), 1.0];
            gl.uniform4fv(lightPositionUniformRedPerFragment, lightPositionRed);

            gl.uniform3fv(laUniformGreenPerFragment, lightAmbientOne);
            gl.uniform3fv(ldUniformGreenPerFragment, lightDiffuseOne);
            gl.uniform3fv(lsUniformGreenPerFragment, lightSpecularOne);
            var lightPositionGreen = [100.0 * Math.cos(lightAngleOne), 0.0, 100.0 * Math.sin(lightAngleOne), 1.0];
            gl.uniform4fv(lightPositionUniformGreenPerFragment, lightPositionGreen);

            gl.uniform3fv(laUniformBluePerFragment, lightAmbientTwo);
            gl.uniform3fv(ldUniformBluePerFragment, lightDiffuseTwo);
            gl.uniform3fv(lsUniformBluePerFragment, lightSpecularTwo);
            var lightPositionBlue = [100.0 * Math.cos(lightAngleTwo), 100.0 * Math.sin(lightAngleTwo), 0.0, 1.0];
            gl.uniform4fv(lightPositionUniformBluePerFragment, lightPositionBlue);

            gl.uniform3fv(kaUniformPerFragment, materialAmbient);
            gl.uniform3fv(kdUniformPerFragment, materialDiffuse);
            gl.uniform3fv(ksUniformPerFragment, materialSpecular);
            gl.uniform1f(materialShinynessUniformPerFragment, materialShininess);

            gl.uniform1i(lKeyPressedUniformPerFragment, 1);
        }
        else {
            gl.uniform1i(lKeyPressedUniformPerFragment, 0);
        }

        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();

        mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);

        gl.uniformMatrix4fv(mUniformPerFragment, false, modelMatrix);
        gl.uniformMatrix4fv(vUniformPerFragment, false, viewMatrix);
        gl.uniformMatrix4fv(pUniformPerFragment, false, perspectiveProjectionMatrix);
   }
    else {
        gl.useProgram(shaderProgramObjectPerVertex);
        if (isLKeyPressed) {
            gl.uniform3fv(laUniformRedPerVertex, lightAmbientZero);
            gl.uniform3fv(ldUniformRedPerVertex, lightDiffuseZero);
            gl.uniform3fv(lsUniformRedPerVertex, lightSpecularZero);
            var lightPositionRed = [0.0, 100.0 * Math.cos(lightAngleZero), 100.0 * Math.sin(lightAngleZero), 1.0];
            gl.uniform4fv(lightPositionUniformRedPerVertex, lightPositionRed);


            gl.uniform3fv(laUniformGreenPerVertex, lightAmbientOne);
            gl.uniform3fv(ldUniformGreenPerVertex, lightDiffuseOne);
            gl.uniform3fv(lsUniformGreenPerVertex, lightSpecularOne);
            var lightPositionGreen = [100.0 * Math.cos(lightAngleOne), 0.0, 100.0 * Math.sin(lightAngleOne), 1.0];
            gl.uniform4fv(lightPositionUniformGreenPerVertex, lightPositionGreen);

            gl.uniform3fv(laUniformBluePerVertex, lightAmbientTwo);
            gl.uniform3fv(ldUniformBluePerVertex, lightDiffuseTwo);
            gl.uniform3fv(lsUniformBluePerVertex, lightSpecularTwo);
            var lightPositionBlue = [100.0 * Math.cos(lightAngleTwo), 100.0 * Math.sin(lightAngleTwo), 0.0, 1.0];
            gl.uniform4fv(lightPositionUniformBluePerVertex, lightPositionBlue);

            gl.uniform3fv(kaUniformPerVertex, materialAmbient);
            gl.uniform3fv(kdUniformPerVertex, materialDiffuse);
            gl.uniform3fv(ksUniformPerVertex, materialSpecular);
            gl.uniform1f(materialShinyninessUniformPerVertex, materialShininess);

            gl.uniform1i(lKeyPressedUniformPerVertex, 1);
        }
        else {
            gl.uniform1i(lKeyPressedUniformPerVertex, 0);
        }
 
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();

        mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);

        gl.uniformMatrix4fv(mUniformPerVertex, false, modelMatrix);
        gl.uniformMatrix4fv(vUniformPerVertex, false, viewMatrix);
        gl.uniformMatrix4fv(pUniformPerVertex, false, perspectiveProjectionMatrix);
    }

    // Square
    sphere.draw();
    gl.useProgram(null);

    // animation loop
    update();
    requestAnimationFrame(draw, canvas);
}

function update() {
    /* code */
    lightAngleZero -= 0.005;
    if (lightAngleZero > 360.0)
    {
        lightAngleZero += 360.0;
    }
    lightAngleOne -= 0.005;
    if (lightAngleOne > 360.0)
    {
        lightAngleOne += 360.0;
    }
    lightAngleTwo -= 0.005;
    if (lightAngleTwo > 360.0)
    {
        lightAngleTwo += 360.0;
    }
}

function uninitialize() {
    // Code
    if (shaderProgramObjectPerVertex) {
        if (fragmentShaderObjectPerVertex) {
            gl.detachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);
            gl.deleteShader(fragmentShaderObjectPerVertex);
            fragmentShaderObjectPerVertex = null;
        }
        
        if (vertexShaderObjectPerVertex) {
            gl.detachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
            gl.deleteShader(vertexShaderObjectPerVertex);
            vertexShaderObjectPerVertex = null;
        }

        gl.deleteProgram(shaderProgramObjectPerVertex);
        shaderProgramObjectPerVertex = null;
    }
}

function keyDown(event) {
    // Code
    switch(event.keyCode) {
        case 27:
            uninitialize();
            window.close();
            break;

        case 69:
            toggleFullScreen();
            break;

        case 70: // For 'F' or 'f'
            bFragmentShaderToggel = true;
            break;

        case 86:
            bFragmentShaderToggel = false;
            break;

        case 76: //For 'L' or 'l'
            if (isLKeyPressed == true) {
                isLKeyPressed = false;
            }
            else {
                isLKeyPressed = true;
            }
            break;
    }
}


function mouseDown() {
    // Code
}

function degToRad(degrees) {
    return(degrees*Math.PI/180.0);
}

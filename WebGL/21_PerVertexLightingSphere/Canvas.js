// global variables
var canvas = null;
var gl = null;      // webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
    AMC_ATTRIBUTE_VERTEX : 0,
    AMC_ATTRIBUTE_COLOR : 1,
    AMC_ATTRIBUTE_NORMAL : 2,
    AMC_ATTRIBUTE_TEXTURE0 : 3,
};

var lightAmbient = [ 0.0, 0.0, 0.0 ];
var lightDiffuse = [ 1.0, 1.0, 1.0 ];
var lightSpecular = [ 1.0, 1.0, 1.0 ];
var lightPosition = [ 100.0, 100.0, 100.0, 1.0 ];

var materialAmbient = [ 0.0, 0.0, 0.0 ];
var materialDiffuse = [ 1.0, 1.0, 1.0 ];
var materialSpecular = [ 1.0, 1.0, 1.0 ];
var materialShininess = 128.0;

var sphere;

// PerVertex Variables
var mUniformPerVertex;
var vUniformPerVertex;
var pUniformPerVertex;

var laUniformPerVertex;
var ldUniformPerVertex;
var lsUniformPerVertex;
var lightPositionUniformPerVertex;

var kaUniformPerVertex;
var kdUniformPerVertex;
var ksUniformPerVertex;
var materialShinyninessUniformPerVertex;

var lKeyPressedUniformPerVertex;

// PerVertex Shader variables
var vertexShaderObjectPerVertex;
var fragmentShaderObjectPerVertex;
var shaderProgramObjectPerVertex;

var perspectiveProjectionMatrix;

// Light Variables
var isLKeyPressed = false;

// To start animation:
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame ||
null;

// onload function
function main() {
    // get <Canvas> element
    canvas = document.getElementById("AMC");
    if (!canvas) {
        console.log("Obtaining Canvas failed\n");
    } 
    else
        console.log("Obtaining Canvas successed\n");
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    window.addEventListener("keydown",keyDown, false);
    window.addEventListener("click",mouseDown, false);
    window.addEventListener("resize",resize, false);

    // initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen() {
    // Code
    var fullscreen_element =
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    // if not fullscreen
    if (fullscreen_element == null) {
        if (canvas.requestFullscreen) {
            canvas.requestFullscreen();
        }
        else if (canvas.mozRequestFullScreen) {
            canvas.mozRequestFullScreen();
        }
        else if (canvas.webkitRequestFullscreen) {
            canvas.webkitRequestFullscreen();
        }
        else if (canvas.msRequestFullscreen) {
            canvas.msRequestFullscreen();
        }
        bFullscreen = true;
    }
    else // if already fullscreen
    {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        }
        else if (document.webkitExitFullscreen) {
            document.webkitExitullscreen();
        }
        else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        bFullscreen = false;
    }
}

function init() {
    // code
    // get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if (gl == null) {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader PerVertex
    var vertexShaderSourceCodePerVertex =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec3 vNormal;" +
        "uniform mat4 u_model_matrix;" +
        "uniform mat4 u_view_matrix;" +
        "uniform mat4 u_projection_matrix;" +
        "uniform mediump int u_lKeyPressed;" +
        "uniform vec3 u_la;" +
        "uniform vec3 u_ld;" +
        "uniform vec3 u_ls;" +
        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_shininess;"    +
        "uniform vec4 u_light_position;" +
        "out vec3 phong_ads_light;" +
        "void main(void)" +
        "{" +
        "   if(u_lKeyPressed == 1)" +
        "   {" +
        "       vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
        "       vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal );" +
        "       vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz );" +
        "       float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" +
        "       vec3 ambient = u_la * u_ka;" +
        "       vec3 diffuse =u_ld * u_kd * tn_dot_ld;" +
        "       vec3 reflection_vector = reflect(-light_direction, transformed_normals);" +
        "       vec3 viewer_vector = normalize(-eye_coordinates.xyz);" +
        "       vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_shininess);" +
        "       phong_ads_light = ambient + diffuse + specular;" +
        "   }" +
        "   else" +
        "   {" +
        "       phong_ads_light = vec3(1.0, 1.0, 1.0);" +
        "   }" +
        "   gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
        "}";

    vertexShaderObjectPerVertex = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObjectPerVertex, vertexShaderSourceCodePerVertex);
    gl.compileShader(vertexShaderObjectPerVertex);

    if (!gl.getShaderParameter(vertexShaderObjectPerVertex, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(vertexShaderObjectPerVertex);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader
    var fragmentShaderSourceCodePerVertex =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec3 phong_ads_light;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "   FragColor = vec4(phong_ads_light, 1.0);"    +
        "}";

    fragmentShaderObjectPerVertex = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObjectPerVertex, fragmentShaderSourceCodePerVertex);
    gl.compileShader(fragmentShaderObjectPerVertex);

    if (!gl.getShaderParameter(fragmentShaderObjectPerVertex, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(fragmentShaderObjectPerVertex);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // shader program 
    shaderProgramObjectPerVertex = gl.createProgram();
    gl.attachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
    gl.attachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);

    // pre-linking binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObjectPerVertex, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObjectPerVertex, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

    // linking
    gl.linkProgram(shaderProgramObjectPerVertex);
    if (!gl.getProgramParameter(shaderProgramObjectPerVertex, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObjectPerVertex);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // get mvp uniform location
    mUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_model_matrix");
    vUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_view_matrix");
    pUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_projection_matrix");
    lKeyPressedUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_lKeyPressed");
    laUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_la");
    ldUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ld");
    lsUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ls");
    lightPositionUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_light_position");
    kaUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ka");
    kdUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_kd");
    ksUniformPerVertex = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_ks");
    materialShinynessUniform = gl.getUniformLocation(shaderProgramObjectPerVertex, "u_shininess");

    // vertex, colors, shader attribs, vbo, vao initalizations
    // sphere
    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

    // set clear color
    gl.clearColor(0.0, 0.0, 1.0, 1.0);

    // enable depth
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    // initialize projection matrix
    perspectiveProjectionMatrix = mat4.create();
}

function resize() {
    // Code
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    // set viewport
    gl.viewport(0, 0, canvas.width, canvas.height);

    // Perspective Projection
        mat4.perspective(perspectiveProjectionMatrix,45.0,
        parseFloat(canvas.width) / parseFloat(canvas.height),
        0.1,
        100.0);
}

function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObjectPerVertex);

    if (isLKeyPressed) {
        gl.uniform3fv(laUniformPerVertex, lightAmbient);
        gl.uniform3fv(ldUniformPerVertex, lightDiffuse);
        gl.uniform3fv(lsUniformPerVertex, lightSpecular);
        gl.uniform4fv(lightPositionUniformPerVertex, lightPosition);

        gl.uniform3fv(kaUniformPerVertex, materialAmbient);
        gl.uniform3fv(kdUniformPerVertex, materialDiffuse);
        gl.uniform3fv(ksUniformPerVertex, materialSpecular);
        gl.uniform1f(materialShinynessUniform, materialShininess);

        gl.uniform1i(lKeyPressedUniformPerVertex, 1);
    }
    else {
        gl.uniform1i(lKeyPressedUniformPerVertex, 0);
    }
    // Square
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();

    mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(mUniformPerVertex, false, modelMatrix);
    gl.uniformMatrix4fv(vUniformPerVertex, false, viewMatrix);
    gl.uniformMatrix4fv(pUniformPerVertex, false, perspectiveProjectionMatrix);

    sphere.draw();
    gl.useProgram(null);

    // animation loop
    update();
    requestAnimationFrame(draw, canvas);
}

function update() {
    /* code */
}

function uninitialize() {
    // Code
    if (shaderProgramObjectPerVertex) {
        if (fragmentShaderObjectPerVertex) {
            gl.detachShader(shaderProgramObjectPerVertex, fragmentShaderObjectPerVertex);
            gl.deleteShader(fragmentShaderObjectPerVertex);
            fragmentShaderObjectPerVertex = null;
        }
        
        if (vertexShaderObjectPerVertex) {
            gl.detachShader(shaderProgramObjectPerVertex, vertexShaderObjectPerVertex);
            gl.deleteShader(vertexShaderObjectPerVertex);
            vertexShaderObjectPerVertex = null;
        }

        gl.deleteProgram(shaderProgramObjectPerVertex);
        shaderProgramObjectPerVertex = null;
    }
}

function keyDown(event) {
    // Code
    switch(event.keyCode) {
        case 27:
            uninitialize();
            window.close();
            break;
        case 70: // For 'F' or 'f'
            toggleFullScreen();
            break;
        case 76: //For 'L' or 'l'
            if (isLKeyPressed == true) {
                isLKeyPressed = false;
            }
            else {
                isLKeyPressed = true;
            }
            break;
    }
}


function mouseDown() {
    // Code
}

function degToRad(degrees) {
    return(degrees*Math.PI/180.0);
}

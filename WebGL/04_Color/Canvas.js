// global variables
var canvas = null;
var gl = null;		// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

// To start animation:
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame ||
null;

// onload function
function main()
{
	// get <Canvas> element
	canvas = document.getElementById("AMC");
	if (!canvas) {
		console.log("Obtaining Canvas failed\n");
	} 
	else
		console.log("Obtaining Canvas successed\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	window.addEventListener("keydown",keyDown, false);
	window.addEventListener("click",mouseDown, false);
	window.addEventListener("resize",resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen() {
	// Code
	var fullscreen_element =
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if (fullscreen_element == null) {
		if (canvas.requestFullscreen) {
			canvas.requestFullscreen();
		}
		else if (canvas.mozRequestFullScreen) {
			canvas.mozRequestFullScreen();
		}
		else if (canvas.webkitRequestFullscreen) {
			canvas.webkitRequestFullscreen();
		}
		else if (canvas.msRequestFullscreen) {
			canvas.msRequestFullscreen();
		}
		bFullscreen = true;
	}
	else // if already fullscreen
	{
		if (document.exitFullscreen) {
			document.exitFullscreen();
		}
		else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}
		else if (document.webkitExitFullscreen) {
			document.webkitExitullscreen();
		}
		else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}
		bFullscreen = false;
	}
}

function init() {
	// Code
	// get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if (gl == null) {
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.vewportWidth = canvas.width;
	gl.vewportHeight = canvas.height;

	// Clear Color
	gl.clearColor(0.0, 0.0, 1.0, 1.0); // blue
}

function resize() {
	// Code
	if (bFullscreen == true) {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else {
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set viewport
	gl.viewport(0, 0, canvas.width, canvas.height);
}

function draw() {
	gl.clear(gl.COLOR_BUFFER_BIT);

	// animation loop
	requestAnimationFrame(draw, canvas);
}
function keyDown(event) {
	// Code
	switch(event.keyCode) {
		case 70: // For 'F' or 'f'
			toggleFullScreen();

			// repaint
			drawText("Hello World !!!");
			break;
	}
}


function mouseDown() {
	// Code
}

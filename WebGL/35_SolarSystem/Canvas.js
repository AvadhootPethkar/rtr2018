// global variables
var canvas = null;
var gl = null;      // webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
    AMC_ATTRIBUTE_VERTEX : 0,
    AMC_ATTRIBUTE_COLOR : 1,
    AMC_ATTRIBUTE_NORMAL : 2,
    AMC_ATTRIBUTE_TEXTURE0 : 3,
};

var lightAmbient = [ 0.0, 0.0, 0.0 ];
var lightDiffuse = [ 1.0, 1.0, 1.0 ];
var lightSpecular = [ 1.0, 1.0, 1.0 ];
var lightPosition = [ 100.0, 100.0, 100.0, 1.0 ];

var materialAmbient = [ 0.0, 0.0, 0.0 ];
var materialDiffuse = [ 1.0, 1.0, 1.0 ];
var materialSpecular = [ 1.0, 1.0, 1.0 ];
var materialShininess = 128.0;

var sphere;

// PerVertex Variables
var mvpUniform;

var coords;
var year = 0;
var day = 0;

var sphere = null;

var stack = new Stack();

// PerVertex Shader variables
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var perspectiveProjectionMatrix;

// To start animation:
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame ||
null;

// onload function
function main() {
    // get <Canvas> element
    canvas = document.getElementById("AMC");
    if (!canvas) {
        console.log("Obtaining Canvas failed\n");
    } 
    else
        console.log("Obtaining Canvas successed\n");
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    window.addEventListener("keydown",keyDown, false);
    window.addEventListener("click",mouseDown, false);
    window.addEventListener("resize",resize, false);

    // initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen() {
    // Code
    var fullscreen_element =
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    // if not fullscreen
    if (fullscreen_element == null) {
        if (canvas.requestFullscreen) {
            canvas.requestFullscreen();
        }
        else if (canvas.mozRequestFullScreen) {
            canvas.mozRequestFullScreen();
        }
        else if (canvas.webkitRequestFullscreen) {
            canvas.webkitRequestFullscreen();
        }
        else if (canvas.msRequestFullscreen) {
            canvas.msRequestFullscreen();
        }
        bFullscreen = true;
    }
    else // if already fullscreen
    {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        }
        else if (document.webkitExitFullscreen) {
            document.webkitExitullscreen();
        }
        else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        bFullscreen = false;
    }
}

function init() {
    // code
    // get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if (gl == null) {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader PerVertex
    var vertexShaderSourceCodePerVertex =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec4 vColor;" +
        "uniform mat4 u_mvp_matrix;" +
        "out vec4 out_Color;" +
        "void main (void)" +
        "{" +
        "   gl_Position = u_mvp_matrix * vPosition;" +
        "   out_Color = vColor;" +
        "}";

    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCodePerVertex);
    gl.compileShader(vertexShaderObject);

    if (!gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader
    var fragmentShaderSourceCodePerVertex =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec4 out_Color;" +
        "out vec4 FragColor;" +
        "void main (void)" +
        "{" +
        "   FragColor = out_Color;" +
        "}";

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCodePerVertex);
    gl.compileShader(fragmentShaderObject);

    if (!gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // shader program 
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // pre-linking binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");

    // linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // get mvp uniform location
    mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

    gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, 1.0, 1.0, 0.0);

    // vertex, colors, shader attribs, vbo, vao initalizations
    // sphere
    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

    // set clear color
    gl.clearColor(0.0, 0.0, 1.0, 1.0);

    // enable depth
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    // initialize projection matrix
    perspectiveProjectionMatrix = mat4.create();
}

function resize() {
    // Code
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    // set viewport
    gl.viewport(0, 0, canvas.width, canvas.height);

    // Perspective Projection
        mat4.perspective(perspectiveProjectionMatrix,45.0,
        parseFloat(canvas.width) / parseFloat(canvas.height),
        0.1,
        100.0);
}

function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);

    // Square
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();

    mat4.lookAt(modelViewMatrix, [0.0, 0.0, 12.0], [0.0, 0.0, 0.0], [0.0, 1.0, 0.0]);
    stack.push(modelViewMatrix);

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    sphere.draw();

    mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(year));
    mat4.translate(modelViewMatrix, modelViewMatrix, [3.5, 0.0, 0.0]);
    mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(day));

    stack.push(modelViewMatrix);

    mat4.scale(modelViewMatrix, modelViewMatrix, [0.5, 0.5, 0.5]);
    mat4.multiply(modelViewProjectionMatrix,
        perspectiveProjectionMatrix, modelViewMatrix);

    gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

    sphere.draw();
    stack.pop();
    stack.pop();

    gl.useProgram(null);

    // animation loop
    update();
    requestAnimationFrame(draw, canvas);
}

function update() {
    /* code */
}

function uninitialize() {
    // Code
    if (shaderProgramObject) {
        if (fragmentShaderObject) {
            gl.detachShader(shaderProgramObject, fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject = null;
        }
        
        if (vertexShaderObject) {
            gl.detachShader(shaderProgramObject, vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject = null;
        }

        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}

function keyDown(event) {
    // Code
    switch(event.keyCode) {
        case 27:
            uninitialize();
            window.close();
            break;
        case 68:
            day = (day + 3) % 360;
            break;
        case 70: // For 'F' or 'f'
            toggleFullScreen();
            break;
        case 89:
            year = (year + 3) % 360;
            break;
    }
}


function mouseDown() {
    // Code
}

function degToRad(degrees) {
    return(degrees*Math.PI/180.0);
}

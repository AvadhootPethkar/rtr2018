// global variables
var canvas = null;
var gl = null;      // webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
    AMC_ATTRIBUTE_VERTEX : 0,
    AMC_ATTRIBUTE_COLOR : 1,
    AMC_ATTRIBUTE_NORMAL : 2,
    AMC_ATTRIBUTE_TEXTURE0 : 3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var lightAmbient = [ 0.0, 0.0, 0.0 ];
var lightDiffuse = [ 1.0, 1.0, 1.0 ];
var lightSpecular = [ 1.0, 1.0, 1.0 ];
var lightPosition = [ 100.0, 100.0, 100.0, 1.0 ];

var materialAmbient = [ 0.0, 0.0, 0.0 ];
var materialDiffuse = [ 1.0, 1.0, 1.0 ];
var materialSpecular = [ 1.0, 1.0, 1.0 ];
var materialShininess = 128.0;

var sphere;

var mUniform;
var vUniform;
var pUniform;

var laUniform;
var ldUniform;
var lsUniform;
var lightPositionUniform;
var lightRotationUniform;

var kaUniform;
var kdUniform;
var ksUniform;
var materialShinyninessUniform;

var lKeyPressedUniform;

var perspectiveProjectionMatrix;

// Light Variables
var isLKeyPressed = false;

// To start animation:
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame ||
null;

class Material
{
    constructor(ambient_material,diffuse_material,specular_material,shininess)
    {
        this.ambient_material=ambient_material;
        this.diffuse_material =diffuse_material;
        this.specular_material =specular_material;
        this.shininess =shininess;
    }
}

var material = new Array(6);

var gIsXKeyPressed = false;
var gIsYKeyPressed = false;
var gIsZKeyPressed = false;

var sphere_angle = 0.0; 

// onload function
function main() {
    // get <Canvas> element
    canvas = document.getElementById("AMC");
    if (!canvas) {
        console.log("Obtaining Canvas failed\n");
    } 
    else
        console.log("Obtaining Canvas successed\n");
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    window.addEventListener("keydown",keyDown, false);
    window.addEventListener("click",mouseDown, false);
    window.addEventListener("resize",resize, false);

    // initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen() {
    // Code
    var fullscreen_element =
    document.webkitFullscreenElement ||
    document.mozFullScreenElement ||
    document.msFullscreenElement ||
    null;

    // if not fullscreen
    if (fullscreen_element == null) {
        if (canvas.requestFullscreen) {
            canvas.requestFullscreen();
        }
        else if (canvas.mozRequestFullScreen) {
            canvas.mozRequestFullScreen();
        }
        else if (canvas.webkitRequestFullscreen) {
            canvas.webkitRequestFullscreen();
        }
        else if (canvas.msRequestFullscreen) {
            canvas.msRequestFullscreen();
        }
        bFullscreen = true;
    }
    else // if already fullscreen
    {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        }
        else if (document.webkitExitFullscreen) {
            document.webkitExitullscreen();
        }
        else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        bFullscreen = false;
    }
}

function init() {
    // code
    // get WebGL 2.0 context
    gl = canvas.getContext("webgl2");
    if (gl == null) {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec3 vNormal;" +
        "uniform mat4 u_model_matrix;" +
        "uniform mat4 u_view_matrix;" +
        "uniform mat4 u_projection_matrix;" +
        "uniform vec4 u_light_position;" +
        "uniform mat4 u_light_rotation;"+
        "uniform mediump int u_lKeyPressed;" +
        "out vec3 tNorm;" +
        "out vec3 light_direction;" +
        "out vec3 viewer_vector;" +
        "void main(void)" +
        "{" +
        "   if(u_lKeyPressed == 1)" +
        "   {" +
        "       vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;" +
        "       tNorm = mat3(u_view_matrix * u_model_matrix) * vNormal;" +
        "       light_direction = vec3(u_light_position * u_light_rotation) - eye_coordinates.xyz;" +
        "       float tn_dot_ld = max(dot(tNorm, light_direction), 0.0);" +
        "       viewer_vector = vec3(-eye_coordinates.xyz);" +
        "   }" +
        "   gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
        "}";

    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);

    if (!gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // fragment shader
    var fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec3 tNorm;" +
        "in vec3 light_direction;" +
        "in vec3 viewer_vector;" +
        "uniform vec3 u_la;" +
        "uniform vec3 u_ld;" +
        "uniform vec3 u_ls;" +
        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_shininess;" +
        "uniform mediump int u_lKeyPressed;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "   if(u_lKeyPressed == 1)" +
        "   {" +
        "       vec3 ntNorm = normalize(tNorm);" +
        "       vec3 nlight_direction = normalize(light_direction);" +
        "       vec3 nviewer_vector  = normalize(viewer_vector);" +
        "       vec3 reflection_vector  = reflect(-nlight_direction, ntNorm);" +
        "       float tn_dot_ld = max(dot(ntNorm, nlight_direction), 0.0);" +
        "       vec3 ambient = u_la * u_ka;" +
        "       vec3 diffuse =u_ld * u_kd * tn_dot_ld;" +
        "       vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, nviewer_vector), 0.0), u_shininess);" +
        "       vec3 phong_ads_light = ambient + diffuse + specular;" +
        "       FragColor = vec4(phong_ads_light, 1.0);" +
        "   }" +
        "   else" +
        "   {" +
        "       FragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
        "   }" +
        "}";

    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
    gl.compileShader(fragmentShaderObject);

    if (!gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS)) {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // shader program 
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    // pre-linking binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

    // linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    // get mvp uniform location
    mUniform = gl.getUniformLocation(shaderProgramObject, "u_model_matrix");
    vUniform = gl.getUniformLocation(shaderProgramObject, "u_view_matrix");
    pUniform = gl.getUniformLocation(shaderProgramObject, "u_projection_matrix");
    lKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_lKeyPressed");
    laUniform = gl.getUniformLocation(shaderProgramObject, "u_la");
    ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld");
    lsUniform = gl.getUniformLocation(shaderProgramObject, "u_ls");
    lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");
    lightRotationUniform = gl.getUniformLocation(shaderProgramObject, "u_light_rotation");
    kaUniform = gl.getUniformLocation(shaderProgramObject, "u_ka");
    kdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
    ksUniform = gl.getUniformLocation(shaderProgramObject, "u_ks");
    materialShinynessUniform = gl.getUniformLocation(shaderProgramObject, "u_shininess");

    // vertex, colors, shader attribs, vbo, vao initalizations
    // sphere
    sphere = new Mesh();
    makeSphere(sphere, 2.0, 30, 30);

    // set clear color
    gl.clearColor(0.0, 0.0, 1.0, 1.0);

    // enable depth
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    // initialize projection matrix
    perspectiveProjectionMatrix = mat4.create();

    InitializeMaterials();
}

function resize() {
    // Code
    if (bFullscreen == true) {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    // set viewport
    gl.viewport(0, 0, canvas.width, canvas.height);

    // Perspective Projection
        mat4.perspective(perspectiveProjectionMatrix,45.0,
        parseFloat(canvas.width) / parseFloat(canvas.height),
        0.1,
        100.0);
}

function draw() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    gl.useProgram(shaderProgramObject);

    if (isLKeyPressed) {
        gl.uniform3fv(laUniform, lightAmbient);
        gl.uniform3fv(ldUniform, lightDiffuse);
        gl.uniform3fv(lsUniform, lightSpecular);
        gl.uniform4fv(lightPositionUniform, lightPosition);

/*        gl.uniform3fv(kaUniform, materialAmbient);
        gl.uniform3fv(kdUniform, materialDiffuse);
        gl.uniform3fv(ksUniform, materialSpecular);
        gl.uniform1f(materialShinynessUniform, materialShininess);
*/
        gl.uniform1i(lKeyPressedUniform, 1);
    }
    else {
        gl.uniform1i(lKeyPressedUniform, 0);
    }

    // Square
    var modelMatrix = mat4.create();
    var viewMatrix = mat4.create();
    var rotateMatrix=mat4.create();

    mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -6.0]);
    gl.uniformMatrix4fv(mUniform, false, modelMatrix);
    gl.uniformMatrix4fv(vUniform, false, viewMatrix);
    gl.uniformMatrix4fv(pUniform, false, perspectiveProjectionMatrix);

    var  col = 5;
    for (var i = 0; i < 6; i++)
    {
        for (var j = 0; j < 4; j++)
        {


            var newWidth = (canvas.width / 4.0);
            var newHeight =(canvas.height / 6.0);

            gl.viewport((newWidth*j), newHeight*col, newWidth, newHeight);
            mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(newWidth)/parseFloat(newHeight), 0.1, 100.0);
            

            // setting material's properties
            
            gl.uniform3fv(kaUniform, material[i][j].ambient_material); // ambient reflectivity of material
            gl.uniform3fv(kdUniform, material[i][j].diffuse_material); // diffuse reflectivity of material
            gl.uniform3fv(ksUniform, material[i][j].specular_material); // specular reflectivity of material
            gl.uniform1f(materialShinynessUniform, material[i][j].shininess); // material shininess
    
        

            rotateMatrix = mat4.create();
            
            if (gIsXKeyPressed == true)
            {
                mat4.rotateX(rotateMatrix ,rotateMatrix,sphere_angle);
            
                lightPosition[1] = sphere_angle;
                gl.uniform4fv(lightPositionUniform, lightPosition);
                gl.uniformMatrix4fv(lightRotationUniform, false, rotateMatrix);
            }
            else
            if (gIsYKeyPressed == true)
            {
                mat4.rotateY(rotateMatrix ,rotateMatrix,sphere_angle);
            
                lightPosition[0] = sphere_angle;
                gl.uniform4fv(lightPositionUniform,  lightPosition);
                gl.uniformMatrix4fv(lightRotationUniform, false , rotateMatrix);
            }
            else
            if (gIsZKeyPressed == true)
            {
                mat4.rotateZ(rotateMatrix ,rotateMatrix,sphere_angle);

                lightPosition[0] = sphere_angle;
                gl.uniform4fv(lightPositionUniform, lightPosition);
                gl.uniformMatrix4fv(lightRotationUniform, false, rotateMatrix);
            }
            else
            {
                lightPosition[3] = 0.0;
                lightPosition[1] = 0.0;
                lightPosition[2] = 1.0;
                gl.uniform4fv(lightPositionUniform, lightPosition);
            }
            
            gl.uniformMatrix4fv(mUniform, false, modelMatrix);

            //START DRAWING
            sphere.draw();
            // UNBING VAO

        }

        col--;
    }

    gl.useProgram(null);

    // animation loop
    update();
    requestAnimationFrame(draw, canvas);
}

function update() {
    /* code */
    if (sphere_angle >= 720.0)
        sphere_angle = 360.0;
    else
        sphere_angle = sphere_angle + 0.06;
}

function uninitialize() {
    // Code
    if (shaderProgramObject) {
        if (fragmentShaderObject) {
            gl.detachShader(shaderProgramObject, fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject = null;
        }
        
        if (vertexShaderObject) {
            gl.detachShader(shaderProgramObject, vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject = null;
        }

        gl.deleteProgram(shaderProgramObject);
        shaderProgramObject = null;
    }
}

function keyDown(event) {
    // Code
    switch(event.keyCode) {
        case 27:
            uninitialize();
            window.close();
            break;
        case 70: // For 'F' or 'f'
            toggleFullScreen();
            break;
        case 76: //For 'L' or 'l'
            if (isLKeyPressed == true) {
                isLKeyPressed = false;
            }
            else {
                isLKeyPressed = true;
            }
            break;
        case 88:
            lightPosition[0] = 0.0;
            lightPosition[1] = 0.0;
            lightPosition[2] = 0.0;
            if (gIsXKeyPressed == true)
            {
                gIsXKeyPressed = false;
            }
            else
            {
                gIsXKeyPressed = true;
                gIsYKeyPressed = false;
                gIsZKeyPressed = false;
            }
            break;


        case 89:
            lightPosition[0] = 0.0;
            lightPosition[1] = 0.0;
            lightPosition[2] = 0.0;
            if (gIsYKeyPressed == true)
            {
                gIsYKeyPressed = false;
            }
            else
            {
                gIsYKeyPressed = true;
                gIsXKeyPressed = false;
                gIsZKeyPressed = false;
            }
            break;

        case 90:
            lightPosition[0] = 0.0;
            lightPosition[1] = 0.0;
            lightPosition[2] = 0.0;
            if (gIsZKeyPressed == true)
            {
                gIsZKeyPressed = false;
            }
            else
            {
                gIsZKeyPressed = true;
                gIsXKeyPressed = false;
                gIsYKeyPressed = false;
            }
            break;
    }
}


function mouseDown() {
    // Code
}

function degToRad(degrees) {
    return(degrees*Math.PI/180.0);
}

function InitializeMaterials()
{
    material [0] = new Array(4)
    material [1] = new Array(4)
    material [2] = new Array(4)
    material [3] = new Array(4)
    material [4] = new Array(4)
    material [5] = new Array(4)

    /***emerald**************************************************************************************/
    material[0][0] = new Material([0.0215,0.1745,0.0215],
                                  [0.07568,0.61424,0.07568],
                                  [0.633,0.727811,0.633],
                                  0.6 * 128.0);


    /***jade**************************************************************************************/
    material[1][0] = new Material([0.135,0.135,0.1575],
                                  [0.54,0.89,0.63],
                                  [0.316228,0.7316228,0.316228],
                                  0.1 * 128.0);


    /***obsidian**************************************************************************************/
    material[2][0] = new Material([0.05375,0.05,0.06625],
                                  [0.18275,0.17,0.22525],
                                  [0.332741,0.328634,0.346435],
                                  0.3 * 128.0);
                                  
    /***pearl**************************************************************************************/
    
    material[3][0] = new Material([0.25,0.2075,0.20725],
                                  [1.0,0.829,0.829],
                                  [0.296648,0.296648,0.296648],
                                  0.088* 128.0);
                                 

    /***ruby**************************************************************************************/
    material[4][0]= new Material([0.1745,0.01175,0.01175],
                                  [0.61424,0.04136,0.04136],
                                  [0.727811,0.626959,0.626959],
                                  0.6 * 128.0);


    /***turquoise**************************************************************************************/
    
    material[5][0] = new Material([0.1,0.18725,0.1745],
                                  [0.396,0.74151,0.69102],
                                  [0.297254,0.30829,0.306678],
                                  0.1 * 128.0);
                                
    /***brass**************************************************************************************/
    
    material[0][1] = new Material([0.329412,0.223529,0.027451],
                                  [0.780392,0.568627,0.113725],
                                  [0.992157,0.941176,0.807843],
                                  0.21794872 * 128.0);


    /***bronze**************************************************************************************/
    
    material[1][1] = new Material([0.2125,0.1275,0.054],
                                  [0.714,0.4284,0.4284],
                                  [0.393548,0.271906,0.166721],
                                  0.2 * 128.0);
                                  
    /***chrome**************************************************************************************/
    
    material[2][1]= new Material([0.25,0.25,0.25],
                                  [0.4,0.4,0.4],
                                  [0.774597,0.774597,0.774597],
                                  0.6 * 128.0);

    /****copper*************************************************************************************/
    
    material[3][1] = new Material([0.19125,0.0735,0.0225],
                                  [0.7038,0.27048,0.0828],
                                  [0.256777,0.137622,0.086014],
                                  0.1 * 128.0);
                                  

    /***gold**************************************************************************************/
    
    material[4][1] = new Material([0.24725,0.1995,0.0745],
                                  [0.75164,0.60648,0.22648],
                                  [0.628281,0.555802,0.366065],
                                  0.4 * 128.0);
                                  
    /***silver**************************************************************************************/
    
    material[5][1] = new Material([0.19225,0.19225,0.19225],
                                  [0.50754,0.50754,0.50754],
                                  [0.508273,0.508273,0.508273],
                                  0.2 * 128.0);


    /***black**************************************************************************************/
    
    material[0][2] = new Material([0.0,0.0,0.0],
                                  [0.01,0.01,0.01],
                                  [0.50,0.50,0.50],
                                  0.25 * 128.0);
                                 

    /***cyan**************************************************************************************/
    
    material[1][2] = new Material([0.0,0.1,0.6],
                                  [0.0,0.50980392,0.50980392],
                                  [0.50196078,0.50196078,0.50196078],
                                  0.25 * 128.0);
                                 

    /***green**************************************************************************************/
    
    material[2][2]= new Material([0.0,0.0,0.0],
                                  [0.1,0.3,0.1],
                                  [0.45,0.55,0.45],
                                  0.25 * 128.0);                                  


    /***red**************************************************************************************/
    
    material[3][2] = new Material([0.0,0.0,0.0],
                                  [0.5,0.0,0.0],
                                  [0.7,0.6,0.6],
                                  0.25 * 128.0);

    /***white**************************************************************************************/
    
    material[4][2] = new Material([0.0,0.0,0.0],
                                  [0.55,0.55,0.55],
                                  [0.70,0.70,0.70],
                                  0.25 * 128.0);

    /***plastic**************************************************************************************/
    
    material[5][2] = new Material([0.0,0.0,0.0],
                                  [0.5,0.5,0.0],
                                  [0.60,0.60,0.50],
                                  0.25 * 128.0);


    /***black**************************************************************************************/
    
    material[0][3] = new Material([0.02,0.02,0.02],
                                  [0.01,0.01,0.01],
                                  [0.50,0.50,0.50],
                                  0.07812 * 128.0);



    /***cyan**************************************************************************************/
    
    material[1][3] = new Material([0.0,0.05,0.05],
                                  [0.4,0.5,0.5],
                                  [0.04,0.7,0.7],
                                  0.078125 * 128.0);
                                  
    /********************************************************************************************/


    /***green**************************************************************************************/
    
    material[2][3] = new Material([0.0,0.05,0.0],
                                  [0.4,0.5,0.4],
                                  [0.04,0.7,0.04],
                                  0.078125 * 128.0);

    /***red**************************************************************************************/
    
    material[3][3] = new Material([0.05,0.0,0.0],
                                  [0.5,0.4,0.4],
                                  [0.7,0.04,0.04],
                                  0.078125 * 128.0);

                                  

    /***white**************************************************************************************/
    
    material[4][3] = new Material([0.05,0.05,0.05],
                                  [0.5,0.5,0.5],
                                  [0.7,0.7,0.7],
                                  0.078125 * 128.0);
                                  

    /***yellow rubber**************************************************************************************/
    
    material[5][3] = new Material([0.05,0.05,0.0],
                                  [0.5,0.5,0.4],
                                  [0.7,0.7,0.04],
                                  0.078125 * 128.0);
                                  
    /*******************************************************************************************/
}

// global variables
var canvas = null;
var gl = null;		// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	AMC_ATTRIBUTE_VERTEX : 0,
	AMC_ATTRIBUTE_COLOR : 1,
	AMC_ATTRIBUTE_NORMAL : 2,
	AMC_ATTRIBUTE_TEXTURE0 : 3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vaoTriangle;
var vboTrianglePosition;
var vboTriangleColor;

// Line Variables
var vaoLine;
var vboLinePosition;
var vboLineColor;

// Circle Variables
var vaoCircle;
var vboCirclePosition;
var vboCircleColor;
var fRadiusOfInCircle;
var fOffsetX = 0.0;
var fOffsetY = 0.0;
const iPoints = 1000;
var circleangle = new Float32Array([
    0.0, 1.0, 0.0,
    0.0, -1.0, 0.0
]);
var circlecolor = new Float32Array([
    0.0, 1.0, 0.0,
    0.0, 1.0, 0.0
]);

var mvpUniform;

var perspectiveProjectionMatrix;

// Animating Variables
var gfTriangleX = 1.0;
var gfTriangleY = -1.0;
var gfCircleX = -1.0;
var gfCircleY = -1.0;
var gfAngle = 0.0;
var gfLineY = 1.0;

// To start animation:
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame ||
null;

// onload function
function main()
{
	// get <Canvas> element
	canvas = document.getElementById("AMC");
	if (!canvas) {
		console.log("Obtaining Canvas failed\n");
	} 
	else
		console.log("Obtaining Canvas successed\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	window.addEventListener("keydown",keyDown, false);
	window.addEventListener("click",mouseDown, false);
	window.addEventListener("resize",resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen() {
	// Code
	var fullscreen_element =
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if (fullscreen_element == null) {
		if (canvas.requestFullscreen) {
			canvas.requestFullscreen();
		}
		else if (canvas.mozRequestFullScreen) {
			canvas.mozRequestFullScreen();
		}
		else if (canvas.webkitRequestFullscreen) {
			canvas.webkitRequestFullscreen();
		}
		else if (canvas.msRequestFullscreen) {
			canvas.msRequestFullscreen();
		}
		bFullscreen = true;
	}
	else // if already fullscreen
	{
		if (document.exitFullscreen) {
			document.exitFullscreen();
		}
		else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}
		else if (document.webkitExitFullscreen) {
			document.webkitExitullscreen();
		}
		else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}
		bFullscreen = false;
	}
}

function init() {
	// Code
	// get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if (gl == null) {
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.vewportWidth = canvas.width;
	gl.vewportHeight = canvas.height;

	// Dethly Hollows Variables
	var coX1 = 0.0;
	var coY1 = 0.5;
	var coX2 = -0.5;
	var coY2 = -0.5;
	var coX3 = 0.5;
	var coY3 = -0.5;
	var fdistAB = 0.0;
	var fdistBC = 0.0;
	var fdistAC = 0.0;
	var fPerimeter = 0.0;
	var fSemiPerimeter = 0.0;
	var fAreaOfTriangle = 0.0;
	var fTriangleHeight = 0.0;

	// Distance Calculatin between triangle lines
	fdistAB = getDistance(coX1, coX2, coY1, coY2);
	fdistBC = getDistance(coX2, coX3, coY2, coY3);
	fdistAC = getDistance(coX1, coX3, coY1, coY3);

	fPerimeter = fdistAB + fdistBC + fdistAC;
	fSemiPerimeter = fPerimeter / 2;

	fRadiusOfInCircle = Math.sqrt((fSemiPerimeter - fdistAB) * (fSemiPerimeter - fdistBC) * (fSemiPerimeter - fdistAC) / fSemiPerimeter);
	fOffsetX = getOffset(coX1, coX2, coX3, fdistAB, fdistBC, fdistAC, fPerimeter);
	fOffsetY = getOffset(coY1, coY2, coY3, fdistAB, fdistBC, fdistAC, fPerimeter);

	// vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"in vec4 vColor;" +
	"uniform mat4 u_mvp_matrix;" +
	"out vec4 out_color;" +
	"void main(void)" +
	"{" +
	"	gl_Position = u_mvp_matrix * vPosition;" +
	"	out_color = vColor;" +
	"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	// fragment shader
	var fragmentShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"in vec4 out_color;" +
	"out vec4 FragColor;" +
	"void main(void)" +
	"{" +
	"	FragColor  = out_color;" +
	"}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	// shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	// pre-linking binding
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, 'vPosition');
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_COLOR, 'vColor');

	// linking
	gl.linkProgram(shaderProgramObject);
	if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

	var fCoordinate = 0.5;
	// triangle vertices
	var fTriangleVertices = new Float32Array([
		0.0, fCoordinate, 0.0,
		-fCoordinate, -fCoordinate, 0.0,
		fCoordinate, -fCoordinate, 0.0
		]);
	var fTriangleColor = new Float32Array([
		0.0, fCoordinate, 0.0,
		0.0, fCoordinate, 0.0,
		0.0, fCoordinate, 0.0
		]);

	var fLineVertices = new Float32Array([
		0.0, fCoordinate, 0.0,
		0.0, -fCoordinate, 0.0
		]);
	var fLineColor = new Float32Array([
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0
		]);

	var graphColor = new Float32Array([
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0
		]);

	// Triangle
	vaoTriangle = gl.createVertexArray();
	gl.bindVertexArray(vaoTriangle);

	vboTrianglePosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboTrianglePosition);
	gl.bufferData(gl.ARRAY_BUFFER, fTriangleVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vboTriangleColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboTriangleColor);
	gl.bufferData(gl.ARRAY_BUFFER, fTriangleColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Line
	vaoLine = gl.createVertexArray();
	gl.bindVertexArray(vaoLine);

	vboLinePosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboLinePosition);
	gl.bufferData(gl.ARRAY_BUFFER, fLineVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vboLineColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboLineColor);
	gl.bufferData(gl.ARRAY_BUFFER, fLineColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Circle
	vaoCircle = gl.createVertexArray();
	gl.bindVertexArray(vaoCircle);

	// Vertices
	vboCirclePosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboCirclePosition);
	gl.bufferData(gl.ARRAY_BUFFER, [], gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// Color
	vboCircleColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboCircleColor);
	gl.bufferData(gl.ARRAY_BUFFER, graphColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);
	// Clear Color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); // blue

	perspectiveProjectionMatrix = mat4.create();
}

function getDistance(x1,  x2,  y1,  y2) {
	var xSquare = Math.pow((x2 - x1), 2.0);
	var ySquare = Math.pow((y2 - y1), 2.0);
	return Math.sqrt((xSquare + ySquare));
}

function getOffset(coA,  coB,  coC,  distAB,  distBC,  distAC,  perimeter) {
	var value = (coA * distBC) + (coB * distAC) + (coC * distAB);
	return(value / perimeter);
}

function resize() {
	// Code
	if (bFullscreen == true) {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else {
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set viewport
	gl.viewport(0, 0, canvas.width, canvas.height);

	// Perspective Projection
		mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width) / parseFloat(canvas.height),
		0.1,
		100.0);
}

function draw() {
	gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	// Triangle
	mat4.translate(modelViewMatrix, modelViewMatrix, [gfTriangleX, gfTriangleY, -3.0]);
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(gfAngle));
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoTriangle);

	gl.drawArrays(gl.LINE_LOOP, 0, 3);

	gl.bindVertexArray(null);

	// Line
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, gfLineY, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoLine);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// Circle
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [gfCircleX, gfCircleY, -3.0]);
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(gfAngle));
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoCircle);

	drawCircle(fRadiusOfInCircle, fOffsetX, fOffsetY);
	gl.drawArrays(gl.POINTS, 0, iPoints);

	gl.bindVertexArray(null);

	gl.useProgram(null);

	// animation loop
	update();
	requestAnimationFrame(draw, canvas);
}

function drawCircle(fRadiusOfInCircle, fOffsetX, fOffsetY) {
    gl.bindVertexArray(vaoCircle);

    var numpoints = 1000;

    var radius = 0.5;
    var angle = 0.0;
    //var i = 0, j = 1, k = 2;

    for (index = 0; index < numpoints; index++) {
        angle = (2 * Math.PI * index) / numpoints;
        circleangle[0] = fRadiusOfInCircle * Math.cos(angle) + fOffsetX;
        circleangle[1] = fRadiusOfInCircle * Math.sin(angle) + fOffsetY;
        circleangle[2] = 0;


        angle = (2 * Math.PI * (index + 1)) / numpoints;
        circleangle[3] = fRadiusOfInCircle * Math.cos(angle) + fOffsetX;
        circleangle[4] = fRadiusOfInCircle * Math.sin(angle) + fOffsetY;
        circleangle[5] = 0;

        circlecolor[0] = 1.0;
        circlecolor[1] = 0.0;
        circlecolor[2] = 0.0;

        gl.bindBuffer(gl.ARRAY_BUFFER, vboCirclePosition);
        gl.bufferData(gl.ARRAY_BUFFER, circleangle, gl.DYNAMIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindBuffer(gl.ARRAY_BUFFER, vboCircleColor);
        gl.bufferData(gl.ARRAY_BUFFER, circlecolor, gl.DYNAMIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.drawArrays(gl.LINES, 0, 2);
    }

    gl.bindVertexArray(null);

}

function update() {
	/* code */
	if (gfTriangleX >= 0.0)
	{
		gfTriangleX -= 0.001;
		if (gfAngle <= 360.0)
		{
			gfAngle += 0.1;
		}
		else {
			gfAngle = 0.0;
		}
	}
	else {
		gfAngle = 0.0;
	}
	if (gfTriangleY <= 0.0)
	{
		gfTriangleY += 0.001;
		if (gfAngle <= 360.0)
		{
			gfAngle += 0.1;
		}
		else {
			gfAngle = 0.0;
		}
	}
	if (gfCircleX <= 0.0)
	{
		gfCircleX += 0.001;
	}
	if (gfCircleY <= 0.0)
	{
		gfCircleY += 0.001;
	}
	if (gfLineY >= 0.0)
	{
		gfLineY -= 0.001;
	}
}

function uninitialize() {
	// Code
	if (vaoTriangle) {
		gl.deleteVertexArray(vaoTriangle);
		vaoTriangle = null;
	}

	if (vboTrianglePosition) {
		gl.deleteBuffer(vboTrianglePosition);
		vboTrianglePosition = null;
	}

	if (shaderProgramObject) {
		if (fragmentShaderObject) {
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if (vertexShaderObject) {
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event) {
	// Code
	switch(event.keyCode) {
		case 27:
			uninitialize();
			window.close();
			break;
		case 70: // For 'F' or 'f'
			toggleFullScreen();
			break;
	}
}


function mouseDown() {
	// Code
}

function degToRad(degrees) {
    return(degrees*Math.PI/180.0);
}

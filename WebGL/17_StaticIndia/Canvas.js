// global variables
var canvas = null;
var gl = null;		// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	AMC_ATTRIBUTE_VERTEX : 0,
	AMC_ATTRIBUTE_COLOR : 1,
	AMC_ATTRIBUTE_NORMAL : 2,
	AMC_ATTRIBUTE_TEXTURE0 : 3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vaoI;
var vboI;
var vaoNCross;
var vboNCross;
var vaoD;
var vboDVertices;
var vboDColor;
var vaoDUp;
var vboDUp;
var vaoDDown;
var vboDDown;
var vaoALeft;
var vboALeft;
var vaoACenter;
var vboACenter;
var vboColor;
var mvpUniform;
var perspectiveProjectionMatrix;

// To start animation:
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame ||
null;

// onload function
function main()
{
	// get <Canvas> element
	canvas = document.getElementById("AMC");
	if (!canvas) {
		console.log("Obtaining Canvas failed\n");
	} 
	else
		console.log("Obtaining Canvas successed\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	window.addEventListener("keydown",keyDown, false);
	window.addEventListener("click",mouseDown, false);
	window.addEventListener("resize",resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen() {
	// Code
	var fullscreen_element =
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if (fullscreen_element == null) {
		if (canvas.requestFullscreen) {
			canvas.requestFullscreen();
		}
		else if (canvas.mozRequestFullScreen) {
			canvas.mozRequestFullScreen();
		}
		else if (canvas.webkitRequestFullscreen) {
			canvas.webkitRequestFullscreen();
		}
		else if (canvas.msRequestFullscreen) {
			canvas.msRequestFullscreen();
		}
		bFullscreen = true;
	}
	else // if already fullscreen
	{
		if (document.exitFullscreen) {
			document.exitFullscreen();
		}
		else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}
		else if (document.webkitExitFullscreen) {
			document.webkitExitullscreen();
		}
		else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}
		bFullscreen = false;
	}
}

function init() {
	// Code
	// get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if (gl == null) {
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.vewportWidth = canvas.width;
	gl.vewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"in vec4 vColor;" +
	"uniform mat4 u_mvp_matrix;" +
	"out vec4 out_color;" +
	"void main(void)" +
	"{" +
	"gl_Position = u_mvp_matrix * vPosition;" +
	"out_color = vColor;" +
	"}";
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	// fragment shader
	var fragmentShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"in vec4 out_color;" +
	"out vec4 FragColor;" +
	"void main(void)" +
	"{" +
	"	FragColor = out_color;" +
	"}";
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	// shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	// pre-linking binding
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, 'vPosition');
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_COLOR, 'vColor');

	// linking
	gl.linkProgram(shaderProgramObject);
	if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

	// fIVertices vertices
	var fIVertices = new Float32Array([
		-1.0, 1.0, 0.0,
		-1.0, -1.0, 0.0,
	]);

	var fNCrossVertices = new Float32Array([
		-1.0, 1.0, 0.0,
		-0.5, -1.0,0.0,
	]);

	var fACenterVertices = new Float32Array([
		-1.0, 1.0, 0.0,
		-0.5, 1.0,0.0,
	]);

	var fALeftVertices = new Float32Array([
		1.0, 1.0, 0.0,
		0.7, -1.0, 0.0,
	]);

	var triColor = new Float32Array([
		1.0, 0.6, 0.2,
		0.07, 0.53, 0.02
	]);

	var fOrangeColor = new Float32Array([
		1.0, 0.6, 0.2,
		1.0, 0.6, 0.2
	]);

	var fGreenColor = new Float32Array([
		0.07, 0.53, 0.02,
		0.07, 0.53, 0.02
	]);

	var fWhiteColor = new Float32Array([
		1.0, 1.0, 1.0,
		1.0, 1.0, 1.0
	]);

	var fDVertices = new Float32Array([
		-0.5, 1.0, 0.0,
		0.5, 1.0, 0.0,
		0.5, 1.0, 0.0,
		0.5, -1.0, 0.0,
		0.5, -1.0, 0.0,
		-0.5, -1.0, 0.0,
		-0.4, -1.0, 0.0,
		-0.4, 1.0, 0.0,
	]);

	// Create vaoI
	vaoI = gl.createVertexArray();
	gl.bindVertexArray(vaoI);

	//IVerticesVBO
	vboI = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboI);
	gl.bufferData(gl.ARRAY_BUFFER, fIVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//IColorVBO
	vboColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor);
	gl.bufferData(gl.ARRAY_BUFFER, triColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Create vaoNCross
	vaoNCross = gl.createVertexArray();
	gl.bindVertexArray(vaoNCross);

	//NCrossVertices
	vboNCross = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboNCross);
	gl.bufferData(gl.ARRAY_BUFFER, fNCrossVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//NCrossVBO
	vboColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor);
	gl.bufferData(gl.ARRAY_BUFFER, triColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Create vaoALeft
	vaoALeft = gl.createVertexArray();
	gl.bindVertexArray(vaoALeft);

	//ALeftVertices
	vboALeft = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboALeft);
	gl.bufferData(gl.ARRAY_BUFFER, fALeftVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//ALeftVBO
	vboColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor);
	gl.bufferData(gl.ARRAY_BUFFER, triColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Create vaoDUp
	vaoDUp = gl.createVertexArray();
	gl.bindVertexArray(vaoDUp);

	//DUpVertices
	vboDUp = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboDUp);
	gl.bufferData(gl.ARRAY_BUFFER, fACenterVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//DUpVBO
	vboColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor);
	gl.bufferData(gl.ARRAY_BUFFER, fOrangeColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	//vaoDDown
	vaoDDown = gl.createVertexArray();
	gl.bindVertexArray(vaoDDown);

	//DDownVertices
	vboDDown = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboDDown);
	gl.bufferData(gl.ARRAY_BUFFER, fACenterVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//DDownVBO
	vboColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor);
	gl.bufferData(gl.ARRAY_BUFFER, fGreenColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Create vaoACenter
	vaoACenter = gl.createVertexArray();
	gl.bindVertexArray(vaoACenter);

	//ACenterVertices
	vboACenter = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboACenter);
	gl.bufferData(gl.ARRAY_BUFFER, fACenterVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//ACenterVBO
	vboColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor);
	gl.bufferData(gl.ARRAY_BUFFER, fWhiteColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// D
	// Create vaoD
	vaoD = gl.createVertexArray();
	gl.bindVertexArray(vaoD);

	//ACenterVertices
	vboDVertices = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboDVertices);
	gl.bufferData(gl.ARRAY_BUFFER, fDVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//ACenterVBO
	vboDColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboDColor);
	gl.bufferData(gl.ARRAY_BUFFER, [], gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Plane Vao
	// Create vaoI
	vao = gl.createVertexArray();
	gl.bindVertexArray(vao);

	//PlaneVertices
	vboPosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition);
	gl.bufferData(gl.ARRAY_BUFFER, [], gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//PlaneColorVBO
	vboColorPlane = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColorPlane);
	gl.bufferData(gl.ARRAY_BUFFER, [], gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Clear Color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); // black

	perspectiveProjectionMatrix = mat4.create();
}

function resize() {
	// Code
	if (bFullscreen == true) {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else {
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set viewport
	gl.viewport(0, 0, canvas.width, canvas.height);

	// Perspective Projection
		mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width) / parseFloat(canvas.height),
		0.1,
		100.0);
}

function draw() {
	gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	var translationMatrix = mat4.create();
	var scaleMatrix = mat4.create();
	var rotationMatrix = mat4.create();

	// I
	mat4.translate(modelViewMatrix, modelViewMatrix, [-0.4,0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoI);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// NLeft
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [-0.06,0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoI);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// NCross
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [-0.06,0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoNCross);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// NRight
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.44,0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoI);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// D
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.10,0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	// D Color
	var d_color_array = new Float32Array([
		1.0, 0.6, 0.2,
		1.0, 0.6, 0.2,

		1.0, 0.6, 0.2,
		0.07, 0.53, 0.02,

		0.07, 0.53, 0.02,
		0.07, 0.53, 0.02,

		0.07, 0.53, 0.02,
		1.0, 0.6, 0.2
	]);

    gl.bindBuffer(gl.ARRAY_BUFFER, vboDColor);
    gl.bufferData(gl.ARRAY_BUFFER, d_color_array, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(vaoD);

	gl.drawArrays(gl.LINE_STRIP, 0, 8);

	gl.bindVertexArray(null);

	// ISecond
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [1.76,0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoI);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// ALeft
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.4,0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoALeft);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// ACenterOrange
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [2.63,-0.988, -3.9]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoDUp);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// ACenterWhite
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [2.63,-1.0, -3.9]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoACenter);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// ACenterGreen
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [2.63,-1.01, -3.9]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoDDown);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// ARight
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [2.4,0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoNCross);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	gl.useProgram(null);

	// animation loop
	requestAnimationFrame(draw, canvas);
}

function uninitialize() {
	// Code
	if (vao) {
		gl.deleteVertexArray(vao);
		vao = null;
	}

	if (vbo) {
		gl.deleteBuffer(vbo);
		vbo = null;
	}

	if (shaderProgramObject) {
		if (fragmentShaderObject) {
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if (vertexShaderObject) {
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event) {
	// Code
	switch(event.keyCode) {
		case 27:
			uninitialize();
			window.close();
			break;
		case 70: // For 'F' or 'f'
			toggleFullScreen();
			break;
	}
}


function mouseDown() {
	// Code
}

// global variables
var canvas = null;
var gl = null;		// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	AMC_ATTRIBUTE_VERTEX : 0,
	AMC_ATTRIBUTE_COLOR : 1,
	AMC_ATTRIBUTE_NORMAL : 2,
	AMC_ATTRIBUTE_TEXTURE0 : 3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_pyramid;
var vbo_position_pyramid;
var vbo_texture_pyramid;
var vao_cube;
var vbo_position_cube;
var vbo_texture_cube;
var texture_pyramid;
var texture_cube;

var samplerUniform;
var mvpUniform;
var perspectiveProjectionMatrix;

// To start animation:
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame ||
null;

var angleTriangle = 0.0;
var angleSquare = 0.0;

// onload function
function main()
{
	// get <Canvas> element
	canvas = document.getElementById("AMC");
	if (!canvas) {
		console.log("Obtaining Canvas failed\n");
	} 
	else
		console.log("Obtaining Canvas successed\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	window.addEventListener("keydown",keyDown, false);
	window.addEventListener("click",mouseDown, false);
	window.addEventListener("resize",resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen() {
	// Code
	var fullscreen_element =
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if (fullscreen_element == null) {
		if (canvas.requestFullscreen) {
			canvas.requestFullscreen();
		}
		else if (canvas.mozRequestFullScreen) {
			canvas.mozRequestFullScreen();
		}
		else if (canvas.webkitRequestFullscreen) {
			canvas.webkitRequestFullscreen();
		}
		else if (canvas.msRequestFullscreen) {
			canvas.msRequestFullscreen();
		}
		bFullscreen = true;
	}
	else // if already fullscreen
	{
		if (document.exitFullscreen) {
			document.exitFullscreen();
		}
		else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}
		else if (document.webkitExitFullscreen) {
			document.webkitExitullscreen();
		}
		else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}
		bFullscreen = false;
	}
}

function init() {
	// Code
	// get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if (gl == null) {
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.vewportWidth = canvas.width;
	gl.vewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
	"\n" +
    "in vec4 vPosition;" +
    "in vec2 vTexcord;" +
    "uniform mat4 u_mvp_matrix;" +
    "out vec2 out_texcord;" +
    "void main(void)" +
    "{" +
    "	gl_Position = u_mvp_matrix * vPosition;" +
    "	out_texcord = vTexcord;" +
    "}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	// fragment shader
	var fragmentShaderSourceCode = 
	"#version 300 es" +
	"\n" +
    "precision highp float;" +
    "in vec2 out_texcord;" +
    "uniform sampler2D u_sampler;" +
    "out vec4 FragColor;" +
    "void main(void)" +
    "{" +
    "	FragColor = texture(u_sampler, out_texcord);" +
    "}";

	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	// shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	// pre-linking binding
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, 'vPosition');
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexcord");

	// linking
	gl.linkProgram(shaderProgramObject);
	if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	/* load pyramid texture */
	texture_pyramid = gl.createTexture();
	texture_pyramid.image = new Image();
	// texture_pyramid.image.crossOrigin = "anonymous";
	texture_pyramid.image.src = "stone.png";
	texture_pyramid.image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, texture_pyramid);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture_pyramid.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	};

	/* load cube texture */
	texture_cube = gl.createTexture();
	texture_cube.image = new Image();
	// texture_cube.image.crossOrigin = "anonymous";
	texture_cube.image.src = "vijay_kundali.png";
	texture_cube.image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, texture_cube);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture_cube.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.bindTexture(gl.TEXTURE_2D, null);
	};

	// get mvp uniform
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");
	samplerUniform = gl.getUniformLocation(shaderProgramObject, "u_sampler");

	// triangle vertices
	var pyramidVertices = new Float32Array([
		/* ront */
        0.0, 1.0, 0.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,

        /* Right */
        0.0, 1.0, 0.0,
        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,

        /* Let */
        0.0, 1.0, 0.0,
        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,

        /* Back */
        0.0, 1.0, 0.0,
        1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0
		]);

	var pyramidTexCord = new Float32Array([
		/* Front */
        0.5, 1.0,
        0.0, 0.0,
        1.0, 0.0,

        /* Right */
        0.5, 1.0,
        1.0, 0.0,
        0.0, 0.0,

        /* Left */
        0.5, 1.0,
        0.0, 0.0,
        1.0, 0.0,

        /* Back */
        0.5, 1.0,
        1.0, 0.0,
        0.0, 0.0
		]);

	var cubeVertices = new Float32Array([
		1.0, 1.0, -1.0,
        -1.0, 1.0, -1.0,
        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,

        /* Bottom */
        1.0, -1.0, 1.0,
        -1.0, -1.0, 1.0,
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,

        /* ront */
        1.0, 1.0, 1.0,
        -1.0, 1.0, 1.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,

        /* Back */
        1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0,
        -1.0, 1.0, -1.0,
        1.0, 1.0, -1.0,

        /* Right */
        1.0, 1.0, -1.0,
        1.0, 1.0, 1.0,
        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,

        /* Let */
        -1.0, 1.0, 1.0,
        -1.0, 1.0, -1.0,
        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0
		]);

	var cubeTexCord = new Float32Array([
		/* Top */
        0.0, 1.0,
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,

        /* Bottom */
        1.0, 1.0,
        0.0, 1.0,
        0.0, 0.0,
        1.0, 0.0,

        /* Front */
        1.0, 1.0,
        0.0, 1.0,
        0.0, 0.0,
        1.0, 0.0,

        /* Back */
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
        0.0, 0.0,

        /* Right */
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
        0.0, 0.0,

        /* Left */
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0
		]);

	vao_pyramid = gl.createVertexArray();
	gl.bindVertexArray(vao_pyramid);

	vbo_position_pyramid = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_pyramid);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_texture_pyramid = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture_pyramid);
	gl.bufferData(gl.ARRAY_BUFFER, pyramidTexCord, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,
		2,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	vao_cube = gl.createVertexArray();
	gl.bindVertexArray(vao_cube);

	vbo_position_cube = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_cube);
	gl.bufferData(gl.ARRAY_BUFFER, cubeVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	vbo_texture_cube = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture_cube);
	gl.bufferData(gl.ARRAY_BUFFER, cubeTexCord, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0,
		2,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Clear Color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); // black

	// Depth test
	gl.enable(gl.DEPTH_TEST);

	//cull
	gl.enable(gl.CULL_FACE);

	perspectiveProjectionMatrix = mat4.create();
}

function resize() {
	// Code
	if (bFullscreen == true) {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else {
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set viewport
	gl.viewport(0, 0, canvas.width, canvas.height);

	// Perspective Projection
		mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width) / parseFloat(canvas.height),
		0.1,
		100.0);
}

function draw() {
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	// Triangle
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [-1.5,0.0, -6.0]);
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleTriangle));
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	// BIND WITH TEXTURE
	gl.bindTexture(gl.TEXTURE_2D, texture_pyramid);
	gl.uniform1i(samplerUniform, 0);

	gl.bindVertexArray(vao_pyramid);
	gl.drawArrays(gl.TRIANGLES, 0, 12);

	gl.bindVertexArray(null);

	// Square
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [1.5, 0.0, -6.0]);
	mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angleSquare));
	mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angleSquare));
	mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angleSquare));

	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	// BIND WITH TEXTURE
	gl.bindTexture(gl.TEXTURE_2D, texture_cube);
	gl.uniform1i(samplerUniform, 0);

	gl.bindVertexArray(vao_cube);

	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

	gl.bindVertexArray(null);

	gl.useProgram(null);

	// animation loop
	update();
	requestAnimationFrame(draw, canvas);
}

function update() {
	/* code */
	angleTriangle += 0.1;
	if (angleTriangle >= 360.0)
	{
		angleTriangle = 0.0;
	}
	angleSquare += 0.1;
	if (angleSquare >= 360.0)
	{
		angleSquare = 0.0;
	}
}

function uninitialize() {
	// Code
	if (vao_pyramid) {
		gl.deleteVertexArray(vao_pyramid);
		vao_pyramid = null;
	}

	if (vbo_position_pyramid) {
		gl.deleteBuffer(vbo_position_pyramid);
		vbo_position_pyramid = null;
	}

	if (shaderProgramObject) {
		if (fragmentShaderObject) {
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if (vertexShaderObject) {
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event) {
	// Code
	switch(event.keyCode) {
		case 27:
			uninitialize();
			window.close();
			break;
		case 70: // For 'F' or 'f'
			toggleFullScreen();
			break;
	}
}


function mouseDown() {
	// Code
}

function degToRad(degrees) {
    return(degrees*Math.PI/180.0);
}

// global variables
var canvas = null;
var gl = null;		// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	AMC_ATTRIBUTE_VERTEX : 0,
	AMC_ATTRIBUTE_COLOR : 1,
	AMC_ATTRIBUTE_NORMAL : 2,
	AMC_ATTRIBUTE_TEXTURE0 : 3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vaoI;
var vboI;
var vaoNCross;
var vboNCross;
var vaoD;
var vboDVertices;
var vboDColor;
var vaoDUp;
var vboDUp;
var vaoDDown;
var vboDDown;
var vaoALeft;
var vboALeft;
var vaoACenter;
var vboACenter;
var vboColor;
var mvpUniform;
var perspectiveProjectionMatrix;

// Dynamic India Variable
var gfLIX = -1.3;
var gfAX = 1.52;
var gfAXR = 3.52;
var gfNY = 2.3;
var gfRIY = -2.3;
var gfORC = 0.0;
var gfOGC = 0.0;
var gfOBC = 0.0;
var gfGRC = 0.0;
var gfGBC = 0.0;
var gfGGC = 0.0;

// Aeroplane Variables
var vao;
var vboPosition;
var vboColorPlane;

// Triangle Variables
var iTrianglePoints = 3;

// Line Variables
var iLinePoints = 3;

// Square Variables
var iSquarePoints = 4;

// Animating Variables Plane
var gfUpperLeftAngle = Math.PI;
//var gfUpperLeftAngle_X = -7.0;
var gfUpperLeftAngle_X = -4.0;
var gfUpperLeftAngle_Y = 3.0;

var gfMiddlePlaneTranslate_X = -5.0;
var gfMiddlePlaneTranslate_Y = 0.0;

var gfLowerLeftAngle = Math.PI;
var gfLowerLeftAngle_X = -7.0;
var gfLowerLeftAngle_Y = -3.0;

var gfUpperRightAngle = 3 * (Math.PI / 2);
var gfUpperRightAngle_X = 0.0;
var gfUpperRightAngle_Y = 0.0;

var gfLowerRightAngle = (Math.PI / 2);
var gfLowerRightAngle_X = 0.0;
var gfLowerRightAngle_Y = 0.0;

var flag_offset_X1 = -5.0;
var gfFlagOrange_X1 = flag_offset_X1;
var gfFlagOrange_X2 = 0.0;
var gfFlagOrange_Y = 0.03;
var gfFlagWhite_X1 = flag_offset_X1;
var gfFlagWhite_X2 = 0.0;
var gfFlagWhite_Y = 0.0;
var gfFlagGreen_X1 = flag_offset_X1;
var gfFlagGreen_X2 = 0.0;
var gfFlagGreen_Y = -0.03;

// Small Flag Variables
var gfSmallFlagOrange_X1 = -1.0;
var gfSmallFlagWhite_X1 = -1.0;
var gfSmallFlagGreen_X1 = -1.0;
var gbAnimatePlane = false;

// // D Color
// var d_color_array = new Float32Array([
// 	gfORC, gfOGC, gfOBC,
// 	gfORC, gfOGC, gfOBC,

// 	gfORC, gfOGC, gfOBC,
// 	gfGRC, gfGGC, gfGBC,

// 	gfGRC, gfGGC, gfGBC,
// 	gfGRC, gfGGC, gfGBC,

// 	gfGRC, gfGGC, gfGBC,
// 	gfORC, gfOGC, gfOBC
// ]);

// To start animation:
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame ||
null;

// onload function
function main()
{
	// get <Canvas> element
	canvas = document.getElementById("AMC");
	if (!canvas) {
		console.log("Obtaining Canvas failed\n");
	} 
	else
		console.log("Obtaining Canvas successed\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	window.addEventListener("keydown",keyDown, false);
	window.addEventListener("click",mouseDown, false);
	window.addEventListener("resize",resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen() {
	// Code
	var fullscreen_element =
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if (fullscreen_element == null) {
		if (canvas.requestFullscreen) {
			canvas.requestFullscreen();
		}
		else if (canvas.mozRequestFullScreen) {
			canvas.mozRequestFullScreen();
		}
		else if (canvas.webkitRequestFullscreen) {
			canvas.webkitRequestFullscreen();
		}
		else if (canvas.msRequestFullscreen) {
			canvas.msRequestFullscreen();
		}
		bFullscreen = true;
	}
	else // if already fullscreen
	{
		if (document.exitFullscreen) {
			document.exitFullscreen();
		}
		else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}
		else if (document.webkitExitFullscreen) {
			document.webkitExitullscreen();
		}
		else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}
		bFullscreen = false;
	}
}

function init() {
	// Code
	// get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if (gl == null) {
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.vewportWidth = canvas.width;
	gl.vewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"in vec4 vColor;" +
	"uniform mat4 u_mvp_matrix;" +
	"out vec4 out_color;" +
	"void main(void)" +
	"{" +
	"gl_Position = u_mvp_matrix * vPosition;" +
	"out_color = vColor;" +
	"}";
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	// fragment shader
	var fragmentShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"in vec4 out_color;" +
	"out vec4 FragColor;" +
	"void main(void)" +
	"{" +
	"	FragColor = out_color;" +
	"}";
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	// shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	// pre-linking binding
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, 'vPosition');
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_COLOR, 'vColor');

	// linking
	gl.linkProgram(shaderProgramObject);
	if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

	// fIVertices vertices
	var fIVertices = new Float32Array([
		-1.0, 1.0, 0.0,
		-1.0, -1.0, 0.0,
	]);

	var fNCrossVertices = new Float32Array([
		-1.0, 1.0, 0.0,
		-0.5, -1.0,0.0,
	]);

	var fACenterVertices = new Float32Array([
		-1.0, 1.0, 0.0,
		-0.5, 1.0,0.0,
	]);

	var fALeftVertices = new Float32Array([
		1.0, 1.0, 0.0,
		0.7, -1.0, 0.0,
	]);

	var triColor = new Float32Array([
		1.0, 0.6, 0.2,
		0.07, 0.53, 0.02
	]);

	var fOrangeColor = new Float32Array([
		1.0, 0.6, 0.2,
		1.0, 0.6, 0.2
	]);

	var fGreenColor = new Float32Array([
		0.07, 0.53, 0.02,
		0.07, 0.53, 0.02
	]);

	var fWhiteColor = new Float32Array([
		1.0, 1.0, 1.0,
		1.0, 1.0, 1.0
	]);

	var fDVertices = new Float32Array([
		-0.5, 1.0, 0.0,
		0.5, 1.0, 0.0,
		0.5, 1.0, 0.0,
		0.5, -1.0, 0.0,
		0.5, -1.0, 0.0,
		-0.5, -1.0, 0.0,
		-0.4, -1.0, 0.0,
		-0.4, 1.0, 0.0,
	]);

	// Create vaoI
	vaoI = gl.createVertexArray();
	gl.bindVertexArray(vaoI);

	//IVerticesVBO
	vboI = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboI);
	gl.bufferData(gl.ARRAY_BUFFER, fIVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//IColorVBO
	vboColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor);
	gl.bufferData(gl.ARRAY_BUFFER, triColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Create vaoNCross
	vaoNCross = gl.createVertexArray();
	gl.bindVertexArray(vaoNCross);

	//NCrossVertices
	vboNCross = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboNCross);
	gl.bufferData(gl.ARRAY_BUFFER, fNCrossVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//NCrossVBO
	vboColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor);
	gl.bufferData(gl.ARRAY_BUFFER, triColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Create vaoALeft
	vaoALeft = gl.createVertexArray();
	gl.bindVertexArray(vaoALeft);

	//ALeftVertices
	vboALeft = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboALeft);
	gl.bufferData(gl.ARRAY_BUFFER, fALeftVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//ALeftVBO
	vboColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor);
	gl.bufferData(gl.ARRAY_BUFFER, triColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Create vaoDUp
	vaoDUp = gl.createVertexArray();
	gl.bindVertexArray(vaoDUp);

	//DUpVertices
	vboDUp = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboDUp);
	gl.bufferData(gl.ARRAY_BUFFER, fACenterVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//DUpVBO
	vboColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor);
	gl.bufferData(gl.ARRAY_BUFFER, fOrangeColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	//vaoDDown
	vaoDDown = gl.createVertexArray();
	gl.bindVertexArray(vaoDDown);

	//DDownVertices
	vboDDown = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboDDown);
	gl.bufferData(gl.ARRAY_BUFFER, fACenterVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//DDownVBO
	vboColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor);
	gl.bufferData(gl.ARRAY_BUFFER, fGreenColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Create vaoACenter
	vaoACenter = gl.createVertexArray();
	gl.bindVertexArray(vaoACenter);

	//ACenterVertices
	vboACenter = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboACenter);
	gl.bufferData(gl.ARRAY_BUFFER, fACenterVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//ACenterVBO
	vboColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColor);
	gl.bufferData(gl.ARRAY_BUFFER, fWhiteColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// D
	// Create vaoD
	vaoD = gl.createVertexArray();
	gl.bindVertexArray(vaoD);

	//ACenterVertices
	vboDVertices = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboDVertices);
	gl.bufferData(gl.ARRAY_BUFFER, fDVertices, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//ACenterVBO
	vboDColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboDColor);
	gl.bufferData(gl.ARRAY_BUFFER, [], gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Plane Vao
	// Create vaoI
	vao = gl.createVertexArray();
	gl.bindVertexArray(vao);

	//PlaneVertices
	vboPosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition);
	gl.bufferData(gl.ARRAY_BUFFER, [], gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	//PlaneColorVBO
	vboColorPlane = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboColorPlane);
	gl.bufferData(gl.ARRAY_BUFFER, [], gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// Clear Color
	gl.clearColor(0.0, 0.0, 1.0, 1.0); // blue

	perspectiveProjectionMatrix = mat4.create();
}

function resize() {
	// Code
	if (bFullscreen == true) {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else {
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set viewport
	gl.viewport(0, 0, canvas.width, canvas.height);

	// Perspective Projection
		mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width) / parseFloat(canvas.height),
		0.1,
		100.0);
}

function draw() {
	gl.clear(gl.COLOR_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();
	var translationMatrix = mat4.create();
	var scaleMatrix = mat4.create();
	var rotationMatrix = mat4.create();

	// I
	mat4.translate(modelViewMatrix, modelViewMatrix, [gfLIX,0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoI);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// NLeft
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [-0.06,gfNY, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoI);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// NCross
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [-0.06,gfNY, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoNCross);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// NRight
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.44,gfNY, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoI);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// D
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.10,0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	// D Color
	var d_color_array = new Float32Array([
		gfORC, gfOGC, gfOBC,
		gfORC, gfOGC, gfOBC,

		gfORC, gfOGC, gfOBC,
		gfGRC, gfGGC, gfGBC,

		gfGRC, gfGGC, gfGBC,
		gfGRC, gfGGC, gfGBC,

		gfGRC, gfGGC, gfGBC,
		gfORC, gfOGC, gfOBC
	]);

    gl.bindBuffer(gl.ARRAY_BUFFER, vboDColor);
    gl.bufferData(gl.ARRAY_BUFFER, d_color_array, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

	gl.bindVertexArray(vaoD);

	gl.drawArrays(gl.LINE_STRIP, 0, 8);

	gl.bindVertexArray(null);

	// ISecond
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [1.76,gfRIY, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoI);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// ALeft
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [gfAX,0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoALeft);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// ACenterOrange
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [2.63,-0.988, -3.9]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoDUp);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// ACenterWhite
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [2.63,-1.0, -3.9]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoACenter);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// ACenterGreen
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [2.63,-1.01, -3.9]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoDDown);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	// ARight
	// Initialize above matrices to identity
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [gfAXR,0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoNCross);

	gl.drawArrays(gl.LINES, 0, 2);

	gl.bindVertexArray(null);

	//-----------------------------------------------------------------------------------------------------------
	// AeroPlane
	// Upper Plain
	// Initialize above matrices to identity
	mat4.identity(translationMatrix);
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

//var gfUpperLeftAngle_X = -7.0;
	 mat4.translate(modelViewMatrix, modelViewMatrix, [gfUpperLeftAngle_X, gfUpperLeftAngle_Y, -5.0]);
	// mat4.multiply(modelViewMatrix, modelViewMatrix,translationMatrix);

/*	if (gbAnimatePlane)
	{
		if (gfUpperLeftAngle < ((3.0 * Math.PI) / 2))
		{
			gfUpperLeftAngle_X = 3.0 * Math.cos(gfUpperLeftAngle);
			gfUpperLeftAngle_Y = 3.0 * Math.sin(gfUpperLeftAngle);
			// mat4.translate(modelViewMatrix, translationMatrix, [gfUpperLeftAngle_X,gfUpperLeftAngle_Y, -6.0]);
			 mat4.translate(modelViewMatrix, translationMatrix, [gfUpperLeftAngle_X,gfUpperLeftAngle_Y, -5.0]);
	 		console.log("In Second Translatf");

//			 mat4.multiply(modelViewMatrix, modelViewMatrix,translationMatrix);
			// mat4.translate(modelViewMatrix, modelViewMatrix, [gfUpperLeftAngle_X,gfUpperLeftAngle_Y, -6.0]);
		}

		gfUpperLeftAngle += 0.001;
	}
*/
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	drawAeroPlane();

	gl.useProgram(null);

	// animation loop
	update();
	requestAnimationFrame(draw, canvas);
}

function drawAeroPlane()
{
/*	// Function Declaration
	void drawSquare(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);
	void drawTriangle(float x1, float y1, float x2, float y2, float x3, float y3);
*/
	// Code:
	// Bind with vao
	gl.bindVertexArray(vao);


	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	// Central Body
	// Draw Square
	drawSquare(0.3, 0.1, -0.4, 0.1, -0.4, -0.1, 0.3, -0.1);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	// Unbind vao
	gl.bindVertexArray(null);

	//Upper Wing
	// Triangle
	// Bind with vao
	gl.bindVertexArray(vao);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	// Draw Triangle
	drawTriangle(-0.4, 0.5, -0.2, 0.1, 0.2, 0.1);
	gl.drawArrays(gl.TRIANGLES, 0, iTrianglePoints);

	// Unbind vao
	gl.bindVertexArray(null);

	//Lower Wing
	// Triangle
	// Initialize above matrices to identity
	// Bind with vao
	gl.bindVertexArray(vao);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	// Draw Triangle
	drawTriangle(-0.4, -0.5, -0.2, -0.1, 0.2, -0.1);
	gl.drawArrays(gl.TRIANGLES, 0, iTrianglePoints);

	// Unbind vao
	gl.bindVertexArray(null);

	//Front Nosel
	// Triangle
	// Bind with vao
	gl.bindVertexArray(vao);
	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	// Draw Triangle
	drawTriangle(0.3, 0.1, 0.6, 0.0, 0.3, -0.1);
	gl.drawArrays(gl.TRIANGLES, 0, iTrianglePoints);

	// Unbind vao
	gl.bindVertexArray(null);

	// Back Tail
	// Square
	// Bind with vao
	gl.bindVertexArray(vao);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	// Central Body
	// Draw Square
	drawSquare(-0.4, 0.1, -0.6, 0.3, -0.6, -0.3, -0.4, -0.1);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, iSquarePoints);
	//drawDethlyHollows();

	// Unbind vao
	gl.bindVertexArray(null);

/*	//drawIAF();

	// Bind with vao
	glBindVertexArray(vao);

	// Similarly bind With Textures If Any
	const GLfloat linei[] =
	{
		-0.35f,0.1f,0.0f,
		-0.35f,-0.1f,0.0f,
	};

	const GLfloat lineicolor[] =
	{
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
	};
	// Line Vertices
	glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(linei),
		linei,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Circle Color
	glBindBuffer(GL_ARRAY_BUFFER, vboColorPlane);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(lineicolor),
		lineicolor,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Draw The Necessary Senn
	glLineWidth(3.0f);
	glDrawArrays(GL_LINES,
		0,
		2);

	// Unbind vao
	glBindVertexArray(0);

	// For A
	glBindVertexArray(vao);

	// Similarly bind With Textures If Any
	const GLfloat lineA[] =
	{
		0.0f,-0.1f,0.0f,
		-0.1f,0.1f,0.0f,
		-0.2f,-0.1f,0.0f,
	};

	const GLfloat lineAcolor[] =
	{
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
	};
	// Line Vertices
	glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(lineA),
		lineA,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Circle Color
	glBindBuffer(GL_ARRAY_BUFFER, vboColorPlane);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(lineAcolor),
		lineAcolor,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Draw The Necessary Senn
	glLineWidth(3.0f);
	glDrawArrays(GL_LINE_STRIP,
		0,
		3);

	// Unbind vao
	glBindVertexArray(0);

	// For F
	glBindVertexArray(vao);

	// Similarly bind With Textures If Any
	const GLfloat lineF[] =
	{
		0.05f,-0.1f,0.0f,
		0.05f,0.1f,0.0f,
		0.05f,0.1f,0.0f,
		0.20f,0.1f,0.0f,
		0.05f,0.1f,0.0f,
		0.05f,0.0f,0.0f,
		0.20f,0.0f,0.0f,
	};

	const GLfloat lineFcolor[] =
	{
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
	};
	// Line Vertices
	glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(lineF),
		lineF,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Circle Color
	glBindBuffer(GL_ARRAY_BUFFER, vboColorPlane);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(lineFcolor),
		lineFcolor,
		GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	// Draw The Necessary Senn
	glLineWidth(3.0f);
	glDrawArrays(GL_LINE_STRIP,
		0,
		7);

	// Unbind vao
	glBindVertexArray(0);
*/
}

function drawSquare(x1, y1, x2, y2, x3, y3, x4, y4) {
	// function declaration

	// Variable declaration
	var fSquareVertices = new Float32Array([
		x1, y1, 0.0,
		x2, y2, 0.0,
		x3, y3, 0.0,
		x4, y4, 0.0
		]);

	var fSquareColor = new Float32Array([
		0.7, 0.8, 0.9,
		0.7, 0.8, 0.9,
		0.7, 0.8, 0.9,
		0.7, 0.8, 0.9
		]);

        gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition);
        gl.bufferData(gl.ARRAY_BUFFER, fSquareVertices, gl.DYNAMIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindBuffer(gl.ARRAY_BUFFER, vboColorPlane);
        gl.bufferData(gl.ARRAY_BUFFER, fSquareColor, gl.DYNAMIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
}

function drawTriangle(x1, y1, x2, y2, x3, y3) {
	// function declaration

	// Variable declaration
	var fTriangleVertices = new Float32Array([
		x1, y1, 0.0,
		x2, y2, 0.0,
		x3, y3, 0.0
		]);

	var fTriangleColor = new Float32Array([
		0.7, 0.8, 0.9,
		0.7, 0.8, 0.9,
		0.7, 0.8, 0.9,
		]);

    gl.bindBuffer(gl.ARRAY_BUFFER, vboPosition);
    gl.bufferData(gl.ARRAY_BUFFER, fTriangleVertices, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindBuffer(gl.ARRAY_BUFFER, vboColorPlane);
    gl.bufferData(gl.ARRAY_BUFFER, fTriangleColor, gl.DYNAMIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
}

function update() {
	// Code
	// gbAnimatePlane = true;

	// if(gfUpperLeftAngle_X <= 2.0)
	// {
	// 	gfUpperLeftAngle_X += 0.01;	
	// }
	// if(gfUpperLeftAngle_X <= 2.0)
	// {
	// 	gfUpperLeftAngle_X += 0.01;	
	// }
	// updatePlane();
	if (gfLIX <= -0.4)
	{
		gfLIX += 0.001;
	}
	else
	{
		if (gfAX >= 0.4)
		{
			gfAX -= 0.001;
		}
		if (gfAXR >= 2.4)
		{
			gfAXR -= 0.001;
		}
		else
		{
			if (gfNY >= 0.0)
			{
				gfNY -= 0.001;
			}
			else
			{
				if (gfRIY <= 0.0)
				{
					gfRIY += 0.001;
				}
				else
				{
					if (gfORC <= 1.0)
					{
						gfORC += 0.001;
					}
					if (gfOGC <= 0.6)
					{
						gfOGC += 0.001;
					}
					if (gfOBC <= 0.2)
					{
						gfOBC += 0.001;
					}
					if (gfGRC <= 0.07)
					{
						gfGRC += 0.001;
					}
					if (gfGGC <= 0.53)
					{
						gfGGC += 0.001;
					}
					if (gfGBC <= 0.02)
					{
						gfGBC += 0.001;
					}
					else
					{
						if ((gfORC >= 1.0) && (gfOGC >= 0.6) && (gfOBC >= 0.2) && (gfGRC >= 0.07) && (gfGGC >= 0.53) && (gfGBC >= 0.02))
						{
							gbAnimatePlane = true;
							updatePlane();
						}
					}
				}
			}
		}
	}
}

function updatePlane() {
	// Code:

	if (gfUpperLeftAngle_X <= 0.0)
	{
		gfUpperLeftAngle_X += 0.001;
	}
	if (gfUpperLeftAngle_Y >= 0.0)
	{
		gfUpperLeftAngle_Y -= 0.001;
	}
	if (gfMiddlePlaneTranslate_X <= 7.5)
	{
		gfMiddlePlaneTranslate_X += 0.00227;
		gfFlagOrange_X1 -= 0.001;
		gfFlagWhite_X1 -= 0.001;
		gfFlagGreen_X1 -= 0.001;
	}
	if (gfLowerLeftAngle_X <= 0.0)
	{
		gfLowerLeftAngle_X += 0.001;
	}
	if (gfLowerLeftAngle_Y <= 0.0)
	{
		gfLowerLeftAngle_Y += 0.001;
	}
	if (gfMiddlePlaneTranslate_X >= 7.5)
	{
		flag_offset_X1 += 0.01;
		gfFlagOrange_X1 += 0.001;
		gfFlagWhite_X1 += 0.001;
		gfFlagGreen_X1 += 0.001;
	}
}

function uninitialize() {
	// Code
	if (vao) {
		gl.deleteVertexArray(vao);
		vao = null;
	}

	if (vbo) {
		gl.deleteBuffer(vbo);
		vbo = null;
	}

	if (shaderProgramObject) {
		if (fragmentShaderObject) {
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if (vertexShaderObject) {
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event) {
	// Code
	switch(event.keyCode) {
		case 27:
			uninitialize();
			window.close();
			break;
		case 70: // For 'F' or 'f'
			toggleFullScreen();
			break;
	}
}


function mouseDown() {
	// Code
}

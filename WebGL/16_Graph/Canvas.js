// global variables
var canvas = null;
var gl = null;		// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	AMC_ATTRIBUTE_VERTEX : 0,
	AMC_ATTRIBUTE_COLOR : 1,
	AMC_ATTRIBUTE_NORMAL : 2,
	AMC_ATTRIBUTE_TEXTURE0 : 3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vaoVerticalGraph;
var vboVerticalGraphPosition;
var vboVerticalGraphColor;

var vaoHorizontalGraph;
var vboHorizontalGraphPosition;
var vboHorizontalGraphColor;

var mvpUniform;
var perspectiveProjectionMatrix;

// To start animation:
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame ||
null;

// onload function
function main()
{
	// get <Canvas> element
	canvas = document.getElementById("AMC");
	if (!canvas) {
		console.log("Obtaining Canvas failed\n");
	} 
	else
		console.log("Obtaining Canvas successed\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	window.addEventListener("keydown",keyDown, false);
	window.addEventListener("click",mouseDown, false);
	window.addEventListener("resize",resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen() {
	// Code
	var fullscreen_element =
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if (fullscreen_element == null) {
		if (canvas.requestFullscreen) {
			canvas.requestFullscreen();
		}
		else if (canvas.mozRequestFullScreen) {
			canvas.mozRequestFullScreen();
		}
		else if (canvas.webkitRequestFullscreen) {
			canvas.webkitRequestFullscreen();
		}
		else if (canvas.msRequestFullscreen) {
			canvas.msRequestFullscreen();
		}
		bFullscreen = true;
	}
	else // if already fullscreen
	{
		if (document.exitFullscreen) {
			document.exitFullscreen();
		}
		else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}
		else if (document.webkitExitFullscreen) {
			document.webkitExitullscreen();
		}
		else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}
		bFullscreen = false;
	}
}

function init() {
	// Code
	// get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if (gl == null) {
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.vewportWidth = canvas.width;
	gl.vewportHeight = canvas.height;

	// vertex shader
	var vertexShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"in vec4 vPosition;" +
	"in vec4 vColor;" +
	"out vec4 out_color;" +
	"uniform mat4 u_mvp_matrix;" +
	"void main(void)" +
	"{" +
	"	gl_Position = u_mvp_matrix * vPosition;" +
	"	out_color = vColor;" +
	"}";

	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	// fragment shader
	var fragmentShaderSourceCode = 
	"#version 300 es" +
	"\n" +
	"precision highp float;" +
	"in vec4 out_color;" +
	"out vec4 FragColor;" +
	"void main(void)" +
	"{" +
	"	FragColor = out_color;" +
	"}";
	fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	// shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	// pre-linking binding
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, 'vPosition');
	gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_COLOR, 'vColor');

	// linking
	gl.linkProgram(shaderProgramObject);
	if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if (error.length > 0) {
			alert(error);
			uninitialize();
		}
	}

	// get mvp uniform
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

	// triangle vertices
	var graphColor = new Float32Array([
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0
		]);

	vaoVerticalGraph = gl.createVertexArray();
	gl.bindVertexArray(vaoVerticalGraph);

	// Vertices
	vboVerticalGraphPosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboVerticalGraphPosition);
	gl.bufferData(gl.ARRAY_BUFFER, null, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// Color
	vboVerticalGraphColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboVerticalGraphColor);
	gl.bufferData(gl.ARRAY_BUFFER, graphColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	vaoHorizontalGraph = gl.createVertexArray();
	gl.bindVertexArray(vaoHorizontalGraph);

	// Vertices
	vboHorizontalGraphPosition = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboHorizontalGraphPosition);
	gl.bufferData(gl.ARRAY_BUFFER, null, gl.DYNAMIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// Color
	vboHorizontalGraphColor = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vboHorizontalGraphColor);
	gl.bufferData(gl.ARRAY_BUFFER, graphColor, gl.STATIC_DRAW);
	gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,
		3,
		gl.FLOAT,
		false, 0, 0);
	gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);
	// Clear Color
	gl.clearColor(0.0, 0.0, 1.0, 1.0); // blue

	perspectiveProjectionMatrix = mat4.create();
}

function resize() {
	// Code
	if (bFullscreen == true) {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else {
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set viewport
	gl.viewport(0, 0, canvas.width, canvas.height);

	// Perspective Projection
		mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width) / parseFloat(canvas.height),
		0.1,
		100.0);
}

function draw() {
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	// VerticalGraph
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoVerticalGraph);

	drawVerticalGraph();

	// gl.drawArrays(gl.TRIANGLES, 0, 3);

	gl.bindVertexArray(null);

	// Horizontal Line
	mat4.identity(modelViewMatrix);
	mat4.identity(modelViewProjectionMatrix);

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
	gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);

	gl.bindVertexArray(vaoHorizontalGraph);

	drawHorizontalGraph();

	// gl.drawArrays(gl.TRIANGLES, 0, 3);

	gl.bindVertexArray(null);

	gl.useProgram(null);

	// animation loop
	requestAnimationFrame(draw, canvas);
}

function drawVerticalGraph() {
	// function declaration

	// Variable declaration
	var iLinePoints = 3;

	// Code:
	for (var i = -1.0; i < 1.0; i += 0.05)
	{
		var fVerticalLineVertices = new Float32Array([
	            // Perspective triangle
	            i, 1.0, 0.0,		// Apex
	            i, -1.0, 0.0]);

		// Vertical Vertices
		gl.bindBuffer(gl.ARRAY_BUFFER, vboVerticalGraphPosition);
		gl.bufferData(gl.ARRAY_BUFFER,
			fVerticalLineVertices,
			gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.lineWidth(3.0);
		gl.drawArrays(gl.LINES,
			0,
			2);
	}
}

function drawHorizontalGraph() {
	// function declaration

	// Variable declaration
	var iLinePoints = 3;

	// Code:
	for (var i = 1.0; i > -1.0; i -= 0.05)
	{
		var fHorizontalLineVertices = new Float32Array([
	            // Perspective triangle
	            1.0, i, 0.0,		// Apex
	            -1.0, i, 0.0]);

		// Vertical Vertices
		gl.bindBuffer(gl.ARRAY_BUFFER, vboHorizontalGraphPosition);
		gl.bufferData(gl.ARRAY_BUFFER,
			fHorizontalLineVertices,
			gl.DYNAMIC_DRAW);
		gl.bindBuffer(gl.ARRAY_BUFFER, null);

		gl.lineWidth(3.0);
		gl.drawArrays(gl.LINES,
			0,
			2);
	}
}

function uninitialize() {
	// Code
	if (vaoVerticalGraph) {
		gl.deleteVertexArray(vaoVerticalGraph);
		vaoVerticalGraph = null;
	}

	if (vboVerticalGraphPosition) {
		gl.deleteBuffer(vboVerticalGraphPosition);
		vboVerticalGraphPosition = null;
	}

	if (shaderProgramObject) {
		if (fragmentShaderObject) {
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if (vertexShaderObject) {
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event) {
	// Code
	switch(event.keyCode) {
		case 27:
			uninitialize();
			window.close();
			break;
		case 70: // For 'F' or 'f'
			toggleFullScreen();
			break;
	}
}


function mouseDown() {
	// Code
}

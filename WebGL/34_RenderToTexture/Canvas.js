// global variables
var canvas = null;
var gl = null;		// webgl context
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros = 
{
	AMC_ATTRIBUTE_VERTEX : 0,
	AMC_ATTRIBUTE_COLOR : 1,
	AMC_ATTRIBUTE_NORMAL : 2,
	AMC_ATTRIBUTE_TEXTURE0 : 3,
};

var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_pyramid;
var vao_cube;
var vbo_position_pyramid;
var vbo_texture_pyramid;
var vbo_position_cube;
var vbo_texture_cube;
var texture_pyramid;
var texture_cube;

var samplerUniform;
var mvpUniform;
var perspectiveProjectionMatrix;

// To start animation:
var requestAnimationFrame = 
window.requestAnimationFrame ||
window.webkitRequestAnimationFrame ||
window.mozRequestAnimationFrame ||
window.oRequestAnimationFrame ||
window.msRequestAnimationFrame ||
null;

// To stop animation
var cancelAnimationFrame = 
window.cancelAnimationFrame ||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame ||
null;

var angleTriangle = 0.0;
var angleSquare = 0.0;

//Rander to texture variables
var fbo_shaderProgramObject;
var fbo_mvpUniform;
var fbo_samplerUniform;
var fbo_texture_cube;
var fbo_depth;
var fbo;
var fbo_texture;

var gWidth;
var gHeight;

// onload function
function main()
{
	// get <Canvas> element
	canvas = document.getElementById("AMC");
	if (!canvas) {
		console.log("Obtaining Canvas failed\n");
	} 
	else
		console.log("Obtaining Canvas successed\n");
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;

	window.addEventListener("keydown",keyDown, false);
	window.addEventListener("click",mouseDown, false);
	window.addEventListener("resize",resize, false);

	// initialize WebGL
	init();

	// start drawing here as warming-up
	resize();
	draw();
}

function toggleFullScreen() {
	// Code
	var fullscreen_element =
	document.webkitFullscreenElement ||
	document.mozFullScreenElement ||
	document.msFullscreenElement ||
	null;

	// if not fullscreen
	if (fullscreen_element == null) {
		if (canvas.requestFullscreen) {
			canvas.requestFullscreen();
		}
		else if (canvas.mozRequestFullScreen) {
			canvas.mozRequestFullScreen();
		}
		else if (canvas.webkitRequestFullscreen) {
			canvas.webkitRequestFullscreen();
		}
		else if (canvas.msRequestFullscreen) {
			canvas.msRequestFullscreen();
		}
		bFullscreen = true;
	}
	else // if already fullscreen
	{
		if (document.exitFullscreen) {
			document.exitFullscreen();
		}
		else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}
		else if (document.webkitExitFullscreen) {
			document.webkitExitullscreen();
		}
		else if (document.msExitFullscreen) {
			document.msExitFullscreen();
		}
		bFullscreen = false;
	}
}

function init() {
	// FBO variables
	var fbo_vertexShaderObject;
	var fbo_fragmentShaderObject;

	// Code
	// get WebGL 2.0 context
	gl = canvas.getContext("webgl2");
	if (gl == null) {
		console.log("Failed to get the rendering context for WebGL");
		return;
	}
	gl.vewportWidth = canvas.width;
	gl.vewportHeight = canvas.height;

    // **************** For FBO CODE *****************
    For_DrawShaders_FBO();

    // ************* Normal Shaders *************
    For_DrawShaders_Color();
    //Preparation to draw a triangle

    var triangleVertices = new Float32Array([
        0.0, 1.0, 0.0, //apex of the triangle
        -1.0, -1.0, 1.0, //left-bottom
        1.0, -1.0, 1.0, //right-bottom
        //right face
        0.0, 1.0, 0.0, //apex
        1.0, -1.0, 1.0,//left bottom
        1.0, -1.0, -1.0, //right bottom
        //back face
        0.0, 1.0, 0.0, //apex
        1.0, -1.0, -1.0,
        -1.0, -1.0, -1.0,
        //left face
        0.0, 1.0, 0.0, //apex
        -1.0, -1.0, -1.0, //left bottom
        -1.0, -1.0, 1.0]);

    //vbo for colors
    var triangleColors = new Float32Array([
        1.0, 0.0, 0.0, //red
        0.0, 1.0, 0.0, //green
        0.0, 0.0, 1.0, //blue
        1.0, 0.0, 0.0, //red
        0.0, 0.0, 1.0, //green
        0.0, 1.0, 0.0, //blue
        1.0, 0.0, 0.0, //red
        0.0, 1.0, 0.0, //blue
        0.0, 0.0, 1.0, //green
        1.0, 0.0, 0.0, //red
        0.0, 0.0, 1.0, //green
        0.0, 1.0, 0.0 //blue
    ]);

    vao_triangle = gl.createVertexArray();
    gl.bindVertexArray(vao_triangle);
    //vbo for positions
    vbo_position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position);
    gl.bufferData(gl.ARRAY_BUFFER, triangleVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3,gl.FLOAT,false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color);
    gl.bufferData(gl.ARRAY_BUFFER, triangleColors, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,3,gl.FLOAT,false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);//done with triangle vao

    var squareVertices = new Float32Array([
        //front face
        -1.0, 1.0, 1.0, //left top
        -1.0, -1.0, 1.0,  //left bottom
        1.0, -1.0, 1.0,  //right bottom
        1.0, 1.0, 1.0, //right top
        //right face
        1.0, 1.0, 1.0,//left top
        1.0, -1.0, 1.0, //left bottom
        1.0, -1.0, -1.0, //right bottom
        1.0, 1.0, -1.0,//right top

        //back face
        1.0, 1.0, -1.0,//left top
        1.0, -1.0, -1.0,//left bottom
        -1.0, -1.0, -1.0, //right bottom
        -1.0, 1.0, -1.0, //right top

        //left face
        - 1.0, 1.0, -1.0,
        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        -1.0, 1.0, 1.0,

        //top face
        -1.0, 1.0, -1.0,
        -1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, -1.0,

        //bottom face
        -1.0, -1.0, -1.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0
    ]);

    var squareTexcoord = new Float32Array([
        0.0, 1.0,
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        1.0, 1.0,
        0.0, 1.0,
        0.0, 0.0,
        1.0, 0.0,
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0,
        0.0, 0.0,
        0.0, 0.0,
        1.0, 0.0,
        1.0, 1.0,
        0.0, 1.0
    ]);


    vao_square = gl.createVertexArray();
    gl.bindVertexArray(vao_square);
    //vbo for positions
    vbo_square_position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_square_position);
    gl.bufferData(gl.ARRAY_BUFFER, squareVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_VERTEX, 3, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    vbo_square_color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_square_color);
    gl.bufferData(gl.ARRAY_BUFFER, squareTexcoord, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXTURE0);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);//done with triangle vao

    //Now from here onward add Frame Buffer Facility

    var framebuffer = gl.createFramebuffer();

    FBO = gl.createFramebuffer();
    console.log("Genrate Frame Buffers");
    gl.bindFramebuffer(gl.FRAMEBUFFER, FBO);//Read/Draw

    texture_FBO = gl.createTexture();
    console.log("Genrate Texture Buffer");
    gl.bindTexture(gl.TEXTURE_2D, texture_FBO);

    gl.texStorage2D(gl.TEXTURE_2D, 1, gl.RGBA8, 1024, 1024);

    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture_FBO, 0);
    //Turn Off Mipmaps 4th 0

   // var draw_Buffers = { gl.COLOR_ATTACHMENT0 };
    gl.drawBuffers([gl.NONE, gl.COLOR_ATTACHMENT0 , gl.DEPTH_ATTACHMENT0]);
    console.log("Draw Actually buffer");

    //gl.drawBuffers(1, draw_Buffers);

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    console.log("unbind Frame Buffer");

	// Clear Color
	gl.clearColor(0.0, 0.0, 0.0, 1.0); // black

	// Depth test
	gl.enable(gl.DEPTH_TEST);

	//cull
	gl.enable(gl.CULL_FACE);

	perspectiveProjectionMatrix = mat4.create();
}

function For_DrawShaders_FBO()
{
    var gVertexShaderObject_FBO;
    var gFragementShaderObject_FBO;

    var vertexShaderSourceCodeN =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec2 vTexture0_Coord;" +
        "out vec2 out_texture0_coord;" +
        "uniform mat4 u_mvp_matrix;" +
        "void main(void)" +
        "{" +
        "gl_Position = u_mvp_matrix * vPosition;" +
        "/* Just pass texture coords as it is to fragment shader*/" +
        "out_texture0_coord = vTexture0_Coord;" +
        "}";

    gVertexShaderObject_FBO = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(gVertexShaderObject_FBO, vertexShaderSourceCodeN);
    gl.compileShader(gVertexShaderObject_FBO);
    if (gl.getShaderParameter(gVertexShaderObject_FBO, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(gVertexShaderObject_FBO);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    //fragmentShader
    var fragmentShaderSourceN =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec2 out_texture0_coord;" +
        "uniform highp sampler2D u_texture0_sampler;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "FragColor = texture(u_texture0_sampler, out_texture0_coord);" +
        "}";

    gFragementShaderObject_FBO = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(gFragementShaderObject_FBO, fragmentShaderSourceN);
    gl.compileShader(gFragementShaderObject_FBO);
    if (gl.getShaderParameter(gFragementShaderObject_FBO, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(gFragementShaderObject_FBO);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }
    //shader program
    gShaderProgramObject_FBO = gl.createProgram();
    gl.attachShader(gShaderProgramObject_FBO, gVertexShaderObject_FBO);
    gl.attachShader(gShaderProgramObject_FBO, gFragementShaderObject_FBO);

    //pre-link binidng of shader program object with vertex shader attributes
    gl.bindAttribLocation(gShaderProgramObject_FBO, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(gShaderProgramObject_FBO, WebGLMacros.AMC_ATTRIBUTE_TEXTURE0, "vTexture0_Coord");
    //linking
    gl.linkProgram(gShaderProgramObject_FBO);
    if (!gl.getProgramParameter(gShaderProgramObject_FBO, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(gShaderProgramObject_FBO);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }
    //get MVP uniform
    mvpUniform_FBO = gl.getUniformLocation(gShaderProgramObject_FBO, "u_mvp_matrix");
    samplerUniform_FBO = gl.getUniformLocation(gShaderProgramObject_FBO, "u_sampler");

}

function For_DrawShaders_Color()
{
    //vertex shaderProgramObject
    var vertexShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "in vec4 vPosition;" +
        "in vec4 vColor;" +
        "uniform mat4 u_mvp_matrix;" +
        "out vec4 out_color;" +
        "void main(void)" +
        "{" +
        "gl_Position = u_mvp_matrix * vPosition;" +
        "out_color = vColor;" +
        "}";
    vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
    gl.compileShader(vertexShaderObject);
    if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(vertexShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    //fragmentShader
    var fragmentShaderSource =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec4 out_color;" +
        "out vec4 FragColor;" +
        "void main(void)" +
        "{" +
        "FragColor = out_color;" +
        "}";
    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShaderObject, fragmentShaderSource);
    gl.compileShader(fragmentShaderObject);
    if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(fragmentShaderObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }
    //shader program
    shaderProgramObject = gl.createProgram();
    gl.attachShader(shaderProgramObject, vertexShaderObject);
    gl.attachShader(shaderProgramObject, fragmentShaderObject);

    //pre-link binidng of shader program object with vertex shader attributes
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(shaderProgramObject, WebGLMacros.AMC_ATTRIBUTE_COLOR, "vColor");
    //linking
    gl.linkProgram(shaderProgramObject);
    if (!gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS)) {
        var error = gl.getProgramInfoLog(shaderProgramObject);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }
    //get MVP uniform
    mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");

}

function resize() {
	// Code
	if (bFullscreen == true) {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else {
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set viewport
	gl.viewport(0, 0, canvas.width, canvas.height);

	// Perspective Projection
		mat4.perspective(perspectiveProjectionMatrix,45.0,
		parseFloat(canvas.width) / parseFloat(canvas.height),
		0.1,
		100.0);
}

function draw() {
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

    //draw pyramid
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
    var angleInRadian = degToRad(angleTriangle);

    gl.bindFramebuffer(gl.FRAMEBUFFER, FBO);

        gl.viewport(0, 0, 1024, 1024);
        gl.clearColor(1.0, 0.0, 1.0, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


        mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -5.0]);
        mat4.rotateY(modelViewMatrix, modelViewMatrix, angleInRadian);
        mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

        gl.uniformMatrix4fv(mvpUniform, false, modelViewProjectionMatrix);
    gl.activeTexture(gl.TEXTURE0);
    gl.uniform1i(samplerUniform_FBO, 0);
    gl.bindTexture(gl.TEXTURE_2D, texture_FBO);

            gl.bindVertexArray(vao_triangle);
            gl.drawArrays(gl.TRIANGLES, 0, 12);
            gl.bindVertexArray(null);

            gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    
     gl.useProgram(null);


    gl.useProgram(gShaderProgramObject_FBO);

    gl.clearColor(0.0, 0.0, 1.0, 1.0);
    gl.viewport(0, 0, canvas_original_width, canvas_original_height);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

   
    
    //draw cube
    var modelViewMatrix = mat4.create();
    var modelViewProjectionMatrix = mat4.create();
    //var modelViewMatrix = mat4.create();
    var rotationMatrix = mat4.create();
    var scaleMatrix = mat4.create();

    modelViewMatrix = mat4.identity(modelViewMatrix);
    modelViewProjectionMatrix = mat4.identity(modelViewProjectionMatrix);
    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -5.0]);
    mat4.scale(modelViewMatrix, modelViewMatrix, [0.75, 0.75, 0.75]);
    mat4.rotateX(modelViewMatrix, modelViewMatrix, angleInRadian);
    mat4.rotateY(modelViewMatrix, modelViewMatrix, angleInRadian);
    mat4.rotateZ(modelViewMatrix, modelViewMatrix, angleInRadian);

    mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);
    gl.uniformMatrix4fv(mvpUniform_FBO, false, modelViewProjectionMatrix);

    gl.activeTexture(gl.TEXTURE0);
    gl.uniform1i(samplerUniform_FBO, 0);
    gl.bindTexture(gl.TEXTURE_2D, texture_FBO);

    gl.bindVertexArray(vao_square);
    gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
    gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
    gl.bindVertexArray(null);

    gl.bindTexture(gl.TEXTURE_2D, null);

	gl.useProgram(null);

	// animation loop
	update();
	requestAnimationFrame(draw, canvas);
}

function update() {
	/* code */
	angleTriangle += 0.1;
	if (angleTriangle >= 360.0)
	{
		angleTriangle = 0.0;
	}
	angleSquare += 0.1;
	if (angleSquare >= 360.0)
	{
		angleSquare = 0.0;
	}
}

function uninitialize() {
	// Code
	if (vao_pyramid) {
		gl.deleteVertexArray(vao_pyramid);
		vao_pyramid = null;
	}

	if (vbo_position_pyramid) {
		gl.deleteBuffer(vbo_position_pyramid);
		vbo_position_pyramid = null;
	}

	if (shaderProgramObject) {
		if (fragmentShaderObject) {
			gl.detachShader(shaderProgramObject, fragmentShaderObject);
			gl.deleteShader(fragmentShaderObject);
			fragmentShaderObject = null;
		}
		
		if (vertexShaderObject) {
			gl.detachShader(shaderProgramObject, vertexShaderObject);
			gl.deleteShader(vertexShaderObject);
			vertexShaderObject = null;
		}

		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
}

function keyDown(event) {
	// Code
	switch(event.keyCode) {
		case 27:
			uninitialize();
			window.close();
			break;
		case 70: // For 'F' or 'f'
			toggleFullScreen();
			break;
	}
}


function mouseDown() {
	// Code
}

function degToRad(degrees) {
    return(degrees*Math.PI/180.0);
}

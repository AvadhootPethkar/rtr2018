/* headers */
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags , CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;

// interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char *argv[])
{
	/* code */
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate :[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];
	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	/* code */
	/* log file */
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath, "w");
	if(gpFile == NULL)
	{
		printf("Can Not Create Log File. \nExitting...\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program Is Started Successfully\n");

	/* window */
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	/* create simple window */
	window = [[NSWindow alloc]initWithContentRect:win_rect 
		styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
		backing:NSBackingStoreBuffered defer:NO];
	[window setTitle:@"macOS OpenGL window"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	/* code */
	fprintf(gpFile, "Program Is Trminated Successfully\n");

	if (gpFile)
	{
		/* code */
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	/* code */
	[NSApp terminate:self];
}

-(void)dealloc
{
	/* code */
	[glView release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displaylink;

	// Shader variables
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

	GLuint vao_pyramid;
    GLuint vao_cube;
    GLuint vbo_position_pyramid;
    GLuint vbo_color_pyramid;
    GLuint vbo_position_cube;
    GLuint vbo_texture_cube;
    GLuint texture_cube;

    GLuint samplerUniform;
    GLuint mvpUniform;
    mat4 perspectiveProjectionMatrix;

    // Animating variables
    GLfloat anglePyramid;
    GLfloat angleCube;

    //Rander to texture variables
    GLuint fbo_shaderProgramObject;
    GLuint fbo_mvpUniform;
    GLuint fbo_samplerUniform;
    GLuint fbo_texture_cube;
    GLuint fbo_depth;
    GLuint fbo;
    GLuint fbo_texture;

    GLuint gWidth;
    GLuint gHeight;
}

-(id)initWithFrame:(NSRect)frame;
{
	/* code */
	self = [super initWithFrame:frame];
    
    anglePyramid = 0.0f;
    angleCube = 0.0f;

	if (self)
	{
		/* code */
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFAAccelerated,
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		if (pixelFormat == nil)
		{
			/* code */
			fprintf(gpFile, "No valid OpenGL Pixel Format Is Available.Exitting...");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext];
	}
	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	/* code */
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return(kCVReturnSuccess);
}

- (void)prepareOpenGL
{
    // Shader Error Checking Variables
    GLint iShaderCompileStatus = 0;
    GLint iInfoLength = 0;
    GLchar *szInfoLog = NULL;

    // FBO variables
    GLuint fbo_vertexShaderObject;
    GLuint fbo_fragmentShaderObject;

	/* code */
	// OpenGL info
	[super prepareOpenGL];
	fprintf(gpFile, "OpenGL Version		:	%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version 		:	%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	// FBO Shader
    // VERTEX SHADER
    // Define Vertex Shader Object
    fbo_vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    // Write Vertex Shader Code
    const GLchar *fbo_vertexShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec2 vTexcord;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec2 out_texcord;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_texcord = vTexcord;" \
        "}";
    // Specify above Source Code To The Vertex Shader Object
    glShaderSource(fbo_vertexShaderObject, 1, (const GLchar **)&fbo_vertexShaderSourceCode, NULL);
    // Compile the VertexShader
    glCompileShader(fbo_vertexShaderObject);

    // Error checking
    iShaderCompileStatus = 0;
    iInfoLength = 0;
    szInfoLog = NULL;
    glGetShaderiv(fbo_vertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fbo_vertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        if (iInfoLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fbo_vertexShaderObject, iInfoLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

    // FRAGMENT SHADER
    // Define Fragment Shader Object
    fbo_fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    // Write Vertex Shader Code
    const GLchar *fbo_fragmentShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec2 out_texcord;" \
        "uniform sampler2D u_sampler;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = texture(u_sampler, out_texcord);" \
        "}";
    // Specify above Source Code To The Fragment Shader Object
    glShaderSource(fbo_fragmentShaderObject, 1, (const GLchar **)&fbo_fragmentShaderSourceCode, NULL);
    // Compile the FragmentShader
    glCompileShader(fbo_fragmentShaderObject);

    // Error checking
    iShaderCompileStatus = 0;
    iInfoLength = 0;
    szInfoLog = NULL;
    glGetShaderiv(fbo_fragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(fbo_fragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        if (iInfoLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(fbo_fragmentShaderObject, iInfoLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

    // Create Shader Program Object
    fbo_shaderProgramObject = glCreateProgram();
    // Attach Vertex Shader To Shader Program
    glAttachShader(fbo_shaderProgramObject, fbo_vertexShaderObject);
    // Attach Fragment Shader To Shader Program
    glAttachShader(fbo_shaderProgramObject, fbo_fragmentShaderObject);

    // Prelinking binding to vertex attribute
    glBindAttribLocation(fbo_shaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(fbo_shaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexcord");

    // Link The Shader Program
    glLinkProgram(fbo_shaderProgramObject);

    // Error checking
    GLint iProgramLinkStatus = 0;
    iInfoLength = 0;
    szInfoLog = NULL;
    glGetProgramiv(fbo_shaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(fbo_shaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        if (iInfoLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(fbo_shaderProgramObject, iInfoLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

    // Post Linking Retriving UniformLocation
    fbo_mvpUniform = glGetUniformLocation(fbo_shaderProgramObject, "u_mvp_matrix");
    fbo_samplerUniform = glGetUniformLocation(fbo_shaderProgramObject, "u_sampler");

    // Color Shader
    // VERTEX SHADER
    // Define Vertex Shader Object
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    // Write Vertex Shader Code
    const GLchar *vertexShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec2 vTexcord;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec2 out_texcord;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_texcord = vTexcord;" \
        "}";
    // Specify above Source Code To The Vertex Shader Object
    glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
    // Compile the VertexShader
    glCompileShader(gVertexShaderObject);

    // Error checking
    iShaderCompileStatus = 0;
    iInfoLength = 0;
    szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        if (iInfoLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

    // FRAGMENT SHADER
    // Define Fragment Shader Object
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    // Write Vertex Shader Code
    const GLchar *fragmentShaderSourceCode =
        "#version 410 core" \
        "\n" \
        "in vec2 out_texcord;" \
        "uniform sampler2D u_sampler;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = texture(u_sampler, out_texcord);" \
        "}";
    // Specify above Source Code To The Fragment Shader Object
    glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
    // Compile the FragmentShader
    glCompileShader(gFragmentShaderObject);

    // Error checking
    iShaderCompileStatus = 0;
    iInfoLength = 0;
    szInfoLog = NULL;
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        if (iInfoLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

    // Create Shader Program Object
    gShaderProgramObject = glCreateProgram();
    // Attach Vertex Shader To Shader Program
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    // Attach Fragment Shader To Shader Program
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);

    // Prelinking binding to vertex attribute
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_TEXCOORD0, "vTexcord");

    // Link The Shader Program
    glLinkProgram(gShaderProgramObject);

    // Error checking
    iProgramLinkStatus = 0;
    iInfoLength = 0;
    szInfoLog = NULL;
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
        if (iInfoLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

    // Post Linking Retriving UniformLocation
    mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
    samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");
    const GLfloat pyramidVertices[] =
    {
        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

        0.0f, 1.0f, 0.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,

        0.0f, 1.0f, 0.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f
    };

    const GLfloat pyramidColor[] =
    {
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,

        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
    };

    const GLfloat cubeVertices[] =
    {
        1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,

        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,

        -1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,

        -1.0f, 1.0f, 1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,

        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,

        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f
    };

    const GLfloat cubeTexCord[] =
    {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,

        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,

        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,

        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,

        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,

        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
    };

    // Create vao_triangle
    glGenVertexArrays(1, &vao_pyramid);
    glBindVertexArray(vao_pyramid);
    glGenBuffers(1, &vbo_position_pyramid);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pyramid);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(cubeVertices),
        cubeVertices,
        GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
        3, // No Of Co-ordinates
        GL_FLOAT, // Type Of Co-ordinates
        GL_FALSE, //
        0,
        NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //glBindVertexArray(0);

    glGenBuffers(1, &vbo_color_pyramid);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_pyramid);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(cubeTexCord),
        cubeTexCord,
        GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0,
        2, // No Of Co-ordinates
        GL_FLOAT, // Type Of Co-ordinates
        GL_FALSE, //
        0,
        NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    // Create vao_square
    glGenVertexArrays(1, &vao_cube);
    glBindVertexArray(vao_cube);
    glGenBuffers(1, &vbo_position_cube);
    glGenBuffers(1, &vbo_position_cube);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_cube);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(cubeVertices),
        cubeVertices,
        GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
        3, // No Of Co-ordinates
        GL_FLOAT, // Type Of Co-ordinates
        GL_FALSE, //
        0,
        NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //glBindVertexArray(0);

    glGenBuffers(1, &fbo_texture_cube);
    glBindBuffer(GL_ARRAY_BUFFER, fbo_texture_cube);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(cubeTexCord),
        cubeTexCord,
        GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0,
        2, // No Of Co-ordinates
        GL_FLOAT, // Type Of Co-ordinates
        GL_FALSE, //
        0,
        NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glGenTextures(1, &fbo_texture);
    glBindTexture(GL_TEXTURE_2D, fbo_texture);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, 1024, 1024);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, fbo_texture, 0);

    glGenRenderbuffers(1, &fbo_depth);
    glBindRenderbuffer(GL_RENDERBUFFER, fbo_depth);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, 1024, 1024);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, fbo_depth);

    static const GLenum draw_buffer[] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, draw_buffer);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_TEXTURE_2D);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	perspectiveProjectionMatrix = mat4::identity();

	CVDisplayLinkCreateWithActiveCGDisplays(&displaylink);
	CVDisplayLinkSetOutputCallback(displaylink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displaylink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displaylink);
}

-(GLuint)loadTextureFromBMPFile:(const char *)texFileName
{
	/* code */
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *textureFileNameWithPath = [NSString stringWithFormat:@"%@/%s",parentDirPath,texFileName];

	NSImage *bmpImage = [[NSImage alloc]initWithContentsOfFile : textureFileNameWithPath];
	if (!bmpImage)
	{
		/* code */
		NSLog(@"can't find %@",textureFileNameWithPath);
		return(0);
	}

	CGImageRef cgImage = [bmpImage CGImageForProposedRect:nil context:nil hints:nil];

	int w = (int)CGImageGetWidth(cgImage);
	int h = (int)CGImageGetHeight(cgImage);
	CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
	void *pixels = (void *)CFDataGetBytePtr(imageData);

	GLuint bmpTexture;

	glGenTextures(1, &bmpTexture);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glBindTexture(GL_TEXTURE_2D, bmpTexture);
	glTexParameteri(GL_TEXTURE_2D,
		GL_TEXTURE_MAG_FILTER,
		GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,
		GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);

	glTexImage2D(GL_TEXTURE_2D,
		0,
		GL_RGBA,
		w,
		h,
		0,
		GL_RGBA,
		GL_UNSIGNED_BYTE,
		pixels);
	glGenerateMipmap(GL_TEXTURE_2D);
	CFRelease(imageData);
	return(bmpTexture);
}

- (void)reshape
{
	/* code */
	[super reshape];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	if (height == 0)
		height = 1;

	glViewport(0, 0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
	/* code */
	[self drawView];
}

- (void)drawView
{
	/* code */
	[[self openGLContext]makeCurrentContext];
	
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
	
	// PYRAMID
    // Declaration of matrices
    mat4 translationMatrix;
    mat4 scaleMatrix;
    mat4 rotationMatrix;
    mat4 modelViewMatrix;
    mat4 modelViewProjectionMatrix;

    // Initialize above matrices to identity
    translationMatrix = mat4::identity();
    scaleMatrix = mat4::identity();
    rotationMatrix = mat4::identity();
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glViewport(0, 0, 1024, 1024);

    glClearColor(0.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Do necessary transformations
    translationMatrix = translate(0.0f,
        0.0f,
        -6.0f);
    rotationMatrix = rotate(anglePyramid, 0.0f, 1.0f, 0.0f);

    // Do necessary Matrix Multiplication
    modelViewMatrix = translationMatrix * rotationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    // This was internally donw by gluOrtho2d() in FFP

    // Send Necessary matrix to shader in respective uniform
    glUniformMatrix4fv(fbo_mvpUniform,
        1,
        GL_FALSE,
        modelViewProjectionMatrix);

    // ABU
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_cube);
    glUniform1i(samplerUniform, 0);
    glEnable(GL_TEXTURE_2D);

    // Bind with vao
    glBindVertexArray(vao_pyramid);

    // Similarly bind With Textures If Any


    // Draw The Necessary Senn
    glDrawArrays(GL_TRIANGLE_FAN,
        0,
        4);
    glDrawArrays(GL_TRIANGLE_FAN,
        4,
        4);
    glDrawArrays(GL_TRIANGLE_FAN,
        8,
        4);
    glDrawArrays(GL_TRIANGLE_FAN,
        12,
        4);
    glDrawArrays(GL_TRIANGLE_FAN,
        16,
        4);
    glDrawArrays(GL_TRIANGLE_FAN,
        20,
        4);

    // Unbind vao
    glBindVertexArray(0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glUseProgram(0);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glViewport(0, 0, (GLsizei)gWidth, (GLsizei)gHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(fbo_shaderProgramObject);

    // SQUARE
    // Initialize above matrices to identity
    translationMatrix = mat4::identity();
    scaleMatrix = mat4::identity();
    rotationMatrix = mat4::identity();
    modelViewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    // Do necessary transformations
    translationMatrix = translate(0.0f,
        0.0f,
        -6.0f);
    scaleMatrix = scale(0.75f, 0.75f, 0.75f);
    rotationMatrix = rotate(angleCube, angleCube, angleCube);

    // Do necessary Matrix Multiplication
    modelViewMatrix = translationMatrix * scaleMatrix* rotationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
    // This was internally donw by gluOrtho2d() in FFP

    // Send Necessary matrix to shader in respective uniform
    glUniformMatrix4fv(mvpUniform,
        1,
        GL_FALSE,
        modelViewProjectionMatrix);

    // ABU
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, fbo_texture);
    glUniform1i(fbo_samplerUniform, 0);
    glEnable(GL_TEXTURE_2D);

    // Bind with vao
    glBindVertexArray(vao_cube);

    // Similarly bind With Textures If Any


    // Draw The Necessary Senn
    glDrawArrays(GL_TRIANGLE_FAN,
        0,
        4);
    glDrawArrays(GL_TRIANGLE_FAN,
        4,
        4);
    glDrawArrays(GL_TRIANGLE_FAN,
        8,
        4);
    glDrawArrays(GL_TRIANGLE_FAN,
        12,
        4);
    glDrawArrays(GL_TRIANGLE_FAN,
        16,
        4);
    glDrawArrays(GL_TRIANGLE_FAN,
        20,
        4);

    // Unbind vao
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);

	// Unuse Program
	glUseProgram(0);

	[self update];

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void) update
{
    // code
    anglePyramid += 0.1f;
    if (anglePyramid >= 360.0f)
    {
        anglePyramid = 0.0f;
    }
    angleCube += 0.1f;
    if (angleCube >= 360.0f)
    {
        angleCube = 0.0f;
    }
}

-(BOOL)acceptsFirstResponder
{
	/* code */
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	/* code */
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repainting occures automatically
			break;
        default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	/* code */
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void) dealloc
{
	/* code */
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	CVDisplayLinkStop(displaylink);
	CVDisplayLinkRelease(displaylink);

	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displaylink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagOut, void *pDisplayLingContext)
{
	/* code */
	CVReturn result = [(GLView *)pDisplayLingContext getFrameForTime:pOutputTime];
	return(result);
}

/* headers */
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags , CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;

// interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char *argv[])
{
	/* code */
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate :[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];
	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	/* code */
	/* log file */
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath, "w");
	if(gpFile == NULL)
	{
		printf("Can Not Create Log File. \nExitting...\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program Is Started Successfully\n");

	/* window */
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	/* create simple window */
	window = [[NSWindow alloc]initWithContentRect:win_rect 
		styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
		backing:NSBackingStoreBuffered defer:NO];
	[window setTitle:@"macOS window"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	/* code */
	fprintf(gpFile, "Program Is Trminated Successfully\n");

	if (gpFile)
	{
		/* code */
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	/* code */
	[NSApp terminate:self];
}

-(void)dealloc
{
	/* code */
	[glView release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displaylink;

    // Shader variables
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint vaoI;
    GLuint vboI;
    GLuint vaoNCross;
    GLuint vboNCross;
    GLuint vaoD;
    GLuint vboDVertices;
    GLuint vboDColor;
    GLuint vaoDUp;
    GLuint vboDUp;
    GLuint vaoDDown;
    GLuint vboDDown;
    GLuint vaoALeft;
    GLuint vboALeft;
    GLuint vaoACenter;
    GLuint vboACenter;
    GLuint vboColor;
    GLuint mvpUniform;
    mat4 perspectiveProjectionMatrix;

    // Dynamic India Variable
    GLfloat gfLIX;
    GLfloat gfAX;
    GLfloat gfAXR;
    GLfloat gfNY;
    GLfloat gfRIY;
    GLfloat gfORC;
    GLfloat gfOGC;
    GLfloat gfOBC;
    GLfloat gfGRC;
    GLfloat gfGBC;
    GLfloat gfGGC;

    // Aeroplane Variables
    GLuint vao;
    GLuint vboPosition;
    GLuint vboColorPlane;

    // Triangle Variables
    GLint iTrianglePoints;

    // Line Variables
    GLint iLinePoints;

    // Square Variables
    GLint iSquarePoints;

    // Animating Variables Plane
    GLfloat gfUpperLeftAngle;
    GLfloat gfUpperLeftAngle_X;
    GLfloat gfUpperLeftAngle_Y;

    GLfloat gfMiddlePlaneTranslate_X;
    GLfloat gfMiddlePlaneTranslate_Y;

    GLfloat gfLowerLeftAngle;
    GLfloat gfLowerLeftAngle_X;
    GLfloat gfLowerLeftAngle_Y;

    GLfloat gfUpperRightAngle;
    GLfloat gfUpperRightAngle_X;
    GLfloat gfUpperRightAngle_Y;

    GLfloat gfLowerRightAngle;
    GLfloat gfLowerRightAngle_X;
    GLfloat gfLowerRightAngle_Y;

    GLfloat flag_offset_X1;
    GLfloat gfFlagOrange_X1;
    GLfloat gfFlagOrange_X2;
    GLfloat gfFlagOrange_Y;
    GLfloat gfFlagWhite_X1;
    GLfloat gfFlagWhite_X2;
    GLfloat gfFlagWhite_Y;
    GLfloat gfFlagGreen_X1;
    GLfloat gfFlagGreen_X2;
    GLfloat gfFlagGreen_Y;

    // Small Flag Variables
    GLfloat gfSmallFlagOrange_X1;
    GLfloat gfSmallFlagWhite_X1;
    GLfloat gfSmallFlagGreen_X1;
    bool gbAnimatePlane;
}

-(id)initWithFrame:(NSRect)frame;
{
	/* code */
	self = [super initWithFrame:frame];

	if (self)
	{
		/* code */
		[[self window]setContentView:self];

		// Dynamic India Variable
        gfLIX = -1.3f;
        gfAX = 1.52f;
        gfAXR = 3.52f;
        gfNY = 2.3f;
        gfRIY = -2.3f;
        gfORC = 0.0f;
        gfOGC = 0.0f;
        gfOBC = 0.0f;
        gfGRC = 0.0f;
        gfGBC = 0.0f;
        gfGGC = 0.0f;
        
        // Triangle Variables
        iTrianglePoints = 3;

        // Line Variables
        iLinePoints = 3;

        // Square Variables
        iSquarePoints = 4;

        // Animating Variables Plane
        gfUpperLeftAngle = M_PI;
        gfUpperLeftAngle_X = -7.0f;
        gfUpperLeftAngle_Y = 3.0f;

        gfMiddlePlaneTranslate_X = -5.0f;
        gfMiddlePlaneTranslate_Y = 0.0f;

        gfLowerLeftAngle = M_PI;
        gfLowerLeftAngle_X = -7.0f;
        gfLowerLeftAngle_Y = -3.0f;

        gfUpperRightAngle = 3 * (M_PI / 2);
        gfUpperRightAngle_X = 0.0f;
        gfUpperRightAngle_Y = 0.0f;

        gfLowerRightAngle = (M_PI / 2);
        gfLowerRightAngle_X = 0.0f;
        gfLowerRightAngle_Y = 0.0f;

        flag_offset_X1 = -5.0f;
        gfFlagOrange_X1 = flag_offset_X1;
        gfFlagOrange_X2 = 0.0f;
        gfFlagOrange_Y = 0.03f;
        gfFlagWhite_X1 = flag_offset_X1;
        gfFlagWhite_X2 = 0.0f;
        gfFlagWhite_Y = 0.0f;
        gfFlagGreen_X1 = flag_offset_X1;
        gfFlagGreen_X2 = 0.0f;
        gfFlagGreen_Y = -0.03f;

        // Small Flag Variables
        gfSmallFlagOrange_X1 = -1.0f;
        gfSmallFlagWhite_X1 = -1.0f;
        gfSmallFlagGreen_X1 = -1.0f;
        gbAnimatePlane = false;
		
		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFAAccelerated,
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		if (pixelFormat == nil)
		{
			/* code */
			fprintf(gpFile, "No valid OpenGL Pixel Format Is Available.Exitting...");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext];
	}
	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	/* code */
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return(kCVReturnSuccess);
}

- (void)prepareOpenGL
{
	/* code */
	// OpenGL info
	fprintf(gpFile, "OpenGL Version		:	%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version 		:	%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    // VERTEX SHADER
	// Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;" \
        "in vec4 vColor;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec4 out_color;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_color = vColor;" \
		"}";

	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObject);

	// Error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER
	// Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"in vec4 out_color;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = out_color;" \
		"}";

	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	
    const GLfloat fIVertices[] =
        {
            -1.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 0.0f,
        };
    
        const GLfloat fNCrossVertices[] =
        {
            -1.0f, 1.0f, 0.0f,
            -0.5f, -1.0f,0.0f,
        };
    
        const GLfloat fACenterVertices[] =
        {
            -1.0f, 1.0f, 0.0f,
            -0.5f, 1.0f,0.0f,
        };
        const GLfloat fALeftVertices[] =
        {
            1.0f, 1.0f, 0.0f,
            0.7f, -1.0f, 0.0f,
        };
        const GLfloat triColor[] =
        {
            1.0f, 0.6f, 0.2f,
            0.07f, 0.53f, 0.02f
        };
        const GLfloat fOrangeColor[] =
        {
            1.0f, 0.6f, 0.2f,
            1.0f, 0.6f, 0.2f
        };
        const GLfloat fGreenColor[] =
        {
            0.07f, 0.53f, 0.02f,
            0.07f, 0.53f, 0.02f
        };
        const GLfloat fWhiteColor[] =
        {
            1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f
        };
    
        const GLfloat fDVertices[] =
        {
            -0.5f, 1.0f, 0.0f,
            0.5f, 1.0f, 0.0f,
            0.5f, 1.0f, 0.0f,
            0.5f, -1.0f, 0.0f,
            0.5f, -1.0f, 0.0f,
            -0.5f, -1.0f, 0.0f,
            -0.4f, -1.0f, 0.0f,
            -0.4f, 1.0f, 0.0f,
        };
    
        // Create vaoI
        glGenVertexArrays(1, &vaoI);
        glBindVertexArray(vaoI);
        glGenBuffers(1, &vboI);
    
        //IVerticesVBO
        glBindBuffer(GL_ARRAY_BUFFER, vboI);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(fIVertices),
            fIVertices,
            GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //IColorVBO
        glGenBuffers(1, &vboColor);
        glBindBuffer(GL_ARRAY_BUFFER, vboColor);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(triColor),
            triColor,
            GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    
        // Create vaoNCross
        glGenVertexArrays(1, &vaoNCross);
        //NCrossVertices
        glBindVertexArray(vaoNCross);
        glGenBuffers(1, &vboNCross);
        glBindBuffer(GL_ARRAY_BUFFER, vboNCross);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(fNCrossVertices),
            fNCrossVertices,
            GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //IColorVBO
        glGenBuffers(1, &vboColor);
        glBindBuffer(GL_ARRAY_BUFFER, vboColor);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(triColor),
            triColor,
            GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        glBindVertexArray(0);
    
        // Create vaoALeft
        glGenVertexArrays(1, &vaoALeft);
        //ALeftVertices
        glBindVertexArray(vaoALeft);
        glGenBuffers(1, &vboALeft);
        glBindBuffer(GL_ARRAY_BUFFER, vboALeft);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(fALeftVertices),
            fALeftVertices,
            GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //IColorVBO
        glGenBuffers(1, &vboColor);
        glBindBuffer(GL_ARRAY_BUFFER, vboColor);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(triColor),
            triColor,
            GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    
        // Create vaoDUp
        glGenVertexArrays(1, &vaoDUp);
        // ACenterVertices
        glBindVertexArray(vaoDUp);
        //vboDUpVertices
        glGenBuffers(1, &vboDUp);
        glBindBuffer(GL_ARRAY_BUFFER, vboDUp);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(fACenterVertices),
            fACenterVertices,
            GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //vboColor
        glGenBuffers(1, &vboDUp);
        glBindBuffer(GL_ARRAY_BUFFER, vboDUp);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(fOrangeColor),
            fOrangeColor,
            GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    
        //vaoDDown
        glGenVertexArrays(1, &vaoDDown);
        // ACenterVertices
        glBindVertexArray(vaoDDown);
        //vboDUpVertices
        glGenBuffers(1, &vboDDown);
        glBindBuffer(GL_ARRAY_BUFFER, vboDDown);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(fACenterVertices),
            fACenterVertices,
            GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //vboColor
        glGenBuffers(1, &vboDDown);
        glBindBuffer(GL_ARRAY_BUFFER, vboDDown);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(fGreenColor),
            fGreenColor,
            GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    
        // Create vaoACenter
        glGenVertexArrays(1, &vaoACenter);
        // ACenterVertices
        glBindVertexArray(vaoACenter);
        glGenBuffers(1, &vboACenter);
        glBindBuffer(GL_ARRAY_BUFFER, vboACenter);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(fACenterVertices),
            fACenterVertices,
            GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //ACenterColorVBO
        glGenBuffers(1, &vboColor);
        glBindBuffer(GL_ARRAY_BUFFER, vboColor);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(fWhiteColor),
            fWhiteColor,
            GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    
        // D
        // Create vaoD
        glGenVertexArrays(1, &vaoD);
        glBindVertexArray(vaoD);
        glGenBuffers(1, &vboDVertices);
    
        //IVerticesVBO
        glBindBuffer(GL_ARRAY_BUFFER, vboDVertices);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(fDVertices),
            fDVertices,
            GL_STATIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //IColorVBO
        glGenBuffers(1, &vboDColor);
        glBindBuffer(GL_ARRAY_BUFFER, vboDColor);
        glBufferData(GL_ARRAY_BUFFER,
            4 * 2 * sizeof(float),
            NULL,
            GL_DYNAMIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    
        // Plane Vao
        // Create vaoI
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        glGenBuffers(1, &vboPosition);
    
        //IVerticesVBO
        glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
        glBufferData(GL_ARRAY_BUFFER,
            4 * 2 * sizeof(GLfloat),
            NULL,
            GL_DYNAMIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        //IColorVBO
        glGenBuffers(1, &vboColorPlane);
        glBindBuffer(GL_ARRAY_BUFFER, vboColorPlane);
        glBufferData(GL_ARRAY_BUFFER,
            4 * 2 * sizeof(GLfloat),
            NULL,
            GL_DYNAMIC_DRAW);
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
            3, // No Of Co-ordinates
            GL_FLOAT, // Type Of Co-ordinates
            GL_FALSE, //
            0,
            NULL);
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	perspectiveProjectionMatrix = mat4::identity();

	CVDisplayLinkCreateWithActiveCGDisplays(&displaylink);
	CVDisplayLinkSetOutputCallback(displaylink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displaylink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displaylink);
}

- (void)reshape
{
	/* code */
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	if (height == 0)
		height = 1;

	glViewport(0, 0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
	/* code */
	[self drawView];
}

- (void)drawView
{
	/* code */
	[[self openGLContext]makeCurrentContext];
	
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

    // Declaration of matrices
        mat4 translationMatrix;
        mat4 scaleMatrix;
        mat4 rotationMatrix;
        mat4 modelViewMatrix;
        mat4 modelViewProjectionMatrix;
    
        // I
        // Initialize above matrices to identity
        translationMatrix = mat4::identity();
        scaleMatrix = mat4::identity();
        rotationMatrix = mat4::identity();
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
    
        // Do necessary transformations
        modelViewMatrix = translate(gfLIX,
            0.0f,
            -3.0f);
    
        // Do necessary Matrix Multiplication
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP
    
    // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);
    
        glLineWidth(3.0f);
        // Bind with vao
        glBindVertexArray(vaoI);
    
        // Similarly bind With Textures If Any
    
    
        // Draw The Necessary Senn
        glDrawArrays(GL_LINES,
            0,
            3);
    
        // Unbind vao
        glBindVertexArray(0);
    
    
        // NLeft
        // Initialize above matrices to identity
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
    
        // Do necessary transformations
        modelViewMatrix = translate(-0.06f,
            gfNY,
            -3.0f);
    
        // Do necessary Matrix Multiplication
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP
    
    // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);
    
        // Bind with vao
        glBindVertexArray(vaoI);
    
        // Similarly bind With Textures If Any
    
    
        // Draw The Necessary Senn
        glDrawArrays(GL_LINES,
            0,
            3);
    
        // Unbind vao
        glBindVertexArray(0);
    
        // NCross
        // Initialize above matrices to identity
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
    
        // Do necessary transformations
        modelViewMatrix = translate(-0.06f,
            gfNY,
            -3.0f);
    
        // Do necessary Matrix Multiplication
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP
    
    // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);
    
        // Bind with vao
        glBindVertexArray(vaoNCross);
    
        // Similarly bind With Textures If Any
    
    
        // Draw The Necessary Senn
        glDrawArrays(GL_LINES,
            0,
            3);
    
        // Unbind vao
        glBindVertexArray(0);
    
        // NRight
        // Initialize above matrices to identity
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
    
        // Do necessary transformations
        modelViewMatrix = translate(0.44f,
            gfNY,
            -3.0f);
    
        // Do necessary Matrix Multiplication
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP
    
        // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);
    
        // Bind with vao
        glBindVertexArray(vaoI);
    
        // Similarly bind With Textures If Any
    
    
        // Draw The Necessary Senn
        glDrawArrays(GL_LINES,
            0,
            3);
    
        // Unbind vao
        glBindVertexArray(0);
    
        // D
        // Initialize above matrices to identity
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
    
        // Do necessary transformations
        modelViewMatrix = translate(0.10f,
            0.0f,
            -3.0f);
    
        // Do necessary Matrix Multiplication
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP
    
        // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);
    
        // D color array
        float d_color_array[] =
        {
            gfORC, gfOGC, gfOBC,
            gfORC, gfOGC, gfOBC,
    
            gfORC, gfOGC, gfOBC,
            gfGRC, gfGGC, gfGBC,
    
            gfGRC, gfGGC, gfGBC,
            gfGRC, gfGGC, gfGBC,
    
            gfGRC, gfGGC, gfGBC,
            gfORC, gfOGC, gfOBC,
        };
        glBindBuffer(GL_ARRAY_BUFFER, vboDColor);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(d_color_array),
            d_color_array,
            GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    
        // Bind with vao
        glBindVertexArray(vaoD);
    
        // Similarly bind With Textures If Any
    
    
        // Draw The Necessary Senn
        glDrawArrays(GL_LINE_STRIP,
            0,
            8);
    
        // Unbind vao
        glBindVertexArray(0);
    
        // ISecond
        // Initialize above matrices to identity
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
    
        // Do necessary transformations
        modelViewMatrix = translate(1.76f,
            gfRIY,
            -3.0f);
    
        // Do necessary Matrix Multiplication
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP
    
        // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);
    
        // Bind with vao
        glBindVertexArray(vaoI);
    
        // Similarly bind With Textures If Any
    
    
        // Draw The Necessary Senn
        glDrawArrays(GL_LINES,
            0,
            3);
    
        // Unbind vao
        glBindVertexArray(0);
    
        // ALeft
        // Initialize above matrices to identity
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
    
        // Do necessary transformations
        modelViewMatrix = translate(gfAX,
            0.0f,
            -3.0f);
    
        // Do necessary Matrix Multiplication
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP
    
        // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);
    
        // Bind with vao
        glBindVertexArray(vaoALeft);
    
        // Similarly bind With Textures If Any
    
    
        // Draw The Necessary Senn
        glDrawArrays(GL_LINES,
            0,
            3);
    
        // Unbind vao
        glBindVertexArray(0);
    
        // ACenterOrange
        // Initialize above matrices to identity
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
    
        // Do necessary transformations
        modelViewMatrix = translate(2.63f,
            -0.988f,
            -3.9f);
    
        // Do necessary Matrix Multiplication
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP
    
        // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);
    
        // Bind with vao
        glBindVertexArray(vaoDUp);
    
        // Similarly bind With Textures If Any
    
    
        // Draw The Necessary Senn
        glDrawArrays(GL_LINES,
            0,
            3);
    
        // Unbind vao
        glBindVertexArray(0);
    
        // ACenterWhite
        // Initialize above matrices to identity
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
    
        // Do necessary transformations
        modelViewMatrix = translate(2.63f,
            -1.0f,
            -3.9f);
    
        // Do necessary Matrix Multiplication
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP
    
        // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);
    
        // Bind with vao
        glBindVertexArray(vaoACenter);
    
        // Similarly bind With Textures If Any
    
    
        // Draw The Necessary Senn
        glDrawArrays(GL_LINES,
            0,
            3);
    
        // Unbind vao
        glBindVertexArray(0);
    
        // ACenterGreen
        // Initialize above matrices to identity
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
    
        // Do necessary transformations
        modelViewMatrix = translate(2.63f,
            -1.01f,
            -3.9f);
    
        // Do necessary Matrix Multiplication
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP
    
        // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);
    
        // Bind with vao
        glBindVertexArray(vaoDDown);
    
        // Similarly bind With Textures If Any
    
    
        // Draw The Necessary Senn
        glDrawArrays(GL_LINES,
            0,
            3);
    
        // Unbind vao
        glBindVertexArray(0);
    
        // ARight
        // Initialize above matrices to identity
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
    
        // Do necessary transformations
        modelViewMatrix = translate(gfAXR,
            0.0f,
            -3.0f);
    
        // Do necessary Matrix Multiplication
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP
    
        // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);
    
        // Bind with vao
        glBindVertexArray(vaoNCross);
    
        // Similarly bind With Textures If Any
    
    
        // Draw The Necessary Senn
        glDrawArrays(GL_LINES,
            0,
            3);
    
        // Unbind vao
        glBindVertexArray(0);
    
        //-----------------------------------------------------------------------------------------------------------
        // AeroPlane
        // Upper Plain
        // Initialize above matrices to identity
        translationMatrix = mat4::identity();
        rotationMatrix = mat4::identity();
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
    
        // Do necessary transformations
        translationMatrix *= translate(-3.5f,
            3.0f,
            -1.0f);
        if (gbAnimatePlane)
        {
            if (gfUpperLeftAngle < ((3.0f * M_PI) / 2))
            {
                gfUpperLeftAngle_X = 3.0f * cos(gfUpperLeftAngle);
                gfUpperLeftAngle_Y = 3.0f * sin(gfUpperLeftAngle);
                translationMatrix *= translate(gfUpperLeftAngle_X,
                    gfUpperLeftAngle_Y,
                    -6.0f);
            }
            gfUpperLeftAngle += 0.001f;
        }
    
        //rotationMatrix = rotate(gfAngle, 0.0f, 1.0f, 0.0f);
    
        // Do necessary Matrix Multiplication
        modelViewMatrix *= translationMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP
    
        // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);
    
        [self drawAeroPlane];
        // Middle Flag
        [self drawFlag:gfFlagOrange_X1 flagOrange_X2:gfFlagOrange_X2 flagOrange_Y:gfFlagOrange_Y flagWhite_X1:gfFlagWhite_X1 flagWhite_X2:gfFlagWhite_X2 flagWhite_Y:gfFlagWhite_Y flagGreen_X1:gfFlagGreen_X1 flagGreen_X2:gfFlagGreen_X2 flagGreen_Y:gfFlagGreen_Y];
        
        // Middle Plain
        // Initialize above matrices to identity
        translationMatrix = mat4::identity();
        scaleMatrix = mat4::identity();
        rotationMatrix = mat4::identity();
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
    
        // Do necessary transformations
        translationMatrix = translate(gfMiddlePlaneTranslate_X,
            gfMiddlePlaneTranslate_Y,
            -3.0f);
        scaleMatrix = scale(0.5f, 0.5f, 0.5f);
    
        //rotationMatrix = rotate(gfAngle, 0.0f, 1.0f, 0.0f);
    
        // Do necessary Matrix Multiplication
        modelViewMatrix *= translationMatrix;
        modelViewMatrix *= scaleMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP
    
        // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);
    
        [self drawAeroPlane];
        [self drawFlag:gfFlagOrange_X1 flagOrange_X2:gfFlagOrange_X2 flagOrange_Y:gfFlagOrange_Y flagWhite_X1:gfFlagWhite_X1 flagWhite_X2:gfFlagWhite_X2 flagWhite_Y:gfFlagWhite_Y flagGreen_X1:gfFlagGreen_X1 flagGreen_X2:gfFlagGreen_X2 flagGreen_Y:gfFlagGreen_Y];
    
        // Lower Plain
        // Initialize above matrices to identity
        translationMatrix = mat4::identity();
        scaleMatrix = mat4::identity();
        rotationMatrix = mat4::identity();
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();
    
        // Do necessary transformations
        translationMatrix *= translate(-3.5f,
            -3.0f,
            -1.0f);
        if (gbAnimatePlane)
        {
            if (gfLowerLeftAngle > (M_PI / 2))
            {
                gfLowerLeftAngle_X = 3.0f * cos(gfLowerLeftAngle);
                gfLowerLeftAngle_Y = 3.0f * sin(gfLowerLeftAngle);
                translationMatrix *= translate(gfLowerLeftAngle_X,
                    gfLowerLeftAngle_Y,
                    -6.0f);
            }
            gfLowerLeftAngle -= 0.001f;
        }
    
        //rotationMatrix = rotate(gfAngle, 0.0f, 1.0f, 0.0f);
    
        // Do necessary Matrix Multiplication
        modelViewMatrix *= translationMatrix;
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP
    
        // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);
    
        [self drawAeroPlane];
        [self drawFlag:gfFlagOrange_X1 flagOrange_X2:gfFlagOrange_X2 flagOrange_Y:gfFlagOrange_Y flagWhite_X1:gfFlagWhite_X1 flagWhite_X2:gfFlagWhite_X2 flagWhite_Y:gfFlagWhite_Y flagGreen_X1:gfFlagGreen_X1 flagGreen_X2:gfFlagGreen_X2 flagGreen_Y:gfFlagGreen_Y];
    
        if (gfMiddlePlaneTranslate_X >= 1.3f)
        {
            // Small Flag Adjustment
            gfSmallFlagOrange_X1 = gfSmallFlagWhite_X1 = gfSmallFlagGreen_X1 = -1.25f;
            // Upper Plane
            // Initialize above matrices to identity
            translationMatrix = mat4::identity();
            rotationMatrix = mat4::identity();
            modelViewMatrix = mat4::identity();
            modelViewProjectionMatrix = mat4::identity();
    
            // Do necessary transformations
            translationMatrix *= translate(4.0f,
                3.0f,
                -4.0f);
            if (gfUpperRightAngle < (2.0f * M_PI))
            {
                gfUpperRightAngle_X = 3.0f * cos(gfUpperRightAngle);
                gfUpperRightAngle_Y = 3.0f * sin(gfUpperRightAngle);
                translationMatrix *= translate(gfUpperRightAngle_X,
                    gfUpperRightAngle_Y,
                    -6.0f);
            }
            gfUpperRightAngle += 0.003f;
    
            //rotationMatrix = rotate(gfAngle, 0.0f, 1.0f, 0.0f);
    
            // Do necessary Matrix Multiplication
            modelViewMatrix *= translationMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            // This was internally donw by gluOrtho2d() in FFP
    
            // Send Necessary matrix to shader in respective uniform
            glUniformMatrix4fv(mvpUniform,
                1,
                GL_FALSE,
                modelViewProjectionMatrix);
    
            [self drawAeroPlane];
            [self drawFlag:gfSmallFlagOrange_X1 flagOrange_X2:gfFlagOrange_X2 flagOrange_Y:gfFlagOrange_Y flagWhite_X1:gfSmallFlagWhite_X1 flagWhite_X2:gfFlagWhite_X2 flagWhite_Y:gfFlagWhite_Y flagGreen_X1:gfSmallFlagGreen_X1 flagGreen_X2:gfFlagGreen_X2 flagGreen_Y:gfFlagGreen_Y];
//            drawFlag(gfSmallFlagOrange_X1, gfFlagOrange_X2, gfFlagOrange_Y, gfSmallFlagWhite_X1, gfFlagWhite_X2, gfFlagWhite_Y, gfSmallFlagGreen_X1, gfFlagGreen_X2, gfFlagGreen_Y);
    
            // Lower Plane
            // Initialize above matrices to identity
            translationMatrix = mat4::identity();
            rotationMatrix = mat4::identity();
            modelViewMatrix = mat4::identity();
            modelViewProjectionMatrix = mat4::identity();
    
            // Do necessary transformations
            translationMatrix *= translate(4.0f,
                -3.0f,
                -4.0f);
            if (gfLowerRightAngle > 0)
            {
                gfLowerRightAngle_X = 3.0f * cos(gfLowerRightAngle);
                gfLowerRightAngle_Y = 3.0f * sin(gfLowerRightAngle);
                translationMatrix *= translate(gfLowerRightAngle_X,
                    gfLowerRightAngle_Y,
                    -6.0f);
            }
            gfLowerRightAngle -= 0.003f;
    
            //rotationMatrix = rotate(gfAngle, 0.0f, 1.0f, 0.0f);
    
            // Do necessary Matrix Multiplication
            modelViewMatrix *= translationMatrix;
            modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
            // This was internally donw by gluOrtho2d() in FFP
    
            // Send Necessary matrix to shader in respective uniform
            glUniformMatrix4fv(mvpUniform,
                1,
                GL_FALSE,
                modelViewProjectionMatrix);
    
            [self drawAeroPlane];
            [self drawFlag:gfSmallFlagOrange_X1 flagOrange_X2:gfFlagOrange_X2 flagOrange_Y:gfFlagOrange_Y flagWhite_X1:gfSmallFlagWhite_X1 flagWhite_X2:gfFlagWhite_X2 flagWhite_Y:gfFlagWhite_Y flagGreen_X1:gfSmallFlagGreen_X1 flagGreen_X2:gfFlagGreen_X2 flagGreen_Y:gfFlagGreen_Y];
//            drawFlag(gfSmallFlagOrange_X1, gfFlagOrange_X2, gfFlagOrange_Y, gfSmallFlagWhite_X1, gfFlagWhite_X2, gfFlagWhite_Y, gfSmallFlagGreen_X1, gfFlagGreen_X2, gfFlagGreen_Y);
        }

	// Unuse Program
	glUseProgram(0);

    [self update];

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void)drawAeroPlane
{
    // Code:
    // Bind with vao
    glBindVertexArray(vao);

    // Similarly bind With Textures If Any


    // Draw The Necessary Senn
    // Central Body
    // Draw Square
    [self drawSquare:0.3f fy1:0.1f fx2:-0.4f fy2:0.1f fx3:-0.4f fy3:-0.1f fx4:0.3f fy4:-0.1f];
    glDrawArrays(GL_TRIANGLE_FAN,
        0,
        iSquarePoints);

    // Unbind vao
    glBindVertexArray(0);

    //Upper Wing
    // Triangle
    // Bind with vao
    glBindVertexArray(vao);

    // Similarly bind With Textures If Any


    // Draw The Necessary Senn
    // Draw Triangle
    glPointSize(3.0f);
    [self drawTriangle:-0.4f fy1:0.5f fx2:-0.2f fy2:0.1f fx3:0.2f fy3:0.1f];
    glDrawArrays(GL_TRIANGLES,
        0,
        iTrianglePoints);

    // Unbind vao
    glBindVertexArray(0);

    //Lower Wing
    // Triangle
    // Initialize above matrices to identity
    // Bind with vao
    glBindVertexArray(vao);

    // Similarly bind With Textures If Any


    // Draw The Necessary Senn
    // Draw Triangle
    glPointSize(3.0f);
    [self drawTriangle:-0.4f fy1:-0.5f fx2:-0.2f fy2:-0.1f fx3:0.2f fy3:-0.1f];
    glDrawArrays(GL_TRIANGLES,
        0,
        iTrianglePoints);

    // Unbind vao
    glBindVertexArray(0);

    //Front Nosel
    // Triangle
    // Bind with vao
    glBindVertexArray(vao);

    // Similarly bind With Textures If Any


    // Draw The Necessary Senn
    // Draw Triangle
    glPointSize(3.0f);
    [self drawTriangle:0.3f fy1:0.1f fx2:0.6f fy2:0.0f fx3:0.3f fy3:-0.1f];
    glDrawArrays(GL_TRIANGLES,
        0,
        iTrianglePoints);

    // Unbind vao
    glBindVertexArray(0);

    // Back Tail
    // Square
    // Bind with vao
    glBindVertexArray(vao);

    // Similarly bind With Textures If Any


    // Draw The Necessary Senn
    // Central Body
    // Draw Square
    [self drawSquare:-0.4f fy1:0.1f fx2:-0.6f fy2:0.3f fx3:-0.6f fy3:-0.3f fx4:-0.4f fy4:-0.1f];
//    drawSquare(-0.4f, 0.1f, -0.6f, 0.3f, -0.6f, -0.3f, -0.4f, -0.1f);
    glDrawArrays(GL_TRIANGLE_FAN,
        0,
        iSquarePoints);
    //drawDethlyHollows();

    // Unbind vao
    glBindVertexArray(0);

    //drawIAF();

    // Bind with vao
    glBindVertexArray(vao);

    // Similarly bind With Textures If Any
    const GLfloat linei[] =
    {
        -0.35f,0.1f,0.0f,
        -0.35f,-0.1f,0.0f,
    };

    const GLfloat lineicolor[] =
    {
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
    };
    // Line Vertices
    glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(linei),
        linei,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Circle Color
    glBindBuffer(GL_ARRAY_BUFFER, vboColorPlane);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(lineicolor),
        lineicolor,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // Draw The Necessary Senn
    glLineWidth(3.0f);
    glDrawArrays(GL_LINES,
        0,
        2);

    // Unbind vao
    glBindVertexArray(0);

    // For A
    glBindVertexArray(vao);

    // Similarly bind With Textures If Any
    const GLfloat lineA[] =
    {
        0.0f,-0.1f,0.0f,
        -0.1f,0.1f,0.0f,
        -0.2f,-0.1f,0.0f,
    };

    const GLfloat lineAcolor[] =
    {
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
    };
    // Line Vertices
    glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(lineA),
        lineA,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Circle Color
    glBindBuffer(GL_ARRAY_BUFFER, vboColorPlane);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(lineAcolor),
        lineAcolor,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // Draw The Necessary Senn
    glLineWidth(3.0f);
    glDrawArrays(GL_LINE_STRIP,
        0,
        3);

    // Unbind vao
    glBindVertexArray(0);

    // For F
    glBindVertexArray(vao);

    // Similarly bind With Textures If Any
    const GLfloat lineF[] =
    {
        0.05f,-0.1f,0.0f,
        0.05f,0.1f,0.0f,
        0.05f,0.1f,0.0f,
        0.20f,0.1f,0.0f,
        0.05f,0.1f,0.0f,
        0.05f,0.0f,0.0f,
        0.20f,0.0f,0.0f,
    };

    const GLfloat lineFcolor[] =
    {
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
    };
    // Line Vertices
    glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(lineF),
        lineF,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Circle Color
    glBindBuffer(GL_ARRAY_BUFFER, vboColorPlane);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(lineFcolor),
        lineFcolor,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // Draw The Necessary Senn
    glLineWidth(3.0f);
    glDrawArrays(GL_LINE_STRIP,
        0,
        7);

    // Unbind vao
    glBindVertexArray(0);

}

-(void) drawSquare:(float) x1 fy1:(float) y1 fx2:(float) x2 fy2:(float) y2 fx3:(float) x3 fy3:(float) y3 fx4:(float) x4 fy4:(float) y4
{
    // function declaration

    // Variable declaration
    GLfloat fSquareVertices[3 * iSquarePoints];
    GLfloat fSquareColor[3 * iSquarePoints];

    // Vertices
    fSquareVertices[0] = x1;
    fSquareVertices[1] = y1;
    fSquareVertices[2] = 0;

    fSquareVertices[3] = x2;
    fSquareVertices[4] = y2;
    fSquareVertices[5] = 0;

    fSquareVertices[6] = x3;
    fSquareVertices[7] = y3;
    fSquareVertices[8] = 0;

    fSquareVertices[9] = x4;
    fSquareVertices[10] = y4;
    fSquareVertices[11] = 0;

    // Color
    fSquareColor[0] = 0.7f;
    fSquareColor[1] = 0.8f;
    fSquareColor[2] = 0.9f;

    fSquareColor[3] = 0.7f;
    fSquareColor[4] = 0.8f;
    fSquareColor[5] = 0.9f;

    fSquareColor[6] = 0.7f;
    fSquareColor[7] = 0.8f;
    fSquareColor[8] = 0.9f;

    fSquareColor[9] = 0.7f;
    fSquareColor[10] = 0.8f;
    fSquareColor[11] = 0.9f;

    // Line Vertices
    glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(fSquareVertices),
        fSquareVertices,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Circle Color
    glBindBuffer(GL_ARRAY_BUFFER, vboColorPlane);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(fSquareColor),
        fSquareColor,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

-(void) drawTriangle:(float) x1 fy1:(float) y1 fx2:(float) x2 fy2:(float) y2 fx3:(float) x3 fy3:(float) y3
{
    // function declaration

    // Variable declaration
    GLfloat fTriangleVertices[3 * iTrianglePoints];
    GLfloat fTriangleColor[3 * iTrianglePoints];

    //Code
    //Triangle Vertices
    fTriangleVertices[0] = x1;
    fTriangleVertices[1] = y1;
    fTriangleVertices[2] = 0.0f;

    fTriangleVertices[3] = x2;
    fTriangleVertices[4] = y2;
    fTriangleVertices[5] = 0.0f;

    fTriangleVertices[6] = x3;
    fTriangleVertices[7] = y3;
    fTriangleVertices[8] = 0.0f;

    //Triangle Color
    fTriangleColor[0] = 0.7f;
    fTriangleColor[1] = 0.8f;
    fTriangleColor[2] = 0.9f;

    fTriangleColor[3] = 0.7f;
    fTriangleColor[4] = 0.8f;
    fTriangleColor[5] = 0.9f;

    fTriangleColor[6] = 0.7f;
    fTriangleColor[7] = 0.8f;
    fTriangleColor[8] = 0.9f;

    // Circle Vertices
    glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(fTriangleVertices),
        fTriangleVertices,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Circle Color
    glBindBuffer(GL_ARRAY_BUFFER, vboColorPlane);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(fTriangleColor),
        fTriangleColor,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

-(void) drawFlag:(GLfloat) fFlagOrange_X1 flagOrange_X2:(GLfloat) fFlagOrange_X2 flagOrange_Y:(GLfloat) fFlagOrange_Y flagWhite_X1:(GLfloat) fFlagWhite_X1 flagWhite_X2:(GLfloat) fFlagWhite_X2 flagWhite_Y:(GLfloat) fFlagWhite_Y flagGreen_X1:(GLfloat) fFlagGreen_X1 flagGreen_X2:(GLfloat) fFlagGreen_X2 flagGreen_Y:(GLfloat) fFlagGreen_Y
{
    // Code
    if (fFlagOrange_X2 == 0 || fFlagWhite_X2 == 0 || fFlagGreen_X2 == 0)
    {
        fFlagOrange_X2 = fFlagWhite_X2 = fFlagGreen_X2 = -0.6f;
    }
    glLineWidth(6.0f);
    glBindVertexArray(vao);
    [self drawLine:fFlagOrange_X1 fy1:fFlagOrange_Y fx2:fFlagOrange_X2 fy2:fFlagOrange_Y fr:1.0f fg:0.6f fb:0.2f];
    glDrawArrays(GL_LINES,
        0,
        iLinePoints);
    [self drawLine:fFlagWhite_X1 fy1:fFlagWhite_Y fx2:fFlagWhite_X2 fy2:fFlagWhite_Y fr:1.0f fg:1.0f fb:1.0f];
    glDrawArrays(GL_LINES,
        0,
        iLinePoints);
    [self drawLine:fFlagGreen_X1 fy1:fFlagGreen_Y fx2:fFlagGreen_X2 fy2:fFlagGreen_Y fr:0.07f fg:0.53f fb:0.02f];
    glDrawArrays(GL_LINES,
        0,
        iLinePoints);
    glBindVertexArray(0);
}

-(void)drawLine:(float) x1 fy1:(float) y1 fx2:(float) x2 fy2:(float) y2 fr:(float) r fg:(float) g fb:(float) b
{
    // Variable declaration
    GLfloat fLineVertices[2 * iLinePoints];
    GLfloat fLineColor[3 * iLinePoints];

    // Vertices
    fLineVertices[0] = x1;
    fLineVertices[1] = y1;
    fLineVertices[2] = 0.0f;

    fLineVertices[3] = x2;
    fLineVertices[4] = y2;
    fLineVertices[5] = 0.0f;

    // Color
    fLineColor[0] = r;
    fLineColor[1] = g;
    fLineColor[2] = b;

    fLineColor[3] = r;
    fLineColor[4] = g;
    fLineColor[5] = b;

    // Line Vertices
    glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(fLineVertices),
        fLineVertices,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Circle Color
    glBindBuffer(GL_ARRAY_BUFFER, vboColorPlane);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(fLineColor),
        fLineColor,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

- (void) update
{
    // Code
    if (gfLIX <= -0.4f)
    {
        gfLIX += 0.001f;
    }
    else
    {
        if (gfAX >= 0.4f)
        {
            gfAX -= 0.001f;
        }
        if (gfAXR >= 2.4f)
        {
            gfAXR -= 0.001f;
        }
        else
        {
            if (gfNY >= 0.0f)
            {
                gfNY -= 0.001f;
            }
            else
            {
                if (gfRIY <= 0.0f)
                {
                    gfRIY += 0.001f;
                }
                else
                {
                    if (gfORC <= 1.0f)
                    {
                        gfORC += 0.001f;
                    }
                    if (gfOGC <= 0.6f)
                    {
                        gfOGC += 0.001f;
                    }
                    if (gfOBC <= 0.2f)
                    {
                        gfOBC += 0.001f;
                    }
                    if (gfGRC <= 0.07f)
                    {
                        gfGRC += 0.001f;
                    }
                    if (gfGGC <= 0.53f)
                    {
                        gfGGC += 0.001f;
                    }
                    if (gfGBC <= 0.02f)
                    {
                        gfGBC += 0.001f;
                    }
                    else
                    {
                        if ((gfORC >= 1.0f) && (gfOGC >= 0.6f) && (gfOBC >= 0.2f) && (gfGRC >= 0.07f) && (gfGGC >= 0.53f) && (gfGBC >= 0.02f))
                        {
                            gbAnimatePlane = true;
                            [self updatePlane];
                        }
                    }
                }
            }
        }
    }
}

-(void) updatePlane
{
    // Code:
    if (gfUpperLeftAngle_X <= 0.0f)
    {
        gfUpperLeftAngle_X += 0.001f;
    }
    if (gfUpperLeftAngle_Y >= 0.0f)
    {
        gfUpperLeftAngle_Y -= 0.001f;
    }
    if (gfMiddlePlaneTranslate_X <= 7.5f)
    {
        gfMiddlePlaneTranslate_X += 0.00227f;
        gfFlagOrange_X1 -= 0.001f;
        gfFlagWhite_X1 -= 0.001f;
        gfFlagGreen_X1 -= 0.001f;
    }
    if (gfLowerLeftAngle_X <= 0.0f)
    {
        gfLowerLeftAngle_X += 0.001f;
    }
    if (gfLowerLeftAngle_Y <= 0.0f)
    {
        gfLowerLeftAngle_Y += 0.001f;
    }
    if (gfMiddlePlaneTranslate_X >= 7.5f)
    {
        flag_offset_X1 += 0.01;
        gfFlagOrange_X1 += 0.001f;
        gfFlagWhite_X1 += 0.001f;
        gfFlagGreen_X1 += 0.001f;
    }
}

-(BOOL)acceptsFirstResponder
{
	/* code */
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	/* code */
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repainting occures automatically
			break;
        default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	/* code */
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void) dealloc
{
	/* code */
	if (vboI)
	{
		glDeleteBuffers(1, &vboI);
		vboI = 0;
	}
	if (vaoI)
	{
		glDeleteBuffers(1, &vaoI);
		vaoI = 0;
	}
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	CVDisplayLinkStop(displaylink);
	CVDisplayLinkRelease(displaylink);

	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displaylink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagOut, void *pDisplayLingContext)
{
	/* code */
	CVReturn result = [(GLView *)pDisplayLingContext getFrameForTime:pOutputTime];
	return(result);
}

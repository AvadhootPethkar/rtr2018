/* headers */
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags , CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;

// interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char *argv[])
{
	/* code */
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate :[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];
	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	/* code */
	/* log file */
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath, "w");
	if(gpFile == NULL)
	{
		printf("Can Not Create Log File. \nExitting...\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program Is Started Successfully\n");

	/* window */
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	/* create simple window */
	window = [[NSWindow alloc]initWithContentRect:win_rect 
		styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
		backing:NSBackingStoreBuffered defer:NO];
	[window setTitle:@"macOS window"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	/* code */
	fprintf(gpFile, "Program Is Trminated Successfully\n");

	if (gpFile)
	{
		/* code */
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	/* code */
	[NSApp terminate:self];
}

-(void)dealloc
{
	/* code */
	[glView release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displaylink;

	// Shader variables
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;
    
    GLuint gNumVertices;
    GLuint gNumElements;

    GLuint vao_Sphare;
    GLuint vbo_position_sphere;
    GLuint vbo_normal_sphere;
    GLuint vbo_element_sphere;

    // Uniforms
    GLuint modelUniform;
	GLuint viewUniform;
	GLuint projectionUniform;
	GLuint lKeyPressedUniform;
	GLuint laUniformRed;
	GLuint ldUniformRed;
	GLuint lsUniformRed;
	GLuint lightPositionUniformRed;

	GLuint laUniformBlue;
	GLuint ldUniformBlue;
	GLuint lsUniformBlue;
	GLuint lightPositionUniformBlue;

	GLuint kaUniform;
	GLuint kdUniform;
	GLuint ksUniform;
	GLuint materialShinynessUniform;

    mat4 perspectiveProjectionMatrix;

    // Animating variables
    GLfloat anglePyramid;

    GLfloat lightAmbientZero[4];
    GLfloat lightDiffuseZero[4];
    GLfloat lightSpecularZero[4];
    GLfloat lightPositionZero[4];

    GLfloat lightAmbientTwo[4];
    GLfloat lightDiffuseTwo[4];
    GLfloat lightSpecularTwo[4];
    GLfloat lightPositionTwo[4];

    GLfloat materialAmbient[4];
    GLfloat materialDiffuse[4];
    GLfloat materialSpecular[4];
    GLfloat materialShininess;

    // Light Variables
    bool isLKeyPressed;
    bool gbLight;
    
    bool isAKeyPressed;
}

-(id)initWithFrame:(NSRect)frame
{
	/* code */
	self = [super initWithFrame:frame];

	if (self)
	{
		/* code */
		[[self window]setContentView:self];

        // Light Variables
        isLKeyPressed = true;
        gbLight = false;
        anglePyramid = 0.0f;
        isAKeyPressed = true;

        lightAmbientZero[0] = 0.0f;
        lightAmbientZero[1] = 0.0f;
        lightAmbientZero[2] = 0.0f;
        lightAmbientZero[3] = 1.0f;
        
        lightDiffuseZero[0] = 1.0f;
        lightDiffuseZero[1] = 0.0f;
        lightDiffuseZero[2] = 0.0f;
        lightDiffuseZero[3] = 1.0f;
        
        lightSpecularZero[0] = 1.0f;
        lightSpecularZero[1] = 0.0f;
        lightSpecularZero[2] = 0.0f;
        lightSpecularZero[3] = 1.0f;
        
        lightPositionZero[0] = -2.0f;
        lightPositionZero[1] = 0.0f;
        lightPositionZero[2] = 0.0f;
        lightPositionZero[3] = 1.0f;

        lightAmbientTwo[0] = 0.0f;
        lightAmbientTwo[1] = 0.0f;
        lightAmbientTwo[2] = 0.0f;
        lightAmbientTwo[3] = 1.0f;
        
        lightDiffuseTwo[0] = 0.0f;
        lightDiffuseTwo[1] = 0.0f;
        lightDiffuseTwo[2] = 1.0f;
        lightDiffuseTwo[3] = 1.0f;
        
        lightSpecularTwo[0] = 0.0f;
        lightSpecularTwo[1] = 0.0f;
        lightSpecularTwo[2] = 1.0f;
        lightSpecularTwo[3] = 1.0f;
        
        lightPositionTwo[0] = 2.0f;
        lightPositionTwo[1] = 0.0f;
        lightPositionTwo[2] = 0.0f;
        lightPositionTwo[3] = 1.0f;

        materialAmbient[0] = 0.0f;
        materialAmbient[1] = 0.0f;
        materialAmbient[2] = 0.0f;
        materialAmbient[3] = 1.0f;
        
        materialDiffuse[0] = 1.0f;
        materialDiffuse[1] = 1.0f;
        materialDiffuse[2] = 1.0f;
        materialDiffuse[3] = 1.0f;
        
        materialSpecular[0] = 1.0f;
        materialSpecular[1] = 1.0f;
        materialSpecular[2] = 1.0f;
        materialSpecular[3] = 1.0f;
        
        materialShininess = 128.0f;
        NSOpenGLPixelFormatAttribute attrs[] =
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFAAccelerated,
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		if (pixelFormat == nil)
		{
			/* code */
			fprintf(gpFile, "No valid OpenGL Pixel Format Is Available.Exitting...");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext];
	}
	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	/* code */
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return(kCVReturnSuccess);
}

- (void)prepareOpenGL
{
	/* code */
	// OpenGL info
	[super prepareOpenGL];
	fprintf(gpFile, "OpenGL Version		:	%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version 		:	%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    // VERTEX SHADER
	// Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_la_red;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_ls_red;" \
		"uniform vec4 u_light_position_red;" \
		"uniform vec3 u_la_green;" \
		"uniform vec3 u_ld_green;" \
		"uniform vec3 u_ls_green;" \
		"uniform vec4 u_light_position_green;" \
		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_ls_blue;" \
		"uniform vec4 u_light_position_blue;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_shininess;"	\
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{" \
		"		vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"	\
		"		vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal );" \
		"		vec3 viewer_vector = normalize(-eye_coordinates.xyz);" \

		"		vec3 light_direction_red = normalize(vec3(u_light_position_red) - eye_coordinates.xyz );" \
		"		vec3 reflection_vector_red = reflect(-light_direction_red, transformed_normals);" \
		"		float tn_dot_ld_red = max(dot(transformed_normals, light_direction_red), 0.0);" \
		"		vec3 ambient_red = u_la_red * u_ka;" \
		"		vec3 diffuse_red =u_ld_red * u_kd * tn_dot_ld_red;" \
		"		vec3 specular_red = u_ls_red * u_ks * pow(max(dot(reflection_vector_red, viewer_vector), 0.0), u_shininess);" \

		"		vec3 light_direction_green = normalize(vec3(u_light_position_green) - eye_coordinates.xyz );" \
		"		vec3 reflection_vector_green = reflect(-light_direction_green, transformed_normals);" \
		"		float tn_dot_ld_green = max(dot(transformed_normals, light_direction_green), 0.0);" \
		"		vec3 ambient_green = u_la_green * u_ka;" \
		"		vec3 diffuse_green =u_ld_green * u_kd * tn_dot_ld_green;" \
		"		vec3 specular_green = u_ls_green * u_ks * pow(max(dot(reflection_vector_green, viewer_vector), 0.0), u_shininess);" \

		"		vec3 light_direction_blue = normalize(vec3(u_light_position_blue) - eye_coordinates.xyz );" \
		"		vec3 reflection_vector_blue = reflect(-light_direction_blue, transformed_normals);" \
		"		float tn_dot_ld_blue = max(dot(transformed_normals, light_direction_blue), 0.0);" \
		"		vec3 ambient_blue = u_la_blue * u_ka;" \
		"		vec3 diffuse_blue =u_ld_blue * u_kd * tn_dot_ld_blue;" \
		"		vec3 specular_blue = u_ls_blue * u_ks * pow(max(dot(reflection_vector_blue, viewer_vector), 0.0), u_shininess);" \

		"		phong_ads_light = ambient_red + diffuse_red + specular_red;" \
		"		phong_ads_light = phong_ads_light + ambient_green + diffuse_green + specular_green;" \
		"		phong_ads_light = phong_ads_light + ambient_blue + diffuse_blue + specular_blue;" \
		"	}" \
		"	else" \
		"	{" \
		"		phong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"	}" \
		"	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObject);

	// Error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER
	// Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"in vec3 phong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"	FragColor = vec4(phong_ads_light, 1.0);"	\
		"}";

	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
    modelUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	viewUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");

	laUniformRed = glGetUniformLocation(gShaderProgramObject, "u_la_red");
	ldUniformRed = glGetUniformLocation(gShaderProgramObject, "u_ld_red");
	lsUniformRed = glGetUniformLocation(gShaderProgramObject, "u_ls_red");
	lightPositionUniformRed = glGetUniformLocation(gShaderProgramObject, "u_light_position_red");

	laUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_la_blue");
	ldUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_ld_blue");
	lsUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_ls_blue");
	lightPositionUniformBlue = glGetUniformLocation(gShaderProgramObject, "u_light_position_blue");

	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");

	materialShinynessUniform = glGetUniformLocation(gShaderProgramObject, "u_shininess");

    // Vertices, Colors, Shader attributes, vbo, vao initializations:
    const GLfloat pyramideNormals[] =
	{
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,

		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,

		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,

		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,
	};

	const GLfloat pyramidVertices[] =
	{
		0.0f, 1.0f, 0.0f,		// left-bottom
		-1.0f, -1.0f, 1.0f,		// left-top
		1.0f, -1.0f, 1.0f,		// right-top

		0.0f, 1.0f, 0.0f,		// left-bottom
		1.0f, -1.0f, 1.0f,		// left-top
		1.0f, -1.0f, -1.0f,		// right-top

		0.0f, 1.0f, 0.0f,		// left-bottom
		-1.0f, -1.0f, -1.0f,		// left-top
		-1.0f, -1.0f, 1.0f,		// right-top

		0.0f, 1.0f, 0.0f,		// left-bottom
		1.0f, -1.0f, -1.0f,		// left-top
		-1.0f, -1.0f, -1.0f,		// right-top
	};

	// Create vao_square
	glGenVertexArrays(1, &vao_Sphare);
	glBindVertexArray(vao_Sphare);

	glGenBuffers(1, &vbo_position_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);

	glBufferData(GL_ARRAY_BUFFER,
		sizeof(pyramidVertices),
		pyramidVertices,
		GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glBindVertexArray(0);

	glGenBuffers(1, &vbo_normal_sphere);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);
	glBufferData(GL_ARRAY_BUFFER,
		sizeof(pyramideNormals),
		pyramideNormals,
		GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	perspectiveProjectionMatrix = mat4::identity();

	CVDisplayLinkCreateWithActiveCGDisplays(&displaylink);
	CVDisplayLinkSetOutputCallback(displaylink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displaylink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displaylink);
}

- (void)reshape
{
	/* code */
	[super reshape];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	if (height == 0)
		height = 1;

	glViewport(0, 0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
	/* code */
	[self drawView];
}

- (void)drawView
{
	/* code */
	[[self openGLContext]makeCurrentContext];
	
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
	
    // SQUARE
	// Declaration of matrices
	mat4 translationMatrix;
	mat4 rotationMatrix;
	mat4 modelMatrix;
	mat4 viewMatrix;
	mat4 modelViewProjectionMatrix;

	// Initialize above matrices to identity
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	modelMatrix = mat4::identity();
	viewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	translationMatrix = translate(0.0f,
		0.0f,
		-6.0f);

	rotationMatrix = rotate(0.0f, anglePyramid, 0.0f);

	// Do necessary Matrix Multiplication
	modelMatrix *= translationMatrix;
	modelMatrix *= rotationMatrix;

	// This was internally donw by gluOrtho2d() in FFP

	// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(modelUniform,
		1,
		GL_FALSE,
		modelMatrix);

	glUniformMatrix4fv(viewUniform,
		1,
		GL_FALSE,
		viewMatrix);

	glUniformMatrix4fv(projectionUniform,
		1,
		GL_FALSE,
		perspectiveProjectionMatrix);

	glUniform3fv(laUniformRed, 1, lightAmbientZero);
	glUniform3fv(ldUniformRed, 1, lightDiffuseZero);
	glUniform3fv(lsUniformRed, 1, lightSpecularZero);
	glUniform4fv(lightPositionUniformRed, 1, (GLfloat*)lightPositionZero);

	glUniform3fv(laUniformBlue, 1, lightAmbientTwo);
	glUniform3fv(ldUniformBlue, 1, lightDiffuseTwo);
	glUniform3fv(lsUniformBlue, 1, lightSpecularTwo);
	glUniform4fv(lightPositionUniformBlue, 1, (GLfloat*)lightPositionTwo);

	glUniform3fv(kaUniform, 1, materialAmbient);
	glUniform3fv(kdUniform, 1, materialDiffuse);
	glUniform3fv(ksUniform, 1, materialSpecular);
	glUniform1f(materialShinynessUniform, materialShininess);

	if (gbLight == true)
	{
		glUniform1i(lKeyPressedUniform, 1);
	}
	else
	{
		glUniform1i(lKeyPressedUniform, 0);
	}

	// Bind with vao
	glBindVertexArray(vao_Sphare);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	glDrawArrays(GL_TRIANGLE_FAN,
		0,
		12);

	// Unbind vao
	glBindVertexArray(0);

	[self update];

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void) update
{
	// Code:
	if (isAKeyPressed)
	{
		anglePyramid += 0.1f;
		if (anglePyramid >= 360.0f)
		{
			anglePyramid = 0.0f;
		}
	}
}

-(BOOL)acceptsFirstResponder
{
	/* code */
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	/* code */
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[self release];
			[NSApp terminate:self];
			break;

        case 'l':
		case 'L':
			if (isLKeyPressed == false)
			{
				gbLight = true;
				isLKeyPressed = true;
			}
			else
			{
				gbLight = false;
				isLKeyPressed = false;
			}
			break;
		case 'a':
		case 'A':
			if (isAKeyPressed == true)
			{
				isAKeyPressed = false;
			}
			else
			{
				isAKeyPressed = true;
			}
			break;

        case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repainting occures automatically
			break;
        default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	/* code */
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void) dealloc
{
	/* code */
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	CVDisplayLinkStop(displaylink);
	CVDisplayLinkRelease(displaylink);

	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displaylink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagOut, void *pDisplayLingContext)
{
	/* code */
	CVReturn result = [(GLView *)pDisplayLingContext getFrameForTime:pOutputTime];
	return(result);
}

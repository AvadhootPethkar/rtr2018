/* headers */
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags , CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;

// interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char *argv[])
{
	/* code */
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate :[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];
	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	/* code */
	/* log file */
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath, "w");
	if(gpFile == NULL)
	{
		printf("Can Not Create Log File. \nExitting...\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program Is Started Successfully\n");

	/* window */
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	/* create simple window */
	window = [[NSWindow alloc]initWithContentRect:win_rect 
		styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
		backing:NSBackingStoreBuffered defer:NO];
	[window setTitle:@"macOS window"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	/* code */
	fprintf(gpFile, "Program Is Trminated Successfully\n");

	if (gpFile)
	{
		/* code */
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	/* code */
	[NSApp terminate:self];
}

-(void)dealloc
{
	/* code */
	[glView release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displaylink;

	// Shader variables
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

	// Circle Variables
	GLuint vaoCircle;
	GLuint vboCirclePosition;
	GLuint vboCircleColor;
	GLfloat fRadiusOfInCircle;
	GLfloat foffsetX;
	GLfloat foffsetY;
	GLint iPoints;

	// Triangle Variables
	GLuint vaoTriangle;
	GLuint vboTrianglePosition;
	GLuint vboTriangleColor;
	GLint iTrianglePoints;

	// Line Variables
	GLuint vaoLine;
	GLuint vboLinePosition;
	GLuint vboLineColor;
	GLint iLinePoints;

	// Animating Variables
	GLfloat gfTriangleX;
	GLfloat gfTriangleY;
	GLfloat gfCircleX;
	GLfloat gfCircleY;
	GLfloat gfAngle;
	GLfloat gfLineY;

	GLuint mvpUniform;
	mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame;
{
	/* code */
	self = [super initWithFrame:frame];

	// Circle Variables init
	fRadiusOfInCircle = 1.0f;
	foffsetX = 0.0f;
	foffsetY = 0.0f;
	static int iCirclePoints = 0;
	iPoints = 10000;

	// Triangle Variables init
	iTrianglePoints = 3;

	// Line Variables init 
	iLinePoints = 2;

	// Animating Variables init
	gfTriangleX = 1.0f;
	gfTriangleY = -1.0f;
	gfCircleX = -1.0f;
	gfCircleY = -1.0f;
	gfAngle = 0.0f;
	gfLineY = 1.0f;

	if (self)
	{
		/* code */
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFAAccelerated,
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		if (pixelFormat == nil)
		{
			/* code */
			fprintf(gpFile, "No valid OpenGL Pixel Format Is Available.Exitting...");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext];
	}
	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	/* code */
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return(kCVReturnSuccess);
}

- (void)prepareOpenGL
{
	// Variable declaration
	// Dethly Hollows Variables
	GLfloat coX1 = 0.0f;
	GLfloat coY1 = 0.5f;
	GLfloat coX2 = -0.5f;
	GLfloat coY2 = -0.5f;
	GLfloat coX3 = 0.5f;
	GLfloat coY3 = -0.5f;
	GLfloat fdistAB = 0.0f;
	GLfloat fdistBC = 0.0f;
	GLfloat fdistAC = 0.0f;
	GLfloat fPerimeter = 0.0f;
	GLfloat fSemiPerimeter = 0.0f;
	GLfloat fAreaOfTriangle = 0.0f;
	GLfloat fTriangleHeight = 0.0f;

	/* code */
	// OpenGL info
	fprintf(gpFile, "OpenGL Version		:	%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version 		:	%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

	// VERTEX SHADER
	// Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}";

	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObject);

	// Error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER
	// Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = out_color;" \
		"}";

	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");

	// Distance Calculatin between triangle lines
    fdistAB = [self getDistance:coX1 fx2:coX2 fy1:coY1 fy2:coY2];
	//fdistAB = getDistance(coX1, coX2, coY1, coY2);
    
    fdistAB = [self getDistance:coX2 fx2:coX3 fy1:coY2 fy2:coY3];
	//fdistBC = getDistance(coX2, coX3, coY2, coY3);
    
    fdistAB = [self getDistance:coX1 fx2:coX3 fy1:coY1 fy2:coY3];
	//fdistAC = getDistance(coX1, coX3, coY1, coY3);

	fPerimeter = fdistAB + fdistBC + fdistAC;
	fSemiPerimeter = fPerimeter / 2;

//	fRadiusOfInCircle = sqrtf((fSemiPerimeter - fdistAB) * (fSemiPerimeter - fdistBC) * (fSemiPerimeter - fdistAC) / fSemiPerimeter);

  //  foffsetX = getOffset(coX1, coX2, coX3, fdistAB, fdistBC, fdistAC, fPerimeter);
    foffsetX = [self getOffset:coX1 fcoB:coX2 fcoC:coX3 fdistAB:fdistAB fdistBC:fdistBC fdistAC:fdistAC fperimeter:fPerimeter];
  //  foffsetY = getOffset(coY1, coY2, coY3, fdistAB, fdistBC, fdistAC, fPerimeter);
    foffsetY = [self getOffset:coY1 fcoB:coY2 fcoC:coY3 fdistAB:fdistAB fdistBC:fdistBC fdistAC:fdistAC fperimeter:fPerimeter];

    const GLfloat triangleVertices[] =
	{
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f
	};

	// Circle
	// Create vao
	glGenVertexArrays(1, &vaoCircle);
	glBindVertexArray(vaoCircle);
	glGenBuffers(1, &vboCirclePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboCirclePosition);
	glBufferData(GL_ARRAY_BUFFER,
		6 * sizeof(GLfloat),
		NULL,
		GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Color
	glGenBuffers(1, &vboCircleColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboCircleColor);
	glBufferData(GL_ARRAY_BUFFER,
		6 * sizeof(GLfloat),
		NULL,
		GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	// Triangle
	// Create vao
	glGenVertexArrays(1, &vaoTriangle);
	glBindVertexArray(vaoTriangle);
	glGenBuffers(1, &vboTrianglePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboTrianglePosition);
	glBufferData(GL_ARRAY_BUFFER,
		4 * 2 * sizeof(GLfloat),
		NULL,
		GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Color
	glGenBuffers(1, &vboTriangleColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboTriangleColor);
	glBufferData(GL_ARRAY_BUFFER,
		4 * 2 * sizeof(GLfloat),
		NULL,
		GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	// Line
	// Create vao
	glGenVertexArrays(1, &vaoLine);
	glBindVertexArray(vaoLine);
	glGenBuffers(1, &vboLinePosition);
	glBindBuffer(GL_ARRAY_BUFFER, vboLinePosition);
	glBufferData(GL_ARRAY_BUFFER,
		6 * sizeof(GLfloat),
		NULL,
		GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Color
	glGenBuffers(1, &vboLineColor);
	glBindBuffer(GL_ARRAY_BUFFER, vboLineColor);
	glBufferData(GL_ARRAY_BUFFER,
		6 * sizeof(GLfloat),
		NULL,
		GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
		3, // No Of Co-ordinates
		GL_FLOAT, // Type Of Co-ordinates 
		GL_FALSE, // 
		0,
		NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

	perspectiveProjectionMatrix = mat4::identity();

	CVDisplayLinkCreateWithActiveCGDisplays(&displaylink);
	CVDisplayLinkSetOutputCallback(displaylink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displaylink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displaylink);
}

-(GLfloat) getDistance:(GLfloat) x1 fx2:(GLfloat) x2 fy1:(GLfloat) y1 fy2:(GLfloat) y2 {
    GLfloat xSquare = powf((x2 - x1), 2.0f);
    GLfloat ySquare = powf((y2 - y1), 2.0f);
    return sqrtf((xSquare + ySquare));
}

-(GLfloat) getOffset:(GLfloat) coA fcoB:(GLfloat) coB fcoC:(GLfloat) coC fdistAB:(GLfloat) distAB fdistBC:(GLfloat) distBC fdistAC:(GLfloat) distAC fperimeter:(GLfloat) perimeter {
    GLfloat value = (coA * distBC) + (coB * distAC) + (coC * distAB);
    return(value / perimeter);
}

-(GLvoid) drawLine:(GLfloat) fCoordinate {
    // function declaration

    // Variable declaration
    GLfloat fLineVertices[3 * iLinePoints];
    GLfloat fLineColor[3 * iLinePoints];

    //Code
    // Line Vertices
    fLineVertices[0] = 0.0f;
    fLineVertices[1] = fCoordinate;
    fLineVertices[2] = 0.0f;

    fLineVertices[3] = 0.0f;
    fLineVertices[4] = -fCoordinate;
    fLineVertices[5] = 0.0f;

    // Line Color
    fLineColor[0] = 1.0f;
    fLineColor[1] = 0.0f;
    fLineColor[2] = 0.0f;

    fLineColor[3] = 1.0f;
    fLineColor[4] = 0.0f;
    fLineColor[5] = 0.0f;

    // Line Vertices
    glBindBuffer(GL_ARRAY_BUFFER, vboLinePosition);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(fLineVertices),
        fLineVertices,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Circle Color
    glBindBuffer(GL_ARRAY_BUFFER, vboLineColor);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(fLineColor),
        fLineColor,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

- (void)reshape
{
	/* code */
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	if (height == 0)
		height = 1;

	glViewport(0, 0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
	/* code */
	[self drawView];
}

- (void)drawView
{
	/* code */
	[[self openGLContext]makeCurrentContext];
	
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);

	// Declaration of matrices
	mat4 translationMatrix;
	mat4 rotationMatrix;
	mat4 modelViewMatrix;
	mat4 modelViewProjectionMatrix;

	// Triangle
	// Initialize above matrices to identity
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	translationMatrix = translate(gfTriangleX,
		gfTriangleY,
		-3.0f);

	rotationMatrix = rotate(gfAngle, 0.0f, 1.0f, 0.0f);

	// Do necessary Matrix Multiplication
	modelViewMatrix = translationMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

	// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoTriangle);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	// Draw Triangle
	glLineWidth(3.0f);
	[self drawTriangle:0.5f];
	glDrawArrays(GL_LINE_LOOP,
		0,
		iTrianglePoints);
	//drawDethlyHollows();

	// Unbind vao
	glBindVertexArray(0);

	// Circle
	// Initialize above matrices to identity
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	translationMatrix = translate(gfCircleX-0.5f,
		gfCircleY-0.2f,
		-10.3f);

	rotationMatrix = rotate(gfAngle, 0.0f, 1.0f, 0.0f);

	// Do necessary Matrix Multiplication
	modelViewMatrix = translationMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

	// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoCircle);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	// Draw Circle
	glPointSize(3.0f);
    [self drawCircleWithRadius:fRadiusOfInCircle offsetX:foffsetX offsetY:foffsetY];
	glDrawArrays(GL_POINTS,
		0,
		iPoints);

	// Unbind vao
	glBindVertexArray(0);

	// Line
	// Initialize above matrices to identity
	translationMatrix = mat4::identity();
	rotationMatrix = mat4::identity();
	modelViewMatrix = mat4::identity();
	modelViewProjectionMatrix = mat4::identity();

	// Do necessary transformations
	translationMatrix = translate(0.0f,
		gfLineY,
		-3.0f);

	// Do necessary Matrix Multiplication
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	// This was internally donw by gluOrtho2d() in FFP

	// Send Necessary matrix to shader in respective uniform
	glUniformMatrix4fv(mvpUniform,
		1,
		GL_FALSE,
		modelViewProjectionMatrix);

	// Bind with vao
	glBindVertexArray(vaoLine);

	// Similarly bind With Textures If Any


	// Draw The Necessary Senn
	// Draw Line
	glLineWidth(3.0f);
	[self drawLine:0.5f];
	glDrawArrays(GL_LINES,
		0,
		iLinePoints);

	// Unbind vao
	glBindVertexArray(0);

	// Unuse Program
	glUseProgram(0);

    [self update];

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(GLvoid) drawTriangle:(GLfloat) fCoordinate {
    // function declaration

    // Variable declaration
    GLfloat fTriangleVertices[3 * iTrianglePoints];
    GLfloat fTriangleColor[3 * iTrianglePoints];

    //Code
    //Triangle Vertices
    fTriangleVertices[0] = 0.0f;
    fTriangleVertices[1] = fCoordinate;
    fTriangleVertices[2] = 0.0f;

    fTriangleVertices[3] = -fCoordinate;
    fTriangleVertices[4] = -fCoordinate;
    fTriangleVertices[5] = 0.0f;

    fTriangleVertices[6] = fCoordinate;
    fTriangleVertices[7] = -fCoordinate;
    fTriangleVertices[8] = 0.0f;

    //Triangle Color
    fTriangleColor[0] = 0.0f;
    fTriangleColor[1] = fCoordinate;
    fTriangleColor[2] = 0.0f;

    fTriangleColor[3] = 0.0f;
    fTriangleColor[4] = fCoordinate;
    fTriangleColor[5] = 0.0f;

    fTriangleColor[6] = 0.0f;
    fTriangleColor[7] = fCoordinate;
    fTriangleColor[8] = 0.0f;

    // Circle Vertices
    glBindBuffer(GL_ARRAY_BUFFER, vboTrianglePosition);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(fTriangleVertices),
        fTriangleVertices,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Circle Color
    glBindBuffer(GL_ARRAY_BUFFER, vboTriangleColor);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(fTriangleColor),
        fTriangleColor,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

- (void) drawCircleWithRadius:(GLfloat) fRadius offsetX:(GLfloat) fOffsetX offsetY:(GLfloat) fOffsetY {
    // function declaration
    // Dethly halls

    // Variable declaration
    GLdouble dAngle = 0.0;
    GLfloat fCircleVertices[3 * iPoints];
    GLfloat fCircleColor[3 * iPoints];

    // Code:
    for (GLint i = 0; i < iPoints; i += 3)
    {
        dAngle = 2 * M_PI * i / iPoints;
        fCircleVertices[i++] = fRadius * cos(dAngle) + fOffsetX;
        fCircleVertices[i++] = fRadius * sin(dAngle) + fOffsetY;
        fCircleVertices[i++] = 0.0f;
    }
    // Circle Vertices
    glBindBuffer(GL_ARRAY_BUFFER, vboCirclePosition);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(fCircleVertices),
        fCircleVertices,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    for (GLint i = 0; i < iPoints; i += 3)
    {
        fCircleColor[i++] = 0.0f;
        fCircleColor[i++] = 1.0f;
        fCircleColor[i++] = 0.0f;
    }
    // Circle Color
    glBindBuffer(GL_ARRAY_BUFFER, vboCircleColor);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(fCircleColor),
        fCircleColor,
        GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

- (void) update 
{   
    // Code:
    if (gfTriangleX >= 0.0f)
    {
        gfTriangleX -= 0.001f;
        if (gfAngle <= 360.0f)
        {
            gfAngle += 0.1f;
        }
        else {
            gfAngle = 0.0f;
        }
    }
    else {
        gfAngle = 0.0f;
    }
    if (gfTriangleY <= 0.0f)
    {
        gfTriangleY += 0.001f;
        if (gfAngle <= 360.0f)
        {
            gfAngle += 0.1f;
        }
        else {
            gfAngle = 0.0f;
        }
    }
    if (gfCircleX <= 0.0f)
    {
        gfCircleX += 0.001f;
    }
    if (gfCircleY <= 0.0f)
    {
        gfCircleY += 0.001f;
    }
    if (gfLineY >= 0.0f)
    {
        gfLineY -= 0.001f;
    }
}

-(BOOL)acceptsFirstResponder
{
	/* code */
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	/* code */
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repainting occures automatically
			break;
        default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	/* code */
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void) dealloc
{
	/* code */
	if (vboTrianglePosition)
	{
		glDeleteBuffers(1, &vboTrianglePosition);
		vboTrianglePosition = 0;
	}
	if (vaoTriangle)
	{
		glDeleteBuffers(1, &vaoTriangle);
		vaoTriangle = 0;
	}
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	CVDisplayLinkStop(displaylink);
	CVDisplayLinkRelease(displaylink);

	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displaylink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagOut, void *pDisplayLingContext)
{
	/* code */
	CVReturn result = [(GLView *)pDisplayLingContext getFrameForTime:pOutputTime];
	return(result);
}

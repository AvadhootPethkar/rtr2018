/* headers */
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
#import "Sphere.h"

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags , CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;

// interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char *argv[])
{
	/* code */
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate :[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];
	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	/* code */
	/* log file */
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath, "w");
	if(gpFile == NULL)
	{
		printf("Can Not Create Log File. \nExitting...\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program Is Started Successfully\n");

	/* window */
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	/* create simple window */
	window = [[NSWindow alloc]initWithContentRect:win_rect 
		styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
		backing:NSBackingStoreBuffered defer:NO];
	[window setTitle:@"macOS window"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	/* code */
	fprintf(gpFile, "Program Is Trminated Successfully\n");

	if (gpFile)
	{
		/* code */
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	/* code */
	[NSApp terminate:self];
}

-(void)dealloc
{
	/* code */
	[glView release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displaylink;

	// Shader variables
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;
    
    // Sphere variables:
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    short sphere_elements[2280];

    GLuint gNumVertices;
    GLuint gNumElements;

    GLuint vao_Sphare;
    GLuint vbo_position_sphere;
    GLuint vbo_normal_sphere;
    GLuint vbo_element_sphere;

    // PerVertex Variables
    GLuint modelUniformPerVertex;
    GLuint viewUniformPerVertex;
    GLuint projectionUniformPerVertex;

    GLuint laUniformPerVertex;
    GLuint ldUniformPerVertex;
    GLuint lsUniformPerVertex;
    GLuint lightPositionUniformPerVertex;

    GLuint kaUniformPerVertex;
    GLuint kdUniformPerVertex;
    GLuint ksUniformPerVertex;
    GLuint materialShinynessUniformPerVertex;
    GLuint lKeyPressedUniformPerVertex;

    // PerVertex Shader variables
    GLuint gVertexShaderObjectPerVertex;
    GLuint gFragmentShaderObjectPerVertex;
    GLuint gShaderProgramObjectPerVertex;

    // PerFragment Variables
    GLuint modelUniformPerFragment;
    GLuint viewUniformPerFragment;
    GLuint projectionUniformPerFragment;

    GLuint laUniformPerFragment;
    GLuint ldUniformPerFragment;
    GLuint lsUniformPerFragment;
    GLuint lightPositionUniformPerFragment;

    GLuint kaUniformPerFragment;
    GLuint kdUniformPerFragment;
    GLuint ksUniformPerFragment;
    GLuint materialShinynessUniformPerFragment;
    GLuint lKeyPressedUniformPerFragment;

    // PerFragment Shader variables
    GLuint gVertexShaderObjectPerFragment;
    GLuint gFragmentShaderObjectPerFragment;
    GLuint gShaderProgramObjectPerFragment;
    bool gbFragmentShaderToggel;

    mat4 perspectiveProjectionMatrix;

    // Animating variables

    // Light Variables
    bool isLKeyPressed;
    bool gbLight;
    GLfloat lightAmbient[4];
    GLfloat lightDiffuse[4];
    GLfloat lightSpecular[4];
    GLfloat lightPosition[4];

    GLfloat materialAmbient[4];
    GLfloat materialDiffuse[4];
    GLfloat materialSpecular[4];
    GLfloat materialShininess;
}

-(id)initWithFrame:(NSRect)frame
{
	/* code */
	self = [super initWithFrame:frame];

	if (self)
	{
		/* code */
		[[self window]setContentView:self];

        // Light Variables
        isLKeyPressed = false;
        gbLight = false;

        lightAmbient[0] = 0.0f;
        lightAmbient[1] = 0.0f;
        lightAmbient[2] = 0.0f;
        lightAmbient[3] = 1.0f;
        
        lightDiffuse[0] = 1.0f;
        lightDiffuse[1] = 1.0f;
        lightDiffuse[2] = 1.0f;
        lightDiffuse[3] = 1.0f;
        
        lightSpecular[0] = 1.0f;
        lightSpecular[1] = 1.0f;
        lightSpecular[2] = 1.0f;
        lightSpecular[3] = 1.0f;
        
        lightPosition[0] = 100.0f;
        lightPosition[1] = 100.0f;
        lightPosition[2] = 100.0f;
        lightPosition[3] = 1.0f;

        materialAmbient[0] = 0.0f;
        materialAmbient[1] = 0.0f;
        materialAmbient[2] = 0.0f;
        materialAmbient[3] = 1.0f;
        
        materialDiffuse[0] = 1.0f;
        materialDiffuse[1] = 1.0f;
        materialDiffuse[2] = 1.0f;
        materialDiffuse[3] = 1.0f;
        
        materialSpecular[0] = 1.0f;
        materialSpecular[1] = 1.0f;
        materialSpecular[2] = 1.0f;
        materialSpecular[3] = 1.0f;
        
        materialShininess = 128.0f;
        gbFragmentShaderToggel = false;

        NSOpenGLPixelFormatAttribute attrs[] =
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFAAccelerated,
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		if (pixelFormat == nil)
		{
			/* code */
			fprintf(gpFile, "No valid OpenGL Pixel Format Is Available.Exitting...");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext];
	}
	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	/* code */
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return(kCVReturnSuccess);
}

- (void)prepareOpenGL
{
    // Shader Error Checking Variables
    GLint iShaderCompileStatus = 0;
    GLint iInfoLength = 0;
    GLchar *szInfoLog = NULL;
    
	/* code */
	// OpenGL info
	[super prepareOpenGL];
	fprintf(gpFile, "OpenGL Version		:	%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version 		:	%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    // VERTEX SHADER PerVertex
    // Define Vertex Shader Object
    gVertexShaderObjectPerVertex = glCreateShader(GL_VERTEX_SHADER);
    // Write Vertex Shader Code
    const GLchar *vertexShaderSourceCodePerVertex =
        "#version 410 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_lKeyPressed;" \
        "uniform vec3 u_la;" \
        "uniform vec3 u_ld;" \
        "uniform vec3 u_ls;" \
        "uniform vec3 u_ka;" \
        "uniform vec3 u_kd;" \
        "uniform vec3 u_ks;" \
        "uniform float u_shininess;"    \
        "uniform vec4 u_light_position;" \
        "out vec3 phong_ads_light;" \
        "void main(void)" \
        "{" \
        "    if(u_lKeyPressed == 1)" \
        "    {" \
        "        vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"    \
        "        vec3 transformed_normals = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal );" \
        "        vec3 light_direction = normalize(vec3(u_light_position) - eye_coordinates.xyz );" \
        "        float tn_dot_ld = max(dot(transformed_normals, light_direction), 0.0);" \
        "        vec3 ambient = u_la * u_ka;" \
        "        vec3 diffuse =u_ld * u_kd * tn_dot_ld;" \
        "        vec3 reflection_vector = reflect(-light_direction, transformed_normals);" \
        "        vec3 viewer_vector = normalize(-eye_coordinates.xyz);" \
        "        vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, viewer_vector), 0.0), u_shininess);" \
        "        phong_ads_light = ambient + diffuse + specular;" \
        "    }" \
        "    else" \
        "    {" \
        "        phong_ads_light = vec3(1.0, 1.0, 1.0);" \
        "    }" \
        "    gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
    // Specify above Source Code To The Vertex Shader Object
    glShaderSource(gVertexShaderObjectPerVertex, 1, (const GLchar **)&vertexShaderSourceCodePerVertex, NULL);
    // Compile the VertexShader
    glCompileShader(gVertexShaderObjectPerVertex);

    // Error checking
    iShaderCompileStatus = 0;
    iInfoLength = 0;
    szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLength);
        if (iInfoLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObjectPerVertex, iInfoLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

    // FRAGMENT SHADER PerVertex
    // Define Fragment Shader Object
    gFragmentShaderObjectPerVertex = glCreateShader(GL_FRAGMENT_SHADER);
    // Write Vertex Shader Code
    const GLchar *fragmentShaderSourceCodePerVertex =
        "#version 410 core" \
        "\n" \
        "in vec3 phong_ads_light;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "    FragColor = vec4(phong_ads_light, 1.0);"    \
        "}";

    // Specify above Source Code To The Fragment Shader Object
    glShaderSource(gFragmentShaderObjectPerVertex, 1, (const GLchar **)&fragmentShaderSourceCodePerVertex, NULL);
    // Compile the FragmentShader
    glCompileShader(gFragmentShaderObjectPerVertex);

    // Error checking
    iShaderCompileStatus = 0;
    iInfoLength = 0;
    szInfoLog = NULL;
    glGetShaderiv(gFragmentShaderObjectPerVertex, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLength);
        if (iInfoLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObjectPerVertex, iInfoLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

    // Create Shader Program Object
    gShaderProgramObjectPerVertex = glCreateProgram();
    // Attach Vertex Shader To Shader Program
    glAttachShader(gShaderProgramObjectPerVertex, gVertexShaderObjectPerVertex);
    // Attach Fragment Shader To Shader Program
    glAttachShader(gShaderProgramObjectPerVertex, gFragmentShaderObjectPerVertex);

    // Prelinking binding to vertex attribute
    glBindAttribLocation(gShaderProgramObjectPerVertex, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObjectPerVertex, AMC_ATTRIBUTE_NORMAL, "vNormal");

    // Link The Shader Program
    glLinkProgram(gShaderProgramObjectPerVertex);

    // Error checking
    GLint iProgramLinkStatus = 0;
    iInfoLength = 0;
    szInfoLog = NULL;
    glGetProgramiv(gShaderProgramObjectPerVertex, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObjectPerVertex, GL_INFO_LOG_LENGTH, &iInfoLength);
        if (iInfoLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObjectPerVertex, iInfoLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

    // Post Linking Retriving UniformLocation
    modelUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_model_matrix");
    viewUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_view_matrix");
    projectionUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_projection_matrix");

    lKeyPressedUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_lKeyPressed");
    lightPositionUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_light_position");

    laUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_la");
    ldUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ld");
    lsUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ls");

    kaUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ka");
    kdUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_kd");
    ksUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_ks");

    materialShinynessUniformPerVertex = glGetUniformLocation(gShaderProgramObjectPerVertex, "u_shininess");

    // VERTEX SHADER PerFragment
    // Define Vertex Shader Object
    gVertexShaderObjectPerFragment = glCreateShader(GL_VERTEX_SHADER);
    // Write Vertex Shader Code
    const GLchar *vertexShaderSourceCodePerFragment =
        "#version 410 core" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform vec4 u_light_position;" \
        "uniform int u_lKeyPressed;" \
        "out vec3 tNorm;" \
        "out vec3 light_direction;" \
        "out vec3 viewer_vector;" \
        "void main(void)" \
        "{" \
        "    if(u_lKeyPressed == 1)" \
        "    {" \
        "        vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"    \
        "        tNorm = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
        "        light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
        "        float tn_dot_ld = max(dot(tNorm, light_direction), 0.0);" \
        "        viewer_vector = vec3(-eye_coordinates.xyz);" \
        "    }" \
        "    gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
    // Specify above Source Code To The Vertex Shader Object
    glShaderSource(gVertexShaderObjectPerFragment, 1, (const GLchar **)&vertexShaderSourceCodePerFragment, NULL);
    // Compile the VertexShader
    glCompileShader(gVertexShaderObjectPerFragment);

    // Error checking
    iShaderCompileStatus = 0;
    iInfoLength = 0;
    szInfoLog = NULL;
    glGetShaderiv(gVertexShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLength);
        if (iInfoLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gVertexShaderObjectPerFragment, iInfoLength, &written, szInfoLog);
                fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

    // FRAGMENT SHADER PerFragment
    // Define Fragment Shader Object
    gFragmentShaderObjectPerFragment = glCreateShader(GL_FRAGMENT_SHADER);
    // Write Vertex Shader Code
    const GLchar *fragmentShaderSourceCodePerFragment =
        "#version 410 core" \
        "\n" \
        "in vec3 tNorm;" \
        "in vec3 light_direction;" \
        "in vec3 viewer_vector;" \
        "uniform vec3 u_la;" \
        "uniform vec3 u_ld;" \
        "uniform vec3 u_ls;" \
        "uniform vec3 u_ka;" \
        "uniform vec3 u_kd;" \
        "uniform vec3 u_ks;" \
        "uniform float u_shininess;"    \
        "uniform int u_lKeyPressed;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "    if(u_lKeyPressed == 1)" \
        "    {" \
        "        vec3 ntNorm = normalize(tNorm);" \
        "        vec3 nlight_direction = normalize(light_direction);" \
        "        vec3 nviewer_vector  = normalize(viewer_vector);" \
        "        vec3 reflection_vector  = reflect(-nlight_direction, ntNorm);" \
        "        float tn_dot_ld = max(dot(ntNorm, nlight_direction), 0.0);" \
        "        vec3 ambient = u_la * u_ka;" \
        "        vec3 diffuse =u_ld * u_kd * tn_dot_ld;" \
        "        vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, nviewer_vector), 0.0), u_shininess);" \
        "        vec3 phong_ads_light = ambient + diffuse + specular;" \
        "        FragColor = vec4(phong_ads_light, 1.0);"    \
        "    }" \
        "    else" \
        "    {" \
        "        FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
        "    }" \
        "}";

    // Specify above Source Code To The Fragment Shader Object
    glShaderSource(gFragmentShaderObjectPerFragment, 1, (const GLchar **)&fragmentShaderSourceCodePerFragment, NULL);
    // Compile the FragmentShader
    glCompileShader(gFragmentShaderObjectPerFragment);

    // Error checking
    iShaderCompileStatus = 0;
    iInfoLength = 0;
    szInfoLog = NULL;
    glGetShaderiv(gFragmentShaderObjectPerFragment, GL_COMPILE_STATUS, &iShaderCompileStatus);
    if (iShaderCompileStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLength);
        if (iInfoLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetShaderInfoLog(gFragmentShaderObjectPerFragment, iInfoLength, &written, szInfoLog);
                fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

    // Create Shader Program Object
    gShaderProgramObjectPerFragment = glCreateProgram();
    // Attach Vertex Shader To Shader Program
    glAttachShader(gShaderProgramObjectPerFragment, gVertexShaderObjectPerFragment);
    // Attach Fragment Shader To Shader Program
    glAttachShader(gShaderProgramObjectPerFragment, gFragmentShaderObjectPerFragment);

    // Prelinking binding to vertex attribute
    glBindAttribLocation(gShaderProgramObjectPerFragment, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObjectPerFragment, AMC_ATTRIBUTE_NORMAL, "vNormal");

    // Link The Shader Program
    glLinkProgram(gShaderProgramObjectPerFragment);

    // Error checking
    iProgramLinkStatus = 0;
    iInfoLength = 0;
    szInfoLog = NULL;
    glGetProgramiv(gShaderProgramObjectPerFragment, GL_LINK_STATUS, &iProgramLinkStatus);
    if (iProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObjectPerFragment, GL_INFO_LOG_LENGTH, &iInfoLength);
        if (iInfoLength > 0)
        {
            szInfoLog = (GLchar *)malloc(iInfoLength);
            if (szInfoLog != NULL)
            {
                GLsizei written;
                glGetProgramInfoLog(gShaderProgramObjectPerFragment, iInfoLength, &written, szInfoLog);
                fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
                free(szInfoLog);
                exit(0);
            }
        }
    }

    // Post Linking Retriving UniformLocation
    modelUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_model_matrix");
    viewUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_view_matrix");
    projectionUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_projection_matrix");

    lKeyPressedUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_lKeyPressed");
    lightPositionUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_light_position");

    laUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_la");
    ldUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ld");
    lsUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ls");

    kaUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ka");
    kdUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_kd");
    ksUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_ks");

    materialShinynessUniformPerFragment = glGetUniformLocation(gShaderProgramObjectPerFragment, "u_shininess");

    Sphere sphere;
    sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    gNumVertices = sphere.getNumberOfSphereVertices();
    gNumElements = sphere.getNumberOfSphereElements();

	// BLOCK FOR SPHERE:
    glGenVertexArrays(1, &vao_Sphare);
    glBindVertexArray(vao_Sphare);

    // A. BUFFER BLOCK FOR VERTICES:
    glGenBuffers(1, &vbo_position_sphere);                    // Buffer to store vertex position
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);        // Find that named object in memory
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);    // Takes data from CPU to GPU

    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    // Release the buffer for vertices:
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // B. BUFFER BLOCK FOR NORMALS:
    glGenBuffers(1, &vbo_normal_sphere);                    // Buffer to store vertex normals
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);        // Find that named object in memory
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);    // Takes data from CPU to GPU

    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

    // C. BUFFER BLOCK FOR ELEMENTS:
    glGenBuffers(1, &vbo_element_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Release the buffer for colors:
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	perspectiveProjectionMatrix = mat4::identity();

	CVDisplayLinkCreateWithActiveCGDisplays(&displaylink);
	CVDisplayLinkSetOutputCallback(displaylink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displaylink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displaylink);
}

- (void)reshape
{
	/* code */
	[super reshape];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	if (height == 0)
		height = 1;

	glViewport(0, 0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
	/* code */
	[self drawView];
}

- (void)drawView
{
	/* code */
	[[self openGLContext]makeCurrentContext];
	
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (gbFragmentShaderToggel)
    {
        glUseProgram(gShaderProgramObjectPerFragment);
    }
    else
    {
        glUseProgram(gShaderProgramObjectPerVertex);
    }

    // SQUARE
    // Declaration of matrices
    mat4 modelMatrix;
    mat4 viewMatrix;
    mat4 modelViewProjectionMatrix;

    // Initialize above matrices to identity
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();
    modelViewProjectionMatrix = mat4::identity();

    // Do necessary transformations
    modelMatrix = translate(0.0f,
        0.0f,
        -2.0f);

    // Do necessary Matrix Multiplication

    // This was internally donw by gluOrtho2d() in FFP

    // Send Necessary matrix to shader in respective uniform
    if (gbFragmentShaderToggel)
    {
        glUniformMatrix4fv(modelUniformPerFragment,
            1,
            GL_FALSE,
            modelMatrix);

        glUniformMatrix4fv(viewUniformPerFragment,
            1,
            GL_FALSE,
            viewMatrix);

        glUniformMatrix4fv(projectionUniformPerFragment,
            1,
            GL_FALSE,
            perspectiveProjectionMatrix);

        glUniform3fv(laUniformPerFragment, 1, lightAmbient);
        glUniform3fv(ldUniformPerFragment, 1, lightDiffuse);
        glUniform3fv(lsUniformPerFragment, 1, lightSpecular);
        glUniform4fv(lightPositionUniformPerFragment, 1, lightPosition);

        glUniform3fv(kaUniformPerFragment, 1, materialAmbient);
        glUniform3fv(kdUniformPerFragment, 1, materialDiffuse);
        glUniform3fv(ksUniformPerFragment, 1, materialSpecular);
        glUniform1f(materialShinynessUniformPerFragment, materialShininess);

        float lightPosition[] = {
            0.0f, 0.0f, 2.0f, 1.0f
        };

        glUniform4fv(lightPositionUniformPerFragment, 1, (GLfloat*)lightPosition);

        if (gbLight == true)
        {
            glUniform1i(lKeyPressedUniformPerFragment, 1);
        }
        else
        {
            glUniform1i(lKeyPressedUniformPerFragment, 0);
        }
    }
    else
    {
        glUniformMatrix4fv(modelUniformPerVertex,
            1,
            GL_FALSE,
            modelMatrix);

        glUniformMatrix4fv(viewUniformPerVertex,
            1,
            GL_FALSE,
            viewMatrix);

        glUniformMatrix4fv(projectionUniformPerVertex,
            1,
            GL_FALSE,
            perspectiveProjectionMatrix);

        glUniform3fv(laUniformPerVertex, 1, lightAmbient);
        glUniform3fv(ldUniformPerVertex, 1, lightDiffuse);
        glUniform3fv(lsUniformPerVertex, 1, lightSpecular);
        glUniform4fv(lightPositionUniformPerVertex, 1, lightPosition);

        glUniform3fv(kaUniformPerVertex, 1, materialAmbient);
        glUniform3fv(kdUniformPerVertex, 1, materialDiffuse);
        glUniform3fv(ksUniformPerVertex, 1, materialSpecular);
        glUniform1f(materialShinynessUniformPerVertex, materialShininess);

        float lightPosition[] = {
            0.0f, 0.0f, 2.0f, 1.0f
        };

        glUniform4fv(lightPositionUniformPerVertex, 1, (GLfloat*)lightPosition);

        if (gbLight == true)
        {
            glUniform1i(lKeyPressedUniformPerVertex, 1);
        }
        else
        {
            glUniform1i(lKeyPressedUniformPerVertex, 0);
        }
    }

    // Bind with vao
    glBindVertexArray(vao_Sphare);

    // Similarly bind With Textures If Any


    // Draw The Necessary Senn
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

    // Unbind vao
    glBindVertexArray(0);

	[self update];

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void) update
{

}

-(BOOL)acceptsFirstResponder
{
	/* code */
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	/* code */
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
            [[self window]toggleFullScreen:self]; // repainting occures automatically
			break;

        case 'l':
        case 'L':
            if (isLKeyPressed == false)
            {
                gbLight = true;
                isLKeyPressed = true;
            }
            else
            {
                gbLight = false;
                isLKeyPressed = false;
            }
            break;

        case 'e':
        case 'E':
            [self release];
            [NSApp terminate:self];
            break;

        case 'f':
        case 'F':
            gbFragmentShaderToggel = true;
            break;

        case 'v':
        case 'V':
            gbFragmentShaderToggel = false;
            break;

        default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	/* code */
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void) dealloc
{
	/* code */
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	CVDisplayLinkStop(displaylink);
	CVDisplayLinkRelease(displaylink);

	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displaylink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagOut, void *pDisplayLingContext)
{
	/* code */
	CVReturn result = [(GLView *)pDisplayLingContext getFrameForTime:pOutputTime];
	return(result);
}


@interface Sphere : NSObject
-(id)initSphere;
-(void) getSphereVertexData:(float[1146]) spherePositionCoords fSphereNormalCoords:(float[1146]) sphereNormalCoords fSphereTexCoords:(float[764]) sphereTexCoords fSphereElements:(unsigned short[2280]) sphereElements;
-(int) getNumberOfSphereVertices;
-(int) getNumberOfSphereElements;

@end


/* headers */
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"
#import "Sphere.h"

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags , CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;

// interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char *argv[])
{
	/* code */
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate :[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];
	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	/* code */
	/* log file */
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath, "w");
	if(gpFile == NULL)
	{
		printf("Can Not Create Log File. \nExitting...\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program Is Started Successfully\n");

	/* window */
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	/* create simple window */
	window = [[NSWindow alloc]initWithContentRect:win_rect 
		styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
		backing:NSBackingStoreBuffered defer:NO];
	[window setTitle:@"macOS window"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	/* code */
	fprintf(gpFile, "Program Is Trminated Successfully\n");

	if (gpFile)
	{
		/* code */
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	/* code */
	[NSApp terminate:self];
}

-(void)dealloc
{
	/* code */
	[glView release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displaylink;

	// Shader variables
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;
    
    // Sphere variables:
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    short sphere_elements[2280];

    GLuint gNumVertices;
    GLuint gNumElements;

    GLuint vao_Sphare;
    GLuint vbo_position_sphere;
    GLuint vbo_normal_sphere;
    GLuint vbo_element_sphere;

    // Uniforms
    GLuint modelUniform;
    GLuint viewUniform;
    GLuint projectionUniform;
    GLuint lKeyPressedUniform;
    GLuint laUniform;
    GLuint ldUniform;
    GLuint lsUniform;
    GLuint kaUniform;
    GLuint kdUniform;
    GLuint ksUniform;
    GLuint lightPositionUniform;
    GLuint materialShinynessUniform;

    mat4 perspectiveProjectionMatrix;

    // Animating variables

    GLfloat lightAmbient[4];
    GLfloat lightDiffuse[4];
    GLfloat lightSpecular[4];
    GLfloat lightPosition[4];

    GLfloat materialAmbient[4];
    GLfloat materialDiffuse[4];
    GLfloat materialSpecular[4];
    GLfloat materialShininess;

    // Light Variables
    bool isLKeyPressed;
    bool gbLight;
}

-(id)initWithFrame:(NSRect)frame
{
	/* code */
	self = [super initWithFrame:frame];

	if (self)
	{
		/* code */
		[[self window]setContentView:self];

        // Light Variables
        isLKeyPressed = true;
        gbLight = false;

        lightAmbient[0] = 0.0f;
        lightAmbient[1] = 0.0f;
        lightAmbient[2] = 0.0f;
        lightAmbient[3] = 1.0f;
        
        lightDiffuse[0] = 1.0f;
        lightDiffuse[1] = 1.0f;
        lightDiffuse[2] = 1.0f;
        lightDiffuse[3] = 1.0f;
        
        lightSpecular[0] = 1.0f;
        lightSpecular[1] = 1.0f;
        lightSpecular[2] = 1.0f;
        lightSpecular[3] = 1.0f;
        
        lightPosition[0] = 100.0f;
        lightPosition[1] = 100.0f;
        lightPosition[2] = 100.0f;
        lightPosition[3] = 1.0f;

        materialAmbient[0] = 0.0f;
        materialAmbient[1] = 0.0f;
        materialAmbient[2] = 0.0f;
        materialAmbient[3] = 1.0f;
        
        materialDiffuse[0] = 1.0f;
        materialDiffuse[1] = 1.0f;
        materialDiffuse[2] = 1.0f;
        materialDiffuse[3] = 1.0f;
        
        materialSpecular[0] = 1.0f;
        materialSpecular[1] = 1.0f;
        materialSpecular[2] = 1.0f;
        materialSpecular[3] = 1.0f;
        
        materialShininess = 128.0f;
        NSOpenGLPixelFormatAttribute attrs[] =
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFAAccelerated,
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		if (pixelFormat == nil)
		{
			/* code */
			fprintf(gpFile, "No valid OpenGL Pixel Format Is Available.Exitting...");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext];
	}
	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	/* code */
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return(kCVReturnSuccess);
}

- (void)prepareOpenGL
{
	/* code */
	// OpenGL info
	[super prepareOpenGL];
	fprintf(gpFile, "OpenGL Version		:	%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version 		:	%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

    // VERTEX SHADER
	// Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform vec4 u_light_position;" \
        "uniform int u_lKeyPressed;" \
        "out vec3 tNorm;" \
        "out vec3 light_direction;" \
        "out vec3 viewer_vector;" \
        "void main(void)" \
        "{" \
        "    if(u_lKeyPressed == 1)" \
        "    {" \
        "        vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"    \
        "        tNorm = mat3(u_view_matrix * u_model_matrix) * vNormal;" \
        "        light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
        "        float tn_dot_ld = max(dot(tNorm, light_direction), 0.0);" \
        "        viewer_vector = vec3(-eye_coordinates.xyz);" \
        "    }" \
        "    gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";

	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObject);

	// Error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER
	// Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"in vec3 tNorm;" \
        "in vec3 light_direction;" \
        "in vec3 viewer_vector;" \
        "uniform vec3 u_la;" \
        "uniform vec3 u_ld;" \
        "uniform vec3 u_ls;" \
        "uniform vec3 u_ka;" \
        "uniform vec3 u_kd;" \
        "uniform vec3 u_ks;" \
        "uniform float u_shininess;"    \
        "uniform int u_lKeyPressed;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "    if(u_lKeyPressed == 1)" \
        "    {" \
        "        vec3 ntNorm = normalize(tNorm);" \
        "        vec3 nlight_direction = normalize(light_direction);" \
        "        vec3 nviewer_vector  = normalize(viewer_vector);" \
        "        vec3 reflection_vector  = reflect(-nlight_direction, ntNorm);" \
        "        float tn_dot_ld = max(dot(ntNorm, nlight_direction), 0.0);" \
        "        vec3 ambient = u_la * u_ka;" \
        "        vec3 diffuse =u_ld * u_kd * tn_dot_ld;" \
        "        vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, nviewer_vector), 0.0), u_shininess);" \
        "        vec3 phong_ads_light = ambient + diffuse + specular;" \
        "        FragColor = vec4(phong_ads_light, 1.0);"    \
        "    }" \
        "    else" \
        "    {" \
        "        FragColor = vec4(1.0, 1.0, 1.0, 1.0);" \
        "    }" \
        "}";

	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_NORMAL, "vNormal");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
    modelUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
    viewUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
    projectionUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

    lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");
    lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");

    laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
    ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
    lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");

    kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
    kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
    ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
    
    materialShinynessUniform = glGetUniformLocation(gShaderProgramObject, "u_shininess");

    // Vertices, Colors, Shader attributes, vbo, vao initializations:
    Sphere sphere;
    sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    gNumVertices = sphere.getNumberOfSphereVertices();
    gNumElements = sphere.getNumberOfSphereElements();

	// BLOCK FOR SPHERE:
    glGenVertexArrays(1, &vao_Sphare);
    glBindVertexArray(vao_Sphare);

    // A. BUFFER BLOCK FOR VERTICES:
    glGenBuffers(1, &vbo_position_sphere);                    // Buffer to store vertex position
    glBindBuffer(GL_ARRAY_BUFFER, vbo_position_sphere);        // Find that named object in memory
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);    // Takes data from CPU to GPU

    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

    // Release the buffer for vertices:
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // B. BUFFER BLOCK FOR NORMALS:
    glGenBuffers(1, &vbo_normal_sphere);                    // Buffer to store vertex normals
    glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_sphere);        // Find that named object in memory
    glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);    // Takes data from CPU to GPU

    glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

    // C. BUFFER BLOCK FOR ELEMENTS:
    glGenBuffers(1, &vbo_element_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    // Release the buffer for colors:
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	perspectiveProjectionMatrix = mat4::identity();

	CVDisplayLinkCreateWithActiveCGDisplays(&displaylink);
	CVDisplayLinkSetOutputCallback(displaylink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displaylink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displaylink);
}

- (void)reshape
{
	/* code */
	[super reshape];
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	if (height == 0)
		height = 1;

	glViewport(0, 0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
	/* code */
	[self drawView];
}

- (void)drawView
{
	/* code */
	[[self openGLContext]makeCurrentContext];
	
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(gShaderProgramObject);
	
    // SPHERE
    // Declaration of matrices
    mat4 translationMatrix;
    mat4 modelMatrix;
    mat4 viewMatrix;

    // Initialize above matrices to identity
    translationMatrix = mat4::identity();
    modelMatrix = mat4::identity();
    viewMatrix = mat4::identity();

    // Do necessary transformations
    translationMatrix = translate(0.0f,
        0.0f,
        -2.0f);

    // Do necessary Matrix Multiplication
    modelMatrix *= translationMatrix;

    // Send Necessary matrix to shader in respective uniform
    glUniformMatrix4fv(modelUniform,
        1,
        GL_FALSE,
        modelMatrix);

    glUniformMatrix4fv(viewUniform,
        1,
        GL_FALSE,
        viewMatrix);

    glUniformMatrix4fv(projectionUniform,
        1,
        GL_FALSE,
        perspectiveProjectionMatrix);

    glUniform3fv(laUniform, 1, lightAmbient);
    glUniform3fv(ldUniform, 1, lightDiffuse);
    glUniform3fv(lsUniform, 1, lightSpecular);
    glUniform4fv(lightPositionUniform, 1, lightPosition);

    glUniform3fv(kaUniform, 1, materialAmbient);
    glUniform3fv(kdUniform, 1, materialDiffuse);
    glUniform3fv(ksUniform, 1, materialSpecular);
    glUniform1f(materialShinynessUniform, materialShininess);

    float lightPosition[] = {
        0.0f, 0.0f, 2.0f, 1.0f
    };

    glUniform4fv(lightPositionUniform, 1, (GLfloat*)lightPosition);

    if (gbLight == true)
    {
        glUniform1i(lKeyPressedUniform, 1);
    }
    else
    {
        glUniform1i(lKeyPressedUniform, 0);
    }

    // Bind with vao
    glBindVertexArray(vao_Sphare);

    // Similarly bind With Textures If Any


    // Draw The Necessary Senn
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_sphere);
    glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

    // Unbind vao
    glBindVertexArray(0);

	[self update];

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void) update
{

}

-(BOOL)acceptsFirstResponder
{
	/* code */
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	/* code */
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[self release];
			[NSApp terminate:self];
			break;

        case 'l':
        case 'L':
            if (isLKeyPressed == false)
            {
                gbLight = true;
                isLKeyPressed = true;
            }
            else
            {
                gbLight = false;
                isLKeyPressed = false;
            }
            break;

        case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repainting occures automatically
			break;
        default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	/* code */
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void) dealloc
{
	/* code */
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	CVDisplayLinkStop(displaylink);
	CVDisplayLinkRelease(displaylink);

	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displaylink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagOut, void *pDisplayLingContext)
{
	/* code */
	CVReturn result = [(GLView *)pDisplayLingContext getFrameForTime:pOutputTime];
	return(result);
}

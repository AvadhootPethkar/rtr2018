/* headers */
#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

#import <QuartzCore/CVDisplayLink.h>

#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

#import "vmath.h"

using namespace vmath;

enum
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,
	AMC_ATTRIBUTE_NORMAL,
	AMC_ATTRIBUTE_TEXCOORD0
};

// 'C' style global function declaration
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp *,CVOptionFlags , CVOptionFlags *, void *);

// global variables
FILE *gpFile = NULL;

// interface declaration
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>
@end

@interface GLView : NSOpenGLView
@end

// Entry point function
int main(int argc, const char *argv[])
{
	/* code */
	NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];
	
	NSApp = [NSApplication sharedApplication];

	[NSApp setDelegate :[[AppDelegate alloc]init]];

	[NSApp run];

	[pPool release];
	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private
	NSWindow *window;
	GLView *glView;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	/* code */
	/* log file */
	NSBundle *mainBundle = [NSBundle mainBundle];
	NSString *appDirName = [mainBundle bundlePath];
	NSString *parentDirPath = [appDirName stringByDeletingLastPathComponent];
	NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/Log.txt",parentDirPath];
	const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
	gpFile = fopen(pszLogFileNameWithPath, "w");
	if(gpFile == NULL)
	{
		printf("Can Not Create Log File. \nExitting...\n");
		[self release];
		[NSApp terminate:self];
	}
	fprintf(gpFile, "Program Is Started Successfully\n");

	/* window */
	NSRect win_rect;
	win_rect = NSMakeRect(0.0, 0.0, 800.0, 600.0);

	/* create simple window */
	window = [[NSWindow alloc]initWithContentRect:win_rect 
		styleMask:NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskMiniaturizable | NSWindowStyleMaskResizable 
		backing:NSBackingStoreBuffered defer:NO];
	[window setTitle:@"macOS window"];
	[window center];

	glView = [[GLView alloc]initWithFrame:win_rect];

	[window setContentView:glView];
	[window setDelegate:self];
	[window makeKeyAndOrderFront:self];
}

- (void)applicationWillTerminate:(NSNotification *)notification
{
	/* code */
	fprintf(gpFile, "Program Is Trminated Successfully\n");

	if (gpFile)
	{
		/* code */
		fclose(gpFile);
		gpFile = NULL;
	}
}

- (void)windowWillClose:(NSNotification *)notification
{
	/* code */
	[NSApp terminate:self];
}

-(void)dealloc
{
	/* code */
	[glView release];

	[window release];

	[super dealloc];
}
@end

@implementation GLView
{
@private
	CVDisplayLinkRef displaylink;

	// Shader variables
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;
	GLuint gShaderProgramObject;

	GLuint vaoVerticalGraph;
	GLuint vboVerticalGraphPosition;
	GLuint vboVerticalGraphColor;
	GLuint vaoHorizontalGraph;
	GLuint vboHorizontalGraphPosition;
	GLuint vboHorizontalGraphColor;

	GLuint mvpUniform;
	mat4 perspectiveProjectionMatrix;
}

-(id)initWithFrame:(NSRect)frame;
{
	/* code */
	self = [super initWithFrame:frame];

	if (self)
	{
		/* code */
		[[self window]setContentView:self];

		NSOpenGLPixelFormatAttribute attrs[] = 
		{
			NSOpenGLPFAOpenGLProfile, NSOpenGLProfileVersion4_1Core,
			NSOpenGLPFAScreenMask, CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
			NSOpenGLPFAAccelerated,
			NSOpenGLPFANoRecovery,
			NSOpenGLPFAColorSize, 24,
			NSOpenGLPFADepthSize, 24,
			NSOpenGLPFAAlphaSize, 8,
			NSOpenGLPFADoubleBuffer,
			0
		};

		NSOpenGLPixelFormat *pixelFormat = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];
		if (pixelFormat == nil)
		{
			/* code */
			fprintf(gpFile, "No valid OpenGL Pixel Format Is Available.Exitting...");
			[self release];
			[NSApp terminate:self];
		}

		NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

		[self setPixelFormat:pixelFormat];

		[self setOpenGLContext:glContext];
	}
	return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
	/* code */
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];

	[self drawView];

	[pool release];
	return(kCVReturnSuccess);
}

- (void)prepareOpenGL
{
	/* code */
	// OpenGL info
	fprintf(gpFile, "OpenGL Version		:	%s\n",glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL Version 		:	%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

	[[self openGLContext]makeCurrentContext];

	GLint swapInt = 1;
	[[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

// VERTEX SHADER
	// Define Vertex Shader Object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	// Write Vertex Shader Code
	const GLchar *vertexShaderSourceCode =
		"#version 410 core" \
		"\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec4 out_color;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_color = vColor;" \
		"}";

	// Specify above Source Code To The Vertex Shader Object
	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);
	// Compile the VertexShader
	glCompileShader(gVertexShaderObject);

	// Error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLength = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// FRAGMENT SHADER
	// Define Fragment Shader Object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	// Write Vertex Shader Code
	const GLchar *fragmentShaderSourceCode =
		"#version 410 core" \
		"\n" \
		"in vec4 out_color;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "FragColor = out_color;" \
		"}";

	// Specify above Source Code To The Fragment Shader Object
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);
	// Compile the FragmentShader
	glCompileShader(gFragmentShaderObject);

	// Error checking
	iShaderCompileStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompileStatus);
	if (iShaderCompileStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Create Shader Program Object
	gShaderProgramObject = glCreateProgram();
	// Attach Vertex Shader To Shader Program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	// Attach Fragment Shader To Shader Program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// Prelinking binding to vertex attribute
	glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_POSITION, "vPosition");
    glBindAttribLocation(gShaderProgramObject, AMC_ATTRIBUTE_COLOR, "vColor");

	// Link The Shader Program
	glLinkProgram(gShaderProgramObject);

	// Error checking
	GLint iProgramLinkStatus = 0;
	iInfoLength = 0;
	szInfoLog = NULL;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iProgramLinkStatus);
	if (iProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLength);
		if (iInfoLength > 0)
		{
			szInfoLog = (GLchar *)malloc(iInfoLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : \n\t\t%s \n", szInfoLog);
				free(szInfoLog);
				[self release];
				[NSApp terminate:self];
				exit(0);
			}
		}
	}

	// Post Linking Retriving UniformLocation
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");
	const GLfloat graphColor[] =
    {
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f
    };
    // Create vao
    glGenVertexArrays(1, &vaoVerticalGraph);
    glBindVertexArray(vaoVerticalGraph);
    glGenBuffers(1, &vboVerticalGraphPosition);
    glBindBuffer(GL_ARRAY_BUFFER, vboVerticalGraphPosition);
    glBufferData(GL_ARRAY_BUFFER,
        6 *sizeof(GLfloat),
        NULL,
        GL_DYNAMIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
        3, // No Of Co-ordinates
        GL_FLOAT, // Type Of Co-ordinates
        GL_FALSE, //
        0,
        NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Color
    glGenBuffers(1, &vboVerticalGraphColor);
    glBindBuffer(GL_ARRAY_BUFFER, vboVerticalGraphColor);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(graphColor),
        graphColor,
        GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
        3, // No Of Co-ordinates
        GL_FLOAT, // Type Of Co-ordinates
        GL_FALSE, //
        0,
        NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    // Create vao
    glGenVertexArrays(1, &vaoHorizontalGraph);
    glBindVertexArray(vaoHorizontalGraph);
    glGenBuffers(1, &vboHorizontalGraphPosition);
    glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalGraphPosition);
    glBufferData(GL_ARRAY_BUFFER,
        6 * sizeof(GLfloat),
        NULL,
        GL_DYNAMIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,
        3, // No Of Co-ordinates
        GL_FLOAT, // Type Of Co-ordinates
        GL_FALSE, //
        0,
        NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Color
    glGenBuffers(1, &vboHorizontalGraphColor);
    glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalGraphColor);
    glBufferData(GL_ARRAY_BUFFER,
        sizeof(graphColor),
        graphColor,
        GL_STATIC_DRAW);
    glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,
        3, // No Of Co-ordinates
        GL_FLOAT, // Type Of Co-ordinates
        GL_FALSE, //
        0,
        NULL);
    glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);
    
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

	perspectiveProjectionMatrix = mat4::identity();

	CVDisplayLinkCreateWithActiveCGDisplays(&displaylink);
	CVDisplayLinkSetOutputCallback(displaylink, &MyDisplayLinkCallback, self);
	CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
	CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
	CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displaylink, cglContext, cglPixelFormat);
	CVDisplayLinkStart(displaylink);
}

- (void)reshape
{
	/* code */
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	NSRect rect = [self bounds];

	GLfloat width = rect.size.width;
	GLfloat height = rect.size.height;

	if (height == 0)
		height = 1;

	glViewport(0, 0,(GLsizei)width,(GLsizei)height);

	perspectiveProjectionMatrix = vmath::perspective(45.0f,
		(GLfloat)width / (GLfloat)height,
		0.1f,
		100.0f);

	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

- (void)drawRect:(NSRect)dirtyRect
{
	/* code */
	[self drawView];
}

- (void)drawView
{
	/* code */
	[[self openGLContext]makeCurrentContext];
	
	CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(gShaderProgramObject);

 	 	// Vertical Line
       // Declaration of matrices
       mat4 modelViewMatrix;
       mat4 modelViewProjectionMatrix;

       // Initialize above matrices to identity
       modelViewMatrix = mat4::identity();
       modelViewProjectionMatrix = mat4::identity();

       // Do necessary transformations
       modelViewMatrix = translate(0.0f,
           0.0f,
           -3.0f);

       // Do necessary Matrix Multiplication
       modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
       // This was internally donw by gluOrtho2d() in FFP

   // Send Necessary matrix to shader in respective uniform
       glUniformMatrix4fv(mvpUniform,
           1,
           GL_FALSE,
           modelViewProjectionMatrix);

       // Bind with vao
       glBindVertexArray(vaoVerticalGraph);

       // Similarly bind With Textures If Any


       // Draw The Necessary Senn
       [self drawVerticalGraph];

       // Unbind vao
       glBindVertexArray(0);
    
    // Horizontal Line
        // Initialize above matrices to identity
        modelViewMatrix = mat4::identity();
        modelViewProjectionMatrix = mat4::identity();

        // Do necessary transformations
        modelViewMatrix = translate(0.0f,
            0.0f,
            -3.0f);

        // Do necessary Matrix Multiplication
        modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
        // This was internally donw by gluOrtho2d() in FFP

    // Send Necessary matrix to shader in respective uniform
        glUniformMatrix4fv(mvpUniform,
            1,
            GL_FALSE,
            modelViewProjectionMatrix);

        // Bind with vao
        glBindVertexArray(vaoHorizontalGraph);

        // Similarly bind With Textures If Any


        // Draw The Necessary Senn
        [self drawHorizontalGraph];

        // Unbind vao
        glBindVertexArray(0);
    
	// Unuse Program
	glUseProgram(0);

	CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
	CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(void) drawVerticalGraph {
    // function declaration

    // Variable declaration
    const GLfloat iLinePoints = 3;
    GLfloat fVerticalLineVertices[3 * 2];

    // Code:
    for (float i = -1.0f; i < 1.0f; i += 0.05f)
    {
        fVerticalLineVertices[0] = 1.0;
        fVerticalLineVertices[1] = i;
        fVerticalLineVertices[2] = 0.0f;

        fVerticalLineVertices[3] = -1.0;
        fVerticalLineVertices[4] = i;
        fVerticalLineVertices[5] = 0.0f;

        // Vertical Vertices
        glBindBuffer(GL_ARRAY_BUFFER, vboVerticalGraphPosition);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(fVerticalLineVertices),
            fVerticalLineVertices,
            GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glLineWidth(3.0f);
        glDrawArrays(GL_LINES,
            0,
            2);
    }
}

-(void) drawHorizontalGraph {
    // function declaration

    // Variable declaration
    const GLfloat iLinePoints = 3;
    GLfloat fHorizontalLineVertices[3 * 2];

    // Code:
    for (float i = 1.0f; i > -1.0f; i -= 0.05f)
    {
        fHorizontalLineVertices[0] = i;
        fHorizontalLineVertices[1] = 1.0f;
        fHorizontalLineVertices[2] = 0.0f;

        fHorizontalLineVertices[3] = i;
        fHorizontalLineVertices[4] = -1.0f;
        fHorizontalLineVertices[5] = 0.0f;

        // Vertical Vertices
        glBindBuffer(GL_ARRAY_BUFFER, vboHorizontalGraphPosition);
        glBufferData(GL_ARRAY_BUFFER,
            sizeof(fHorizontalLineVertices),
            fHorizontalLineVertices,
            GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glLineWidth(3.0f);
        glDrawArrays(GL_LINES,
            0,
            2);
    }
}

-(BOOL)acceptsFirstResponder
{
	/* code */
	[[self window]makeFirstResponder:self];
	return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
	/* code */
	int key = (int)[[theEvent characters]characterAtIndex:0];
	switch(key)
	{
		case 27: // Esc key
			[self release];
			[NSApp terminate:self];
			break;
		case 'F':
		case 'f':
			[[self window]toggleFullScreen:self]; // repainting occures automatically
			break;
        default:
			break;
	}
}

-(void)mouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	/* code */
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	/* code */
}

-(void) dealloc
{
	/* code */
	if (vboVerticalGraphPosition)
	{
		glDeleteBuffers(1, &vboVerticalGraphPosition);
		vboVerticalGraphPosition = 0;
	}
	if (vaoVerticalGraph)
	{
		glDeleteBuffers(1, &vaoVerticalGraph);
		vaoVerticalGraph = 0;
	}
	glUseProgram(gShaderProgramObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);
	gFragmentShaderObject = 0;
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;
	glUseProgram(0);

	CVDisplayLinkStop(displaylink);
	CVDisplayLinkRelease(displaylink);

	[super dealloc];
}

@end

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displaylink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *pFlagOut, void *pDisplayLingContext)
{
	/* code */
	CVReturn result = [(GLView *)pDisplayLingContext getFrameForTime:pOutputTime];
	return(result);
}

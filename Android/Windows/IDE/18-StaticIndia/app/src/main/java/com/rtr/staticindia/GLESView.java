package com.rtr.staticindia;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32; // 32 = version 3.2
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

// New additions
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    // Declaration For I
    private int[] vaoI = new int[1];
    private int[] vboI = new int[1];							// For the vertices
    private int[] vboColor = new int[1];								// For the colors

    // Declaration For NCross
    private int[] vaoNCross = new int[1];
    private int[] vboNCross = new int[1];							// For the vertices

    // Declaration For ALeft
    private int[] vaoALeft = new int[1];								// For the colors
    private int[] vboALeft = new int[1];								// For the colors

    // Declaration For D DOWN
    private int[] vaoDDown = new int[1];
    private int[] vboDDown = new int[1];							// For the vertices
    private int[] vboGreenColor = new int[1];								// For the colors

    // Declaration For D UP
    private int[] vaoDUp = new int[1];
    private int[] vboDUp = new int[1];							// For the vertices
    private int[] vboOrangeColor = new int[1];								// For the colors

    // Declaration For ACenter
    private int[] vaoACenter = new int[1];
    private int[] vboACenter = new int[1];							// For the vertices
    private int[] vboWhiteColor = new int[1];								// For the colors

    private int mvpUniform;
    private float PerspectiveProjectionMatrix[] = new float[16];		// 4x4 matrix

    // Constructor:
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;

        // Set the EGLContext to current supported version of OpenGL-ES:
        setEGLContextClientVersion(3);

        // Set the renderer:
        setRenderer(this);

        // Set the render mode to render only when there is change in the drawing data:
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector = new GestureDetector(context, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }

    // Initialize function:
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        // Print OpenGL ES version:
        String glesVersion = gl.glGetString(GL10.GL_VERSION);
        System.out.println("AMC : OpenGL ES Version : " + glesVersion);

        // Print OpenGL Shading Language version:
        String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("AMC : OpenGL Shading Language Version : " + glslVersion);

        // Print supported OpenGL extensions:
        //String glesExtensions = gl.glGetString(GL10.GL_EXTENSIONS);
        //System.out.println("AMC : OpenGL Extensions supported on this device : " + glesExtensions);

        initialize(gl);
    }

    // Resize funtion:
    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    // Display function:
    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

    // 0. Handle 'onTouchEvent' because it triggers all gesture and tap events:
    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        int eventAction = e.getAction();
        if(!gestureDetector.onTouchEvent(e))
            super.onTouchEvent(e);
        return(true);
    }

    // 1. Double Tap:
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        return(true);
    }

    // 2. Double Tap event:
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return(true);
    }

    // 3. Single Tap:
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        return(true);
    }

    // 4. On Down:
    @Override
    public boolean onDown(MotionEvent e)
    {
        return(true);
    }

    // 5. On Single Tap UP:
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }

    // 6. Fling:
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }

    // 7. Scroll: (Exit the program)
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        uninitialize();
        System.exit(0);
        return(true);
    }

    // 8. Long Press:
    @Override
    public void onLongPress(MotionEvent e)
    {
    }

    // 9. Show Press:
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    private void initialize(GL10 gl)
    {
        /*<-- VERTEX SHADER -->*/

        // Create vertex shader
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        // Vertex shader source code:
        final String vertexShaderSourceCode = String.format (
                "#version 320 es"+
                "\n" +
                "in vec4 vPosition;" +
                "in vec4 vColor;" +
                "uniform mat4 u_mvp_matrix;" +
                "out vec4 out_color;" +
                "void main(void)" +
                "{" +
                "gl_Position = u_mvp_matrix * vPosition;" +
                "out_color = vColor;" +
                "}"
        );

        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        // Error checking:
        int[] iShaderCompilationStatus = new int[1];	// 1 member integer array
        int[] iInfoLogLength = new int[1];				// 1 member integer array
        String szInfoLog = null;						// String to store the log

        // As there are no addresses in Java, we use an array of size 1
        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
        if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)	// Compilation failed
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR:Vertex Shader Log : "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // FRAGMENT SHADER
        // Define Fragment Shader Object
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        // Fragment shader source code:
        final String FragmentShaderSourceCode = String.format (
                "#version 320 es"+
                "\n" +
                "precision highp float;" +
                "in vec4 out_color;" +
                "out vec4 FragColor;" +
                "void main(void)" +
                "{" +
                "FragColor = out_color;" +
                "}"
        );

        // Specify above Source Code To The Fragment Shader Object
        GLES32.glShaderSource(fragmentShaderObject, FragmentShaderSourceCode);
        // Compile the shader and check For any errors:
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking:
        iShaderCompilationStatus[0] = 0;			// Re initialize
        iInfoLogLength[0] = 0;						// Re initialize
        szInfoLog = null;							// Re initialize

        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
        if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR: Fragment Shader: "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Create the Shader Program:
        shaderProgramObject = GLES32.glCreateProgram();

        // Attach Vertex shader to the shader program:
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);

        // Attach the Fragment shader to the shader program:
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Pre-link binding of shader program object with vertex shader attributes:
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, "vTexcord");

        // Link the two shaders together to shader program object to get a single executable and check For errors:
        GLES32.glLinkProgram(shaderProgramObject);

        // Error checking:
        int[] iShaderProgramLinkStatus = new int[1];		// Linking check
        iInfoLogLength[0] = 0;								// Re initialize
        szInfoLog = null;									// Re initialize

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
        if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("Shader Program link log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Get MVP UniForm location:
        // The actual locations assigned to uniForm variables are not known until the program object is linked successfully.
        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

        // Vertices, Colors, Shader attributes, vbo, vao initializations:
        //Vertices
        final float fIVertices[] =new float[]
                {
                        -1.0f, 1.0f,0.0f,
                        -1.0f, -1.0f,0.0f
                };

        final float fNCrossVertices[] =new float[]
                {
                        -1.0f, 1.0f, 0.0f,
                        -0.5f, -1.0f,0.0f,
                };

        final float fACenterVertices[] =new float[]
                {
                        -1.0f, 1.0f, 0.0f,
                        -0.5f, 1.0f,0.0f,
                };

        final float green_Color[]=new float[]
                {
                        0.0f,1.0f,0.0f,
                        0.0f,1.0f,0.0f
                };
        final float fALeftVertices[] =new float[]
                {
                        1.0f, 1.0f, 0.0f,
                        0.7f, -1.0f, 0.0f,
                };

        final float triColor[] =new float[]
                {
                        1.0f, 0.6f, 0.2f,
                        0.07f, 0.53f, 0.02f
                };

        final float fOrangeColor[]=new float[]
                {
                        1.0f, 0.6f, 0.2f,
                        1.0f, 0.6f, 0.2f
                };

        final float fGreenColor[] =new float[]
                {
                        0.07f, 0.53f, 0.02f,
                        0.07f, 0.53f, 0.02f

                };

        final float fWhiteColor[] =new float[]
                {
                        1.0f, 1.0f, 1.0f,
                        1.0f, 1.0f, 1.0f
                };

        // ************************************************  For "I" IN STATIC INDIA

        GLES32.glGenVertexArrays(1, vaoI, 0);
        GLES32.glBindVertexArray(vaoI[0]);
        GLES32.glGenBuffers(1, vboI, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboI[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(fIVertices.length * 4);

        byteBuffer.order(ByteOrder.nativeOrder());

        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        positionBuffer.put(fIVertices);

        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fIVertices.length * 4,
                positionBuffer,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // I Color
        GLES32.glGenBuffers(1, vboColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor[0]);

        ByteBuffer byteBufferTriangleColor = ByteBuffer.allocateDirect(triColor.length * 4);

        byteBufferTriangleColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferTriangle = byteBufferTriangleColor.asFloatBuffer();

        colorBufferTriangle.put(triColor);

        colorBufferTriangle.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                triColor.length * 4,
                colorBufferTriangle,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // ************************************************  For "N" Cross IN STATIC INDIA

        GLES32.glGenVertexArrays(1, vaoNCross, 0);
        GLES32.glBindVertexArray(vaoNCross[0]);
        GLES32.glGenBuffers(1, vboNCross, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboNCross[0]);

        ByteBuffer byteBufferNCross = ByteBuffer.allocateDirect(fNCrossVertices.length * 4);

        byteBufferNCross.order(ByteOrder.nativeOrder());

        FloatBuffer positionBufferNCross = byteBufferNCross.asFloatBuffer();

        positionBufferNCross.put(fNCrossVertices);

        positionBufferNCross.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fNCrossVertices.length * 4,
                positionBufferNCross,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // Triangle Color
        GLES32.glGenBuffers(1, vboColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor[0]);

        ByteBuffer byteBufferNCrossColor = ByteBuffer.allocateDirect(triColor.length * 4);

        byteBufferNCrossColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferNCross = byteBufferNCrossColor.asFloatBuffer();

        colorBufferNCross.put(triColor);

        colorBufferNCross.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                triColor.length * 4,
                colorBufferNCross,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // ************************************************  For "ALeft" IN STATIC INDIA

        GLES32.glGenVertexArrays(1, vaoALeft, 0);
        GLES32.glBindVertexArray(vaoALeft[0]);
        GLES32.glGenBuffers(1, vboALeft, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboALeft[0]);

        ByteBuffer byteBufferALeft = ByteBuffer.allocateDirect(fALeftVertices.length * 4);

        byteBufferALeft.order(ByteOrder.nativeOrder());

        FloatBuffer positionBufferALeft = byteBufferALeft.asFloatBuffer();

        positionBufferALeft.put(fALeftVertices);

        positionBufferALeft.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fALeftVertices.length * 4,
                positionBufferALeft,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // ALeft Color
        GLES32.glGenBuffers(1, vboColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor[0]);

        ByteBuffer byteBufferALeftColor = ByteBuffer.allocateDirect(triColor.length * 4);

        byteBufferALeftColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferALeft = byteBufferALeftColor.asFloatBuffer();

        colorBufferALeft.put(triColor);

        colorBufferALeft.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                triColor.length * 4,
                colorBufferALeft,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // ************************************************  For "DUp" IN STATIC INDIA

        GLES32.glGenVertexArrays(1, vaoDUp, 0);
        GLES32.glBindVertexArray(vaoDUp[0]);
        GLES32.glGenBuffers(1, vboDUp, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboDUp[0]);

        ByteBuffer byteBufferDUp = ByteBuffer.allocateDirect(fACenterVertices.length * 4);

        byteBufferDUp.order(ByteOrder.nativeOrder());

        FloatBuffer positionBufferDUp = byteBufferDUp.asFloatBuffer();

        positionBufferDUp.put(fACenterVertices);

        positionBufferDUp.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fACenterVertices.length * 4,
                positionBufferDUp,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // ALeft Color
        GLES32.glGenBuffers(1, vboOrangeColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboOrangeColor[0]);

        ByteBuffer byteBufferDUpColor = ByteBuffer.allocateDirect(fOrangeColor.length * 4);

        byteBufferDUpColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferDUp = byteBufferDUpColor.asFloatBuffer();

        colorBufferDUp.put(fOrangeColor);

        colorBufferDUp.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fOrangeColor.length * 4,
                colorBufferDUp,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // ************************************************  For "DDown" IN STATIC INDIA

        GLES32.glGenVertexArrays(1, vaoDDown, 0);
        GLES32.glBindVertexArray(vaoDDown[0]);
        GLES32.glGenBuffers(1, vboDDown, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboDDown[0]);

        ByteBuffer byteBufferDDown = ByteBuffer.allocateDirect(fACenterVertices.length * 4);

        byteBufferDDown.order(ByteOrder.nativeOrder());

        FloatBuffer positionBufferDDown = byteBufferDDown.asFloatBuffer();

        positionBufferDDown.put(fACenterVertices);

        positionBufferDDown.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fACenterVertices.length * 4,
                positionBufferDDown,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // DDown Color
        GLES32.glGenBuffers(1, vboGreenColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboGreenColor[0]);

        ByteBuffer byteBufferDDownColor = ByteBuffer.allocateDirect(fGreenColor.length * 4);

        byteBufferDDownColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferDDown = byteBufferDDownColor.asFloatBuffer();

        colorBufferDDown.put(fGreenColor);

        colorBufferDDown.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fGreenColor.length * 4,
                colorBufferDDown,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // ************************************************  For "ACenter" IN STATIC INDIA

        GLES32.glGenVertexArrays(1, vaoACenter, 0);
        GLES32.glBindVertexArray(vaoACenter[0]);
        GLES32.glGenBuffers(1, vboACenter, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboACenter[0]);

        ByteBuffer byteBufferACenter = ByteBuffer.allocateDirect(fACenterVertices.length * 4);

        byteBufferACenter.order(ByteOrder.nativeOrder());

        FloatBuffer positionBufferACenter = byteBufferACenter.asFloatBuffer();

        positionBufferACenter.put(fACenterVertices);

        positionBufferACenter.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fACenterVertices.length * 4,
                positionBufferACenter,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // DDown Color
        GLES32.glGenBuffers(1, vboWhiteColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboWhiteColor[0]);

        ByteBuffer byteBufferACenterColor = ByteBuffer.allocateDirect(fWhiteColor.length * 4);

        byteBufferACenterColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferACenter = byteBufferDDownColor.asFloatBuffer();

        colorBufferACenter.put(fWhiteColor);

        colorBufferACenter.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fWhiteColor.length * 4,
                colorBufferACenter,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // Enable depth testing and specify the depth test to perForm:
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        // We will always cull back faces For better perFormance
        GLES32.glEnable(GLES32.GL_CULL_FACE);

        // Set the background color to Black:
        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        // Set projectionMatrix and Identity matrix:
        Matrix.setIdentityM(PerspectiveProjectionMatrix, 0);
    }

    private void resize(int width, int height)
    {
        //Code:
        GLES32.glViewport(0, 0, width, height);

        Matrix.perspectiveM(PerspectiveProjectionMatrix, 0, 45.0f,
                ((float)width/(float)height), 0.1f, 100.0f);
    }

    public void display()
    {
        // Code:
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        GLES32.glUseProgram(shaderProgramObject);

        // Declaration of matrices
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

        //vaoI
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, -0.4f, 0.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                PerspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoI[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //vaoNLeft
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, -0.06f, 0.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                PerspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoI[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //NCross
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, -0.06f, 0.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                PerspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoNCross[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //NRight
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 0.44f, 0.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                PerspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoI[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //DLeft
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 0.88f, 0.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                PerspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoI[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //DUp
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 0.88f, 0.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                PerspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoDUp[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //DDown
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 0.88f, -2.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                PerspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoDDown[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //DRight
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 1.38f, 0.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                PerspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoI[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);		// 3 (each with x,y,z) vertices in triangleVertices array

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //ISecond
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 1.76f, 0.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                PerspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoI[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //ALeft
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 0.4f, 0.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                PerspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoALeft[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //ACenterOrange
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 2.63f, -0.988f, -3.9f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                PerspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoDUp[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //ACenterWhite
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 2.63f, -1.0f, -3.9f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                PerspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoACenter[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);		// 3 (each with x,y,z) vertices in triangleVertices array

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //ACenterGreen
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 2.63f, -1.01f, -3.9f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                PerspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoDDown[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //ARight
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 2.4f, 0.0f, -3.0f);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                PerspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoNCross[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        // Stop using the shader program object:
        GLES32.glUseProgram(0);

        // Render/Flush:
        requestRender();
    }

    void uninitialize()
    {
        // Code:
        // Detach and delete the shaders one by one that are attached to the the shader program object
        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                // Detach vertex shader from the shader program and then delete it:
                GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }

            if(fragmentShaderObject != 0)
            {
                // Detach fragment shader from the shader program and then delete it:
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        // Delete the shader program object
        if(shaderProgramObject != 0)
        {
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }
}

package com.rtr.graph;

import android.content.Context;
import android.opengl.GLES32;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

// New additions

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener {
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vaoVerticalGraph = new int[1];
    private int[] vboVerticalGraphPosition = new int[1];
    private int[] vboVerticalGraphColor = new int[1];
    private FloatBuffer positionBufferVerticalGraph;

    private int[] vaoHorizontalGraph = new int[1];
    private int[] vboHorizontalGraphPosition = new int[1];
    private int[] vboHorizontalGraphColor = new int[1];
    private FloatBuffer positionBufferHorizontalGraph;

    private int mvpUniform;

    private float[] perspectiveProjectionMatrix = new float[16];


    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;

        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector = new GestureDetector(context, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int eventAction = event.getAction();
        if (!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);

        return(true);
    }

    @Override
    public boolean onDoubleTap(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onDown(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY)
    {
        return(true);
    }

    @Override
    public void onLongPress(MotionEvent event)
    {
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX, float distanceY)
    {
        System.exit(0);
        return(true);
    }

    @Override
    public void onShowPress(MotionEvent event)
    {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event)
    {
        return(true);
    }

    // Implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        //String version = gl.glGetString(GL10.GL_VERSION);
        String version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

    // Our Custom Methods
    private void initialize()
    {
        Log.d("AMC", "initialize: Started");
        GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        final String vertexShaderSourceCode = String.format(
                "#version 320 es" +
                "\n" +
                "in vec4 vPosition;" +
                "in vec4 vColor;" +
                "uniform mat4 u_mvp_matrix;" +
                "out vec4 out_color;" +
                "void main(void)" +
                "{" +
                "gl_Position = u_mvp_matrix * vPosition;" +
                "out_color = vColor;" +
                "}");

        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        //Error Checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR:Vertex Shader Log : "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // FRAGMENT SHADER
        // Define Fragment Shader Object
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        // Write Vertex Shader Code
        final String fragmentShaderSourceCode = String.format(
                "#version 320 es" +
                "\n" +
                "precision highp float;" +
                "in vec4 out_color;" +
                "out vec4 FragColor;" +
                "void main(void)" +
                "{" +
                "	FragColor = out_color;" +
                "}");

        // Specify above Source Code To The Fragment Shader Object
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        // Compile the FragmentShader
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking
        iShaderCompileStatus[0] = 0;
        iInfoLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR: Fragment Shader: " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Create Shader Program Object
        shaderProgramObject = GLES32.glCreateProgram();
        // Attach Vertex Shader To Shader Program
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        // Attach Fragment Shader To Shader Program
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Prelinking binding to vertex attribute
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");

        // Link The Shader Program
        GLES32.glLinkProgram(shaderProgramObject);

        // Error checking
        int[] iProgramLinkStatus = new int[1];
        iInfoLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if (iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("Shader Program Link Log : " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Post Linking Retriving UniformLocation
        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        final float[] graphColor = new float[]
        {
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f
        };

        final float[] triangleVertices = new float[]
                {
                        0.0f, 1.0f, 0.0f,
                        -1.0f, -1.0f, 0.0f,
                        1.0f, -1.0f, 0.0f
                };

        // Create vaoVerticalGraph
        Log.d("AMC", "VAO: Started");
        GLES32.glGenVertexArrays(1, vaoVerticalGraph, 0);
        GLES32.glBindVertexArray(vaoVerticalGraph[0]);
        GLES32.glGenBuffers(1, vboVerticalGraphPosition, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboVerticalGraphPosition[0]);

        ByteBuffer byteBufferVerticalGraph = ByteBuffer.allocateDirect(6 * 4);

        byteBufferVerticalGraph.order(ByteOrder.nativeOrder());

        positionBufferVerticalGraph = byteBufferVerticalGraph.asFloatBuffer();

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                6 * 4,
                null,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // Triangle Color
        GLES32.glGenBuffers(1, vboVerticalGraphColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboVerticalGraphColor[0]);

        ByteBuffer byteBufferTriangleColor = ByteBuffer.allocateDirect(graphColor.length * 4);

        byteBufferTriangleColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferTriangle = byteBufferTriangleColor.asFloatBuffer();

        colorBufferTriangle.put(graphColor);

        colorBufferTriangle.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                graphColor.length * 4,
                colorBufferTriangle,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // Create vaoHorizontalGraph
        GLES32.glGenVertexArrays(1, vaoHorizontalGraph, 0);
        GLES32.glBindVertexArray(vaoHorizontalGraph[0]);
        GLES32.glGenBuffers(1, vboHorizontalGraphPosition, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboHorizontalGraphPosition[0]);

        ByteBuffer byteBufferHorizontalGraphPosition = ByteBuffer.allocateDirect(6 * 4);

        byteBufferHorizontalGraphPosition.order(ByteOrder.nativeOrder());

        positionBufferHorizontalGraph = byteBufferHorizontalGraphPosition.asFloatBuffer();

//        positionBufferVerticalGraph.put(triangleVertices);
//
//        positionBufferVerticalGraph.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                6 * 4,
                null,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // Triangle Color
        GLES32.glGenBuffers(1, vboHorizontalGraphColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboHorizontalGraphColor[0]);

        ByteBuffer byteBufferHorizontalGraphColor = ByteBuffer.allocateDirect(graphColor.length * 4);

        byteBufferHorizontalGraphColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferHorizontalGraph = byteBufferHorizontalGraphColor.asFloatBuffer();

        colorBufferHorizontalGraph.put(graphColor);

        colorBufferHorizontalGraph.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                graphColor.length * 4,
                colorBufferHorizontalGraph,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);
        Log.d("AMC", "VAO: END");

        GLES32.glClearDepthf(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
        Log.d("AMC", "initialize: END");
    }

    private void resize(int width, int height)
    {
        if(height <= 0)
        {
            height = 1;
        }
        GLES32.glViewport(0, 0, width, height);

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0,
                45.0f,
                (float)width / (float)height,
                0.1f,
                100.0f);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        GLES32.glUseProgram(shaderProgramObject);

        // Declaration of matrices
        float[] translationMatrix = new float[16];
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

        // Initialize above matrices to identity
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Do necessary transformations
        Matrix.translateM(translationMatrix, 0,
                0.0f, 0.0f, -3.0f);


        // Do necessary Matrix Multiplication
        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                translationMatrix, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        // Send Necessary matrix to shader in respective uniform
        GLES32.glUniformMatrix4fv(mvpUniform,
                1,
                false,
                modelViewProjectionMatrix, 0);

        // Bind with vaoVerticalGraph
        GLES32.glBindVertexArray(vaoVerticalGraph[0]);

        // Similarly bind With Textures If Any


        // Draw The Necessary Senn
        drawVerticalGraph();

        // Unbind vaoVerticalGraph
        GLES32.glBindVertexArray(0);

//        HorizontalGraph
        // Initialize above matrices to identity
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Do necessary transformations
        Matrix.translateM(translationMatrix, 0,
                0.0f, 0.0f, -3.0f);


        // Do necessary Matrix Multiplication
        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                translationMatrix, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        // Send Necessary matrix to shader in respective uniform
        GLES32.glUniformMatrix4fv(mvpUniform,
                1,
                false,
                modelViewProjectionMatrix, 0);

        // Bind with vaoVerticalGraph
        GLES32.glBindVertexArray(vaoHorizontalGraph[0]);

        // Similarly bind With Textures If Any


        // Draw The Necessary Senn
        Log.d("AMC", "drawHR: START");
        drawHorizontalGraph();
        Log.d("AMC", "drawHR: END");

        // Unbind vaoVerticalGraph
        GLES32.glBindVertexArray(0);

        // Unuse Program
        GLES32.glUseProgram(0);

        requestRender();
    }

    void drawVerticalGraph() {
        // function declaration

        // Variable declaration
        final float iLinePoints = 3;
        float[] fVerticalLineVertices = new float[3 * 2];

        // Code:
        for (float i = -1.0f; i < 1.0f; i += 0.05f)
        {
            fVerticalLineVertices[0] = 1.0f;
            fVerticalLineVertices[1] = i;
            fVerticalLineVertices[2] = 0.0f;

            fVerticalLineVertices[3] = -1.0f;
            fVerticalLineVertices[4] = i;
            fVerticalLineVertices[5] = 0.0f;

            // Vertical Vertices
        positionBufferVerticalGraph.put(fVerticalLineVertices);

        positionBufferVerticalGraph.position(0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboVerticalGraphPosition[0]);
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                    fVerticalLineVertices.length * 4,
                    positionBufferVerticalGraph,
                    GLES32.GL_DYNAMIC_DRAW);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

            GLES32.glLineWidth(3.0f);
            GLES32.glDrawArrays(GLES32.GL_LINES,
                    0,
                    2);
        }
    }

    void drawHorizontalGraph() {
        // function declaration

        // Variable declaration
	final float iLinePoints = 3;
        float[] fHorizontalLineVertices = new float[3 * 2];

        // Code:
        for (float i = 1.0f; i > -1.0f; i -= 0.05f)
        {
            fHorizontalLineVertices[0] = i;
            fHorizontalLineVertices[1] = 1.0f;
            fHorizontalLineVertices[2] = 0.0f;

            fHorizontalLineVertices[3] = i;
            fHorizontalLineVertices[4] = -1.0f;
            fHorizontalLineVertices[5] = 0.0f;

            positionBufferHorizontalGraph.put(fHorizontalLineVertices);

            positionBufferHorizontalGraph.position(0);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboHorizontalGraphPosition[0]);
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                    fHorizontalLineVertices.length * 4,
                    positionBufferHorizontalGraph,
                    GLES32.GL_DYNAMIC_DRAW);
            GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

            GLES32.glLineWidth(3.0f);
            GLES32.glDrawArrays(GLES32.GL_LINES,
                    0,
                    2);
        }
    }

    private void uninitialize() {
        if (vboVerticalGraphPosition[0] != 0) {
            GLES32.glDeleteBuffers(1, vboVerticalGraphPosition, 0);
            vboVerticalGraphPosition[0] = 0;
        }

        if (vaoVerticalGraph[0] != 0) {
            GLES32.glDeleteVertexArrays(1, vaoVerticalGraph, 0);
            vaoVerticalGraph[0] = 0;
        }

        if (shaderProgramObject != 0) {
            int[] shaderCount = new int[1];
            int shaderNumber;

            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            int[] shaders = new int[shaderCount[0]];

            GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);

            for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++) {
                // detach shader
                GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

                // delete shader
                GLES32.glDeleteShader(shaders[shaderNumber]);
                shaders[shaderNumber] = 0;
            }

            GLES32.glUseProgram(0);
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }
}

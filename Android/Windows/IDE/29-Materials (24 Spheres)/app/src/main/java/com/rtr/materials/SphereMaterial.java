package com.rtr.materials;

public class SphereMaterial {
    public float[] materialAmbient = new float[4];
    public float[] materialDiffuse = new float[4];
    public float[] materialSpecular = new float[4];
    public float materialShininess;
}

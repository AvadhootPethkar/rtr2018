package com.rtr.materials;

import android.content.Context;
import android.opengl.GLES32;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.view.GestureDetector;
import android.view.MotionEvent;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int modelUniform;
    private int viewUniform;
    private int projectionUniform;
    private int lKeyPressedUniform;
    private int laUniform;
    private int ldUniform;
    private int lsUniform;
    private int kaUniform;
    private int kdUniform;
    private int ksUniform;
    private int lightPositionUniform;
    private int materialShinynessUniform;

    private float[] perspectiveProjectionMatrix = new float[16];

    private boolean gbLight = false;
    private boolean isDoubleTap = false;

    private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];

    private int numVertices;
    private int numElements;

    // Animating variables
    private float angleOfXRotation = 0.0f;
    private float angleOfYRotation = 0.0f;
    private float angleOfZRotation = 0.0f;

    // Light Variables
    private boolean isLKeyPressed = false;
    float[] lightAmbient = { 0.0f,0.0f, 0.0f, 1.0f };
    float[] lightDiffuse = { 1.0f,1.0f,1.0f,1.0f };
    float[] lightSpecular = { 1.0f,1.0f, 1.0f, 1.0f };
    float[] lightPosition = { 100.0f,100.0f,100.0f,1.0f };

    float[] materialAmbient = { 0.0f,0.0f, 0.0f, 1.0f };
    float[] materialDiffuse = { 1.0f,1.0f,1.0f,1.0f };
    float[] materialSpecular = { 1.0f,1.0f, 1.0f, 1.0f };
    float materialShininess = 128.0f;

    // Viewport Variables
    private int gX;
    private int gY;
    private int gWidth;
    private int gHeight;

    final int MATERIAL_ROWS = 6;
    final int MATERIAL_COLUMNS = 4;
    private int tapCount = 0;
    private SphereMaterial[][] sphereMaterials = new SphereMaterial[MATERIAL_ROWS][MATERIAL_COLUMNS];

    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;

        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector = new GestureDetector(context, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int eventAction = event.getAction();
        if (!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);

        return(true);
    }

    @Override
    public boolean onDoubleTap(MotionEvent event)
    {
        if (isDoubleTap == false) {
            gbLight = true;
            isDoubleTap = true;
        }
        else {
            gbLight = false;
            isDoubleTap = false;
        }
        return(true);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event)
    {
        tapCount++;
        if (tapCount > 2) {
            tapCount = 0;
        }
        return(true);
    }

    @Override
    public boolean onDown(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY)
    {
        return(true);
    }

    @Override
    public void onLongPress(MotionEvent event)
    {
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX, float distanceY)
    {
        System.exit(0);
        return(true);
    }

    @Override
    public void onShowPress(MotionEvent event)
    {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event)
    {
        return(true);
    }

    // Implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        //String version = gl.glGetString(GL10.GL_VERSION);
        String version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

    // Our Custom Methods
    private void initialize()
    {
        GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        final String vertexShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "in vec4 vPosition;" +
            "in vec3 vNormal;" +
            "uniform mat4 u_model_matrix;" +
            "uniform mat4 u_view_matrix;" +
            "uniform mat4 u_projection_matrix;" +
            "uniform vec4 u_light_position;" +
            "uniform mediump int u_lKeyPressed;" +
            "out vec3 tNorm;" +
            "out vec3 light_direction;" +
            "out vec3 viewer_vector;" +
            "void main(void)" +
            "{" +
            "	if(u_lKeyPressed == 1)" +
            "	{" +
            "		vec4 eye_coordinates = u_view_matrix * u_model_matrix * vPosition;"	+
            "		tNorm = mat3(u_view_matrix * u_model_matrix) * vNormal;" +
            "		light_direction = vec3(u_light_position) - eye_coordinates.xyz;" +
            "		float tn_dot_ld = max(dot(tNorm, light_direction), 0.0);" +
            "		viewer_vector = vec3(-eye_coordinates.xyz);" +
            "	}" +
            "	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
            "}");

        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        //Error Checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR:Vertex Shader Log : "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // FRAGMENT SHADER
        // Define Fragment Shader Object
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        // Write Vertex Shader Code
        final String fragmentShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;" +
            "in vec3 tNorm;" +
            "in vec3 light_direction;" +
            "in vec3 viewer_vector;" +
            "uniform vec3 u_la;" +
            "uniform vec3 u_ld;" +
            "uniform vec3 u_ls;" +
            "uniform vec3 u_ka;" +
            "uniform vec3 u_kd;" +
            "uniform vec3 u_ks;" +
            "uniform float u_shininess;"	+
            "uniform mediump int u_lKeyPressed;" +
            "out vec4 FragColor;" +
            "void main(void)" +
            "{" +
            "	if(u_lKeyPressed == 1)" +
            "	{" +
            "		vec3 ntNorm = normalize(tNorm);" +
            "		vec3 nlight_direction = normalize(light_direction);" +
            "		vec3 nviewer_vector  = normalize(viewer_vector);" +
            "		vec3 reflection_vector  = reflect(-nlight_direction, ntNorm);" +
            "		float tn_dot_ld = max(dot(ntNorm, nlight_direction), 0.0);" +
            "		vec3 ambient = u_la * u_ka;" +
            "		vec3 diffuse =u_ld * u_kd * tn_dot_ld;" +
            "		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, nviewer_vector), 0.0), u_shininess);" +
            "		vec3 phong_ads_light = ambient + diffuse + specular;" +
            "		FragColor = vec4(phong_ads_light, 1.0);"	+
            "	}" +
            "	else" +
            "	{" +
            "		FragColor = vec4(1.0, 1.0, 1.0, 1.0);" +
            "	}" +
            "}");

        // Specify above Source Code To The Fragment Shader Object
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        // Compile the FragmentShader
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking
        iShaderCompileStatus[0] = 0;
        iInfoLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR: Fragment Shader: " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Create Shader Program Object
        shaderProgramObject = GLES32.glCreateProgram();
        // Attach Vertex Shader To Shader Program
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        // Attach Fragment Shader To Shader Program
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Prelinking binding to vertex attribute
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_NORMAL, "vNormal");

        // Link The Shader Program
        GLES32.glLinkProgram(shaderProgramObject);

        // Error checking
        int[] iProgramLinkStatus = new int[1];
        iInfoLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if (iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("Shader Program Link Log : " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Post Linking Retriving UniformLocation
        modelUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_model_matrix");
        viewUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_view_matrix");
        projectionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_projection_matrix");
        lKeyPressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lKeyPressed");
        lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_light_position");
        laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_la");
        ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld");
        lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls");

        kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ka");
        kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
        ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ks");

        materialShinynessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_shininess");

        Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

        // Initialize materials object array
        for (int i = 0; i < MATERIAL_ROWS; i++) {
            for (int j = 0; j < MATERIAL_COLUMNS; j++) {
                sphereMaterials[i][j] = new SphereMaterial();
            }
        }
        InitializeMaterials();

        // vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);

        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);

        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                sphere_vertices.length * 4,
                verticesBuffer,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3,
                GLES32.GL_FLOAT,
                false,0,0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);

        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                sphere_normals.length * 4,
                verticesBuffer,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL,
                3,
                GLES32.GL_FLOAT,
                false,0,0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);

        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                sphere_elements.length * 2,
                elementsBuffer,
                GLES32.GL_STATIC_DRAW);

        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);

        GLES32.glClearDepthf(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
    }

    private void resize(int width, int height)
    {
        if(height <= 0)
        {
            height = 1;
        }
        GLES32.glViewport(0, 0, width, height);

        gWidth = width / MATERIAL_ROWS;
        gHeight= height / MATERIAL_ROWS;
        gX = (width - (gWidth * MATERIAL_COLUMNS)) / 2;
        gY = (height - (gHeight * MATERIAL_ROWS)) / 2;

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0,
                45.0f,
                (float)width / (float)height,
                0.1f,
                100.0f);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        for (int i = 0; i < MATERIAL_ROWS; i++) {
            for (int j = 0; j < MATERIAL_COLUMNS; j++) {
                GLES32.glUseProgram(shaderProgramObject);
                GLES32.glViewport(gX + (i * gWidth), gY + (j * gHeight), gWidth, gHeight);
                if (gbLight == true) {
                    GLES32.glUniform3fv(laUniform, 1, lightAmbient, 0);
                    GLES32.glUniform3fv(ldUniform, 1, lightDiffuse, 0);
                    GLES32.glUniform3fv(lsUniform, 1, lightSpecular, 0);
                    GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);

                    GLES32.glUniform3fv(kaUniform, 1, sphereMaterials[i][j].materialAmbient, 0);
                    GLES32.glUniform3fv(kdUniform, 1, sphereMaterials[i][j].materialDiffuse, 0);
                    GLES32.glUniform3fv(ksUniform, 1, sphereMaterials[i][j].materialSpecular, 0);
                    GLES32.glUniform1f(materialShinynessUniform, sphereMaterials[i][j].materialShininess);

                    if (tapCount == 1)
                    {
                        lightPosition[0] = 0.0f;
                        lightPosition[1] = (float) Math.sin(angleOfXRotation) * 100.0f - 3.0f;
                        lightPosition[2] = (float) Math.cos(angleOfXRotation) * 100.0f - 3.0f;
                    }

                    if (tapCount == 2)
                    {
                        lightPosition[0] = (float) Math.sin(angleOfYRotation) * 100.0f - 3.0f;
                        lightPosition[1] = 0.0f;
                        lightPosition[2] = (float) Math.cos(angleOfYRotation) * 100.0f - 3.0f;
                    }

                    if (tapCount == 3)
                    {
                        lightPosition[0] = (float) Math.sin(angleOfZRotation) * 100.0f - 3.0f;
                        lightPosition[1] = (float) Math.cos(angleOfZRotation) * 100.0f - 3.0f;
                        lightPosition[2] = 0.0f;
                    }

                    GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);
                    GLES32.glUniform1i(lKeyPressedUniform, 1);
                }
                else {
                    GLES32.glUniform1i(lKeyPressedUniform, 0);
                }
                // Declaration of matrices
                float[] translationMatrix = new float[16];
                float[] modelMatrix = new float[16];
                float[] viewMatrix = new float[16];
                float[] modelViewProjectionMatrix = new float[16];

                // Rectangle
                // Initialize above matrices to identity
                Matrix.setIdentityM(translationMatrix, 0);
                Matrix.setIdentityM(modelMatrix, 0);
                Matrix.setIdentityM(viewMatrix, 0);
                Matrix.setIdentityM(modelViewProjectionMatrix, 0);

                // Do necessary transformations
                Matrix.translateM(translationMatrix, 0,
                        0.0f, 0.0f, -2.0f);

                // Do necessary Matrix Multiplication
                Matrix.multiplyMM(modelMatrix, 0,
                        modelMatrix, 0,
                        translationMatrix, 0);

                // Send Necessary matrix to shader in respective uniform
                GLES32.glUniformMatrix4fv(modelUniform,
                        1,
                        false,
                        modelMatrix, 0);

                GLES32.glUniformMatrix4fv(viewUniform,
                        1,
                        false,
                        viewMatrix, 0);

                GLES32.glUniformMatrix4fv(projectionUniform,
                        1,
                        false,
                        perspectiveProjectionMatrix, 0);

                GLES32.glUniform3fv(laUniform, 1, lightAmbient, 0);
                GLES32.glUniform3fv(ldUniform, 1, lightDiffuse, 0);
                GLES32.glUniform3fv(lsUniform, 1, lightSpecular, 0);
                GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);

                GLES32.glUniform3fv(kaUniform, 1, materialAmbient, 0);
                GLES32.glUniform3fv(kdUniform, 1, materialDiffuse, 0);
                GLES32.glUniform3fv(ksUniform, 1, materialSpecular, 0);
                GLES32.glUniform1f(materialShinynessUniform, materialShininess);

                float lightPosition[] = {
                        0.0f, 0.0f, 2.0f, 1.0f
                };

                GLES32.glUniform4fv(lightPositionUniform, 1, lightPosition, 0);

                if (gbLight == true)
                {
                    GLES32.glUniform1i(lKeyPressedUniform, 1);
                }
                else
                {
                    GLES32.glUniform1i(lKeyPressedUniform, 0);
                }

                // Bind with vaoCube
                GLES32.glBindVertexArray(vao_sphere[0]);

                // Similarly bind With Textures If Any


                // Draw The Necessary Senn
                GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
                GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);

                // Unbind vaoCube
                GLES32.glBindVertexArray(0);

                // Unuse Program
                GLES32.glUseProgram(0);
                update();
            }
        }
        requestRender();
    }

    private void uninitialize() {
        // destroy vao
        if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }

        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }

        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }

        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }

        if (shaderProgramObject != 0) {
            int[] shaderCount = new int[1];
            int shaderNumber;

            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            int[] shaders = new int[shaderCount[0]];

            GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);

            for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++) {
                // detach shader
                GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

                // delete shader
                GLES32.glDeleteShader(shaders[shaderNumber]);
                shaders[shaderNumber] = 0;
            }

            GLES32.glUseProgram(0);
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }

    private void update() {
        // variable Declaration
        float iOffset = 0.01f;
        // Code:
        if (tapCount == 1) {
            angleOfXRotation = 0.0f;
        }
        if (tapCount == 2) {
            angleOfYRotation = 0.0f;
        }
        if (tapCount == 3) {
            angleOfZRotation = 0.0f;
        }

        angleOfXRotation -= iOffset;
        if (angleOfXRotation > 360.0f)
        {
            angleOfXRotation += 360.0f;
        }
        angleOfYRotation -= iOffset;
        if (angleOfYRotation > 360.0f)
        {
            angleOfYRotation += 360.0f;
        }
        angleOfZRotation -= iOffset;
        if (angleOfZRotation > 360.0f)
        {
            angleOfZRotation += 360.0f;
        }
    }

    void InitializeMaterials() {
        // emerald
        sphereMaterials[0][0].materialAmbient[0] = 0.0215f;
        sphereMaterials[0][0].materialAmbient[1] = 0.1745f;
        sphereMaterials[0][0].materialAmbient[2] = 0.0215f;
        sphereMaterials[0][0].materialAmbient[3] = 1.0f;

        sphereMaterials[0][0].materialDiffuse[0] = 0.07568f;
        sphereMaterials[0][0].materialDiffuse[1] = 0.61424f;
        sphereMaterials[0][0].materialDiffuse[2] = 0.07568f;
        sphereMaterials[0][0].materialDiffuse[3] = 1.0f;

        sphereMaterials[0][0].materialSpecular[0] = 0.633f;
        sphereMaterials[0][0].materialSpecular[1] = 0.727811f;
        sphereMaterials[0][0].materialSpecular[2] = 0.633f;
        sphereMaterials[0][0].materialSpecular[3] = 1.0f;

        sphereMaterials[0][0].materialShininess = 0.6f * 128f;

        // jade
        sphereMaterials[0][1].materialAmbient[0] = 0.135f;
        sphereMaterials[0][1].materialAmbient[1] = 0.2225f;
        sphereMaterials[0][1].materialAmbient[2] = 0.1575f;
        sphereMaterials[0][1].materialAmbient[3] = 1.0f;

        sphereMaterials[0][1].materialDiffuse[0] = 0.54f;
        sphereMaterials[0][1].materialDiffuse[1] = 0.89f;
        sphereMaterials[0][1].materialDiffuse[2] = 0.63f;
        sphereMaterials[0][1].materialDiffuse[3] = 1.0f;

        sphereMaterials[0][1].materialSpecular[0] = 0.316228f;
        sphereMaterials[0][1].materialSpecular[1] = 0.316228f;
        sphereMaterials[0][1].materialSpecular[2] = 0.316228f;
        sphereMaterials[0][1].materialSpecular[3] = 1.0f;

        sphereMaterials[0][1].materialShininess = 0.1f * 128f;

        // obsidian
        sphereMaterials[0][2].materialAmbient[0] = 0.05375f;
        sphereMaterials[0][2].materialAmbient[1] = 0.05f;
        sphereMaterials[0][2].materialAmbient[2] = 0.06625f;
        sphereMaterials[0][2].materialAmbient[3] = 1.0f;

        sphereMaterials[0][2].materialDiffuse[0] = 0.18275f;
        sphereMaterials[0][2].materialDiffuse[1] = 0.17f;
        sphereMaterials[0][2].materialDiffuse[2] = 0.22525f;
        sphereMaterials[0][2].materialDiffuse[3] = 1.0f;

        sphereMaterials[0][2].materialSpecular[0] = 0.332741f;
        sphereMaterials[0][2].materialSpecular[1] = 0.328634f;
        sphereMaterials[0][2].materialSpecular[2] = 0.346435f;
        sphereMaterials[0][2].materialSpecular[3] = 1.0f;

        sphereMaterials[0][2].materialShininess = 0.3f * 128f;

        // pearl
        sphereMaterials[0][3].materialAmbient[0] = 0.25f;
        sphereMaterials[0][3].materialAmbient[1] = 0.20725f;
        sphereMaterials[0][3].materialAmbient[2] = 0.20725f;
        sphereMaterials[0][3].materialAmbient[3] = 1.0f;

        sphereMaterials[0][3].materialDiffuse[0] = 1.0f;
        sphereMaterials[0][3].materialDiffuse[1] = 0.829f;
        sphereMaterials[0][3].materialDiffuse[2] = 0.829f;
        sphereMaterials[0][3].materialDiffuse[3] = 1.0f;

        sphereMaterials[0][3].materialSpecular[0] = 0.296648f;
        sphereMaterials[0][3].materialSpecular[1] = 0.296648f;
        sphereMaterials[0][3].materialSpecular[2] = 0.296648f;
        sphereMaterials[0][3].materialSpecular[3] = 1.0f;

        sphereMaterials[0][3].materialShininess = 0.088f * 128f;

        // ruby
        sphereMaterials[1][0].materialAmbient[0] = 0.1745f;
        sphereMaterials[1][0].materialAmbient[1] = 0.01175f;
        sphereMaterials[1][0].materialAmbient[2] = 0.01175f;
        sphereMaterials[1][0].materialAmbient[3] = 1.0f;

        sphereMaterials[1][0].materialDiffuse[0] = 0.61424f;
        sphereMaterials[1][0].materialDiffuse[1] = 0.04136f;
        sphereMaterials[1][0].materialDiffuse[2] = 0.04136f;
        sphereMaterials[1][0].materialDiffuse[3] = 1.0f;

        sphereMaterials[1][0].materialSpecular[0] = 0.727811f;
        sphereMaterials[1][0].materialSpecular[1] = 0.626959f;
        sphereMaterials[1][0].materialSpecular[2] = 0.626959f;
        sphereMaterials[1][0].materialSpecular[3] = 1.0f;

        sphereMaterials[1][0].materialShininess = 0.6f * 128f;

        // turquoise
        sphereMaterials[1][1].materialAmbient[0] = 0.1f;
        sphereMaterials[1][1].materialAmbient[1] = 0.18725f;
        sphereMaterials[1][1].materialAmbient[2] = 0.1745f;
        sphereMaterials[1][1].materialAmbient[3] = 1.0f;

        sphereMaterials[1][1].materialDiffuse[0] = 0.396f;
        sphereMaterials[1][1].materialDiffuse[1] = 0.74151f;
        sphereMaterials[1][1].materialDiffuse[2] = 0.69102f;
        sphereMaterials[1][1].materialDiffuse[3] = 1.0f;

        sphereMaterials[1][1].materialSpecular[0] = 0.297254f;
        sphereMaterials[1][1].materialSpecular[1] = 0.30829f;
        sphereMaterials[1][1].materialSpecular[2] = 0.306678f;
        sphereMaterials[1][1].materialSpecular[3] = 1.0f;

        sphereMaterials[1][1].materialShininess = 0.1f * 128f;
//---------------------------------------------------------------

        // brass
        sphereMaterials[1][2].materialAmbient[0] = 0.329412f;
        sphereMaterials[1][2].materialAmbient[1] = 0.223529f;
        sphereMaterials[1][2].materialAmbient[2] = 0.027451f;
        sphereMaterials[1][2].materialAmbient[3] = 1.0f;

        sphereMaterials[1][2].materialDiffuse[0] = 0.780392f;
        sphereMaterials[1][2].materialDiffuse[1] = 0.568627f;
        sphereMaterials[1][2].materialDiffuse[2] = 0.113725f;
        sphereMaterials[1][2].materialDiffuse[3] = 1.0f;

        sphereMaterials[1][2].materialSpecular[0] = 0.992157f;
        sphereMaterials[1][2].materialSpecular[1] = 0.941176f;
        sphereMaterials[1][2].materialSpecular[2] = 0.807843f;
        sphereMaterials[1][2].materialSpecular[3] = 1.0f;

        sphereMaterials[1][2].materialShininess = 0.21794872f * 128f;

        // bronze
        sphereMaterials[1][3].materialAmbient[0] = 0.2125f;
        sphereMaterials[1][3].materialAmbient[1] = 0.1275f;
        sphereMaterials[1][3].materialAmbient[2] = 0.054f;
        sphereMaterials[1][3].materialAmbient[3] = 1.0f;

        sphereMaterials[1][3].materialDiffuse[0] = 0.714f;
        sphereMaterials[1][3].materialDiffuse[1] = 0.4284f;
        sphereMaterials[1][3].materialDiffuse[2] = 0.18144f;
        sphereMaterials[1][3].materialDiffuse[3] = 1.0f;

        sphereMaterials[1][3].materialSpecular[0] = 0.393548f;
        sphereMaterials[1][3].materialSpecular[1] = 0.271906f;
        sphereMaterials[1][3].materialSpecular[2] = 0.166721f;
        sphereMaterials[1][3].materialSpecular[3] = 1.0f;

        sphereMaterials[1][3].materialShininess = 0.2f * 128f;

        // chrome
        sphereMaterials[2][0].materialAmbient[0] = 0.25f;
        sphereMaterials[2][0].materialAmbient[1] = 0.25f;
        sphereMaterials[2][0].materialAmbient[2] = 0.25f;
        sphereMaterials[2][0].materialAmbient[3] = 1.0f;

        sphereMaterials[2][0].materialDiffuse[0] = 0.4f;
        sphereMaterials[2][0].materialDiffuse[1] = 0.4f;
        sphereMaterials[2][0].materialDiffuse[2] = 0.4f;
        sphereMaterials[2][0].materialDiffuse[3] = 1.0f;

        sphereMaterials[2][0].materialSpecular[0] = 0.774597f;
        sphereMaterials[2][0].materialSpecular[1] = 0.774597f;
        sphereMaterials[2][0].materialSpecular[2] = 0.774597f;
        sphereMaterials[2][0].materialSpecular[3] = 1.0f;

        sphereMaterials[2][0].materialShininess = 0.6f * 128f;

        // copper
        sphereMaterials[2][1].materialAmbient[0] = 0.19125f;
        sphereMaterials[2][1].materialAmbient[1] = 0.0735f;
        sphereMaterials[2][1].materialAmbient[2] = 0.0225f;
        sphereMaterials[2][1].materialAmbient[3] = 1.0f;

        sphereMaterials[2][1].materialDiffuse[0] = 0.7038f;
        sphereMaterials[2][1].materialDiffuse[1] = 0.27048f;
        sphereMaterials[2][1].materialDiffuse[2] = 0.0828f;
        sphereMaterials[2][1].materialDiffuse[3] = 1.0f;

        sphereMaterials[2][1].materialSpecular[0] = 0.256777f;
        sphereMaterials[2][1].materialSpecular[1] = 0.137622f;
        sphereMaterials[2][1].materialSpecular[2] = 0.086014f;
        sphereMaterials[2][1].materialSpecular[3] = 1.0f;

        sphereMaterials[2][1].materialShininess = 0.1f * 128f;

        // gold
        sphereMaterials[2][2].materialAmbient[0] = 0.24725f;
        sphereMaterials[2][2].materialAmbient[1] = 0.1995f;
        sphereMaterials[2][2].materialAmbient[2] = 0.0745f;
        sphereMaterials[2][2].materialAmbient[3] = 1.0f;

        sphereMaterials[2][2].materialDiffuse[0] = 0.75164f;
        sphereMaterials[2][2].materialDiffuse[1] = 0.60648f;
        sphereMaterials[2][2].materialDiffuse[2] = 0.22648f;
        sphereMaterials[2][2].materialDiffuse[3] = 1.0f;

        sphereMaterials[2][2].materialSpecular[0] = 0.628281f;
        sphereMaterials[2][2].materialSpecular[1] = 0.555802f;
        sphereMaterials[2][2].materialSpecular[2] = 0.366065f;
        sphereMaterials[2][2].materialSpecular[3] = 1.0f;

        sphereMaterials[2][2].materialShininess = 0.4f * 128f;

        // silver
        sphereMaterials[2][3].materialAmbient[0] = 0.19225f;
        sphereMaterials[2][3].materialAmbient[1] = 0.19225f;
        sphereMaterials[2][3].materialAmbient[2] = 0.19225f;
        sphereMaterials[2][3].materialAmbient[3] = 1.0f;

        sphereMaterials[2][3].materialDiffuse[0] = 0.50754f;
        sphereMaterials[2][3].materialDiffuse[1] = 0.50754f;
        sphereMaterials[2][3].materialDiffuse[2] = 0.50754f;
        sphereMaterials[2][3].materialDiffuse[3] = 1.0f;

        sphereMaterials[2][3].materialSpecular[0] = 0.508273f;
        sphereMaterials[2][3].materialSpecular[1] = 0.508273f;
        sphereMaterials[2][3].materialSpecular[2] = 0.508273f;
        sphereMaterials[2][3].materialSpecular[3] = 1.0f;

        sphereMaterials[2][3].materialShininess = 0.4f * 128f;

//---------------------------------------------------------------

        // black
        sphereMaterials[3][0].materialAmbient[0] = 0.0f;
        sphereMaterials[3][0].materialAmbient[1] = 0.0f;
        sphereMaterials[3][0].materialAmbient[2] = 0.0f;
        sphereMaterials[3][0].materialAmbient[3] = 1.0f;

        sphereMaterials[3][0].materialDiffuse[0] = 0.01f;
        sphereMaterials[3][0].materialDiffuse[1] = 0.01f;
        sphereMaterials[3][0].materialDiffuse[2] = 0.01f;
        sphereMaterials[3][0].materialDiffuse[3] = 1.0f;

        sphereMaterials[3][0].materialSpecular[0] = 0.50f;
        sphereMaterials[3][0].materialSpecular[1] = 0.50f;
        sphereMaterials[3][0].materialSpecular[2] = 0.50f;
        sphereMaterials[3][0].materialSpecular[3] = 1.0f;

        sphereMaterials[3][0].materialShininess = 0.25f * 128f;

        // cyan
        sphereMaterials[3][1].materialAmbient[0] = 0.0f;
        sphereMaterials[3][1].materialAmbient[1] = 0.1f;
        sphereMaterials[3][1].materialAmbient[2] = 0.06f;
        sphereMaterials[3][1].materialAmbient[3] = 1.0f;

        sphereMaterials[3][1].materialDiffuse[0] = 0.0f;
        sphereMaterials[3][1].materialDiffuse[1] = 0.50980392f;
        sphereMaterials[3][1].materialDiffuse[2] = 0.50980392f;
        sphereMaterials[3][1].materialDiffuse[3] = 1.0f;

        sphereMaterials[3][1].materialSpecular[0] = 0.50196078f;
        sphereMaterials[3][1].materialSpecular[1] = 0.50196078f;
        sphereMaterials[3][1].materialSpecular[2] = 0.50196078f;
        sphereMaterials[3][1].materialSpecular[3] = 1.0f;

        sphereMaterials[3][1].materialShininess = 0.25f * 128f;

        // green
        sphereMaterials[3][2].materialAmbient[0] = 0.0f;
        sphereMaterials[3][2].materialAmbient[1] = 0.0f;
        sphereMaterials[3][2].materialAmbient[2] = 0.0f;
        sphereMaterials[3][2].materialAmbient[3] = 1.0f;

        sphereMaterials[3][2].materialDiffuse[0] = 0.1f;
        sphereMaterials[3][2].materialDiffuse[1] = 0.35f;
        sphereMaterials[3][2].materialDiffuse[2] = 0.1f;
        sphereMaterials[3][2].materialDiffuse[3] = 1.0f;

        sphereMaterials[3][2].materialSpecular[0] = 0.45f;
        sphereMaterials[3][2].materialSpecular[1] = 0.55f;
        sphereMaterials[3][2].materialSpecular[2] = 0.45f;
        sphereMaterials[3][2].materialSpecular[3] = 1.0f;

        sphereMaterials[3][2].materialShininess = 0.25f * 128f;

        // red
        sphereMaterials[3][3].materialAmbient[0] = 0.0f;
        sphereMaterials[3][3].materialAmbient[1] = 0.0f;
        sphereMaterials[3][3].materialAmbient[2] = 0.0f;
        sphereMaterials[3][3].materialAmbient[3] = 1.0f;

        sphereMaterials[3][3].materialDiffuse[0] = 0.5f;
        sphereMaterials[3][3].materialDiffuse[1] = 0.0f;
        sphereMaterials[3][3].materialDiffuse[2] = 0.0f;
        sphereMaterials[3][3].materialDiffuse[3] = 1.0f;

        sphereMaterials[3][3].materialSpecular[0] = 0.7f;
        sphereMaterials[3][3].materialSpecular[1] = 0.6f;
        sphereMaterials[3][3].materialSpecular[2] = 0.6f;
        sphereMaterials[3][3].materialSpecular[3] = 1.0f;

        sphereMaterials[3][3].materialShininess = 0.25f * 128f;

        // white
        sphereMaterials[4][0].materialAmbient[0] = 0.0f;
        sphereMaterials[4][0].materialAmbient[1] = 0.0f;
        sphereMaterials[4][0].materialAmbient[2] = 0.0f;
        sphereMaterials[4][0].materialAmbient[3] = 1.0f;

        sphereMaterials[4][0].materialDiffuse[0] = 0.55f;
        sphereMaterials[4][0].materialDiffuse[1] = 0.55f;
        sphereMaterials[4][0].materialDiffuse[2] = 0.55f;
        sphereMaterials[4][0].materialDiffuse[3] = 1.0f;

        sphereMaterials[4][0].materialSpecular[0] = 0.70f;
        sphereMaterials[4][0].materialSpecular[1] = 0.70f;
        sphereMaterials[4][0].materialSpecular[2] = 0.70f;
        sphereMaterials[4][0].materialSpecular[3] = 1.0f;

        sphereMaterials[4][0].materialShininess = 0.25f * 128f;

        // yellow
        sphereMaterials[4][1].materialAmbient[0] = 0.0f;
        sphereMaterials[4][1].materialAmbient[1] = 0.0f;
        sphereMaterials[4][1].materialAmbient[2] = 0.0f;
        sphereMaterials[4][1].materialAmbient[3] = 1.0f;

        sphereMaterials[4][1].materialDiffuse[0] = 0.5f;
        sphereMaterials[4][1].materialDiffuse[1] = 0.5f;
        sphereMaterials[4][1].materialDiffuse[2] = 0.0f;
        sphereMaterials[4][1].materialDiffuse[3] = 1.0f;

        sphereMaterials[4][1].materialSpecular[0] = 0.60f;
        sphereMaterials[4][1].materialSpecular[1] = 0.60f;
        sphereMaterials[4][1].materialSpecular[2] = 0.50f;
        sphereMaterials[4][1].materialSpecular[3] = 1.0f;

        sphereMaterials[4][1].materialShininess = 0.25f * 128f;

//---------------------------------------------------------------

        // black
        sphereMaterials[4][2].materialAmbient[0] = 0.02f;
        sphereMaterials[4][2].materialAmbient[1] = 0.02f;
        sphereMaterials[4][2].materialAmbient[2] = 0.02f;
        sphereMaterials[4][2].materialAmbient[3] = 1.0f;

        sphereMaterials[4][2].materialDiffuse[0] = 0.01f;
        sphereMaterials[4][2].materialDiffuse[1] = 0.01f;
        sphereMaterials[4][2].materialDiffuse[2] = 0.01f;
        sphereMaterials[4][2].materialDiffuse[3] = 1.0f;

        sphereMaterials[4][2].materialSpecular[0] = 0.4f;
        sphereMaterials[4][2].materialSpecular[1] = 0.4f;
        sphereMaterials[4][2].materialSpecular[2] = 0.4f;
        sphereMaterials[4][2].materialSpecular[3] = 0.4f;

        sphereMaterials[4][2].materialShininess = 0.078125f * 128f;

        // cyan
        sphereMaterials[4][3].materialAmbient[0] = 0.0f;
        sphereMaterials[4][3].materialAmbient[1] = 0.05f;
        sphereMaterials[4][3].materialAmbient[2] = 0.05f;
        sphereMaterials[4][3].materialAmbient[3] = 1.0f;

        sphereMaterials[4][3].materialDiffuse[0] = 0.4f;
        sphereMaterials[4][3].materialDiffuse[1] = 0.5f;
        sphereMaterials[4][3].materialDiffuse[2] = 0.5f;
        sphereMaterials[4][3].materialDiffuse[3] = 1.0f;

        sphereMaterials[4][3].materialSpecular[0] = 0.4f;
        sphereMaterials[4][3].materialSpecular[1] = 0.7f;
        sphereMaterials[4][3].materialSpecular[2] = 0.7f;
        sphereMaterials[4][3].materialSpecular[3] = 1.0f;

        sphereMaterials[4][3].materialShininess = 0.078125f * 128f;

        // green
        sphereMaterials[5][0].materialAmbient[0] = 0.0f;
        sphereMaterials[5][0].materialAmbient[1] = 0.05f;
        sphereMaterials[5][0].materialAmbient[2] = 0.0f;
        sphereMaterials[5][0].materialAmbient[3] = 1.0f;

        sphereMaterials[5][0].materialDiffuse[0] = 0.4f;
        sphereMaterials[5][0].materialDiffuse[1] = 0.5f;
        sphereMaterials[5][0].materialDiffuse[2] = 0.4f;
        sphereMaterials[5][0].materialDiffuse[3] = 1.0f;

        sphereMaterials[5][0].materialSpecular[0] = 0.04f;
        sphereMaterials[5][0].materialSpecular[1] = 0.7f;
        sphereMaterials[5][0].materialSpecular[2] = 0.04f;
        sphereMaterials[5][0].materialSpecular[3] = 1.0f;

        sphereMaterials[5][0].materialShininess = 0.078125f * 128f;

        // red
        sphereMaterials[5][1].materialAmbient[0] = 0.05f;
        sphereMaterials[5][1].materialAmbient[1] = 0.0f;
        sphereMaterials[5][1].materialAmbient[2] = 0.0f;
        sphereMaterials[5][1].materialAmbient[3] = 1.0f;

        sphereMaterials[5][1].materialDiffuse[0] = 0.5f;
        sphereMaterials[5][1].materialDiffuse[1] = 0.4f;
        sphereMaterials[5][1].materialDiffuse[2] = 0.4f;
        sphereMaterials[5][1].materialDiffuse[3] = 1.0f;

        sphereMaterials[5][1].materialSpecular[0] = 0.7f;
        sphereMaterials[5][1].materialSpecular[1] = 0.04f;
        sphereMaterials[5][1].materialSpecular[2] = 0.04f;
        sphereMaterials[5][1].materialSpecular[3] = 1.0f;

        sphereMaterials[5][1].materialShininess = 0.078125f * 128f;

        // white
        sphereMaterials[5][2].materialAmbient[0] = 0.05f;
        sphereMaterials[5][2].materialAmbient[1] = 0.05f;
        sphereMaterials[5][2].materialAmbient[2] = 0.05f;
        sphereMaterials[5][2].materialAmbient[3] = 1.0f;

        sphereMaterials[5][2].materialDiffuse[0] = 0.5f;
        sphereMaterials[5][2].materialDiffuse[1] = 0.5f;
        sphereMaterials[5][2].materialDiffuse[2] = 0.5f;
        sphereMaterials[5][2].materialDiffuse[3] = 1.0f;

        sphereMaterials[5][2].materialSpecular[0] = 0.7f;
        sphereMaterials[5][2].materialSpecular[1] = 0.7f;
        sphereMaterials[5][2].materialSpecular[2] = 0.7f;
        sphereMaterials[5][2].materialSpecular[3] = 0.4f;

        sphereMaterials[5][2].materialShininess = 0.078125f * 128f;

        // yellow
        sphereMaterials[5][3].materialAmbient[0] = 0.05f;
        sphereMaterials[5][3].materialAmbient[1] = 0.05f;
        sphereMaterials[5][3].materialAmbient[2] = 0.0f;
        sphereMaterials[5][3].materialAmbient[3] = 1.0f;

        sphereMaterials[5][3].materialDiffuse[0] = 0.5f;
        sphereMaterials[5][3].materialDiffuse[1] = 0.5f;
        sphereMaterials[5][3].materialDiffuse[2] = 0.4f;
        sphereMaterials[5][3].materialDiffuse[3] = 1.0f;

        sphereMaterials[5][3].materialSpecular[0] = 0.7f;
        sphereMaterials[5][3].materialSpecular[1] = 0.7f;
        sphereMaterials[5][3].materialSpecular[2] = 0.04f;
        sphereMaterials[5][3].materialSpecular[3] = 1.0f;

        sphereMaterials[5][3].materialShininess = 0.078125f * 128f;
    }
}

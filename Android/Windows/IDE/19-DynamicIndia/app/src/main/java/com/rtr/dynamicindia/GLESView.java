package com.rtr.dynamicindia;

import android.content.Context;
import android.opengl.GLES32;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.view.GestureDetector;
import android.view.MotionEvent;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    // Declaration For I
    private int[] vaoI = new int[1];
    private int[] vboI = new int[1];
    private int[] vboColor = new int[1];

    // Declaration For NCross
    private int[] vaoNCross = new int[1];
    private int[] vboNCross = new int[1];

    // Declaration For ALeft
    private int[] vaoALeft = new int[1];
    private int[] vboALeft = new int[1];

    // Declaration For D DOWN
    private int[] vaoDDown = new int[1];
    private int[] vboDDown = new int[1];
    private int[] vboGreenColor = new int[1];

    // Declaration For D UP
    private int[] vaoDUp = new int[1];
    private int[] vboDUp = new int[1];
    private int[] vboOrangeColor = new int[1];

    // Declaration For ACenter
    private int[] vaoACenter = new int[1];
    private int[] vboACenter = new int[1];
    private int[] vboWhiteColor = new int[1];

    private int mvpUniform;
    private float[] perspectiveProjectionMatrix = new float[16];

    // Dynamic India Variable
    float gfLIX = -1.3f;
    float gfAX = 1.72f;
    float gfAXR = 3.72f;
    float gfNY = 2.3f;
    float gfRIY = -2.3f;
    float gfORC = 0.0f;
    float gfOGC = 0.0f;
    float gfOBC = 0.0f;
    float gfGRC = 0.0f;
    float gfGBC = 0.0f;
    float gfGGC = 0.0f;

    // Aeroplane
    private int[] vao = new int[1];
    private int[] vboPosition = new int[1];

    private boolean gbAnimatePlain = false;
    private FloatBuffer positionBuffer;

    // Triangle Variables
    final int iTrianglePoints = 3;

    // Line Variables
    final int iLinePoints = 3;

    // Square Variables
    final int iSquarePoints = 4;

    // Animating Variables Plane
    float gfUpperLeftAngle = (float) Math.PI;
    float gfUpperLeftAngle_X = -3.0f;
    float gfUpperLeftAngle_Y = 3.0f;

    float gfMiddlePlaneTranslate_X = -3.0f;
    float gfMiddlePlaneTranslate_Y = 0.0f;

    float gfLowerLeftAngle = (float) Math.PI;
    float gfLowerLeftAngle_X = -3.0f;
    float gfLowerLeftAngle_Y = -3.0f;

    float gfUpperRightAngle = 3 * ((float) Math.PI / 2);
    float gfUpperRightAngle_X = 0.0f;
    float gfUpperRightAngle_Y = 0.0f;

    float gfLowerRightAngle = ((float) Math.PI / 2);
    float gfLowerRightAngle_X = 0.0f;
    float gfLowerRightAngle_Y = 0.0f;

    float gfFlagOrange_X1 = -1.5f;
    float gfFlagOrange_X2 = 0.0f;
    float gfFlagOrange_Y = 0.03f;
    float gfFlagWhite_X1 = -1.5f;
    float gfFlagWhite_X2 = 0.0f;
    float gfFlagWhite_Y = 0.0f;
    float gfFlagGreen_X1 = -1.5f;
    float gfFlagGreen_X2 = 0.0f;
    float gfFlagGreen_Y = -0.03f;

    // Small Flag Variables
    float gfSmallFlagOrange_X1 = -1.0f;
    float gfSmallFlagWhite_X1 = -1.0f;
    float gfSmallFlagGreen_X1 = -1.0f;

    // Constructor:
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;

        // Set the EGLContext to current supported version of OpenGL-ES:
        setEGLContextClientVersion(3);

        // Set the renderer:
        setRenderer(this);

        // Set the render mode to render only when there is change in the drawing data:
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector = new GestureDetector(context, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }

    // Initialize function:
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        // Print OpenGL ES version:
        String glesVersion = gl.glGetString(GL10.GL_VERSION);
        System.out.println("AMC : OpenGL ES Version : " + glesVersion);

        // Print OpenGL Shading Language version:
        String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("AMC : OpenGL Shading Language Version : " + glslVersion);

        initialize(gl);
    }

    // Resize funtion:
    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    // Display function:
    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
        update();
    }

    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        int eventAction = e.getAction();
        if(!gestureDetector.onTouchEvent(e))
            super.onTouchEvent(e);
        return(true);
    }

    // 1. Double Tap:
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        return(true);
    }

    // 2. Double Tap event:
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return(true);
    }

    // 3. Single Tap:
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        return(true);
    }

    // 4. On Down:
    @Override
    public boolean onDown(MotionEvent e)
    {
        return(true);
    }

    // 5. On Single Tap UP:
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }

    // 6. Fling:
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }

    // 7. Scroll: (Exit the program)
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        uninitialize();
        System.exit(0);
        return(true);
    }

    // 8. Long Press:
    @Override
    public void onLongPress(MotionEvent e)
    {
    }

    // 9. Show Press:
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    private void initialize(GL10 gl)
    {
        // VERTEX SHADER
        // Create vertex shader
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        // Vertex shader source code:
        final String vertexShaderSourceCode = String.format (
                "#version 320 es"+
                "\n" +
                "in vec4 vPosition;" +
                "in vec4 vColor;" +
                "uniform mat4 u_mvp_matrix;" +
                "out vec4 out_color;" +
                "void main(void)" +
                "{" +
                "   gl_Position = u_mvp_matrix * vPosition;" +
                "   out_color = vColor;" +
                "}"
        );

        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        // Error checking:
        int[] iShaderCompilationStatus = new int[1];
        int[] iInfoLogLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
        if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR:Vertex Shader Log : "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // FRAGMENT SHADER
        // Define Fragment Shader Object
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        // Fragment shader source code:
        final String FragmentShaderSourceCode = String.format (
                "#version 320 es"+
                "\n" +
                "precision highp float;" +
                "in vec4 out_color;" +
                "out vec4 FragColor;" +
                "void main(void)" +
                "{" +
                "   FragColor = out_color;" +
                "}"
        );

        // Specify above Source Code To The Fragment Shader Object
        GLES32.glShaderSource(fragmentShaderObject, FragmentShaderSourceCode);
        // Compile the shader and check For any errors:
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking:
        iShaderCompilationStatus[0] = 0;
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
        if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR: Fragment Shader: "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Create the Shader Program:
        shaderProgramObject = GLES32.glCreateProgram();

        // Attach Vertex shader to the shader program:
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);

        // Attach the Fragment shader to the shader program:
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Pre-link binding of shader program object with vertex shader attributes:
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, "vTexcord");

        // Link the two shaders together to shader program object to get a single executable and check For errors:
        GLES32.glLinkProgram(shaderProgramObject);

        // Error checking:
        int[] iShaderProgramLinkStatus = new int[1];		// Linking check
        iInfoLogLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
        if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("Shader Program link log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Get MVP UniForm location:
        // The actual locations assigned to uniForm variables are not known until the program object is linked successfully.
        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

        // Vertices, Colors, Shader attributes, vbo, vao initializations:
        //Vertices
        final float fIVertices[] =new float[]
                {
                        -1.0f, 1.0f,0.0f,
                        -1.0f, -1.0f,0.0f
                };

        final float fNCrossVertices[] =new float[]
                {
                        -1.0f, 1.0f, 0.0f,
                        -0.5f, -1.0f,0.0f,
                };

        final float fACenterVertices[] =new float[]
                {
                        -1.0f, 1.0f, 0.0f,
                        -0.5f, 1.0f,0.0f,
                };

        final float green_Color[]=new float[]
                {
                        0.0f,1.0f,0.0f,
                        0.0f,1.0f,0.0f
                };
        final float[] fALeftVertices =new float[]
                {
                        1.0f, 1.0f, 0.0f,
                        0.7f, -1.0f, 0.0f,
                };

        final float[] triColor =new float[]
                {
                        1.0f, 0.6f, 0.2f,
                        0.07f, 0.53f, 0.02f
                };

        final float[] fOrangeColor=new float[]
                {
                        1.0f, 0.6f, 0.2f,
                        1.0f, 0.6f, 0.2f
                };

        final float[] fGreenColor =new float[]
                {
                        0.07f, 0.53f, 0.02f,
                        0.07f, 0.53f, 0.02f

                };

        final float[] fWhiteColor =new float[]
                {
                        1.0f, 1.0f, 1.0f,
                        1.0f, 1.0f, 1.0f
                };

        // ************************************************  For "I" IN STATIC INDIA

        GLES32.glGenVertexArrays(1, vaoI, 0);
        GLES32.glBindVertexArray(vaoI[0]);
        GLES32.glGenBuffers(1, vboI, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboI[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(fIVertices.length * 4);

        byteBuffer.order(ByteOrder.nativeOrder());

        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        positionBuffer.put(fIVertices);

        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fIVertices.length * 4,
                positionBuffer,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // I Color
        GLES32.glGenBuffers(1, vboColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor[0]);

        ByteBuffer byteBufferTriangleColor = ByteBuffer.allocateDirect(triColor.length * 4);

        byteBufferTriangleColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferTriangle = byteBufferTriangleColor.asFloatBuffer();

        colorBufferTriangle.put(triColor);

        colorBufferTriangle.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                triColor.length * 4,
                colorBufferTriangle,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // ************************************************  For "N" Cross IN STATIC INDIA

        GLES32.glGenVertexArrays(1, vaoNCross, 0);
        GLES32.glBindVertexArray(vaoNCross[0]);
        GLES32.glGenBuffers(1, vboNCross, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboNCross[0]);

        ByteBuffer byteBufferNCross = ByteBuffer.allocateDirect(fNCrossVertices.length * 4);

        byteBufferNCross.order(ByteOrder.nativeOrder());

        FloatBuffer positionBufferNCross = byteBufferNCross.asFloatBuffer();

        positionBufferNCross.put(fNCrossVertices);

        positionBufferNCross.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fNCrossVertices.length * 4,
                positionBufferNCross,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // Triangle Color
        GLES32.glGenBuffers(1, vboColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor[0]);

        ByteBuffer byteBufferNCrossColor = ByteBuffer.allocateDirect(triColor.length * 4);

        byteBufferNCrossColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferNCross = byteBufferNCrossColor.asFloatBuffer();

        colorBufferNCross.put(triColor);

        colorBufferNCross.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                triColor.length * 4,
                colorBufferNCross,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // ************************************************  For "ALeft" IN STATIC INDIA

        GLES32.glGenVertexArrays(1, vaoALeft, 0);
        GLES32.glBindVertexArray(vaoALeft[0]);
        GLES32.glGenBuffers(1, vboALeft, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboALeft[0]);

        ByteBuffer byteBufferALeft = ByteBuffer.allocateDirect(fALeftVertices.length * 4);

        byteBufferALeft.order(ByteOrder.nativeOrder());

        FloatBuffer positionBufferALeft = byteBufferALeft.asFloatBuffer();

        positionBufferALeft.put(fALeftVertices);

        positionBufferALeft.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fALeftVertices.length * 4,
                positionBufferALeft,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // ALeft Color
        GLES32.glGenBuffers(1, vboColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor[0]);

        ByteBuffer byteBufferALeftColor = ByteBuffer.allocateDirect(triColor.length * 4);

        byteBufferALeftColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferALeft = byteBufferALeftColor.asFloatBuffer();

        colorBufferALeft.put(triColor);

        colorBufferALeft.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                triColor.length * 4,
                colorBufferALeft,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // ************************************************  For "DUp" IN STATIC INDIA

        GLES32.glGenVertexArrays(1, vaoDUp, 0);
        GLES32.glBindVertexArray(vaoDUp[0]);
        GLES32.glGenBuffers(1, vboDUp, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboDUp[0]);

        ByteBuffer byteBufferDUp = ByteBuffer.allocateDirect(fACenterVertices.length * 4);

        byteBufferDUp.order(ByteOrder.nativeOrder());

        FloatBuffer positionBufferDUp = byteBufferDUp.asFloatBuffer();

        positionBufferDUp.put(fACenterVertices);

        positionBufferDUp.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fACenterVertices.length * 4,
                positionBufferDUp,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // ALeft Color
        GLES32.glGenBuffers(1, vboOrangeColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboOrangeColor[0]);

        ByteBuffer byteBufferDUpColor = ByteBuffer.allocateDirect(fOrangeColor.length * 4);

        byteBufferDUpColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferDUp = byteBufferDUpColor.asFloatBuffer();

        colorBufferDUp.put(fOrangeColor);

        colorBufferDUp.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fOrangeColor.length * 4,
                colorBufferDUp,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // ************************************************  For "DDown" IN STATIC INDIA

        GLES32.glGenVertexArrays(1, vaoDDown, 0);
        GLES32.glBindVertexArray(vaoDDown[0]);
        GLES32.glGenBuffers(1, vboDDown, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboDDown[0]);

        ByteBuffer byteBufferDDown = ByteBuffer.allocateDirect(fACenterVertices.length * 4);

        byteBufferDDown.order(ByteOrder.nativeOrder());

        FloatBuffer positionBufferDDown = byteBufferDDown.asFloatBuffer();

        positionBufferDDown.put(fACenterVertices);

        positionBufferDDown.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fACenterVertices.length * 4,
                positionBufferDDown,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // DDown Color
        GLES32.glGenBuffers(1, vboGreenColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboGreenColor[0]);

        ByteBuffer byteBufferDDownColor = ByteBuffer.allocateDirect(fGreenColor.length * 4);

        byteBufferDDownColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferDDown = byteBufferDDownColor.asFloatBuffer();

        colorBufferDDown.put(fGreenColor);

        colorBufferDDown.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fGreenColor.length * 4,
                colorBufferDDown,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // ************************************************  For "ACenter" IN STATIC INDIA

        GLES32.glGenVertexArrays(1, vaoACenter, 0);
        GLES32.glBindVertexArray(vaoACenter[0]);
        GLES32.glGenBuffers(1, vboACenter, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboACenter[0]);

        ByteBuffer byteBufferACenter = ByteBuffer.allocateDirect(fACenterVertices.length * 4);

        byteBufferACenter.order(ByteOrder.nativeOrder());

        FloatBuffer positionBufferACenter = byteBufferACenter.asFloatBuffer();

        positionBufferACenter.put(fACenterVertices);

        positionBufferACenter.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fACenterVertices.length * 4,
                positionBufferACenter,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // DDown Color
        GLES32.glGenBuffers(1, vboWhiteColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboWhiteColor[0]);

        ByteBuffer byteBufferACenterColor = ByteBuffer.allocateDirect(fWhiteColor.length * 4);

        byteBufferACenterColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferACenter = byteBufferDDownColor.asFloatBuffer();

        colorBufferACenter.put(fWhiteColor);

        colorBufferACenter.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fWhiteColor.length * 4,
                colorBufferACenter,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // Aeroplane
        // Createvao
        GLES32.glGenVertexArrays(1, vao, 0);
        GLES32.glBindVertexArray(vao[0]);

        // A. BUFFER BLOCK FOR VERTICES:
        GLES32.glGenBuffers(1, vboPosition, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPosition[0]);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                4 * 2 * 4,
                null,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3,
                GLES32.GL_FLOAT,
                false, 0, 0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vboColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor[0]);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                4 * 2 * 4,
                null,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3,
                GLES32.GL_FLOAT,
                false, 0, 0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

        // Release the buffer for colors:
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // Release vao:
        GLES32.glBindVertexArray(0);

        // Enable depth testing and specify the depth test to perForm:
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        // We will always cull back faces For better perFormance
        GLES32.glEnable(GLES32.GL_CULL_FACE);

        // Set the background color to Black:
        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        // Set projectionMatrix and Identity matrix:
        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
    }

    private void resize(int width, int height)
    {
        //Code:
        GLES32.glViewport(0, 0, width, height);

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f,
                ((float)width/(float)height), 0.1f, 100.0f);
    }

    public void display()
    {
        // Code:
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        GLES32.glUseProgram(shaderProgramObject);

        // Declaration of matrices
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

        //vaoI
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, gfLIX, 0.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoI[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //vaoNLeft
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, -0.06f, gfNY, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoI[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //NCross
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, -0.06f, gfNY, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoNCross[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //NRight
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 0.44f, gfNY, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoI[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //DLeft
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 0.88f, 0.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoI[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //DUp
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 0.88f, 0.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoDUp[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //DDown
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 0.88f, -2.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoDDown[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //DRight
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 1.38f, 0.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoI[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);		// 3 (each with x,y,z) vertices in triangleVertices array

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //ISecond
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 1.76f, gfRIY, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoI[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //ALeft
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, gfAX, 0.0f, -3.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoALeft[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //ACenterOrange
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 2.63f, -0.988f, -3.9f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoDUp[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //ACenterWhite
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 2.63f, -1.0f, -3.9f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoACenter[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);		// 3 (each with x,y,z) vertices in triangleVertices array

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //ACenterGreen
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, 2.63f, -1.01f, -3.9f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoDDown[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        //ARight
        // Initialize above matrices to identity
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0, gfAXR, 0.0f, -3.0f);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoNCross[0]);

        // Draw either using glDrawTriangles() or glDrawArrays() or glDrawElements():
        GLES32.glDrawArrays(GLES32.GL_LINES, 0, 2);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        if (gbAnimatePlain) {
            displayPlain();
        }

        // Stop using the shader program object:
        GLES32.glUseProgram(0);

        // Render/Flush:
        requestRender();
    }

    private void displayPlain() {
        // OpenGL ES drawing:
        float[] translationMatrixOne = new float[16];
        float[] translationMatrixTwo = new float[16];
        float[] rotationMatrix = new float[16];
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

        // Upper Plain
        // Initialize above matrices to identity
        Matrix.setIdentityM(translationMatrixOne, 0);
        Matrix.setIdentityM(translationMatrixTwo, 0);
        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Do necessary transformations
        Matrix.translateM(translationMatrixOne, 0,
                -2.5f, 3.0f, -4.0f);
        if (gfUpperLeftAngle < ((3.0f * Math.PI) / 2))
        {
            gfUpperLeftAngle_X = 3.0f * (float) Math.cos(gfUpperLeftAngle);
            gfUpperLeftAngle_Y = 3.0f * (float) Math.sin(gfUpperLeftAngle);
            Matrix.translateM(translationMatrixOne, 0,
                    gfUpperLeftAngle_X, gfUpperLeftAngle_Y, -6.0f);
        }
        gfUpperLeftAngle += 0.001f;

        // Do necessary Matrix Multiplication
        Matrix.multiplyMM(translationMatrixOne, 0,
                translationMatrixOne, 0,
                translationMatrixTwo, 0);

        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                translationMatrixOne, 0);

        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                rotationMatrix, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        // Send Necessary matrix to shader in respective uniform
        GLES32.glUniformMatrix4fv(mvpUniform,
                1,
                false,
                modelViewProjectionMatrix, 0);

        // Draw The Necessary Senn
        drawAeroPlane();
        // Upper Flag
//        drawFlag(gfFlagOrange_X1, gfFlagOrange_X2, gfFlagOrange_Y, gfFlagWhite_X1, gfFlagWhite_X2, gfFlagWhite_Y, gfFlagGreen_X1, gfFlagGreen_X2, gfFlagGreen_Y);

        // Middle Plain
        // Initialize above matrices to identity
        Matrix.setIdentityM(translationMatrixOne, 0);
        Matrix.setIdentityM(translationMatrixTwo, 0);
        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Do necessary transformations
        Matrix.translateM(translationMatrixOne, 0,
                gfMiddlePlaneTranslate_X, gfMiddlePlaneTranslate_Y, -6.0f);
        // Do necessary Matrix Multiplication
        Matrix.multiplyMM(translationMatrixOne, 0,
                translationMatrixOne, 0,
                translationMatrixTwo, 0);

        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                translationMatrixOne, 0);

        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                rotationMatrix, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        // Send Necessary matrix to shader in respective uniform
        GLES32.glUniformMatrix4fv(mvpUniform,
                1,
                false,
                modelViewProjectionMatrix, 0);

        // Draw The Necessary Senn
        drawAeroPlane();
        // Middle Flag
//        drawFlag(gfFlagOrange_X1, gfFlagOrange_X2, gfFlagOrange_Y, gfFlagWhite_X1, gfFlagWhite_X2, gfFlagWhite_Y, gfFlagGreen_X1, gfFlagGreen_X2, gfFlagGreen_Y);

        // Lower Plain
        // Initialize above matrices to identity
        Matrix.setIdentityM(translationMatrixOne, 0);
        Matrix.setIdentityM(translationMatrixTwo, 0);
        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Do necessary transformations
        Matrix.translateM(translationMatrixOne, 0,
                -2.5f, -3.0f, -4.0f);
        if (gfLowerLeftAngle > (Math.PI / 2))
        {
            gfLowerLeftAngle_X = 3.0f * (float) Math.cos(gfLowerLeftAngle);
            gfLowerLeftAngle_Y = 3.0f * (float) Math.sin(gfLowerLeftAngle);
            Matrix.translateM(translationMatrixOne, 0,
                    gfLowerLeftAngle_X, gfLowerLeftAngle_Y, -6.0f);
        }
        gfLowerLeftAngle -= 0.001f;

        // Do necessary Matrix Multiplication
        Matrix.multiplyMM(translationMatrixOne, 0,
                translationMatrixOne, 0,
                translationMatrixTwo, 0);

        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                translationMatrixOne, 0);

        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                rotationMatrix, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        // Send Necessary matrix to shader in respective uniform
        GLES32.glUniformMatrix4fv(mvpUniform,
                1,
                false,
                modelViewProjectionMatrix, 0);

        // Draw The Necessary Senn
        drawAeroPlane();
        // Lower Flag
//        drawFlag(gfFlagOrange_X1, gfFlagOrange_X2, gfFlagOrange_Y, gfFlagWhite_X1, gfFlagWhite_X2, gfFlagWhite_Y, gfFlagGreen_X1, gfFlagGreen_X2, gfFlagGreen_Y);

        if (gfMiddlePlaneTranslate_X >= 2.5f) {
            // Small Flag Adjustment
            gfSmallFlagOrange_X1 = gfSmallFlagWhite_X1 = gfSmallFlagGreen_X1 = -1.25f;
            // Upper Plane
            // Initialize above matrices to identity
            Matrix.setIdentityM(translationMatrixOne, 0);
            Matrix.setIdentityM(translationMatrixTwo, 0);
            Matrix.setIdentityM(rotationMatrix, 0);
            Matrix.setIdentityM(modelViewMatrix, 0);
            Matrix.setIdentityM(modelViewProjectionMatrix, 0);

            // Do necessary transformations
            Matrix.translateM(translationMatrixOne, 0,
                    4.0f, 3.0f, -4.0f);
            if (gfUpperRightAngle < (2.0f * Math.PI)) {
                gfUpperRightAngle_X = 3.0f * (float) Math.cos(gfUpperRightAngle);
                gfUpperRightAngle_Y = 3.0f * (float) Math.sin(gfUpperRightAngle);
                Matrix.translateM(translationMatrixOne, 0,
                        gfUpperRightAngle_X, gfUpperRightAngle_Y, -6.0f);
            }
            gfUpperRightAngle += 0.001f;

            // Do necessary Matrix Multiplication
            Matrix.multiplyMM(translationMatrixOne, 0,
                    translationMatrixOne, 0,
                    translationMatrixTwo, 0);

            Matrix.multiplyMM(modelViewMatrix, 0,
                    modelViewMatrix, 0,
                    translationMatrixOne, 0);

            Matrix.multiplyMM(modelViewMatrix, 0,
                    modelViewMatrix, 0,
                    rotationMatrix, 0);

            Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                    perspectiveProjectionMatrix, 0,
                    modelViewMatrix, 0);

            // Send Necessary matrix to shader in respective uniform
            GLES32.glUniformMatrix4fv(mvpUniform,
                    1,
                    false,
                    modelViewProjectionMatrix, 0);

            // Draw The Necessary Senn
            drawAeroPlane();
            // Upper Right Flag
//        drawFlag(gfFlagOrange_X1, gfFlagOrange_X2, gfFlagOrange_Y, gfFlagWhite_X1, gfFlagWhite_X2, gfFlagWhite_Y, gfFlagGreen_X1, gfFlagGreen_X2, gfFlagGreen_Y);

            // Lower Plane
            // Initialize above matrices to identity
            Matrix.setIdentityM(translationMatrixOne, 0);
            Matrix.setIdentityM(translationMatrixTwo, 0);
            Matrix.setIdentityM(rotationMatrix, 0);
            Matrix.setIdentityM(modelViewMatrix, 0);
            Matrix.setIdentityM(modelViewProjectionMatrix, 0);

            // Do necessary transformations
            Matrix.translateM(translationMatrixOne, 0,
                    4.0f, -3.0f, -4.0f);
            if (gfLowerRightAngle > 0) {
                gfLowerRightAngle_X = 3.0f * (float) Math.cos(gfLowerRightAngle);
                gfLowerRightAngle_Y = 3.0f * (float) Math.sin(gfLowerRightAngle);
                Matrix.translateM(translationMatrixOne, 0,
                        gfLowerRightAngle_X, gfLowerRightAngle_Y, -6.0f);
            }
            gfLowerRightAngle -= 0.001f;

            // Do necessary Matrix Multiplication
            Matrix.multiplyMM(translationMatrixOne, 0,
                    translationMatrixOne, 0,
                    translationMatrixTwo, 0);

            Matrix.multiplyMM(modelViewMatrix, 0,
                    modelViewMatrix, 0,
                    translationMatrixOne, 0);

            Matrix.multiplyMM(modelViewMatrix, 0,
                    modelViewMatrix, 0,
                    rotationMatrix, 0);

            Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                    perspectiveProjectionMatrix, 0,
                    modelViewMatrix, 0);

            // Send Necessary matrix to shader in respective uniform
            GLES32.glUniformMatrix4fv(mvpUniform,
                    1,
                    false,
                    modelViewProjectionMatrix, 0);

            // Draw The Necessary Senn
            drawAeroPlane();
        }
    }

    void drawAeroPlane()
    {
        GLES32.glBindVertexArray(vao[0]);
        drawSquare(0.3f, 0.1f, -0.4f, 0.1f, -0.4f, -0.1f, 0.3f, -0.1f);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,
                0,
                iSquarePoints);
        GLES32.glBindVertexArray(0);

        //Upper Wing
        GLES32.glBindVertexArray(vao[0]);
        drawTriangle(-0.4f, 0.5f, -0.2f, 0.1f, 0.2f, 0.1f);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES,
                0,
                iTrianglePoints);
        GLES32.glBindVertexArray(0);

        //Lower Wing
        GLES32.glBindVertexArray(vao[0]);
        drawTriangle(-0.4f, -0.5f, -0.2f, -0.1f, 0.2f, -0.1f);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES,
                0,
                iTrianglePoints);
        GLES32.glBindVertexArray(0);

        //Front Nosel
        GLES32.glBindVertexArray(vao[0]);
        drawTriangle(0.3f, 0.1f, 0.6f, 0.0f, 0.3f, -0.1f);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES,
                0,
                iTrianglePoints);
        GLES32.glBindVertexArray(0);

        // Back Tail
        GLES32.glBindVertexArray(vao[0]);
        drawSquare(-0.4f, 0.1f, -0.6f, 0.3f, -0.6f, -0.3f, -0.4f, -0.1f);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,
                0,
                iSquarePoints);
        GLES32.glBindVertexArray(0);

        drawIAF();
        drawFlag(gfFlagOrange_X1, gfFlagOrange_X2, gfFlagOrange_Y, gfFlagWhite_X1, gfFlagWhite_X2, gfFlagWhite_Y, gfFlagGreen_X1, gfFlagGreen_X2, gfFlagGreen_Y);
    }

    void drawFlag(float fFlagOrange_X1, float fFlagOrange_X2, float fFlagOrange_Y, float fFlagWhite_X1, float fFlagWhite_X2, float fFlagWhite_Y, float fFlagGreen_X1, float fFlagGreen_X2, float fFlagGreen_Y)
    {
        if (fFlagOrange_X2== 0 || fFlagWhite_X2 == 0 || fFlagGreen_X2 == 0)
        {
            fFlagOrange_X2 = fFlagWhite_X2 = fFlagGreen_X2 = -0.6f;
        }
        GLES32.glLineWidth(6.0f);
        GLES32.glBindVertexArray(vao[0]);
        drawLine(fFlagOrange_X1, fFlagOrange_Y, fFlagOrange_X2, fFlagOrange_Y, 1.0f, 0.6f, 0.2f);
        GLES32.glDrawArrays(GLES32.GL_LINES,
                0,
                iLinePoints);
        drawLine(fFlagWhite_X1, fFlagWhite_Y, fFlagWhite_X2, fFlagWhite_Y, 1.0f, 1.0f, 1.0f);
        GLES32.glDrawArrays(GLES32.GL_LINES,
                0,
                iLinePoints);
        drawLine(fFlagGreen_X1, fFlagGreen_Y, fFlagGreen_X2, fFlagGreen_Y, 0.07f, 0.53f, 0.02f);
        GLES32.glDrawArrays(GLES32.GL_LINES,
                0,
                iLinePoints);
        GLES32.glBindVertexArray(0);
    }

    void drawIAF()
    {
        // ********************draw IAF **************************
        // For I
        GLES32.glBindVertexArray(vao[0]);
        drawLine(-0.35f,0.1f,-0.35f,-0.1f,0.0f,1.0f,0.0f);
        GLES32.glLineWidth(3.0f);
        GLES32.glDrawArrays(GLES32.GL_LINES,
                0,
                iLinePoints);
        GLES32.glBindVertexArray(0);

        // For A
        GLES32.glBindVertexArray(vao[0]);
        float LinePositionA[] = new float[]
                {0.0f,-0.1f,0.0f,
                        -0.1f,0.1f,0.0f,
                        -0.2f,-0.1f,0.0f
                };
        float LineColorA[] = new float[]
                {
                        0.0f, 1.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 1.0f, 0.0f
                };

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPosition[0]);

        ByteBuffer ForAPosByteBuffer = ByteBuffer.allocateDirect(LinePositionA.length * 4);
        ForAPosByteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer ForAVerticesBuffer = ForAPosByteBuffer.asFloatBuffer();
        ForAVerticesBuffer.put(LinePositionA);
        ForAVerticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                LinePositionA.length * 4,
                ForAVerticesBuffer,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3,
                GLES32.GL_FLOAT,
                false, 0, 0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        // Release the buffers for vertices
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor[0]);

        ByteBuffer AiroplainColByteBufferA = ByteBuffer.allocateDirect(LineColorA.length * 4);
        AiroplainColByteBufferA.order(ByteOrder.nativeOrder());
        FloatBuffer AiroplainColorsBufferA= AiroplainColByteBufferA.asFloatBuffer();
        AiroplainColorsBufferA.put(LineColorA);
        AiroplainColorsBufferA.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                LineColorA.length * 4,
                AiroplainColorsBufferA,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3,
                GLES32.GL_FLOAT,
                false, 0, 0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

        // Release the buffer for colors:
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glLineWidth(3.0f);
        GLES32.glDrawArrays(GLES32.GL_LINE_STRIP,
                0,
                3);
        GLES32.glBindVertexArray(0);

        // For F
        GLES32.glBindVertexArray(vao[0]);
        float LinePositionF[] = new float[]
                {
                        0.05f,-0.1f,0.0f,
                        0.05f,0.1f,0.0f,
                        0.05f,0.1f,0.0f,
                        0.20f,0.1f,0.0f,
                        0.05f,0.1f,0.0f,
                        0.05f,0.0f,0.0f,
                        0.20f,0.0f,0.0f
                };
        float LineColorF[] = new float[]
                {
                        0.0f, 1.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                };

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPosition[0]);

        ByteBuffer ForFPosByteBuffer = ByteBuffer.allocateDirect(LinePositionF.length * 4);
        ForFPosByteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer ForFVerticesBuffer = ForFPosByteBuffer.asFloatBuffer();
        ForFVerticesBuffer.put(LinePositionF);
        ForFVerticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                LinePositionF.length * 4,
                ForFVerticesBuffer,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3,
                GLES32.GL_FLOAT,
                false, 0, 0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        // Release the buffers for vertices
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor[0]);

        ByteBuffer AiroplainColByteBufferF = ByteBuffer.allocateDirect(LineColorF.length * 4);
        AiroplainColByteBufferF.order(ByteOrder.nativeOrder());
        FloatBuffer AiroplainColorsBufferF= AiroplainColByteBufferF.asFloatBuffer();
        AiroplainColorsBufferF.put(LineColorF);
        AiroplainColorsBufferF.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                LineColorF.length * 4,
                AiroplainColorsBufferF,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3,
                GLES32.GL_FLOAT,
                false, 0, 0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

        // Release the buffer for colors:
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glLineWidth(3.0f);
        GLES32.glDrawArrays(GLES32.GL_LINE_STRIP,
                0,
                7);
        GLES32.glBindVertexArray(0);
    }

    void drawSquare(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {
        // function declaration

        // Variable declaration
        float fSquareVertices[] = new  float[3 * iSquarePoints];
        float fSquareColor[] = new float[3 * iSquarePoints];

        // Vertices
        fSquareVertices[0] = x1;
        fSquareVertices[1] = y1;
        fSquareVertices[2] = 0;

        fSquareVertices[3] = x2;
        fSquareVertices[4] = y2;
        fSquareVertices[5] = 0;

        fSquareVertices[6] = x3;
        fSquareVertices[7] = y3;
        fSquareVertices[8] = 0;

        fSquareVertices[9] = x4;
        fSquareVertices[10] = y4;
        fSquareVertices[11] = 0;

        // Color
        fSquareColor[0] = 0.7f;
        fSquareColor[1] = 0.8f;
        fSquareColor[2] = 0.9f;

        fSquareColor[3] = 0.7f;
        fSquareColor[4] = 0.8f;
        fSquareColor[5] = 0.9f;

        fSquareColor[6] = 0.7f;
        fSquareColor[7] = 0.8f;
        fSquareColor[8] = 0.9f;

        fSquareColor[9] = 0.7f;
        fSquareColor[10] = 0.8f;
        fSquareColor[11] = 0.9f;

        GLES32.glGenBuffers(1, vboPosition, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPosition[0]);

        ByteBuffer AiroplainPosByteBuffer = ByteBuffer.allocateDirect(fSquareVertices.length * 4);
        AiroplainPosByteBuffer.order(ByteOrder.nativeOrder());
        positionBuffer = AiroplainPosByteBuffer.asFloatBuffer();
        positionBuffer.put(fSquareVertices);
        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fSquareVertices.length * 4,
                positionBuffer,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3,
                GLES32.GL_FLOAT,
                false, 0, 0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        // Release the buffers for vertices
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vboColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor[0]);

        ByteBuffer AiroplainColByteBuffer = ByteBuffer.allocateDirect(fSquareColor.length * 4);
        AiroplainColByteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer colorBuffer= AiroplainColByteBuffer.asFloatBuffer();
        colorBuffer.put(fSquareColor);
        colorBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fSquareColor.length* 4,
                colorBuffer,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3,
                GLES32.GL_FLOAT,
                false, 0, 0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

        // Release the buffer for colors:
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
    }

    void drawTriangle(float x1, float y1, float x2, float y2, float x3, float y3) {
        // function declaration

        // Variable declaration
        float fTriangleVertices[] = new float[3 * iTrianglePoints];
        float fTriangleColor[] = new float[3 * iTrianglePoints];

        //Code
        //Triangle Vertices
        fTriangleVertices[0] = x1;
        fTriangleVertices[1] = y1;
        fTriangleVertices[2] = 0.0f;

        fTriangleVertices[3] = x2;
        fTriangleVertices[4] = y2;
        fTriangleVertices[5] = 0.0f;

        fTriangleVertices[6] = x3;
        fTriangleVertices[7] = y3;
        fTriangleVertices[8] = 0.0f;

        //Triangle Color
        fTriangleColor[0] = 0.7f;
        fTriangleColor[1] = 0.8f;
        fTriangleColor[2] = 0.9f;

        fTriangleColor[3] = 0.7f;
        fTriangleColor[4] = 0.8f;
        fTriangleColor[5] = 0.9f;

        fTriangleColor[6] = 0.7f;
        fTriangleColor[7] = 0.8f;
        fTriangleColor[8] = 0.9f;

        GLES32.glGenBuffers(1, vboPosition, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPosition[0]);

        ByteBuffer AiroplainPosByteBuffer = ByteBuffer.allocateDirect(fTriangleVertices.length * 4);
        AiroplainPosByteBuffer.order(ByteOrder.nativeOrder());
        positionBuffer = AiroplainPosByteBuffer.asFloatBuffer();
        positionBuffer.put(fTriangleVertices);
        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fTriangleVertices.length * 4,
                positionBuffer,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3,
                GLES32.GL_FLOAT,
                false, 0, 0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        // Release the buffers for vertices
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glGenBuffers(1, vboColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor[0]);

        ByteBuffer AiroplainColByteBuffer = ByteBuffer.allocateDirect(fTriangleColor.length * 4);
        AiroplainColByteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer colorBuffer= AiroplainColByteBuffer.asFloatBuffer();
        colorBuffer.put(fTriangleColor);
        colorBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fTriangleColor.length* 4,
                colorBuffer,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3,
                GLES32.GL_FLOAT,
                false, 0, 0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

        // Release the buffer for colors:
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

    }

    void drawLine(float x1, float y1, float x2, float y2, float r, float g, float b){
        // Variable declaration
        float[] fLineVertices = new float[2 * iLinePoints];
        float[] fLineColor = new float[3 * iLinePoints];

        // Vertices
        fLineVertices[0] = x1;
        fLineVertices[1] = y1;
        fLineVertices[2] = 0.0f;

        fLineVertices[3] = x2;
        fLineVertices[4] = y2;
        fLineVertices[5] = 0.0f;

        // Color
        fLineColor[0] = r;
        fLineColor[1] = g;
        fLineColor[2] = b;

        fLineColor[3] = r;
        fLineColor[4] = g;
        fLineColor[5] = b;


        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPosition[0]);

        ByteBuffer ForAPosByteBuffer = ByteBuffer.allocateDirect(fLineVertices.length * 4);
        ForAPosByteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer ForAVerticesBuffer = ForAPosByteBuffer.asFloatBuffer();
        ForAVerticesBuffer.put(fLineVertices);
        ForAVerticesBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fLineVertices.length * 4,
                ForAVerticesBuffer,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3,
                GLES32.GL_FLOAT,
                false, 0, 0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        // Release the buffers for vertices
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboColor[0]);

        ByteBuffer AiroplainColByteBuffer = ByteBuffer.allocateDirect(fLineColor.length * 4);
        AiroplainColByteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer AiroplainColorsBuffer= AiroplainColByteBuffer.asFloatBuffer();
        AiroplainColorsBuffer.put(fLineColor);
        AiroplainColorsBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fLineColor.length * 4,
                AiroplainColorsBuffer,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3,
                GLES32.GL_FLOAT,
                false, 0, 0);

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);

        // Release the buffer for colors:
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

    }

    private void update() {
        if (gfLIX <= -0.4f)
        {
            gfLIX += 0.001f;
        }
        else
        {
            if (gfAX >= 0.4f)
            {
                gfAX -= 0.001f;
            }
            if (gfAXR >= 2.4f)
            {
                gfAXR -= 0.001f;
            }
            else
            {
                if (gfNY >= 0.0f)
                {
                    gfNY -= 0.001f;
                }
                else
                {
                    if (gfRIY <= 0.0f)
                    {
                        gfRIY += 0.001f;
                    }
                    else
                    {
                        if (gfORC <= 1.0f)
                        {
                            gfORC += 0.001f;
                        }
                        if (gfOGC <= 0.6f)
                        {
                            gfOGC += 0.001f;
                        }
                        if (gfOBC <= 0.2f)
                        {
                            gfOBC += 0.001f;
                        }
                        if (gfGRC <= 0.07f)
                        {
                            gfGRC += 0.001f;
                        }
                        if (gfGGC <= 0.53f)
                        {
                            gfGGC += 0.001f;
                        }
                        if (gfGBC <= 0.02f)
                        {
                            gfGBC += 0.001f;
                        }
                        else {
                            if ((gfORC >= 1.0f) && (gfOGC >= 0.6f) && (gfOBC >= 0.2f) && (gfGRC >= 0.07f) && (gfGGC >= 0.53f) && (gfGBC >= 0.02f)) {
                                gbAnimatePlain = true;
                                updatePlain();
                            }
                        }
                    }
                }
            }
        }
    }

    private void updatePlain() {
        // Code:
        if (gfUpperLeftAngle_X <= 0.0f)
        {
            gfUpperLeftAngle_X += 0.001f;
        }
        if (gfUpperLeftAngle_Y >= 0.0f)
        {
            gfUpperLeftAngle_Y -= 0.001f;
        }
        if (gfMiddlePlaneTranslate_X <= 6.5f)
        {
            gfMiddlePlaneTranslate_X += 0.001f;
            gfFlagOrange_X1 -= 0.001f;
            gfFlagWhite_X1 -= 0.001f;
            gfFlagGreen_X1 -= 0.001f;
        }
        if (gfLowerLeftAngle_X <= 0.0f)
        {
            gfLowerLeftAngle_X += 0.001f;
        }
        if (gfLowerLeftAngle_Y <= 0.0f)
        {
            gfLowerLeftAngle_Y += 0.001f;
        }
    }

    void uninitialize() {
        // Code:
        // Detach and delete the shaders one by one that are attached to the the shader program object
        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                // Detach vertex shader from the shader program and then delete it:
                GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }

            if(fragmentShaderObject != 0)
            {
                // Detach fragment shader from the shader program and then delete it:
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        // Delete the shader program object
        if(shaderProgramObject != 0)
        {
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }
}


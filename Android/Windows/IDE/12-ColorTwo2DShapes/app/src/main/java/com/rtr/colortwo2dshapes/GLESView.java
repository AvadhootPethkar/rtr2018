package com.rtr.colortwo2dshapes;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32; // 32 = version 3.2
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

// New additions
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vaoTriangle = new int[1];
    private int[] vboTrianglePosition = new int[1];
    private int[] vboTriangleColor = new int[1];
    private int[] vaoRectangle = new int[1];
    private int[] vboRectanglePosition = new int[1];
    private int mvpUniform;

    private float[] perspectiveProjectionMatrix = new float[16];

    private float angleTriangle;
    private float angleRectangle;

    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;

        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector = new GestureDetector(context, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int eventAction = event.getAction();
        if (!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);

        return(true);
    }

    @Override
    public boolean onDoubleTap(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onDown(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY)
    {
        return(true);
    }

    @Override
    public void onLongPress(MotionEvent event)
    {
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX, float distanceY)
    {
        System.exit(0);
        return(true);
    }

    @Override
    public void onShowPress(MotionEvent event)
    {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event)
    {
        return(true);
    }

    // Implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        //String version = gl.glGetString(GL10.GL_VERSION);
        String version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

    // Our Custom Methods
    private void initialize()
    {
        GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        final String vertexShaderSourceCode = String.format(
                "#version 320 es" +
                        "\n" +
                        "in vec4 vPosition;" +
                        "in vec4 vColor;" +
                        "uniform mat4 u_mvp_matrix;" +
                        "out vec4 out_color;" +
                        "void main(void)" +
                        "{" +
                        "	gl_Position = u_mvp_matrix * vPosition;" +
                        "	out_color = vColor;" +
                        "}");

        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        //Error Checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR:Vertex Shader Log : "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // FRAGMENT SHADER
        // Define Fragment Shader Object
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        // Write Vertex Shader Code
        final String fragmentShaderSourceCode = String.format(
                "#version 320 es" +
                        "\n" +
                        "precision highp float;" +
                        "in vec4 out_color;" +
                        "out vec4 FragColor;" +
                        "void main(void)" +
                        "{" +
                        "	FragColor = out_color;" +
                        "}"
        );
        // Specify above Source Code To The Fragment Shader Object
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        // Compile the FragmentShader
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking
        iShaderCompileStatus[0] = 0;
        iInfoLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR: Fragment Shader: " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Create Shader Program Object
        shaderProgramObject = GLES32.glCreateProgram();
        // Attach Vertex Shader To Shader Program
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        // Attach Fragment Shader To Shader Program
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Prelinking binding to vertex attribute
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");

        // Link The Shader Program
        GLES32.glLinkProgram(shaderProgramObject);

        // Error checking
        int[] iProgramLinkStatus = new int[1];
        iInfoLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if (iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("Shader Program Link Log : " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Post Linking Retriving UniformLocation
        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        final float[] triangleVertices = new float[]
                {
                        0.0f, 1.0f, 0.0f,
                        -1.0f, -1.0f, 0.0f,
                        1.0f, -1.0f, 0.0f
                };

        // rectangle Position
        final float[] rectangleVertices = new float[]
                {
                        1.0f,  1.0f, 0.0f,
                        -1.0f,  1.0f, 0.0f,
                        -1.0f, -1.0f, 0.0f,
                        1.0f, -1.0f, 0.0f
                };

        // triangle color
        final float[] triangleColors = new float[]
                {

                        1.0f, 0.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 0.0f, 1.0f,
                };

        // Create vaoTriangle
        GLES32.glGenVertexArrays(1, vaoTriangle, 0);
        GLES32.glBindVertexArray(vaoTriangle[0]);
        GLES32.glGenBuffers(1, vboTrianglePosition, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboTrianglePosition[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length * 4);

        byteBuffer.order(ByteOrder.nativeOrder());

        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        positionBuffer.put(triangleVertices);

        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                triangleVertices.length * 4,
                positionBuffer,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // Triangle Color
        GLES32.glGenBuffers(1, vboTriangleColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboTriangleColor[0]);

        ByteBuffer byteBufferTriangleColor = ByteBuffer.allocateDirect(triangleColors.length * 4);

        byteBufferTriangleColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferTriangle = byteBufferTriangleColor.asFloatBuffer();

        colorBufferTriangle.put(triangleColors);

        colorBufferTriangle.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                triangleColors.length * 4,
                colorBufferTriangle,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // rectangle
        GLES32.glGenVertexArrays(1,vaoRectangle, 0);
        GLES32.glBindVertexArray(vaoRectangle[0]);

        GLES32.glGenBuffers(1, vboRectanglePosition,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboRectanglePosition[0]);

        ByteBuffer byteBufferRectangle = ByteBuffer.allocateDirect(rectangleVertices.length * 4);

        byteBufferRectangle.order(ByteOrder.nativeOrder());

        FloatBuffer positionBufferRectangle = byteBufferRectangle.asFloatBuffer();

        positionBufferRectangle.put(rectangleVertices);

        positionBufferRectangle.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                rectangleVertices.length * 4,
                positionBufferRectangle,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);

        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, 0.0f, 0.0f, 1.0f);

        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);


        GLES32.glClearDepthf(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
    }

    private void resize(int width, int height)
    {
        if(height <= 0)
        {
            height = 1;
        }
        GLES32.glViewport(0, 0, width, height);

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0,
                45.0f,
                (float)width / (float)height,
                0.1f,
                100.0f);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        GLES32.glUseProgram(shaderProgramObject);

        // Declaration of matrices
        float[] translationMatrix = new float[16];
        float[] rotationMatrix = new float[16];
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

        //Triangle
        // Initialize above matrices to identity
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Do necessary transformations
        Matrix.translateM(translationMatrix, 0,
                -1.5f, 0.0f, -6.0f);

        Matrix.setRotateM(rotationMatrix, 0,
                angleTriangle,0.0f, 1.0f, 0.0f);

        // Do necessary Matrix Multiplication
        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                translationMatrix, 0);

        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                rotationMatrix, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        // Send Necessary matrix to shader in respective uniform
        GLES32.glUniformMatrix4fv(mvpUniform,
                1,
                false,
                modelViewProjectionMatrix, 0);

        // Bind with vaoTriangle
        GLES32.glBindVertexArray(vaoTriangle[0]);

        // Similarly bind With Textures If Any


        // Draw The Necessary Senn
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES,
                0,
                3);

        // Unbind vaoTriangle
        GLES32.glBindVertexArray(0);

        // Rectangle
        // Initialize above matrices to identity
        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Do necessary transformations
        Matrix.translateM(translationMatrix, 0,
                1.5f, 0.0f, -6.0f);

        Matrix.setRotateM(rotationMatrix, 0,
                angleRectangle,1.0f, 0.0f, 0.0f);

        // Do necessary Matrix Multiplication
        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                translationMatrix, 0);

        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                rotationMatrix, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        // Send Necessary matrix to shader in respective uniform
        GLES32.glUniformMatrix4fv(mvpUniform,
                1,
                false,
                modelViewProjectionMatrix, 0);

        // Bind with vaoTriangle
        GLES32.glBindVertexArray(vaoRectangle[0]);

        // Similarly bind With Textures If Any


        // Draw The Necessary Senn
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,
                0,
                4);

        // Unbind vaoTriangle
        GLES32.glBindVertexArray(0);

        // Unuse Program
        GLES32.glUseProgram(0);
        update();

        requestRender();
    }

    private void uninitialize() {
        if (vboTrianglePosition[0] != 0) {
            GLES32.glDeleteBuffers(1, vboTrianglePosition, 0);
            vboTrianglePosition[0] = 0;
        }

        if (vaoTriangle[0] != 0) {
            GLES32.glDeleteVertexArrays(1, vaoTriangle, 0);
            vaoTriangle[0] = 0;
        }

        if (vboRectanglePosition[0] != 0) {
            GLES32.glDeleteBuffers(1, vboRectanglePosition, 0);
            vboRectanglePosition[0] = 0;
        }

        if (vaoRectangle[0] != 0) {
            GLES32.glDeleteVertexArrays(1, vaoRectangle, 0);
            vaoRectangle[0] = 0;
        }

        if (shaderProgramObject != 0) {
            int[] shaderCount = new int[1];
            int shaderNumber;

            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            int[] shaders = new int[shaderCount[0]];

            GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);

            for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++) {
                // detach shader
                GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

                // delete shader
                GLES32.glDeleteShader(shaders[shaderNumber]);
                shaders[shaderNumber] = 0;
            }

            GLES32.glUseProgram(0);
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }

    private void update() {
        if (angleTriangle >= 360.0f)
        {
            angleTriangle = 0.0f;
        }
        else
        {
            angleTriangle += 2.0f;
        }

        if (angleRectangle >= 360.0f)
        {
            angleRectangle = 0.0f;
        }
        else
        {
            angleRectangle += 2.0f;
        }
    }
}

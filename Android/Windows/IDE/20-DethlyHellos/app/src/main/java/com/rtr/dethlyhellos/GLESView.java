package com.rtr.dethlyhellos;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32; // 32 = version 3.2
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

// New additions
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class GLESView  extends GLSurfaceView implements GLSurfaceView.Renderer, GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    // Circle Variables
    private int[] vaoCircle = new int[1];
    private int[] vboCirclePosition = new int[1];
    private int[] vboCircleColor = new int[1];
    static int iCirclePoints = 0;
    final int iPoints = 10000;
    private float fRadiusOfInCircle;
    private float fOffsetX;
    private float fOffsetY;
    private FloatBuffer positionBufferCircle;
    private FloatBuffer colorBufferCircle;

    // Triangle Variables
    private int[] vaoTriangle = new int[1];
    private int[] vboTrianglePosition = new int[1];
    private int[] vboTriangleColor = new int[1];
    private FloatBuffer positionBufferTriangle;
    private FloatBuffer colorBufferTriange;
    final int iTrianglePoints = 3;
    private float fTriangleVertices[] = new float[3 * iTrianglePoints];
    private float fTriangleColor[] = new float[3 * iTrianglePoints];

    // Line Variables
    private int[] vaoLine = new int[1];
    private int[] vboLinePosition = new int[1];
    private int[] vboLineColor = new int[1];
    final int iLinePoints = 2;
    private FloatBuffer positionBufferLine;
    private FloatBuffer colorBufferLine;

    private int mvpUniform;
    private float perspectiveProjectionMatrix[] = new float[16];

    // Animating Variables
    private float gfTriangleX = 1.0f;
    private float gfTriangleY = -1.0f;
    private float gfCircleX = -1.0f;
    private float gfCircleY = -1.0f;
    private float fAngle = 0.0f;
    private float gfLineY = 1.0f;

    // Constructor:
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;

        // Set the EGLContext to current supported version of OpenGL-ES:
        setEGLContextClientVersion(3);

        // Set the renderer:
        setRenderer(this);

        // Set the render mode to render only when there is change in the drawing data:
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector = new GestureDetector(context, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }

    // Initialize function:
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        // Print OpenGL ES version:
        String glesVersion = gl.glGetString(GL10.GL_VERSION);
        System.out.println("AMC : OpenGL ES Version : " + glesVersion);

        // Print OpenGL Shading Language version:
        String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("AMC : OpenGL Shading Language Version : " + glslVersion);

        // Print supported OpenGL extensions:
        //String glesExtensions = gl.glGetString(GL10.GL_EXTENSIONS);
        //System.out.println("AMC : OpenGL Extensions supported on this device : " + glesExtensions);

        initialize(gl);
    }

    // Resize funtion:
    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    // Display function:
    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

    // 0. Handle 'onTouchEvent' because it triggers all gesture and tap events:
    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        int eventAction = e.getAction();
        if(!gestureDetector.onTouchEvent(e))
            super.onTouchEvent(e);
        return(true);
    }

    // 1. Double Tap:
    @Override
    public boolean onDoubleTap(MotionEvent e)
    {
        return(true);
    }

    // 2. Double Tap event:
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)
    {
        return(true);
    }

    // 3. Single Tap:
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)
    {
        return(true);
    }

    // 4. On Down:
    @Override
    public boolean onDown(MotionEvent e)
    {
        return(true);
    }

    // 5. On Single Tap UP:
    @Override
    public boolean onSingleTapUp(MotionEvent e)
    {
        return(true);
    }

    // 6. Fling:
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        return(true);
    }

    // 7. Scroll: (Exit the program)
    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    {
        uninitialize();
        System.exit(0);
        return(true);
    }

    // 8. Long Press:
    @Override
    public void onLongPress(MotionEvent e)
    {
    }

    // 9. Show Press:
    @Override
    public void onShowPress(MotionEvent e)
    {
    }

    private void initialize(GL10 gl)
    {
        // Variable Declaration
        float coX1 = 0.0f;
        float coY1 = 0.5f;
        float coX2 = -0.5f;
        float coY2 = -0.5f;
        float coX3 = 0.5f;
        float coY3 = -0.5f;
        float fdistAB = 0.0f;
        float fdistBC = 0.0f;
        float fdistAC = 0.0f;
        float fPerimeter = 0.0f;
        float fSemiPerimeter = 0.0f;
        float fAreaOfTriangle = 0.0f;
        float fTriangleHeight = 0.0f;

        /*<-- VERTEX SHADER -->*/

        // Create vertex shader
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        // Vertex shader source code:
        final String vertexShaderSourceCode = String.format (
                "#version 320 es"+
                        "\n" +
                        "in vec4 vPosition;" +
                        "in vec4 vColor;" +
                        "uniform mat4 u_mvp_matrix;" +
                        "out vec4 out_color;" +
                        "void main(void)" +
                        "{" +
                        "gl_Position = u_mvp_matrix * vPosition;" +
                        "out_color = vColor;" +
                        "}"
        );

        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        // Error checking:
        int[] iShaderCompilationStatus = new int[1];	// 1 member integer array
        int[] iInfoLogLength = new int[1];				// 1 member integer array
        String szInfoLog = null;						// String to store the log

        // As there are no addresses in Java, we use an array of size 1
        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
        if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)	// Compilation failed
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR:Vertex Shader Log : "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // FRAGMENT SHADER
        // Define Fragment Shader Object
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        // Fragment shader source code:
        final String FragmentShaderSourceCode = String.format (
                "#version 320 es"+
                        "\n" +
                        "precision highp float;" +
                        "in vec4 out_color;" +
                        "out vec4 FragColor;" +
                        "void main(void)" +
                        "{" +
                        "   FragColor = out_color;" +
                        "}"
        );

        // Specify above Source Code To The Fragment Shader Object
        GLES32.glShaderSource(fragmentShaderObject, FragmentShaderSourceCode);
        // Compile the shader and check For any errors:
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking:
        iShaderCompilationStatus[0] = 0;			// Re initialize
        iInfoLogLength[0] = 0;						// Re initialize
        szInfoLog = null;							// Re initialize

        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompilationStatus, 0);
        if(iShaderCompilationStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR: Fragment Shader: "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Create the Shader Program:
        shaderProgramObject = GLES32.glCreateProgram();

        // Attach Vertex shader to the shader program:
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);

        // Attach the Fragment shader to the shader program:
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Pre-link binding of shader program object with vertex shader attributes:
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_COLOR, "vColor");

        // Link the two shaders together to shader program object to get a single executable and check For errors:
        GLES32.glLinkProgram(shaderProgramObject);

        // Error checking:
        int[] iShaderProgramLinkStatus = new int[1];		// Linking check
        iInfoLogLength[0] = 0;								// Re initialize
        szInfoLog = null;									// Re initialize

        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iShaderProgramLinkStatus, 0);
        if(iShaderProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
            if(iInfoLogLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("Shader Program link log = "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Get MVP UniForm location:
        // The actual locations assigned to uniForm variables are not known until the program object is linked successfully.
        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");

        // Distance Calculatin between triangle lines
        fdistAB = getDistance(coX1, coX2, coY1, coY2);
        fdistBC = getDistance(coX2, coX3, coY2, coY3);
        fdistAC = getDistance(coX1, coX3, coY1, coY3);

        fPerimeter = fdistAB + fdistBC + fdistAC;
        fSemiPerimeter = fPerimeter / 2;

        fRadiusOfInCircle = (float)Math.sqrt((fSemiPerimeter - fdistAB) * (fSemiPerimeter - fdistBC) * (fSemiPerimeter - fdistAC) / fSemiPerimeter);
        fOffsetX = getOffset(coX1, coX2, coX3, fdistAB, fdistBC, fdistAC, fPerimeter);
        fOffsetY = getOffset(coY1, coY2, coY3, fdistAB, fdistBC, fdistAC, fPerimeter);

        final float[] triangleVertices = new float[]
                {
                        0.0f, 0.5f, 0.0f,
                        -0.5f, -0.5f, 0.0f,
                        0.5f, -0.5f, 0.0f
                };

        // triangle color
        final float[] triangleColors = new float[]
                {

                        1.0f, 0.0f, 0.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 0.0f, 1.0f,
                };

        final float[] lineVertices = new float[]
                {
                        0.0f, 0.5f, 0.0f,
                        0.0f, -0.5f, 0.0f,
                };

        // triangle color
        final float[] lineColors = new float[]
                {

                        1.0f, 0.0f, 0.0f,
                        1.0f, 0.0f, 0.0f,
                        1.0f, 0.0f, 0.0f,
                };

        // Circle
        // Create vao
        GLES32.glGenVertexArrays(1, vaoCircle, 0);
        GLES32.glBindVertexArray(vaoCircle[0]);
        GLES32.glGenBuffers(1, vboCirclePosition, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboCirclePosition[0]);

        ByteBuffer byteBufferCirclePosition = ByteBuffer.allocateDirect(10000 * 3 * 4);

        byteBufferCirclePosition.order(ByteOrder.nativeOrder());

        positionBufferCircle = byteBufferCirclePosition.asFloatBuffer();

//        positionBuffer.put(fIVertices);

//        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                10000 * 3 * 4,
                null,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // Circle Color
        GLES32.glGenBuffers(1, vboCircleColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboCircleColor[0]);

        ByteBuffer byteBufferCircleColor = ByteBuffer.allocateDirect(10000 * 3 * 4);

        byteBufferCircleColor.order(ByteOrder.nativeOrder());

        colorBufferCircle = byteBufferCircleColor.asFloatBuffer();

//        colorBufferTriangle.put(triColor);

//        colorBufferTriangle.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                10000 * 3 * 4,
                null,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // Triangle
        // Create vao
        GLES32.glGenVertexArrays(1, vaoTriangle, 0);
        GLES32.glBindVertexArray(vaoTriangle[0]);
        GLES32.glGenBuffers(1, vboTrianglePosition, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboTrianglePosition[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length * 4);

        byteBuffer.order(ByteOrder.nativeOrder());

        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        positionBuffer.put(triangleVertices);

        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                triangleVertices.length * 4,
                positionBuffer,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // Triangle Color
        GLES32.glGenBuffers(1, vboTriangleColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboTriangleColor[0]);

        ByteBuffer byteBufferTriangleColor = ByteBuffer.allocateDirect(triangleColors.length * 4);

        byteBufferTriangleColor.order(ByteOrder.nativeOrder());

        FloatBuffer colorBufferTriangle = byteBufferTriangleColor.asFloatBuffer();

        colorBufferTriangle.put(triangleColors);

        colorBufferTriangle.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                triangleColors.length * 4,
                colorBufferTriangle,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // Line
        // Create vao
        GLES32.glGenVertexArrays(1, vaoLine, 0);
        GLES32.glBindVertexArray(vaoLine[0]);
        GLES32.glGenBuffers(1, vboLinePosition, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboLinePosition[0]);

        ByteBuffer byteBufferLinePosition = ByteBuffer.allocateDirect(lineVertices.length * 4);

        byteBufferLinePosition.order(ByteOrder.nativeOrder());

        FloatBuffer positionBufferLine = byteBufferLinePosition.asFloatBuffer();

        positionBufferLine.put(lineVertices);

        positionBufferLine.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                lineVertices.length * 4,
                positionBufferLine,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // Line Color
        GLES32.glGenBuffers(1, vboLineColor, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboLineColor[0]);

        ByteBuffer byteBufferLineColor = ByteBuffer.allocateDirect(lineColors.length * 4);

        byteBufferLineColor.order(ByteOrder.nativeOrder());

        colorBufferLine = byteBufferLineColor.asFloatBuffer();

        colorBufferLine.put(lineColors);

        colorBufferLine.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                lineColors.length * 4,
                null,
                GLES32.GL_DYNAMIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // Enable depth testing and specify the depth test to perForm:
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        // We will always cull back faces For better perFormance
        GLES32.glEnable(GLES32.GL_CULL_FACE);

        // Set the background color to Black:
        GLES32.glClearColor(0.0f, 0.0f, 1.0f, 0.0f);

        // Set projectionMatrix and Identity matrix:
        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
    }

    private void resize(int width, int height)
    {
        //Code:
        GLES32.glViewport(0, 0, width, height);

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f,
                ((float)width/(float)height), 0.1f, 100.0f);
    }

    public void display()
    {
        // Code:
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        GLES32.glUseProgram(shaderProgramObject);

        // Declaration of matrices
        float[] translationMatrix = new float[16];
        float[] scaleMatrix = new float[16];
        float[] rotationMatrix = new float[16];
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

        // Triangle
        // Initialize above matrices to identity
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(scaleMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(translationMatrix, 0,
                gfTriangleX, gfTriangleY, -3.0f);

        Matrix.setRotateM(rotationMatrix, 0,
                fAngle,0.0f, 1.0f, 0.0f);

        // Do necessary Matrix Multiplication
        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                translationMatrix, 0);

        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                rotationMatrix, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        // Send Necessary matrix to shader in respective uniform
        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoTriangle[0]);

        GLES32.glLineWidth(3.0f);
//        this.drawTriangle(0.5f);
        GLES32.glDrawArrays(GLES32.GL_LINE_LOOP,
                0,
                iTrianglePoints);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        // Circle
        // Initialize above matrices to identity
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(scaleMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0,
                gfCircleX, gfCircleY, -3.0f);

        Matrix.setRotateM(rotationMatrix, 0,
                fAngle,0.0f, 1.0f, 0.0f);

        // Multiply the modelview and projection matrix to get the modelviewprojection matrix:
        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                translationMatrix, 0);

        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                rotationMatrix, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoCircle[0]);

//        GLES32.glPointSize(3.0f);
        this.drawCircle(fRadiusOfInCircle, fOffsetX, fOffsetY);
        GLES32.glDrawArrays(GLES32.GL_POINTS,
                0,
                iPoints);
        // Unbind vao:
        GLES32.glBindVertexArray(0);

        // Line
        // Initialize above matrices to identity
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(scaleMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Translate the ModelViewMatrix and store back in the ModelViewMatrix:
        Matrix.translateM(modelViewMatrix, 0, modelViewMatrix, 0,
                -0.06f, gfLineY, -3.0f);

        // Do necessary Matrix Multiplication
        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                translationMatrix, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        GLES32.glUniformMatrix4fv(mvpUniform, 1, false, modelViewProjectionMatrix, 0);

        // Bind vao:
        GLES32.glBindVertexArray(vaoLine[0]);

        GLES32.glLineWidth(3.0f);
//        this.drawLine(0.5f);
        GLES32.glDrawArrays(GLES32.GL_LINE_LOOP,
                0,
                iLinePoints);

        // Unbind vao:
        GLES32.glBindVertexArray(0);

        // Stop using the shader program object:
        GLES32.glUseProgram(0);
        update();

        // Render/Flush:
        requestRender();
    }

    void uninitialize()
    {
        // Code:
        // Detach and delete the shaders one by one that are attached to the the shader program object
        if(shaderProgramObject != 0)
        {
            if(vertexShaderObject != 0)
            {
                // Detach vertex shader from the shader program and then delete it:
                GLES32.glDetachShader(shaderProgramObject, vertexShaderObject);
                GLES32.glDeleteShader(vertexShaderObject);
                vertexShaderObject = 0;
            }

            if(fragmentShaderObject != 0)
            {
                // Detach fragment shader from the shader program and then delete it:
                GLES32.glDetachShader(shaderProgramObject, fragmentShaderObject);
                GLES32.glDeleteShader(fragmentShaderObject);
                fragmentShaderObject = 0;
            }
        }

        // Delete the shader program object
        if(shaderProgramObject != 0)
        {
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }

    private void update() {
        if (gfTriangleX >= 0.0f)
        {
            gfTriangleX -= 0.001f;
            if (fAngle <= 360.0f)
            {
                fAngle += 0.1f;
            }
            else {
                fAngle = 0.0f;
            }
        }
        else {
            fAngle = 0.0f;
        }
        if (gfTriangleY <= 0.0f)
        {
            gfTriangleY += 0.001f;
            if (fAngle <= 360.0f)
            {
                fAngle += 0.1f;
            }
            else {
                fAngle = 0.0f;
            }
        }
        if (gfCircleX <= 0.0f)
        {
            gfCircleX += 0.001f;
        }
        if (gfCircleY <= 0.0f)
        {
            gfCircleY += 0.001f;
        }
        if (gfLineY >= 0.0f)
        {
            gfLineY -= 0.001f;
        }
    }

    float getDistance(float x1, float x2, float y1, float y2) {
        float xSquare = (float)Math.pow((double)(x2 - x1), 2.0);
        float ySquare = (float)Math.pow((double)(y2 - y1), 2.0);
        return (float)Math.sqrt((double)(xSquare + ySquare));
    }

    float getOffset(float coA, float coB, float coC, float distAB, float distBC, float distAC, float perimeter) {
        float value = (coA * distBC) + (coB * distAC) + (coC * distAB);
        return(value / perimeter);
    }

//    void drawTriangle(float fCoordinate) {
//        // function declaration
//
//        // Variable declaration
//
//        //Code
//        //Triangle Vertices
//        fTriangleVertices[0] = 0.0f;
//        fTriangleVertices[1] = fCoordinate;
//        fTriangleVertices[2] = 0.0f;
//
//        fTriangleVertices[3] = -fCoordinate;
//        fTriangleVertices[4] = -fCoordinate;
//        fTriangleVertices[5] = 0.0f;
//
//        fTriangleVertices[6] = fCoordinate;
//        fTriangleVertices[7] = -fCoordinate;
//        fTriangleVertices[8] = 0.0f;
//
//        //Triangle Color
//        fTriangleColor[0] = 0.0f;
//        fTriangleColor[1] = fCoordinate;
//        fTriangleColor[2] = 0.0f;
//
//        fTriangleColor[3] = 0.0f;
//        fTriangleColor[4] = fCoordinate;
//        fTriangleColor[5] = 0.0f;
//
//        fTriangleColor[6] = 0.0f;
//        fTriangleColor[7] = fCoordinate;
//        fTriangleColor[8] = 0.0f;
//
//        positionBufferTriangle.put(fTriangleVertices);
//        positionBufferTriangle.position(0);
//        // Triangle Vertices
//        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboTrianglePosition[0]);
//        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
//                fTriangleVertices.length,
//                positionBufferTriangle,
//                GLES32.GL_DYNAMIC_DRAW);
//        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
//
//        // Circle Color
//        colorBufferTriange.put(fTriangleColor);
//        colorBufferTriange.position(0);
//        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboTriangleColor[0]);
//        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
//                fTriangleColor.length,
//                colorBufferTriange,
//                GLES32.GL_DYNAMIC_DRAW);
//        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
//    }

//    void drawLine(float fCoordinate) {
//        // function declaration
//
//        // Variable declaration
//        float fLineVertices[] = new float[3 * iLinePoints];
//        float fLineColor[] = new float[3 * iLinePoints];
//
//        //Code
//        // Line Vertices
//        fLineVertices[0] = 0.0f;
//        fLineVertices[1] = fCoordinate;
//        fLineVertices[2] = 0.0f;
//
//        fLineVertices[3] = 0.0f;
//        fLineVertices[4] = -fCoordinate;
//        fLineVertices[5] = 0.0f;
//
//        // Line Color
//        fLineColor[0] = 1.0f;
//        fLineColor[1] = 0.0f;
//        fLineColor[2] = 0.0f;
//
//        fLineColor[3] = 1.0f;
//        fLineColor[4] = 0.0f;
//        fLineColor[5] = 0.0f;
//
//        positionBufferLine.put(fLineVertices);
//        positionBufferLine.position(0);
//        // Line Vertices
//        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboLinePosition[0]);
//        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
//                fLineVertices.length,
//                positionBufferLine,
//                GLES32.GL_DYNAMIC_DRAW);
//        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
//
//        colorBufferLine.put(fLineColor);
//        colorBufferLine.position(0);
//        // Circle Color
//        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboLineColor[0]);
//        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
//                fLineColor.length,
//                colorBufferLine,
//                GLES32.GL_DYNAMIC_DRAW);
//        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
//    }

    void drawCircle(float fRadius, float fOffsetX, float fOffsetY) {
        // function declaration

        // Variable declaration
        double dAngle;
        float fCircleVertices[] = new float[3 * iPoints];
        float fCircleColor[] = new float[3 * iPoints];

        // Code:
        for (int i = 0; i < iPoints; i +=3)
        {
            dAngle = 2 * Math.PI * i / iPoints;
            fCircleVertices[i] = fRadius * (float) Math.cos((double) dAngle) + fOffsetX;
            fCircleVertices[i+1] = fRadius * (float) Math.sin((double) dAngle) + fOffsetY;
            fCircleVertices[i+2] = 0.0f;

            fCircleColor[i] = 1.0f;
            fCircleColor[i+1] = 0.0f;
            fCircleColor[i+2] = 0.0f;

        }
        positionBufferCircle.put(fCircleVertices);
        positionBufferCircle.position(0);
        // Circle Vertices
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboCirclePosition[0]);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fCircleVertices.length,
                positionBufferCircle,
                GLES32.GL_DYNAMIC_DRAW);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        colorBufferCircle.put(fCircleColor);
        colorBufferCircle.position(0);
        // Circle Color
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboCircleColor[0]);
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                fCircleColor.length,
                colorBufferCircle,
                GLES32.GL_DYNAMIC_DRAW);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
    }
}

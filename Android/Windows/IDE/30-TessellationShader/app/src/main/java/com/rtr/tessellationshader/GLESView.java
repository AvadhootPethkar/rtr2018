package com.rtr.tessellationshader;

import android.content.Context;
import android.opengl.GLES32;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

// New additions

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, OnGestureListener, OnDoubleTapListener {
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int tessellationControlShaderObject;
    private int tessellationEvaluationShaderObject;
    private int shaderProgramObject;

    private int[] vao = new int[1];
    private int[] vbo = new int[1];
    private int gNumberOfSegmentsUniform;
    private int gNumberOfStripsUniform;
    private int gLineControlUniform;
    private int mvpUniform;

    private int gNumberOfLineSegments = 1;

    private float[] perspectiveProjectionMatrix = new float[16];

    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;

        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector = new GestureDetector(context, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int eventAction = event.getAction();
        if (!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);

        return(true);
    }

    @Override
    public boolean onDoubleTap(MotionEvent event)
    {
        gNumberOfLineSegments--;
        if (gNumberOfLineSegments <= 0)
        {
            gNumberOfLineSegments = 1;
        }
        return(true);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event)
    {
        gNumberOfLineSegments++;
        if (gNumberOfLineSegments >= 50)
        {
            gNumberOfLineSegments = 50;
        }
        return(true);
    }

    @Override
    public boolean onDown(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY)
    {
        return(true);
    }

    @Override
    public void onLongPress(MotionEvent event)
    {
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX, float distanceY)
    {
        System.exit(0);
        return(true);
    }

    @Override
    public void onShowPress(MotionEvent event)
    {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event)
    {
        return(true);
    }

    // Implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        //String version = gl.glGetString(GL10.GL_VERSION);
        String version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

    // Our Custom Methods
    private void initialize()
    {
        GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        final String vertexShaderSourceCode = String.format(
                "#version 320 es" +
                        "\n" +
                        "in vec2 vPosition;" +
                        "void main(void)" +
                        "{" +
                        "	gl_Position = vec4(vPosition, 0.0, 1.0);" +
                        "}");

        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        //Error Checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR:Vertex Shader Log : "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        tessellationControlShaderObject = GLES32.glCreateShader(GLES32.GL_TESS_CONTROL_SHADER);
        final String tessellationControlShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "layout(vertices=4)out;" +
            "uniform int numberOfSegments;" +
            "uniform int numberOfStrips;" +
            "void main(void)" +
            "{" +
            "	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;" +
            "	gl_TessLevelOuter[0] = float(numberOfStrips);" +
            "	gl_TessLevelOuter[1] = float(numberOfSegments);" +
            "}");

        GLES32.glShaderSource(tessellationControlShaderObject, tessellationControlShaderSourceCode);
        GLES32.glCompileShader(tessellationControlShaderObject);

        //Error Checking
        iShaderCompileStatus[0] = 0;
        iInfoLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetShaderiv(tessellationControlShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(tessellationControlShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(tessellationControlShaderObject);
                System.out.println("RTR:Vertex Shader Log : "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        tessellationEvaluationShaderObject = GLES32.glCreateShader(GLES32.GL_TESS_EVALUATION_SHADER);
        final String tessellationEvaluationShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "layout(isolines)in;" +
            "uniform mat4 u_mvp_matrix;" +
            "void main(void)" +
            "{" +
            "	float u = gl_TessCoord.x;" +
            "	vec3 p0 = gl_in[0].gl_Position.xyz;" +
            "	vec3 p1 = gl_in[1].gl_Position.xyz;" +
            "	vec3 p2 = gl_in[2].gl_Position.xyz;" +
            "	vec3 p3 = gl_in[3].gl_Position.xyz;" +
            "	float u1 = (1.0 - u);" +
            "	float u2 = u * u;" +
            "	float b3 = u2 * u;" +
            "	float b2 = 3.0 * u2 * u1;" +
            "	float b1 = 3.0 * u * u1 * u1;" +
            "	float b0 = u1 * u1 * u1;" +
            "	vec3 p = p0 * b0 + p1 * b1 + p2 * b2 + p3 * b3;" +
            "	gl_Position = u_mvp_matrix * vec4(p, 1.0);" +
            "}");

        GLES32.glShaderSource(tessellationEvaluationShaderObject, tessellationEvaluationShaderSourceCode);
        GLES32.glCompileShader(tessellationEvaluationShaderObject);

        //Error Checking
        iShaderCompileStatus[0] = 0;
        iInfoLength[0] = 0;
        szInfoLog = null;

        GLES32.glGetShaderiv(tessellationEvaluationShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(tessellationEvaluationShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(tessellationEvaluationShaderObject);
                System.out.println("RTR:Vertex Shader Log : "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // FRAGMENT SHADER
        // Define Fragment Shader Object
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        // Write Vertex Shader Code
        final String fragmentShaderSourceCode = String.format(
            "#version 320 es" +
            "\n" +
            "precision highp float;" +
            "uniform vec4 lineColor;" +
            "out vec4 FragColor;" +
            "void main(void)" +
            "{" +
            "	FragColor = lineColor;" +
            "}");

        // Specify above Source Code To The Fragment Shader Object
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        // Compile the FragmentShader
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking
        iShaderCompileStatus[0] = 0;
        iInfoLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR: Fragment Shader: " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Create Shader Program Object
        shaderProgramObject = GLES32.glCreateProgram();
        // Attach Vertex Shader To Shader Program
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        // Attach Tessellation Control Shader To Shader Program
        GLES32.glAttachShader(shaderProgramObject, tessellationControlShaderObject);
        // Attach Tessellation Evaluation Shader To Shader Program
        GLES32.glAttachShader(shaderProgramObject, tessellationEvaluationShaderObject);
        // Attach Fragment Shader To Shader Program
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Prelinking binding to vertex attribute
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");

        // Link The Shader Program
        GLES32.glLinkProgram(shaderProgramObject);

        // Error checking
        int[] iProgramLinkStatus = new int[1];
        iInfoLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if (iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("Shader Program Link Log : " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Post Linking Retriving UniformLocation
        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        gNumberOfSegmentsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "numberOfSegments");
        gNumberOfStripsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "numberOfStrips");
        gLineControlUniform = GLES32.glGetUniformLocation(shaderProgramObject, "lineColor");
        final float[] vertices = new float[]
        {
           -1.0f, -1.0f, -0.5f, 1.0f, 0.5f, -1.0f, 1.0f, 1.0f
        };

        // Create vao
        GLES32.glGenVertexArrays(1, vao, 0);
        GLES32.glBindVertexArray(vao[0]);
        GLES32.glGenBuffers(1, vbo, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);

        byteBuffer.order(ByteOrder.nativeOrder());

        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        positionBuffer.put(vertices);

        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                vertices.length * 4,
                positionBuffer,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                2, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        GLES32.glClearDepthf(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
    }

    private void resize(int width, int height)
    {
        if(height <= 0)
        {
            height = 1;
        }
        GLES32.glViewport(0, 0, width, height);

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0,
                45.0f,
                (float)width / (float)height,
                0.1f,
                100.0f);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        GLES32.glUseProgram(shaderProgramObject);

        // Declaration of matrices
        float[] translationMatrix = new float[16];
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

        // Initialize above matrices to identity
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Do necessary transformations
        Matrix.translateM(translationMatrix, 0,
                0.5f, 0.5f, -2.0f);


        // Do necessary Matrix Multiplication
        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                translationMatrix, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        // Send Necessary matrix to shader in respective uniform
        GLES32.glUniformMatrix4fv(mvpUniform,
                1,
                false,
                modelViewProjectionMatrix, 0);
        GLES32.glUniform1i(gNumberOfSegmentsUniform, gNumberOfLineSegments);
        GLES32.glUniform1i(gNumberOfStripsUniform, 1);
        final float[] colorRed = new float[]
                {1.0f, 0.0f, 0.0f, 1.0f};
        final float[] colorYellow = new float[]
                {1.0f, 1.0f, 0.0f, 1.0f};
        final float[] colorGreen = new float[]
                {0.0f, 1.0f, 0.0f, 1.0f};
        if (gNumberOfLineSegments == 1) {
            GLES32.glUniform4fv(gLineControlUniform,
                    1,
                    colorRed, 0);
        } else if ((gNumberOfLineSegments > 0) && (gNumberOfLineSegments < 50)) {
            GLES32.glUniform4fv(gLineControlUniform,
                    1,
                    colorYellow, 0);
        } else {
            GLES32.glUniform4fv(gLineControlUniform,
                    1,
                    colorGreen, 0);
        }
        // Bind with vao
        GLES32.glBindVertexArray(vao[0]);

        // Similarly bind With Textures If Any


        // Draw The Necessary Senn
        GLES32.glLineWidth(3.0f);
        GLES32.glDrawArrays(GLES32.GL_PATCHES,
                0,
                4);

        // Unbind vao
        GLES32.glBindVertexArray(0);

        // Unuse Program
        GLES32.glUseProgram(0);

        requestRender();
    }

    private void uninitialize() {
        if (vbo[0] != 0) {
            GLES32.glDeleteBuffers(1, vbo, 0);
            vbo[0] = 0;
        }

        if (vao[0] != 0) {
            GLES32.glDeleteVertexArrays(1, vao, 0);
            vao[0] = 0;
        }

        if (shaderProgramObject != 0) {
            int[] shaderCount = new int[1];
            int shaderNumber;

            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            int[] shaders = new int[shaderCount[0]];

            GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);

            for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++) {
                // detach shader
                GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

                // delete shader
                GLES32.glDeleteShader(shaders[shaderNumber]);
                shaders[shaderNumber] = 0;
            }

            GLES32.glUseProgram(0);
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }
}

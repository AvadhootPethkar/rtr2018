package com.rtr.texturepyramidandcude;

//Pyramind And Cube

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;

import android.opengl.GLSurfaceView;
import android.opengl.GLES32; // 32 = version 3.2
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

// New additions
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer, GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {
    private final Context context;
    private GestureDetector gestureDetector;

    private int vertexShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vaoPyramid = new int[1];
    private int[] vboPyramidPosition = new int[1];
    private int[] vboPyramidTexture = new int[1];
    private int[] vaoCube = new int[1];
    private int[] vboCubePosition = new int[1];
    private int[] vboCubeTexture = new int[1];
    private int mvpUniform;

    private float[] perspectiveProjectionMatrix = new float[16];

    private int[] texture_pyramid = new int[1];
    private int[] texture_cube = new int[1];
    private int samplerUniform;

    private float anglePyramid;
    private float angleCube;

    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context = drawingContext;

        setEGLContextClientVersion(3);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

        gestureDetector = new GestureDetector(context, this, null, false);
        gestureDetector.setOnDoubleTapListener(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int eventAction = event.getAction();
        if (!gestureDetector.onTouchEvent(event))
            super.onTouchEvent(event);

        return(true);
    }

    @Override
    public boolean onDoubleTap(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onDown(MotionEvent event)
    {
        return(true);
    }

    @Override
    public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY)
    {
        return(true);
    }

    @Override
    public void onLongPress(MotionEvent event)
    {
    }

    @Override
    public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX, float distanceY)
    {
        System.exit(0);
        return(true);
    }

    @Override
    public void onShowPress(MotionEvent event)
    {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event)
    {
        return(true);
    }

    // Implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        //String version = gl.glGetString(GL10.GL_VERSION);
        String version = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
        System.out.println("RTR:" + version);
        initialize();
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        resize(width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused)
    {
        display();
    }

    // Our Custom Methods
    private void initialize()
    {
        GLES32.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
        vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);
        final String vertexShaderSourceCode = String.format(
                "#version 320 es" +
                        "\n" +
                        "in vec4 vPosition;" +
                        "in vec2 vTexcord;" +
                        "uniform mat4 u_mvp_matrix;" +
                        "out vec2 out_texcord;" +
                        "void main(void)" +
                        "{" +
                        "	gl_Position = u_mvp_matrix * vPosition;" +
                        "	out_texcord = vTexcord;" +
                        "}");

        GLES32.glShaderSource(vertexShaderObject, vertexShaderSourceCode);
        GLES32.glCompileShader(vertexShaderObject);

        //Error Checking
        int[] iShaderCompileStatus = new int[1];
        int[] iInfoLength = new int[1];
        String szInfoLog = null;

        GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
                System.out.println("RTR:Vertex Shader Log : "+szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // FRAGMENT SHADER
        // Define Fragment Shader Object
        fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);
        // Write Vertex Shader Code
        final String fragmentShaderSourceCode = String.format(
                "#version 320 es" +
                        "\n" +
                        "precision highp float;" +
                        "in vec2 out_texcord;" +
                        "uniform sampler2D u_sampler;" +
                        "out vec4 FragColor;" +
                        "void main(void)" +
                        "{" +
                        "	FragColor = texture(u_sampler, out_texcord);" +
                        "}"
        );
        // Specify above Source Code To The Fragment Shader Object
        GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);
        // Compile the FragmentShader
        GLES32.glCompileShader(fragmentShaderObject);

        // Error checking
        iShaderCompileStatus[0] = 0;
        iInfoLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_COMPILE_STATUS, iShaderCompileStatus, 0);
        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetShaderiv(fragmentShaderObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
                System.out.println("RTR: Fragment Shader: " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Create Shader Program Object
        shaderProgramObject = GLES32.glCreateProgram();
        // Attach Vertex Shader To Shader Program
        GLES32.glAttachShader(shaderProgramObject, vertexShaderObject);
        // Attach Fragment Shader To Shader Program
        GLES32.glAttachShader(shaderProgramObject, fragmentShaderObject);

        // Prelinking binding to vertex attribute
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_POSITION, "vPosition");
        GLES32.glBindAttribLocation(shaderProgramObject, GLESMacros.AMC_ATTRIBUTE_TEXCOORD0, "vTexcord");

        // Link The Shader Program
        GLES32.glLinkProgram(shaderProgramObject);

        // Error checking
        int[] iProgramLinkStatus = new int[1];
        iInfoLength[0] = 0;
        szInfoLog = null;
        GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_LINK_STATUS, iProgramLinkStatus, 0);
        if (iProgramLinkStatus[0] == GLES32.GL_FALSE)
        {
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_INFO_LOG_LENGTH, iInfoLength, 0);
            if (iInfoLength[0] > 0)
            {
                szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
                System.out.println("Shader Program Link Log : " + szInfoLog);
                uninitialize();
                System.exit(0);
            }
        }

        // Post Linking Retriving UniformLocation
        mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");
        samplerUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_sampler");

        // Pyramid TexCo-ord
        final float[] pyramidTexCord = new float[]
                {
                        0.5f, 1.0f,
                        0.0f, 0.0f,
                        1.0f, 0.0f,

                        0.5f, 1.0f,
                        1.0f, 0.0f,
                        0.0f, 0.0f,

                        0.5f, 1.0f,
                        0.0f, 0.0f,
                        1.0f, 0.0f,

                        0.5f, 1.0f,
                        1.0f, 0.0f,
                        0.0f, 0.0f
                };

        // Pyramid Position
        final float[] pyramidVertices = new float[]
                {
                        0.0f, 1.0f, 0.0f,		// left-bottom
                        -1.0f, -1.0f, 1.0f,		// left-top
                        1.0f, -1.0f, 1.0f,		// right-top

                        0.0f, 1.0f, 0.0f,		// left-bottom
                        1.0f, -1.0f, 1.0f,		// left-top
                        1.0f, -1.0f, -1.0f,		// right-top

                        0.0f, 1.0f, 0.0f,		// left-bottom
                        1.0f, -1.0f, -1.0f,		// left-top
                        -1.0f, -1.0f, -1.0f,		// right-top

                        0.0f, 1.0f, 0.0f,		// left-bottom
                        -1.0f, -1.0f, -1.0f,		// left-top
                        -1.0f, -1.0f, 1.0f,		// right-top
                };


        // Cube TexCo-ord
        final float[] cubeTexCord = new float[]
                {
                        0.0f, 0.0f,
                        1.0f, 0.0f,
                        1.0f, 1.0f,
                        0.0f, 1.0f,

                        0.0f, 0.0f,
                        1.0f, 0.0f,
                        1.0f, 1.0f,
                        0.0f, 1.0f,

                        0.0f, 0.0f,
                        1.0f, 0.0f,
                        1.0f, 1.0f,
                        0.0f, 1.0f,

                        0.0f, 0.0f,
                        1.0f, 0.0f,
                        1.0f, 1.0f,
                        0.0f, 1.0f,

                        0.0f, 0.0f,
                        1.0f, 0.0f,
                        1.0f, 1.0f,
                        0.0f, 1.0f,

                        0.0f, 0.0f,
                        1.0f, 0.0f,
                        1.0f, 1.0f,
                        0.0f, 1.0f,
                };

        // Cube Position
        final float[] cubeVertices = new float[]
                {
                        1.0f, 1.0f, -1.0f,		// Right top
                        -1.0f, 1.0f, -1.0f, 	// Left top
                        -1.0f, 1.0f, 1.0f,		// Left bottom
                        1.0f, 1.0f, 1.0f,		// Right bottom
                        // Perspective square (Bottom face)
                        1.0f, -1.0f, -1.0f,		// Right top
                        -1.0f, -1.0f, -1.0f, 	// Left top
                        -1.0f, -1.0f, 1.0f,		// Left bottom
                        1.0f, -1.0f, 1.0f,		// Right bottom
                        // Perspective square (Front face)
                        1.0f, 1.0f, 1.0f,		// Right top
                        -1.0f, 1.0f, 1.0f,		// Left top
                        -1.0f, -1.0f, 1.0f, 	// Left bottom
                        1.0f, -1.0f, 1.0f,		// Right bottom
                        // Perspective square (Back face)
                        1.0f, 1.0f, -1.0f,		// Right top
                        -1.0f, 1.0f, -1.0f,		// Left top
                        -1.0f, -1.0f, -1.0f, 	// Left bottom
                        1.0f, -1.0f, -1.0f,		// Right bottom

                        // Perspective square (Right face)
                        1.0f, 1.0f, -1.0f,		// Right top
                        1.0f, 1.0f, 1.0f,		// Left top
                        1.0f, -1.0f, 1.0f, 		// Left bottom
                        1.0f, -1.0f, -1.0f,		// Right bottom

                        // Perspective square (Left face)
                        -1.0f, 1.0f, 1.0f,		// Right top
                        -1.0f, 1.0f, -1.0f,		// Left top
                        -1.0f, -1.0f, -1.0f, 	// Left bottom
                        -1.0f, -1.0f, 1.0f		// Right bottom
                };

        // Pyramid
        // Create vaoPyramid
        GLES32.glGenVertexArrays(1, vaoPyramid, 0);
        GLES32.glBindVertexArray(vaoPyramid[0]);
        GLES32.glGenBuffers(1, vboPyramidPosition, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPyramidPosition[0]);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(pyramidVertices.length * 4);

        byteBuffer.order(ByteOrder.nativeOrder());

        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        positionBuffer.put(pyramidVertices);

        positionBuffer.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                pyramidVertices.length * 4,
                positionBuffer,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // Pyramid Color
        GLES32.glGenBuffers(1, vboPyramidTexture, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboPyramidTexture[0]);

        ByteBuffer byteBufferSquareTexture = ByteBuffer.allocateDirect(pyramidTexCord.length * 4);

        byteBufferSquareTexture.order(ByteOrder.nativeOrder());

        FloatBuffer textureBufferSquare = byteBufferSquareTexture.asFloatBuffer();

        textureBufferSquare.put(pyramidTexCord);

        textureBufferSquare.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                pyramidTexCord.length * 4,
                textureBufferSquare,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0,
                2, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // Cube
        // Create vaoPyramid
        GLES32.glGenVertexArrays(1, vaoCube, 0);
        GLES32.glBindVertexArray(vaoCube[0]);
        GLES32.glGenBuffers(1, vboCubePosition, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboCubePosition[0]);

        ByteBuffer byteBufferCube = ByteBuffer.allocateDirect(cubeVertices.length * 4);

        byteBufferCube.order(ByteOrder.nativeOrder());

        FloatBuffer positionBufferCube = byteBufferCube.asFloatBuffer();

        positionBufferCube.put(cubeVertices);

        positionBufferCube.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                cubeVertices.length * 4,
                positionBufferCube,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                3, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);

        // Cube Texture
        GLES32.glGenBuffers(1, vboCubeTexture, 0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vboCubeTexture[0]);

        ByteBuffer byteBufferCubeTexture = ByteBuffer.allocateDirect(cubeTexCord.length * 4);

        byteBufferCubeTexture.order(ByteOrder.nativeOrder());

        FloatBuffer textureBufferCube = byteBufferCubeTexture.asFloatBuffer();

        textureBufferCube.put(cubeTexCord);

        textureBufferCube.position(0);

        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                cubeTexCord.length * 4,
                textureBufferCube,
                GLES32.GL_STATIC_DRAW);

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0,
                2, // No Of Co-ordinates
                GLES32.GL_FLOAT, // Type Of Co-ordinates
                false, //
                0,
                0);
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_TEXCOORD0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);
        GLES32.glBindVertexArray(0);

        // depth
        GLES32.glClearDepthf(1.0f);
        GLES32.glEnable(GLES32.GL_DEPTH_TEST);
        GLES32.glDepthFunc(GLES32.GL_LEQUAL);

        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        // texture
        GLES32.glEnable(GLES32.GL_TEXTURE_2D);
        texture_pyramid[0] = loadTexure(R.raw.stone);
        texture_cube[0] = loadTexure(R.raw.vijay_kundali);

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);
    }

    private void resize(int width, int height)
    {
        if(height <= 0)
        {
            height = 1;
        }
        GLES32.glViewport(0, 0, width, height);

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0,
                45.0f,
                (float)width / (float)height,
                0.1f,
                100.0f);
    }

    private void display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT);
        GLES32.glUseProgram(shaderProgramObject);

        // Declaration of matrices
        float[] translationMatrix = new float[16];
        float[] scaleMatrix = new float[16];
        float[] rotationMatrix = new float[16];
        float[] modelViewMatrix = new float[16];
        float[] modelViewProjectionMatrix = new float[16];

        //Pyramid
        // Initialize above matrices to identity
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(scaleMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Do necessary transformations
        Matrix.translateM(translationMatrix, 0,
                -1.5f, 0.0f, -6.0f);

        Matrix.setRotateM(rotationMatrix, 0,
                anglePyramid,0.0f, 1.0f, 0.0f);

        // Do necessary Matrix Multiplication
        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                translationMatrix, 0);

        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                rotationMatrix, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        // Send Necessary matrix to shader in respective uniform
        GLES32.glUniformMatrix4fv(mvpUniform,
                1,
                false,
                modelViewProjectionMatrix, 0);

        // ABU
        GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_pyramid[0]);
        GLES32.glUniform1i(samplerUniform, 0);

        // Bind with vaoPyramid
        GLES32.glBindVertexArray(vaoPyramid[0]);

        // Similarly bind With Textures If Any


        // Draw The Necessary Senn
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,
                0,
                12);

        // Unbind vaoPyramid
        GLES32.glBindVertexArray(0);

        //Cube
        // Initialize above matrices to identity
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(scaleMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // Do necessary transformations
        Matrix.translateM(translationMatrix, 0,
                1.5f, 0.0f, -6.0f);

        Matrix.scaleM(scaleMatrix, 0,
                0.8f, 0.8f, 0.8f);

        Matrix.setRotateM(rotationMatrix, 0,
                angleCube,1.0f, 1.0f, 1.0f);

        // Do necessary Matrix Multiplication
        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                translationMatrix, 0);

        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                scaleMatrix, 0);

        Matrix.multiplyMM(modelViewMatrix, 0,
                modelViewMatrix, 0,
                rotationMatrix, 0);

        Matrix.multiplyMM(modelViewProjectionMatrix, 0,
                perspectiveProjectionMatrix, 0,
                modelViewMatrix, 0);

        // Send Necessary matrix to shader in respective uniform
        GLES32.glUniformMatrix4fv(mvpUniform,
                1,
                false,
                modelViewProjectionMatrix, 0);

        // ABU
        GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture_cube[0]);
        GLES32.glUniform1i(samplerUniform, 0);

        // Bind with vaoCube
        GLES32.glBindVertexArray(vaoCube[0]);

        // Similarly bind With Textures If Any


        // Draw The Necessary Senn
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,
                0,
                4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,
                4,
                4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,
                8,
                4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,
                12,
                4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,
                16,
                4);
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,
                20,
                4);

        // Unbind vaoCube
        GLES32.glBindVertexArray(0);

        // Unuse Program
        GLES32.glUseProgram(0);
        update();

        requestRender();
    }

    private void uninitialize() {
        if (vboPyramidPosition[0] != 0) {
            GLES32.glDeleteBuffers(1, vboPyramidPosition, 0);
            vboPyramidPosition[0] = 0;
        }

        if (vboPyramidTexture[0] != 0) {
            GLES32.glDeleteBuffers(1, vboPyramidTexture, 0);
            vboPyramidTexture[0] = 0;
        }

        if (vaoPyramid[0] != 0) {
            GLES32.glDeleteVertexArrays(1, vaoPyramid, 0);
            vaoPyramid[0] = 0;
        }

        if (vboCubePosition[0] != 0) {
            GLES32.glDeleteBuffers(1, vboCubePosition, 0);
            vboCubePosition[0] = 0;
        }

        if (vboCubeTexture[0] != 0) {
            GLES32.glDeleteBuffers(1, vboCubeTexture, 0);
            vboCubeTexture[0] = 0;
        }
        if (vaoCube[0] != 0) {
            GLES32.glDeleteVertexArrays(1, vaoCube, 0);
            vaoCube[0] = 0;
        }

        if (shaderProgramObject != 0) {
            int[] shaderCount = new int[1];
            int shaderNumber;

            GLES32.glUseProgram(shaderProgramObject);
            GLES32.glGetProgramiv(shaderProgramObject, GLES32.GL_ATTACHED_SHADERS, shaderCount, 0);

            int[] shaders = new int[shaderCount[0]];

            GLES32.glGetAttachedShaders(shaderProgramObject, shaderCount[0], shaderCount, 0, shaders, 0);

            for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++) {
                // detach shader
                GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);

                // delete shader
                GLES32.glDeleteShader(shaders[shaderNumber]);
                shaders[shaderNumber] = 0;
            }

            GLES32.glUseProgram(0);
            GLES32.glDeleteProgram(shaderProgramObject);
            shaderProgramObject = 0;
        }
    }
    // Converting of image resource to image data:
    private int loadTexure(int imageResourceID) {
        // Variable Declaration:
        int[] texture = new int[1];
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),imageResourceID,options);

        // Code:
        options.inScaled = false;
        GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT, 4);
        GLES32.glGenTextures(1, texture, 0);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, texture[0]);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D,
                GLES32.GL_TEXTURE_MAG_FILTER,
                GLES32.GL_LINEAR);
        GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D,
                GLES32.GL_TEXTURE_MIN_FILTER,
                GLES32.GL_LINEAR_MIPMAP_LINEAR);
        GLUtils.texImage2D(GLES32.GL_TEXTURE_2D,
                0,
                bitmap,
                0);
        GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
        GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, 0);
        return(texture[0]);
    }

    private void update() {
        if (anglePyramid >= 360.0f)
        {
            anglePyramid = 0.0f;
        }
        else
        {
            anglePyramid += 2.0f;
        }

        if (angleCube >= 360.0f)
        {
            angleCube = 0.0f;
        }
        else
        {
            angleCube += 2.0f;
        }
    }
}

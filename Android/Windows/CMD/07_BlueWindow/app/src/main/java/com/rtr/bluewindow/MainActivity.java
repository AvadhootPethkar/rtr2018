package com.rtr.bluewindow;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;
import android.graphics.Color;


public class MainActivity extends AppCompatActivity {

	private GLESView glesView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

		// Get rid of titlebar
		// Method 1
		this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
		// Method 2
		//this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		//getSupportActionBar().hide();

		// Make full Screen
		this.getWindow().setFlags(
			WindowManager.LayoutParams.FLAG_FULLSCREEN,
			WindowManager.LayoutParams.FLAG_FULLSCREEN
		);

		// Do forced landscap orientation
		this.setRequestedOrientation(
			ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
		);

		// Set background color
		this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);

		// Define our own view
		glesView = new GLESView(this);

		// Now set this view as our main view
		setContentView(glesView);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}

package com.rtr.viewasobject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.ActivityInfo;
import android.graphics.Color;

import androidx.appcompat.widget.AppCompatTextView;
import android.view.Gravity;
import android.graphics.Color;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

		// Get rid of titlebar
		// Method 1
		this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
		// Method 2
		//this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		//getSupportActionBar().hide();

		// Make full Screen
		this.getWindow().setFlags(
			WindowManager.LayoutParams.FLAG_FULLSCREEN,
			WindowManager.LayoutParams.FLAG_FULLSCREEN
		);

		// Do forced landscap orientation
		this.setRequestedOrientation(
			ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
		);

		// Set background color
		this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);

		// Define our own view
		AppCompatTextView myView = new AppCompatTextView(this);
		myView.setTextColor(Color.rgb(0,255,0));
		myView.setTextSize(60);
		myView.setGravity(Gravity.CENTER);
		myView.setText("Hello World !!!");

		// Now set this view as our main view
		setContentView(myView);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}

package com.rtr.systemoutprintln;

import androidx.appcompat.widget.AppCompatTextView;
import android.content.Context;
import android.view.Gravity;
import android.graphics.Color;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.MotionEvent;

import android.os.Bundle;

public class MyView extends AppCompatTextView implements OnGestureListener, OnDoubleTapListener {

	private GestureDetector gestureDetector;

	MyView(Context context)
	{
		super(context);
		setTextColor(Color.rgb(0,255,0));
		setTextSize(60);
		setGravity(Gravity.CENTER);
		setText("Hello World !!!");
		gestureDetector = new GestureDetector(context, this, null, false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		int eventAction = event.getAction();
		if (!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		
		return(true);
	}

	@Override
	public boolean onDoubleTap(MotionEvent event)
	{
		setText("Double tapped");
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent event)
	{
		return(true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent event)
	{
		setText("Single Tap");
		return(true);
	}

	@Override
	public boolean onDown(MotionEvent event)
	{
		return(true);
	}

	@Override
	public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY)
	{
		return(true);
	}

	@Override
	public void onLongPress(MotionEvent event)
	{
		setText("Long Press");
	}

	@Override
	public boolean onScroll(MotionEvent event1, MotionEvent event2, float distanceX, float distanceY)
	{
		setText("Scroll");
		return(true);
	}

	@Override
	public void onShowPress(MotionEvent event)
	{
	}

	@Override
	public boolean onSingleTapUp(MotionEvent event)
	{
		return(true);
	}
}

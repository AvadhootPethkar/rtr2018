//
//  main.m
//  Test
//
//  Created by Avadhoot on 19/12/19.
//  Copyright © 2019 Avadhoot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    NSAutoreleasePool *pPool = [[NSAutoreleasePool alloc]init];

    NSString * appDelegateClassName;

    appDelegateClassName = NSStringFromClass([AppDelegate class]);

    int ret = UIApplicationMain(argc, argv, nil, appDelegateClassName);

    [pPool release];

    return(ret);
}
